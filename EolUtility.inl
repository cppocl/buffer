/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType, typename SizeType>
bool EolUtility<CharType, SizeType>::IsEndOfLineCharacter(char_type ch) noexcept
{
    return ch == CHAR_CR || ch == CHAR_LF;
}

template<typename CharType, typename SizeType>
const char* EolUtility<CharType, SizeType>::GetPlatformAsString(EolPlatform platform) noexcept
{
    switch (platform)
    {
        case EolPlatform::Unix    : return "Unix";
        case EolPlatform::Other   : return "Other";
        case EolPlatform::Windows : return "Windows";
        case EolPlatform::Mixed   : return "Mixed";
        case EolPlatform::Unknown : return "Unknown";
    }
    return "";
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::GetEolCharCount(EolPlatform platform) noexcept
{
    size_type count;
    switch (platform)
    {
    case EolPlatform::Windows:
        count = 2;
        break;
    case EolPlatform::Unix:
        count = 1;
        break;
    case EolPlatform::Other:
        count = 1;
        break;
    case EolPlatform::Mixed:
    case EolPlatform::Unknown:
    default:
        count = 0;
    }

    return count;
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::CountLines(char_type const* buffer,
                                                    size_type size,
                                                    bool whole_lines) noexcept
{
    size_type lines = 0;
    if (buffer != nullptr)
    {
        char_type ch = CHAR_NULL;
        char_type const* buffer_end = buffer + size;
        while (buffer < buffer_end)
        {
            char_type prev_ch = ch;
            ch = *buffer++;
            if (ch == CHAR_LF) // ch == '\n'
            {
                if (prev_ch != CHAR_CR) // ch != '\r'
                    ++lines;
            }
            else if (ch == CHAR_CR) // ch == '\r'
                ++lines;
        }
        if (!whole_lines && ch != CHAR_LF && ch != CHAR_CR && ch != CHAR_NULL)
            ++lines;
    }
    return lines;
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::CountLines(char_type const* buffer,
                                                    size_type size,
                                                    size_type& eol_count,
                                                    bool whole_lines) noexcept
{
    size_type lines = 0;
    eol_count = 0;
    if (buffer != nullptr)
    {
        char_type ch = CHAR_NULL;
        char_type const* buffer_end = buffer + size;
        while (buffer < buffer_end)
        {
            char_type prev_ch = ch;
            ch = *buffer++;
            if (ch == CHAR_LF) // ch == '\n'
            {
                ++eol_count;
                if (prev_ch != CHAR_CR) // ch != '\r'
                    ++lines;
            }
            else if (ch == CHAR_CR) // ch == '\r'
            {
                ++lines;
                ++eol_count;
            }
        }
        if (!whole_lines && ch != CHAR_LF && ch != CHAR_CR && ch != CHAR_NULL)
            ++lines;
    }
    return lines;
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::CountContinuousLines(char_type const* buffer,
                                                              size_type size) noexcept
{
    size_type lines = 0;
    if (buffer != nullptr)
    {
        char_type ch = CHAR_NULL;
        char_type const* buffer_end = buffer + size;
        while (buffer < buffer_end)
        {
            char_type prev_ch = ch;
            ch = *buffer++;
            if (ch == CHAR_LF) // ch == '\n'
            {
                if (prev_ch != CHAR_CR) // ch != '\r'
                    ++lines;
            }
            else if (ch == CHAR_CR) // ch == '\r'
                ++lines;
            else
                break;
        }
    }
    return lines;
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::CountContinuousLines(char_type const* buffer,
                                                              size_type size,
                                                              size_type& eol_count) noexcept
{
    size_type lines = 0;
    eol_count = 0;
    if (buffer != nullptr)
    {
        char_type ch = CHAR_NULL;
        char_type const* buffer_end = buffer + size;
        while (buffer < buffer_end)
        {
            char_type prev_ch = ch;
            ch = *buffer++;
            if (ch == CHAR_LF) // ch == '\n'
            {
                ++eol_count;
                if (prev_ch != CHAR_CR) // ch != '\r'
                    ++lines;
            }
            else if (ch == CHAR_CR) // ch == '\r'
            {
                ++lines;
                ++eol_count;
            }
            else
                break;
        }
    }
    return lines;
}

template<typename CharType, typename SizeType>
EolPlatform EolUtility<CharType, SizeType>::GetPlatform(char_type ch1,
                                                        char_type ch2,
                                                        size_type& count) noexcept
{
    if (ch1 == CHAR_CR)
    {
        if (ch2 == CHAR_LF)
        {
            count = 2;
            return EolPlatform::Windows;
        }
        count = 1;
        return EolPlatform::Other;
    }
    if (ch1 == CHAR_LF)
    {
        count = 1;
        return EolPlatform::Unix;
    }
    count = 0;
    return EolPlatform::Unknown;
}

template<typename CharType, typename SizeType>
EolPlatform EolUtility<CharType, SizeType>::GetPlatform(char_type const* buffer,
                                                        size_type size) noexcept
{
    EolPlatform platform = EolPlatform::Unknown;

    if (buffer != nullptr)
    {
        size_type count = 0;
        char_type  const* buffer_end = buffer + size;
        while (buffer < buffer_end)
        {
            char_type ch1 = *buffer;
            char_type ch2 = (buffer < buffer_end - 1) ? *(buffer + 1) : CHAR_NULL;
            EolPlatform tmp_platform = GetPlatform(ch1, ch2, count);
            if (tmp_platform != EolPlatform::Unknown)
            {
                if (platform == EolPlatform::Unknown)
                    platform = tmp_platform; // Set first occurrence of identified platform.
                else if (platform != tmp_platform)
                {
                    platform = EolPlatform::Mixed;
                    break;
                }
            }
            else
                count = 1;
            buffer += count;
        }
    }

    return platform;
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::GetPlatformCount(char_type ch1, char_type ch2) noexcept
{
    if (ch1 == CHAR_CR)
        return (ch2 == CHAR_LF) ? 2 : 1; // Return for Windows or Other character(s).
    if (ch1 == CHAR_LF)
        return 1; // Return for Unix character.
    return 0;
}

template<typename CharType, typename SizeType>
bool EolUtility<CharType, SizeType>::SafeSetEol(char_type* buffer, size_type size, EolPlatform platform)
{
    if (buffer != nullptr && size >= GetEolCharCount(platform))
    {
        switch (platform)
        {
        case EolPlatform::Unix:
            *buffer = CHAR_LF;
            return true;
        case EolPlatform::Other:
            *buffer = CHAR_CR;
            return true;
        case EolPlatform::Windows:
            *buffer = CHAR_CR;
            *(buffer + 1) = CHAR_LF;
            return true;
        case EolPlatform::Mixed:
        case EolPlatform::Unknown:
        default:
            break;
        }
    }

    return false;
}

template<typename CharType, typename SizeType>
bool EolUtility<CharType, SizeType>::SafeSetEol(char_type* buffer,
                                                size_type size,
                                                EolPlatform platform,
                                                size_type& count)
{
    if (buffer != nullptr && size >= GetEolCharCount(platform))
    {
        switch (platform)
        {
        case EolPlatform::Unix:
            *buffer = CHAR_LF;
            count = 1;
            return true;
        case EolPlatform::Other:
            *buffer = CHAR_CR;
            count = 1;
            return true;
        case EolPlatform::Windows:
            *buffer = CHAR_CR;
            *(buffer + 1) = CHAR_LF;
            count = 2;
            return true;
        case EolPlatform::Mixed:
        case EolPlatform::Unknown:
        default:
            break;
        }
    }

    count = 0;
    return false;
}

template<typename CharType, typename SizeType>
void EolUtility<CharType, SizeType>::UnsafeSetEol(char_type* buffer, EolPlatform platform)
{
    switch (platform)
    {
    case EolPlatform::Unix:
        *buffer = CHAR_LF;
        break;
    case EolPlatform::Other:
        *buffer = CHAR_CR;
        break;
    case EolPlatform::Windows:
        *buffer = CHAR_CR;
        *(buffer + 1) = CHAR_LF;
        break;
    case EolPlatform::Mixed:
    case EolPlatform::Unknown:
    default:
        break;
    }
}

template<typename CharType, typename SizeType>
void EolUtility<CharType, SizeType>::UnsafeSetEol(char_type* buffer,
                                                           EolPlatform platform,
                                                           size_type& count)
{
    switch (platform)
    {
    case EolPlatform::Unix:
        *buffer = CHAR_LF;
        count = 1;
        break;
    case EolPlatform::Other:
        *buffer = CHAR_CR;
        count = 1;
        break;
    case EolPlatform::Windows:
        *buffer = CHAR_CR;
        *(buffer + 1) = CHAR_LF;
        count = 2;
        break;
    case EolPlatform::Mixed:
    case EolPlatform::Unknown:
    default:
        count = 0;
        break;
    }
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::GetBufferCount(char_type const* buffer,
                                                        size_type size,
                                                        EolPlatform platform) noexcept
{
    size_type eol_count = 0;
    size_type line_count = CountLines(buffer, size, eol_count, true);
    size_type eol_char_count = GetEolCharCount(platform);
    return eol_char_count > 0 ? (line_count * eol_char_count) + size - eol_count : 0;
}

template<typename CharType, typename SizeType>
SizeType EolUtility<CharType, SizeType>::ConvertEol(char_type* dest_buffer,
                                                    size_type dest_size,
                                                    char_type const* source_buffer,
                                                    size_type source_size,
                                                    EolPlatform platform)
{
    //assert(dest_size >= GetBufferCount(source_buffer, source_size, platform));

    size_type converted;

    if (dest_buffer != nullptr && dest_size > 0 && source_buffer != nullptr && source_size > 0)
    {
        // Setup the end of line characters for writing to destination buffer.
        char_type eol[2];
        UnsafeSetEol(eol, platform);
        size_type eol_count = GetEolCharCount(platform);

        char_type const* source_buffer_end = source_buffer + source_size;
        char_type const* dest_buffer_end = dest_buffer + dest_size;
        while (dest_buffer < dest_buffer_end && source_buffer < source_buffer_end)
        {
            char_type ch1 = *source_buffer;
            char_type ch2 = (source_buffer < source_buffer_end - 1) ? *(source_buffer + 1) : CHAR_NULL;

            // When ch2 is '\0', the source_count will be 0 or 1.
            size_type source_count = GetPlatformCount(ch1, ch2);
            if (source_count > 0)
            {
                if (dest_buffer <= dest_buffer_end - eol_count)
                {
                    ::memcpy(dest_buffer, eol, eol_count * sizeof(char_type));
                    dest_buffer += eol_count;
                }
                else
                {
                    converted = 0; // Destination is too small!
                    break;
                }
            }
            else // Not an end of line character, so copy the other characters.
                *dest_buffer++ = *source_buffer++;

            // There will always be enough space for source_buffer,
            // as source_count is from source_buffer.
            source_buffer += source_count;
        }

        converted = dest_size - static_cast<size_type>(dest_buffer_end - dest_buffer);
    }
    else
        converted = 0;

    return converted;
}
