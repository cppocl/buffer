/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_BUFFER_HPP
#define OCL_GUARD_BUFFER_BUFFER_HPP

#include "internal/InternalBufferInType.hpp"
#include "internal/InternalBufferCompare.hpp"
#include "InsecureBufferUtility.hpp"
#include "BufferVersion.hpp"
#include "Memory.hpp"
#include "MoveFunctor.hpp"
#include <cstddef>
#include <cstdint>

namespace ocl
{

template< typename Type = unsigned char,
          typename SizeType = std::size_t,
          typename MemoryType = Memory<Type, SizeType, MoveFunctor<Type, SizeType>, true> >
class Buffer
{
/// Types and constants.
public:
    typedef          Type                                type;
    typedef          SizeType                            size_type;
    typedef          MemoryType                          memory_type;
    typedef          Buffer<Type, SizeType, MemoryType>  buffer_type;

    typedef typename InternalBufferInType<Type>::in_type in_type;
    typedef          in_type                             const_return_type;

    static bool const CAN_THROW_EXCEPTION = MemoryType::CAN_THROW_EXCEPTION;

/// Types and constants (internal use only).
private:
    typedef InsecureBufferUtility<Type, SizeType> insecure_buffer_utility;

/// Construction and destruction.
public:
    /// This buffer is initialised as empty.
    Buffer() noexcept;

    /// Create this buffer to an initial size.
    Buffer(size_type size);

    /// Allocate this buffer to size elements, and copy buffer elements if not null.
    Buffer(size_type size, type const* buffer);

    /// Copy whole buffer to this object.
    Buffer(buffer_type const& buffer);

    /// Create a buffer from part of another buffer.
    /// If the position is past the end then the buffer is created empty.
    /// If the count goes past the end of the buffer,
    /// then only elements to the end are copied.
    Buffer(buffer_type const& buffer, size_type position, size_type count);

    /// Move source buffer to this buffer, leaving the source buffer empty.
    Buffer(buffer_type&& buffer);

    /// Destructor will automatically free allocated memory.
    ~Buffer();

/// Operators.
public:
    buffer_type& operator=(buffer_type const& buffer);
    buffer_type& operator=(buffer_type&& buffer) noexcept;

    operator type*() noexcept;
    operator type const*() const noexcept;

    /// Access element at an index, without any boundary checking.
    type& operator[](size_type index);

    /// Access element at an index, without any boundary checking.
    const_return_type operator[](size_type index) const;

    /// Compare two buffers and return true when this object is less than right hand side object.
    bool operator <(Buffer const& rhs) const;

/// Member functions.
public:
    /// Get the current size of the buffer in elements.
    size_type GetSize() const noexcept;

    /// Get the maximum number of elements the buffer can contain.
    constexpr size_type GetMaxSize() const noexcept;

    /// Get the number of bytes the buffer uses, including any other internal memory usage.
    size_type GetMemSize() const noexcept;

    /// When there are no stored elements return true.
    bool IsEmpty() const noexcept;

    /// Get access to the underlying memory for low level read/write access.
    type* Ptr() noexcept;

    /// Get access to the underlying memory for low level read access.
    type const* Ptr() const noexcept;

    /// Clear all elements and reset the buffer to empty.
    void Clear();

    /// Shrinking the buffer might not free memory but just change the size,
    /// so use Compact to re-allocate the memory and copy existing elements.
    /// This will depend on the implementation of Memory class, which by default
    /// does not free memory when shrinking the size.
    /// This is the equivalent of Resize(GetSize(), true);
    void Compact();

    /// Set the size without retaining the existing data.
    void SetSize(size_type size);

    // Re-size the buffer, only re-allocating memory when the new size is bigger than the existing size or reallocate is true.
    void Resize(size_type new_size, bool reallocate = false);

    /// Reduce the size of the memory block by the specified count, without re-allocating memory.
    /// If count is greater or equal to current size then the memory is freed.
    void ShrinkBy(size_type size);

    /// Set all elements to the specified value.
    void Set(in_type value);

    /// Set all elements between start and end (inclusive) to the specified value.
    void Set(size_type start, size_type end, in_type value);

    /// Copy the value a number of times specified by count from the start of the buffer,
    // replacing any existing content and resizing the buffer to count.
    void Copy(in_type value, size_type count);

    /// Copy the buffer to this buffer, replacing any existing data.
    void Copy(type const* buffer, size_type size);

    /// Copy the buffer to this buffer, replacing any existing data.
    void Copy(buffer_type const& buffer);

    /// Copy the buffer to this buffer, replacing any existing data.
    /// buffer is assumed to not be null, and size is assumed to be greater than 0.
    void UnsafeCopy(type const* buffer, size_type size);

    /// Copy the buffer to this buffer, replacing any existing data.
    /// buffer is assumed to contain more than 0 elements.
    void UnsafeCopy(buffer_type const& buffer);

    /// Overwrite elements from the start with a specific value a number of times.
    // The buffer will be grow if count is greater than the existing size.
    void Overwrite(in_type value, size_type count);

    /// Overwrite elements from the start with a buffer.
    /// If the provided buffer is bigger than this buffer, then this buffer is grown.
    void Overwrite(type const* buffer, size_type size);

    /// Overwrite elements from the start with a buffer.
    /// If the provided buffer is bigger than this buffer, then this buffer is grown.
    void Overwrite(buffer_type const& buffer);

    /// Copy the value number of times specified by count, replacing any existing content.
    /// Overwrites elements starting at position,
    /// and the buffer will grow if count goes past the end.
    /// position can be less or equal to the buffer size being overwritten.
    void Overwrite(size_type position, in_type value, size_type count);

    /// Copy the buffer to this buffer, replacing any existing data.
    /// Overwrites elements starting at position,
    /// and the buffer will grow if size goes past the end.
    /// position can be less or equal to the buffer size being overwritten.
    void Overwrite(size_type position, type const* buffer, size_type size);

    /// Copy the buffer to this buffer, replacing any existing data.
    /// Overwrites elements starting at position,
    /// and the buffer will grow if size of supplied buffer goes past the end.
    /// position can be less or equal to the buffer size being overwritten.
    void Overwrite(size_type position, buffer_type const& buffer);

    /// Set a single value at a specific position.
    /// This is the safer version of operator[].
    void SetAt(size_type position, in_type value);

    /// Set a sequence of values at a specific position.
    void SetAt(size_type position, type const* buffer, size_type size);

    /// Set a sequence of values from a Buffer object at a specific position.
    void SetAt(size_type position, buffer_type const& buffer);

    /// Move a sequence of values from a Buffer object at a specific position.
    void SetAt(size_type position, buffer_type&& buffer);

    /// Move the buffer to this buffer, replacing any existing data.
    void Move(buffer_type& buffer);

    /// Swap two Buffer objects.
    void Swap(buffer_type& buffer) noexcept;

    /// Copy value into start of this Buffer object.
    bool Prepend(in_type value);

    /// Copy buffer into start of this Buffer object.
    bool Prepend(type const* buffer, size_type size);

    /// Copy buffer into start of this Buffer object.
    bool Prepend(buffer_type const& buffer);

    /// Move buffer into start of this Buffer object.
    bool Prepend(buffer_type&& buffer);

    /// Copy the value to the end, expanding the size by 1.
    bool Append(in_type value);

    /// Copy the buffer to the end, expanding the size.
    bool Append(type const* buffer, size_type size);

    /// Copy the buffer to the end, expanding the size.
    bool Append(buffer_type const& buffer);

    /// Move the buffer to the end, expanding the size.
    bool Append(buffer_type&& buffer);

    /// Insert the value into the specified position.
    /// The buffer will grow to fit the extra elements.
    bool Insert(size_type position, in_type value);

    /// Insert the value into the specified position.
    /// The buffer will grow to fit the extra elements.
    bool Insert(size_type position, in_type value, size_type count);

    /// Insert the buffer into the specified position.
    /// The buffer will grow to fit the extra elements.
    bool Insert(size_type position, type const* buffer, size_type size);

    /// Insert the buffer into the specified position.
    /// The buffer will grow to fit the extra elements.
    bool Insert(size_type position, buffer_type const& buffer);

    /// Insert and move the buffer into the specified position.
    /// The buffer will grow to fit the extra elements.
    bool Insert(size_type position, buffer_type&& buffer);

    /// Insert the value into the specified position,
    /// without altering the size of the buffer.
    bool Embed(size_type position, in_type value);

    /// Insert the value into the specified position,
    /// without altering the size of the buffer.
    bool Embed(size_type position, in_type value, size_type count);

    /// Insert the value into the specified position,
    /// without altering the size of the buffer.
    bool Embed(size_type position, type const* buffer, size_type size);

    /// Insert the value into the specified position,
    /// without altering the size of the buffer.
    bool Embed(size_type position, buffer_type const& buffer);

    /// Insert and move the value into the specified position,
    /// without altering the size of the buffer.
    bool Embed(size_type position, buffer_type&& buffer);

    /// Erase number of elements at a given position specified by count.
    void Erase(size_type position, size_type count);

    /// Remove the first element, and return true.
    /// Return false if there is nothing to remove.
    void EraseFront();

    /// Remove number of elements from front specified by count.
    /// Return false if there is nothing to remove.
    void EraseFront(size_type count);

    /// Remove the last element, and return true.
    /// Return false if there is nothing to remove.
    void EraseBack();

    /// Remove number of elements from end specified by count.
    /// Return false if there is nothing to remove.
    void EraseBack(size_type count);

    /// Move values starting at a given position.
    /// If the buffer is empty, count is zero or position is past the end then false is returned.
    void Extract(buffer_type& buffer, size_type position, size_type count);

    /// Move values from the start of the buffer.
    /// If the buffer is empty or count is zero then false is returned.
    void ExtractFront(buffer_type& buffer, size_type count);

    /// Move values up to the end of the buffer.
    /// If the buffer is empty or count is zero then false is returned.
    void ExtractBack(buffer_type& buffer, size_type count);

    /// SearchFunctorType is defined as:
    ///     bool operator()(type const* values, size_type size, in_type value_to_find);
    template<typename SearchFunctorType>
    bool Exists(in_type value) const;

    /// SearchFunctorType is defined as:
    ///     bool operator()(type const* values, size_type size, in_type value_to_find, size_type& nearest_pos);
    /// The functor is expected to return the insert position if the value is not found.
    template<typename SearchFunctorType>
    bool FindPosition(in_type value, size_type& nearest_pos) const;

    /// Apply a custom algorithm to the buffer.
    /// The functor needs to be in the form of bool operator()(Type* buffer, SizeType size);
    /// Note the pointer passed into the functor will never be null.
    template<typename FunctorType>
    bool Apply(FunctorType& functor);

private:
    /// Common code for prepending functions.
    /// This function expects space_to_prepend to be greater than 0.
    /// Return the start of the memory to have the inserted elements placed.
    type* PrependSpace(size_type space_to_prepend);

    /// Common code for appending functions.
    /// This function expects space_to_prepend to be greater than 0.
    /// Return the start of the memory to have the appended elements placed.
    type* AppendSpace(size_type space_to_append);

    /// Common code for appending functions,
    /// with optimization for only copying limited number of elements.
    /// This function expects space_to_prepend to be greater than 0.
    /// Return the start of the memory to have the appended elements placed.
    type* AppendSpace(size_type space_to_append, size_type copy_limit);

    /// Common code for insert functions.
    /// This function expects space_to_prepend to be greater than 0 and
    /// less then existing size.
    /// Return the start of the memory to have the inserted elements placed.
    type* InsertSpace(size_type position, size_type space_to_insert);

    /// Common code for insert functions.
    /// This function expects space_to_prepend to be greater than 0 and
    /// less then existing size.
    /// Return the start of the memory to have the inserted elements placed.
    type* InsertSpaceNoResize(size_type position, size_type space_to_insert);

    static size_type GetCopySize(size_type position, size_type copy_size, size_type buffer_size) noexcept;

/// Data (internal use only).
private:
    memory_type m_memory;
};

#include "Buffer.inl"

} // namespace ocl

#endif // OCL_GUARD_BUFFER_BUFFER_HPP
