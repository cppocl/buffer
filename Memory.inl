/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

namespace ocl
{

//
// malloc/free and no exceptions implementation.
//

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, false>::Memory(SizeType size) noexcept
    : m_memory(static_cast<Type*>(std::malloc(sizeof(Type) * size)))
    , m_size(size)
{
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, false>::Memory(Memory&& other) noexcept
{
    m_memory = other.m_memory;
    m_size = other.m_size;
    other.m_memory = nullptr;
    other.m_size = 0;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, false>::~Memory() noexcept
{
    InternalFree(m_memory);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, false>& Memory<Type, SizeType, MoveFunctorType, false>::operator =(Memory&& other) noexcept
{
    Move(other);
    return *this;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, false>::operator Type const* () const noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, false>::operator Type* () noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Type const* Memory<Type, SizeType, MoveFunctorType, false>::Ptr() const noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, false>::Ptr() noexcept
{
    return m_memory;
}

// Detach ownership of the pointer to the caller.
template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, false>::Detach() noexcept
{
    Type* memory = m_memory;
    m_memory = nullptr;
    m_size = 0;
    return memory;
}

// Detach ownership of the pointer to the caller.
template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, false>::Detach(SizeType& size) noexcept
{
    Type* memory = m_memory;
    size = m_size;
    m_memory = nullptr;
    m_size = 0;
    return memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::Attach(Type*& memory, SizeType size) noexcept
{
    InternalFree(m_memory);
    m_memory = memory;
    m_size = size;
    memory = nullptr;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::SetSize(SizeType size) noexcept
{
    InternalReserve(size);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::Resize(SizeType size, bool reallocate)
{
    if (size > m_size || reallocate)
    {
        Type* memory = InternalAllocate(size);
        if (memory)
        {
            SizeType move_size = size < m_size ? size : m_size;
            MoveFunctorType()(memory, m_memory, move_size);
            InternalFree(m_memory);
            m_memory = memory;
            m_size = size;
        }
        else
            Free();
    }
    else if (size < m_size)
    {
        if (size > 0)
            m_size = size;
        else
            Free();
    }
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::Free() noexcept
{
    if (m_memory)
    {
        InternalFree(m_memory);
        m_memory = nullptr;
        m_size = 0;
    }
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::Move(Memory& other) noexcept
{
    InternalFree(m_memory);
    m_memory = other.m_memory;
    m_size = other.m_size;
    other.m_memory = nullptr;
    other.m_size = 0;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, false>::GetSize() const noexcept
{
    return m_size;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, false>::GetSizeInBytes() const noexcept
{
    return m_size * sizeof(Type);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, false>::GetMemSize() const noexcept
{
    return GetSizeInBytes() + sizeof(m_memory) + sizeof(m_size);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, false>::GetMaxSize() const noexcept
{
    return GetMaxSizeInBytes() / sizeof(Type);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, false>::GetMaxSizeInBytes() const noexcept
{
    return ~static_cast<size_type>(0);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::Swap(Memory& other) noexcept
{
    Type* other_memory = other.m_memory;
    SizeType other_size = other.m_size;
    other.m_memory = m_memory;
    other.m_size = m_size;
    m_memory = other_memory;
    m_size = other_size;
}

//
// private no exceptions implementation.
//

template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, false>::InternalAllocate(SizeType size) noexcept
{
    return static_cast<Type*>(std::malloc(sizeof(Type) * size));
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::InternalFree(Type* ptr) noexcept
{
    std::free(ptr);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, false>::InternalReserve(SizeType size) noexcept
{
    // NOTE: Memory is allocated before m_memory is freed to test for memory reallocation.
    //       The operating system might decide to re-use the pointer,
    //       if the new allocation is not performed before freeing existing memory.
    Type* memory = (size > 0) ? InternalAllocate(size) : nullptr;
    InternalFree(m_memory);
    m_memory = memory;
    m_size = size;
}

//
// new/delete and with exceptions implementation.
//

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, true>::Memory(SizeType size)
    : m_memory(nullptr)
    , m_size(0)
{
    SetSize(size);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, true>::Memory(Memory&& other) noexcept
    : m_memory(other.m_memory)
    , m_size(other.m_size)
{
    other.m_memory = nullptr;
    other.m_size = 0;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, true>::~Memory()
{
    Free();
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, true>& Memory<Type, SizeType, MoveFunctorType, true>::operator =(Memory&& other)
{
    Move(other);
    return *this;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, true>::operator Type const*() const noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Memory<Type, SizeType, MoveFunctorType, true>::operator Type*() noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Type const* Memory<Type, SizeType, MoveFunctorType, true>::Ptr() const noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, true>::Ptr() noexcept
{
    return m_memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, true>::Detach() noexcept
{
    Type* memory = m_memory;
    m_memory = nullptr;
    m_size = 0;
    return memory;
}

// Detach ownership of the pointer to the caller.
template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, true>::Detach(SizeType& size) noexcept
{
    Type* memory = m_memory;
    size = m_size;
    m_memory = nullptr;
    m_size = 0;
    return memory;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::Attach(Type*& memory, SizeType size) noexcept
{
    InternalFree(m_memory, size);
    m_memory = memory;
    m_size = size;
    memory = nullptr;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::SetSize(SizeType size)
{
    // NOTE: Memory is allocated before m_memory is freed to test for memory reallocation.
    //       The operating system might decide to re-use the pointer,
    //       if the new allocation is not performed before freeing existing memory.
    Type* memory = (size > 0) ? InternalAllocate(size) : nullptr;
    InternalFree(m_memory, m_size);
    m_memory = memory;
    m_size = size;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::Resize(SizeType size, bool reallocate)
{
    if (size > m_size || reallocate)
    {
        Type* memory = InternalAllocate(size);
        if (memory)
        {
            SizeType move_size = size < m_size ? size : m_size;
            MoveFunctorType()(memory, m_memory, move_size);
            InternalFree(m_memory, m_size);
            m_memory = memory;
            m_size = size;
        }
        else
            Free();
    }
    else if (size < m_size)
    {
        if (size > 0)
            m_size = size;
        else
            Free();
    }
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::Free()
{
    if (m_memory)
    {
        InternalFree(m_memory, m_size);
        m_memory = nullptr;
        m_size = 0;
    }
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::Move(Memory& other)
{
    Free();
    m_memory = other.m_memory;
    m_size = other.m_size;
    other.m_memory = nullptr;
    other.m_size = 0;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, true>::GetSize() const noexcept
{
    return m_size;
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, true>::GetSizeInBytes() const noexcept
{
    return m_size * sizeof(Type);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, true>::GetMemSize() const noexcept
{
    return GetSizeInBytes() + sizeof(m_memory) + sizeof(m_size);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, true>::GetMaxSize() const noexcept
{
    return GetMaxSizeInBytes() / sizeof(Type);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
SizeType Memory<Type, SizeType, MoveFunctorType, true>::GetMaxSizeInBytes() const noexcept
{
    return ~static_cast<size_type>(0);
}

template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::Swap(Memory& other) noexcept
{
    Type* other_memory = other.m_memory;
    SizeType other_size = other.m_size;
    other.m_memory = m_memory;
    other.m_size = m_size;
    m_memory = other_memory;
    m_size = other_size;
}

//
// private exceptions implementation.
//

template<typename Type, typename SizeType, typename MoveFunctorType>
Type* Memory<Type, SizeType, MoveFunctorType, true>::InternalAllocate(SizeType size)
{
    Type* memory;
    if (size > 1)
        memory = new Type[size];
    else
        memory = new Type;
    return memory;
}

// Handle the freeing of memory for a single element or an array of elements.
template<typename Type, typename SizeType, typename MoveFunctorType>
void Memory<Type, SizeType, MoveFunctorType, true>::InternalFree(Type* memory, SizeType size)
{
    if (size > 1)
        delete[] memory;
    else
        delete memory;
}

} // namespace ocl
