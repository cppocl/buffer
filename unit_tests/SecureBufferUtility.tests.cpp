/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../SecureBufferUtility.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>

using ocl::SecureBufferUtility;

namespace
{
    struct Data
    {
        int  i;
        char c;

        Data& operator=(Data const& src) noexcept
        {
            i = src.i;
            c = src.c;
            return *this;
        }

        Data& operator=(Data&& src) noexcept
        {
            i = src.i;
            c = src.c;
            src.i = 0;
            src.c = '\0';
            return *this;
        }

        bool operator ==(Data const& other) const noexcept
        {
            return (i == other.i) && (c == other.c);
        }

        bool operator !=(Data const& other) const noexcept
        {
            return (i != other.i) || (c != other.c);
        }
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, std::size_t count)
    {
        for (Type const* end = a + count; a < end; ++a, ++b)
            if (*a != *b)
                return false;
        return true;
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Copy, type_ptr_type_const_ptr_size_t)
{
    std::size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, std::size_t");

    {
        typedef bool type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));

        CHECK_FALSE(secure_buffer_utility_type::Copy(nullptr, src, size));
        CHECK_FALSE(secure_buffer_utility_type::Copy(dest, nullptr, size));
        CHECK_FALSE(secure_buffer_utility_type::Copy(dest, src, 0U));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {

        typedef char const* type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type src[size] = {str1, str2, str3, str4};
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef wchar_t type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef float type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef long double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Copy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Move, type_ptr_type_const_ptr_size_t)
{
    size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];

        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));

        CHECK_FALSE(secure_buffer_utility_type::Move(nullptr, src, size));
        CHECK_FALSE(secure_buffer_utility_type::Move(dest, nullptr, size));
        CHECK_FALSE(secure_buffer_utility_type::Move(dest, src, 0U));

        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {

        typedef char const* type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type src[size] = {str1, str2, str3, str4};
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_NULL(src[0]);
        CHECK_NULL(src[1]);
        CHECK_NULL(src[2]);
        CHECK_NULL(src[3]);
        CHECK_EQUAL(*dest[0], '1');
        CHECK_EQUAL(*dest[1], '2');
        CHECK_EQUAL(*dest[2], '3');
        CHECK_EQUAL(*dest[3], '4');
    }

    {
        typedef wchar_t type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef float type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef long double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef char type;
        typedef char* type_ptr;
        typedef SecureBufferUtility<type_ptr, std::size_t> secure_buffer_utility_type;

        type_ptr dest_ptr[size];
        type_ptr source_ptr[size];
        type src[size];
        src[0] = 'A';
        src[1] = 'B';
        src[2] = 'C';
        src[3] = 'D';
        source_ptr[0] = &src[0];
        source_ptr[1] = &src[1];
        source_ptr[2] = &src[2];
        source_ptr[3] = &src[3];
        ::memset(dest_ptr, 0, sizeof(dest_ptr));
        CHECK_TRUE(secure_buffer_utility_type::Move(dest_ptr, source_ptr, size));
        CHECK_NULL(source_ptr[0]);
        CHECK_NULL(source_ptr[1]);
        CHECK_NULL(source_ptr[2]);
        CHECK_NULL(source_ptr[3]);
        CHECK_EQUAL(*dest_ptr[0], 'A');
        CHECK_EQUAL(*dest_ptr[1], 'B');
        CHECK_EQUAL(*dest_ptr[2], 'C');
        CHECK_EQUAL(*dest_ptr[3], 'D');
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        src[0].i = 1;
        src[0].c = 'A';
        src[1].i = 2;
        src[1].c = 'B';
        src[2].i = 3;
        src[2].c = 'C';
        src[3].i = 4;
        src[3].c = 'D';
        CHECK_TRUE(secure_buffer_utility_type::Move(dest, src, size));
        CHECK_FALSE(Compare(dest, src, size)); // Move should clear src.
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Erase, type_ptr_size_type_size_type_size_type)
{
    // type* buffer, size_type size, size_type position, size_type count
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, size_type");

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        {
            type buffer[] = "Hello";
            std::size_t size = ::strlen(buffer) + 1;

            CHECK_FALSE(secure_buffer_utility_type::Erase(buffer, size, size, 1));

            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 0, 1));
            CHECK_TRUE(Compare(buffer, "ello", 5));

            CHECK_FALSE(secure_buffer_utility_type::Erase(nullptr, size, 0, 1));
            CHECK_FALSE(secure_buffer_utility_type::Erase(buffer, 0, 0, 1));
            CHECK_FALSE(secure_buffer_utility_type::Erase(buffer, size, 0, 0));
        }

        {
            type buffer[] = "Hello";
            std::size_t size = ::strlen(buffer) + 1;
            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 4, 1));
            CHECK_TRUE(Compare(buffer, "Hell", 5));
        }

        {
            type buffer[] = "Hello";
            std::size_t size = ::strlen(buffer) + 1;
            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 5, 1));
            CHECK_TRUE(Compare(buffer, "Hello", 5));
        }

        {
            type buffer[] = "Hello";
            std::size_t size = ::strlen(buffer) + 1;
            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 0, 2));
            CHECK_TRUE(Compare(buffer, "llo", 4));
        }

        {
            type buffer[] = "Hello";
            std::size_t size = ::strlen(buffer) + 1;
            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 3, 2));
            CHECK_TRUE(Compare(buffer, "Hel", 4));
        }
    }

    {
        typedef std::uint16_t type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        {
            type buffer[] = {1234U, 2345U, 3456U, 4567U, 5678U, 0U};
            type expected_buffer[] = {2345U, 3456U, 4567U, 5678U, 0U};
            std::size_t size = 6U;
            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 0, 1));
            CHECK_TRUE(Compare(buffer, expected_buffer, 5));
        }

        {
            type buffer[] = {1234U, 2345U, 3456U, 4567U, 5678U, 0U};
            type expected_buffer[] = {1234U, 2345U, 3456U, 4567U, 0U};
            std::size_t size = 6U;
            CHECK_TRUE(secure_buffer_utility_type::Erase(buffer, size, 4, 1));
            CHECK_TRUE(Compare(buffer, expected_buffer, 5));
        }
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Insert, type_ptr_size_type_size_type_type_const_ptr_size_type)
{
    // type* dest, size_type dest_size, size_type position, type const* to_insert, size_type to_insert_size
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type const*, size_type");

    typedef char type;
    typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

    {
        type str[6] = "ello";
        CHECK_TRUE(secure_buffer_utility_type::Insert(str, 5, 0, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));

        CHECK_FALSE(secure_buffer_utility_type::Insert(nullptr, 5, 0, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::Insert(str, 5, 5, "H", 1));
    }

    {
        type str[6] = "Hllo";
        CHECK_TRUE(secure_buffer_utility_type::Insert(str, 5, 1, "e", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type str[6] = "Hell";
        CHECK_TRUE(secure_buffer_utility_type::Insert(str, 5, 4, "o", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type str[6] = "Hello";
        CHECK_TRUE(secure_buffer_utility_type::Insert(str, 6, 5, "!", 1));
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        // Check that the insert is not performed when 
        type str[6] = "Hello";
        CHECK_FALSE(secure_buffer_utility_type::Insert(str, 6, 5, "!.", 2));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, InsertCopy, type_ptr_size_type_type_const_ptr_type_const_ptr_size_type)
{
    // type* dest, size_type position, type const* src, size_type src_size, type const* to_insert, size_type to_insert_size
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type");

    typedef char type;
    typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

    {
        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertCopy(str, 6, 0, src, 5, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));

        CHECK_FALSE(secure_buffer_utility_type::InsertCopy(nullptr, 5, 0, src, 5, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::InsertCopy(str, 5, 5, src, 5, "H", 1));
    }

    {
        type src[5] = "Hllo";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertCopy(str, 6, 1, src, 5, "e", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type src[5] = "Hell";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertCopy(str, 6, 4, src, 5, "o", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type src[5] = "Hell";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertCopy(str, 6, 4, src, 5, "o", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type src[5] = {'H','e','l','l','o'};
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertCopy(str, 6, 5, src, 5, "!", 1));
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        type src[5] = {'H','e','l','l','o'};
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_FALSE(secure_buffer_utility_type::InsertCopy(str, 6, 0, src, 5, "!.", 2));
    }

    {
        type src[5] = {'H','e','l','l','o'};
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_FALSE(secure_buffer_utility_type::InsertCopy(str, 6, 5, src, 5, "!.", 2));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, InsertMove, type_ptr_size_type_type_ptr_size_type_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type*, size_type, type*, size_type");

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertMove(str, sizeof(str), 0, src, 5, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertMove(str, sizeof(str), 0, src, 5, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5]   = {{2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value = {1, 'A'};
        CHECK_TRUE(secure_buffer_utility_type::InsertMove(dest, 6, 0, src, 5, &value, 1));
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5] = "Hell";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertMove(str, sizeof(str), 4, src, 5, "o", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5] = {'H','e','l','l','o'};
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::InsertMove(str, sizeof(str), 5, src, 5, "!", 1));
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type empty[5] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type dest[6];
        type value = {6, 'F'};
        CHECK_TRUE(secure_buffer_utility_type::InsertMove(dest, 6, 5, src, 5, &value, 1));
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Prepend, type_ptr_size_type_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type const*, size_type");

    typedef char type;
    typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

    {
        type str[6] = "ello";

        CHECK_FALSE(secure_buffer_utility_type::Prepend(nullptr, 6, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::Prepend(str, 6, nullptr, 1));
        CHECK_FALSE(secure_buffer_utility_type::Prepend(str, 6, "ABCDEFG", 7));

        CHECK_TRUE(secure_buffer_utility_type::Prepend(str, 6, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type str[6] = "llo";
        CHECK_TRUE(secure_buffer_utility_type::Prepend(str, 6, "He", 2));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type str[6] = "ABCDE";
        CHECK_TRUE(secure_buffer_utility_type::Prepend(str, 6, "Hello", 6));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, PrependCopy, type_ptr_size_type_type_const_ptr_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type");

    typedef char type;
    typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

    {
        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));

        CHECK_FALSE(secure_buffer_utility_type::PrependCopy(nullptr, 6, src, 5, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::PrependCopy(str, 6, nullptr, 5, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::PrependCopy(str, 6, src, 5, nullptr, 1));
        CHECK_FALSE(secure_buffer_utility_type::PrependCopy(str, 6, src, 5, "ABCDEFG", 7));

        CHECK_TRUE(secure_buffer_utility_type::PrependCopy(str, 6, src, 5, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type src[4] = "llo";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::PrependCopy(str, 6, src, 4, "He", 2));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        type src[6] = "ABCDE";
        type str[12];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::PrependCopy(str, 12, src, 6, "Hello", 6));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, PrependMove, type_ptr_size_type_type_ptr_size_type_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type*, size_type, type*, size_type");

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));

        CHECK_FALSE(secure_buffer_utility_type::PrependMove(nullptr, 6, src, 5, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::PrependMove(str, 6, nullptr, 5, "H", 1));
        CHECK_FALSE(secure_buffer_utility_type::PrependMove(str, 6, src, 5, nullptr, 1));
        CHECK_FALSE(secure_buffer_utility_type::PrependMove(str, 6, src, 5, "ABCDEFG", 7));

        CHECK_TRUE(secure_buffer_utility_type::PrependMove(str, 6, src, 5, "H", 1));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5] = "llo";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::PrependMove(str, 6, src, 5, "He", 2));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[6] = "ABCDE";
        type str[6];
        ::memset(str, 0, sizeof(str));
        CHECK_TRUE(secure_buffer_utility_type::PrependMove(str, 6, src, 6, "Hello", 6));
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[5]   = {{2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value = {1, 'A'};
        CHECK_TRUE(secure_buffer_utility_type::PrependMove(dest, 6, src, 5, &value, 1));
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type src[6]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[6]   = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value[6] = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        CHECK_TRUE(secure_buffer_utility_type::PrependMove(dest, 6, src, 6, value, 6));
        CHECK_FALSE(Compare(src, empty, 6)); // src won't change as there is nothing to move.
        CHECK_TRUE(Compare(dest, cmp, 6));
        CHECK_TRUE(Compare(value, empty, 6));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, ReverseCopy, type_ptr_type_const_ptr_size_t)
{
    std::size_t const copy_size = 4; // Size of buffer to copy.
    std::size_t const offset = 2; // Offset into buffer for copying.
    std::size_t const size = copy_size + offset;
    // Note that the total buffer is size + offset.

    TEST_OVERRIDE_ARGS("type*, type const*, std::size_t");

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size] = {'A', 'B', 'A', 'B', 'C', 'D'};

        CHECK_TRUE(secure_buffer_utility_type::ReverseCopy(buffer + offset, buffer, copy_size));
        CHECK_TRUE(Compare(buffer, cmp, size));

        CHECK_FALSE(secure_buffer_utility_type::ReverseCopy(nullptr, buffer, copy_size));
        CHECK_FALSE(secure_buffer_utility_type::ReverseCopy(buffer + offset, nullptr, copy_size));
        CHECK_FALSE(secure_buffer_utility_type::ReverseCopy(buffer + offset, buffer, static_cast<std::size_t>(0)));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size] = {1, 2, 3, 4, 5, 6};
        type cmp[size] = {1, 2, 1, 2, 3, 4};
        CHECK_TRUE(secure_buffer_utility_type::ReverseCopy(buffer + offset, buffer, copy_size));
        CHECK_TRUE(Compare(buffer, cmp, size));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, ReverseMove, type_ptr_type_ptr_size_t)
{
    size_t const copy_size = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.
    size_t const size = copy_size + offset;
    // Note that the total buffer is size + offset.

    TEST_OVERRIDE_ARGS("type*, type*, size_t");

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size]    = {'A', 'B', 'A', 'B', 'C', 'D'};

        CHECK_FALSE(secure_buffer_utility_type::ReverseMove(nullptr, buffer, copy_size));
        CHECK_FALSE(secure_buffer_utility_type::ReverseMove(buffer + offset, nullptr, copy_size));
        CHECK_FALSE(secure_buffer_utility_type::ReverseMove(buffer + offset, buffer, 0U));

        CHECK_TRUE(secure_buffer_utility_type::ReverseMove(buffer + offset, buffer, copy_size));
        CHECK_TRUE(Compare(buffer, cmp, size));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size] = {1, 2, 3, 4, 5, 6};
        type cmp[size]    = {1, 2, 1, 2, 3, 4};
        CHECK_TRUE(secure_buffer_utility_type::ReverseMove(buffer + offset, buffer, copy_size));
        CHECK_TRUE(Compare(buffer, cmp, size));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size] = {{1, 'A'},  {2, 'B'},  {3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}};
        type cmp[size]    = {{0, '\0'}, {0, '\0'}, {1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}};
        CHECK_TRUE(secure_buffer_utility_type::ReverseMove(buffer + offset, buffer, copy_size));
        CHECK_TRUE(Compare(buffer, cmp, size));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, OverlapCopy, type_ptr_type_const_ptr_size_t)
{
    std::size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, std::size_t");

    {
        typedef bool type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));

        CHECK_FALSE(secure_buffer_utility_type::OverlapCopy(nullptr, src, size));
        CHECK_FALSE(secure_buffer_utility_type::OverlapCopy(dest, nullptr, size));
        CHECK_FALSE(secure_buffer_utility_type::OverlapCopy(dest, src, static_cast<std::size_t>(0)));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef char const* type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type src[size] = {str1, str2, str3, str4};
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef wchar_t type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef float type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef long double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapCopy(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, OverlapMove, type_ptr_type_ptr_size_t)
{
    size_t const size   = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.

    TEST_OVERRIDE_ARGS("type*, type*, size_t");

    {
        typedef bool type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));

        CHECK_FALSE(secure_buffer_utility_type::OverlapMove(nullptr, src, size));
        CHECK_FALSE(secure_buffer_utility_type::OverlapMove(dest, nullptr, size));
        CHECK_FALSE(secure_buffer_utility_type::OverlapMove(dest, src, 0U));

        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {

        typedef char const* type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type src[size] = {str1, str2, str3, str4};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef wchar_t type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned short type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long long int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef float type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef long double type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type dest[size]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type src[size]   = {{1, 'A'},  {2, 'B'},  {3, 'C'},  {4, 'D'}};
        type cmp[size]   = {{1, 'A'},  {2, 'B'},  {3, 'C'},  {4, 'D'}};
        type empty[size] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(dest, src, size));
        CHECK_TRUE(Compare(dest, cmp, size));
        CHECK_TRUE(Compare(src, empty, size));
    }

    // Test overlapping of destination buffer > source buffer.
    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'A', 'B', 'A', 'B', 'C', 'D'};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(buffer + offset, buffer, size));
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {1, 2, 1, 2, 3, 4};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(buffer + offset, buffer, size));
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size + offset] = {{1, 'A'},  {2, 'B'},  {3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}};
        type cmp[size + offset]    = {{0, '\0'}, {0, '\0'}, {1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(buffer + offset, buffer, size));
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    // Test overlapping of destination buffer < source buffer.
    {
        typedef char type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'C', 'D', 'E', 'F', 'E', 'F'};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(buffer, buffer + offset, size));
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {3, 4, 5, 6, 5, 6};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(buffer, buffer + offset, size));
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef Data type;
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type buffer[size + offset] = {{1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}, {5, 'E'},  {6, 'F'}};
        type cmp[size + offset]    = {{3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}, {0, '\0'}, {0, '\0'}};
        CHECK_TRUE(secure_buffer_utility_type::OverlapMove(buffer, buffer + offset, size));
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Swap, type_ref_type_ref)
{
    TEST_OVERRIDE_ARGS("type&, type&");

    typedef char type;

    {
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;
        type value1 = 'A';
        type value2 = 'B';
        secure_buffer_utility_type::Swap(value1, value2);
        CHECK_EQUAL(value1, 'B');
        CHECK_EQUAL(value2, 'A');
    }

    {
        typedef SecureBufferUtility<type*, std::size_t> secure_buffer_utility_type;
        type value1 = 'A';
        type value2 = 'B';
        type* value1_ptr = &value1;
        type* value2_ptr = &value2;
        secure_buffer_utility_type::Swap(value1_ptr, value2_ptr);
        CHECK_EQUAL(value1, 'A');
        CHECK_EQUAL(value2, 'B');
        CHECK_EQUAL(value1_ptr, &value2);
        CHECK_EQUAL(value2_ptr, &value1);
    }
}

TEST_MEMBER_FUNCTION(SecureBufferUtility, Swap, type_ref_size_t_type_ref_size_t)
{
    TEST_OVERRIDE_ARGS("type&, std::size_t&, type&, std::size_t&");

    typedef char type;

    {
        typedef SecureBufferUtility<type, std::size_t> secure_buffer_utility_type;

        type value1 = 'A';
        type value2 = 'B';
        std::size_t value1_size = 1U;
        std::size_t value2_size = 1U;
        secure_buffer_utility_type::Swap(value1, value1_size, value2, value2_size);
        CHECK_EQUAL(value1, 'B');
        CHECK_EQUAL(value2, 'A');
        CHECK_EQUAL(value1_size, 1U);
        CHECK_EQUAL(value2_size, 1U);
    }

    {
        typedef SecureBufferUtility<type*, std::size_t> secure_buffer_utility_type;

        type value1[] = "AB";
        type value2[] = "CDE";
        type* value1_ptr = value1;
        type* value2_ptr = value2;
        std::size_t value1_size = 2U;
        std::size_t value2_size = 3U;
        secure_buffer_utility_type::Swap(value1_ptr, value1_size, value2_ptr, value2_size);
        CHECK_EQUAL(value1[0], 'A');
        CHECK_EQUAL(value1[1], 'B');
        CHECK_EQUAL(value2[0], 'C');
        CHECK_EQUAL(value2[1], 'D');
        CHECK_EQUAL(value2[2], 'E');
        CHECK_EQUAL(value1_ptr, value2);
        CHECK_EQUAL(value2_ptr, value1);
        CHECK_EQUAL(value1_size, 3U);
        CHECK_EQUAL(value2_size, 2U);
    }
}
