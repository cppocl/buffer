/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../MapBits.hpp"

#include <cstring>
#include <cstddef>
#include <cstdint>
#include <limits>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

TEST(MapBits)
{
    float f1 = 1.23f;
    std::uint64_t i = ocl::MapBits<float, std::uint64_t>::ToBits(f1);
    float f2 = ocl::MapBits<std::uint64_t, float>::ToBits(i);
    CHECK_EQUAL(f1, f2);
}
