/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferCopySame.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferCopySame;

namespace
{
    struct Data
    {
        int  i;
        char c;
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferCopySame, CopySame, type_ptr_size_type_type_const_ptr_size_type_type_const_ptr_size_type_size_type_ref)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type, size_type&");

    typedef std::size_t size_type;

    size_type bytes_copied;

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = {true};
        type src2[] = {false};
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 0);
    }

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { false };
        type src2[] = { true };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 0);
    }

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { true };
        type src2[] = { true };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { false };
        type src2[] = { false };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { true, true };
        type src2[] = { true, false };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { true, true };
        type src2[] = { true, true };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 2);
    }

    {
        typedef bool type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { false, false };
        type src2[] = { false, false };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 2);
    }

    {
        typedef char type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 'A' };
        type src2[] = { 'B' };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 0);
    }

    {
        typedef char type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 'A' };
        type src2[] = { 'A' };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef char type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 'A', 'A' };
        type src2[] = { 'A', 'B' };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef char type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 'A', 'B' };
        type src2[] = { 'A', 'B' };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest), src1, sizeof(src1), src2, sizeof(src2), bytes_copied);
        CHECK_EQUAL(bytes_copied, 2);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 0 };
        type src2[] = { 1 };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 0);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 1 };
        type src2[] = { 1 };
        type dest[sizeof(src1)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 1, 1 };
        type src2[] = { 1, 2 };
        type dest[sizeof(src1) / sizeof(type)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 1, 2 };
        type src2[] = { 1, 2 };
        type dest[sizeof(src1) / sizeof(type)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 2);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 1, 2 };
        type src2[] = { 1, 2, 3 };
        type dest[sizeof(src1) / sizeof(type)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 2);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 1, 2, 3 };
        type src2[] = { 1, 2 };
        type dest[sizeof(src1) / sizeof(type)];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 2);
    }

    {
        typedef int type;
        typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

        type src1[] = { 1, 2 };
        type src2[] = { 1, 2, 3 };
        type dest[(sizeof(src1) / sizeof(type)) - 1];

        bytes_copied = 99;
        buffer_copy_same_type::CopySame(dest, sizeof(dest) / sizeof(type), src1, sizeof(src1) / sizeof(type), src2, sizeof(src2) / sizeof(type), bytes_copied);
        CHECK_EQUAL(bytes_copied, 1);
    }
}
