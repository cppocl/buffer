/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferPrependCopy.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferPrependCopy;

namespace
{
    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferPrependCopy, PrependCopy, type_ptr_size_type_type_const_ptr_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type");

    {
        char src[5] = "ello";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependCopy<char, size_t>::PrependCopy(str, 6, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "llo";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependCopy<char, size_t>::PrependCopy(str, 6, src, "He", 2);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[6] = "ABCDE";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependCopy<char, size_t>::PrependCopy(str, 6, src, "Hello", 6);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferPrependCopy, PrependCopy, type_ptr_size_type_type_const_ptr_size_type_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, size_type");

    {
        char src[5] = "ello";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependCopy<char, size_t>::PrependCopy(str, 6, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "llo";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependCopy<char, size_t>::PrependCopy(str, 6, src, "He", 2);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[6] = "ABCDE";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependCopy<char, size_t>::PrependCopy(str, 6, src, "Hello", 6);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }
}
