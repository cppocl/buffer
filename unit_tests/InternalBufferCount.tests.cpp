/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferCount.hpp"
#include <cstddef>
#include <cstring>

TEST_MEMBER_FUNCTION(InternalBufferCount, CountSame, char_const_ptr_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("char const*, char const*, size_t");

    using std::size_t;
    typedef ocl::InternalBufferCount<char, size_t> buffer_count_type;

    size_t count = buffer_count_type::CountSame("", "", 0);
    CHECK_EQUAL(count, 0);

    count = buffer_count_type::CountSame("A", "B", 1);
    CHECK_EQUAL(count, 0);

    count = buffer_count_type::CountSame("A", "A", 1);
    CHECK_EQUAL(count, 1);

    count = buffer_count_type::CountSame("AB", "AB", 2);
    CHECK_EQUAL(count, 2);

    // Check comparison doesn't go beyond 1.
    count = buffer_count_type::CountSame("AB", "AB", 1);
    CHECK_EQUAL(count, 1);

    count = buffer_count_type::CountSame("ABC", "ACB", 3);
    CHECK_EQUAL(count, 1);
}

TEST_MEMBER_FUNCTION(InternalBufferCount, CountSame, char_const_ptr_size_t_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("char const*, size_t, char const*, size_t");

    using std::size_t;
    typedef ocl::InternalBufferCount<char, size_t> buffer_count_type;

    size_t count = buffer_count_type::CountSame("", 0, "", 0);
    CHECK_EQUAL(count, 0);

    count = buffer_count_type::CountSame("A", 1, "B", 1);
    CHECK_EQUAL(count, 0);

    count = buffer_count_type::CountSame("A", 1, "A", 1);
    CHECK_EQUAL(count, 1);

    count = buffer_count_type::CountSame("AB", 2, "AB", 2);
    CHECK_EQUAL(count, 2);

    count = buffer_count_type::CountSame("AB", 1, "AB", 2);
    CHECK_EQUAL(count, 1);

    // Check comparison doesn't go beyond 1.
    count = buffer_count_type::CountSame("AB", 1, "AB", 1);
    CHECK_EQUAL(count, 1);

    count = buffer_count_type::CountSame("ABC", 3, "ACB", 3);
    CHECK_EQUAL(count, 1);
}

TEST_MEMBER_FUNCTION(InternalBufferCount, CountDifferent, char_const_ptr_size_t_char_const_ptr_size_t_size_t_ref_size_t_ref)
{
    TEST_OVERRIDE_ARGS("char const*, size_t, char const*, size_t, size_t&, size_t&");

    using std::size_t;
    typedef ocl::InternalBufferCount<char, size_t> buffer_count_type;

    size_t diff1 = 99;
    size_t diff2 = 99;

    buffer_count_type::CountDifferent("", 0, "", 0, diff1, diff2);
    CHECK_EQUAL(diff1, 0);
    CHECK_EQUAL(diff2, 0);

    buffer_count_type::CountDifferent("A", 1, "B", 1, diff1, diff2);
    CHECK_EQUAL(diff1, 1);
    CHECK_EQUAL(diff2, 1);

    buffer_count_type::CountDifferent("A", 1, "A", 1, diff1, diff2);
    CHECK_EQUAL(diff1, 0);
    CHECK_EQUAL(diff2, 0);

    buffer_count_type::CountDifferent("BA", 2, "AB", 2, diff1, diff2);
    CHECK_EQUAL(diff1, 1);
    CHECK_EQUAL(diff2, 1);

    buffer_count_type::CountDifferent("AB", 1, "AB", 2, diff1, diff2);
    CHECK_EQUAL(diff1, 0);
    CHECK_EQUAL(diff2, 0);

    // Check comparison doesn't go beyond 1.
    buffer_count_type::CountDifferent("AB", 1, "BA", 1, diff1, diff2);
    CHECK_EQUAL(diff1, 1);
    CHECK_EQUAL(diff2, 1);

    buffer_count_type::CountDifferent("ABF", 3, "CDEF", 4, diff1, diff2);
    CHECK_EQUAL(diff1, 2);
    CHECK_EQUAL(diff2, 3);

    buffer_count_type::CountDifferent("CDEF", 4, "ABF", 3, diff1, diff2);
    CHECK_EQUAL(diff1, 3);
    CHECK_EQUAL(diff2, 2);

    buffer_count_type::CountDifferent("AB12F", 5, "CDE34F", 6, diff1, diff2);
    CHECK_EQUAL(diff1, 4);
    CHECK_EQUAL(diff2, 5);

    buffer_count_type::CountDifferent("CDE12F", 6, "AB34F", 5, diff1, diff2);
    CHECK_EQUAL(diff1, 5);
    CHECK_EQUAL(diff2, 4);
}
