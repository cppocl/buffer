/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferOverlapMove.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferOverlapMove;

namespace
{
    struct Data
    {
        int  i;
        char c;

        Data& operator =(Data&& src) noexcept
        {
            i = src.i;
            c = src.c;
            src.i = 0;
            src.c = '\0';
            return *this;
        }

        bool operator !=(Data const& other) const noexcept
        {
            return (i != other.i) || (c != other.c);
        }
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        for (Type const* end = a + count; a < end; ++a, ++b)
            if (*a != *b)
                return false;
        return true;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferOverlapMove, OverlapMove, type_ptr_type_ptr_size_t)
{
    size_t const size   = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.

    TEST_OVERRIDE_ARGS("type*, type*, size_t");

    {
        typedef bool type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef char type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type src[size] = {str1, str2, str3, str4};
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef wchar_t type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef float type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef double type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef long double type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef Data type;
        type dest[size]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type src[size]   = {{1, 'A'},  {2, 'B'},  {3, 'C'},  {4, 'D'}};
        type cmp[size]   = {{1, 'A'},  {2, 'B'},  {3, 'C'},  {4, 'D'}};
        type empty[size] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        InternalBufferOverlapMove<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, cmp, size));
        CHECK_TRUE(Compare(src, empty, size));
    }

    // Test overlapping of destination buffer > source buffer.
    {
        typedef char type;
        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'A', 'B', 'A', 'B', 'C', 'D'};
        InternalBufferOverlapMove<type>::OverlapMove(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {1, 2, 1, 2, 3, 4};
        InternalBufferOverlapMove<type>::OverlapMove(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef Data type;
        type buffer[size + offset] = {{1, 'A'},  {2, 'B'},  {3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}};
        type cmp[size + offset]    = {{0, '\0'}, {0, '\0'}, {1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}};
        InternalBufferOverlapMove<type>::OverlapMove(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    // Test overlapping of destination buffer < source buffer.
    {
        typedef char type;
        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'C', 'D', 'E', 'F', 'E', 'F'};
        InternalBufferOverlapMove<type>::OverlapMove(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {3, 4, 5, 6, 5, 6};
        InternalBufferOverlapMove<type>::OverlapMove(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef Data type;
        type buffer[size + offset] = {{1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}, {5, 'E'},  {6, 'F'}};
        type cmp[size + offset]    = {{3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}, {0, '\0'}, {0, '\0'}};
        InternalBufferOverlapMove<type>::OverlapMove(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }
}
