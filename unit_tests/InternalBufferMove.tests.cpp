/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferMove.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferMove;

namespace
{
    struct Data
    {
        int  i;
        char c;

        Data() noexcept
            : i(0)
            , c('\0')
        {
        }

        Data(Data &&src) noexcept
            : i(src.i)
            , c(src.c)
        {
            src.i = 0;
            src.c = '\0';
        }

        Data& operator=(Data&& src) noexcept
        {
            i = src.i;
            c = src.c;
            src.i = 0;
            src.c = '\0';
            return *this;
        }
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferMove, Move, type_ptr_type_const_ptr_size_t)
{
    size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type source[size] = {str1, str2, str3, str4};
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_NULL(source[0]);
        CHECK_NULL(source[1]);
        CHECK_NULL(source[2]);
        CHECK_NULL(source[3]);
        CHECK_EQUAL(*dest[0], '1');
        CHECK_EQUAL(*dest[1], '2');
        CHECK_EQUAL(*dest[2], '3');
        CHECK_EQUAL(*dest[3], '4');
    }

    {
        typedef wchar_t type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef float type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef long double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        typedef char* type_ptr;
        type_ptr dest_ptr[size];
        type_ptr source_ptr[size];
        type source[size];
        source[0] = 'A';
        source[1] = 'B';
        source[2] = 'C';
        source[3] = 'D';
        source_ptr[0] = &source[0];
        source_ptr[1] = &source[1];
        source_ptr[2] = &source[2];
        source_ptr[3] = &source[3];
        ::memset(dest_ptr, 0, sizeof(dest_ptr));
        InternalBufferMove<type_ptr>::Move(dest_ptr, source_ptr, size);
        CHECK_NULL(source_ptr[0]);
        CHECK_NULL(source_ptr[1]);
        CHECK_NULL(source_ptr[2]);
        CHECK_NULL(source_ptr[3]);
        CHECK_EQUAL(*dest_ptr[0], 'A');
        CHECK_EQUAL(*dest_ptr[1], 'B');
        CHECK_EQUAL(*dest_ptr[2], 'C');
        CHECK_EQUAL(*dest_ptr[3], 'D');
    }

    {
        typedef Data type;
        type dest[size];
        type source[size];
        source[0].i = 1;
        source[0].c = 'A';
        source[1].i = 2;
        source[1].c = 'B';
        source[2].i = 3;
        source[2].c = 'C';
        source[3].i = 4;
        source[3].c = 'D';
        InternalBufferMove<type>::Move(dest, source, size);
        CHECK_FALSE(Compare(dest, source, size)); // Move should clear source.
    }
}
