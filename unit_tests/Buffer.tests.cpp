/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../SequentialSearchFunctor.hpp"
#include "../BinarySearchFunctor.hpp"
#include "../Buffer.hpp"
#include <string.h>
#include <limits>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

namespace
{
    struct Data
    {
        int  i;
        char c;

        Data& operator=(Data const& src) noexcept
        {
            i = src.i;
            c = src.c;
            return *this;
        }

        Data& operator=(Data&& src) noexcept
        {
            i = src.i;
            c = src.c;
            src.i = 0;
            src.c = '\0';
            return *this;
        }

        bool operator ==(Data const& other) const noexcept
        {
            return (i == other.i) && (c == other.c);
        }

        bool operator !=(Data const& other) const noexcept
        {
            return (i != other.i) || (c != other.c);
        }
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, std::size_t count)
    {
        for (Type const* end = a + count; a < end; ++a, ++b)
            if (*a != *b)
                return false;
        return true;
    }
}

using ocl::Buffer;

TEST_MEMBER_FUNCTION(Buffer, constructor, NA)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME("Buffer");

    buffer_type buffer;
    CHECK_TRUE(buffer.IsEmpty());
    CHECK_EQUAL(buffer.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, constructor, size_t)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME("Buffer");

    buffer_type buffer(1);
    CHECK_FALSE(buffer.IsEmpty());
    CHECK_EQUAL(buffer.GetSize(), 1U);
}

TEST_MEMBER_FUNCTION(Buffer, constructor, size_t_char_const_ptr)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME_ARGS("Buffer", "size_t, char const*");

    buffer_type buffer(2, "A");
    CHECK_FALSE(buffer.IsEmpty());
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(::strcmp(buffer.Ptr(), "A"), 0);
}

TEST_MEMBER_FUNCTION(Buffer, constructor, Buffer_const_ref)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME_ARGS("Buffer", "Buffer const&");

    buffer_type buffer1(2, "A");
    buffer_type buffer2(buffer1);
    CHECK_FALSE(buffer2.IsEmpty());
    CHECK_EQUAL(buffer2.GetSize(), 2U);
    CHECK_EQUAL(::strcmp(buffer2.Ptr(), "A"), 0);
}

TEST_MEMBER_FUNCTION(Buffer, constructor, Buffer_move_ref)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME_ARGS("Buffer", "Buffer&&");

    buffer_type buffer1(2, "A");
    buffer_type buffer2(static_cast<buffer_type&&>(buffer1));
    CHECK_TRUE(buffer1.IsEmpty());
    CHECK_EQUAL(buffer1.GetSize(), 0U);
    CHECK_FALSE(buffer2.IsEmpty());
    CHECK_EQUAL(buffer2.GetSize(), 2U);
    CHECK_EQUAL(::strcmp(buffer2.Ptr(), "A"), 0);
}

TEST_MEMBER_FUNCTION(Buffer, operator_equal, Buffer_const_ref)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "Buffer const&");

    {
        buffer_type buffer1;
        buffer_type buffer2;
        buffer2 = buffer1;
        CHECK_TRUE(buffer2.IsEmpty());
        CHECK_EQUAL(buffer2.GetSize(), 0U);
        CHECK_EQUAL(buffer2.Ptr(), nullptr);
    }

    {
        buffer_type buffer1(2, "A");
        buffer_type buffer2;
        buffer2 = buffer1;
        CHECK_FALSE(buffer2.IsEmpty());
        CHECK_EQUAL(buffer2.GetSize(), 2U);
        CHECK_EQUAL(::strcmp(buffer2.Ptr(), "A"), 0);
    }
}

TEST_MEMBER_FUNCTION(Buffer, operator_equal, Buffer_move_ref)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "Buffer&&");

    buffer_type buffer1(2, "A");
    buffer_type buffer2;
    buffer2 = static_cast<buffer_type&&>(buffer1);
    CHECK_TRUE(buffer1.IsEmpty());
    CHECK_EQUAL(buffer1.GetSize(), 0U);
    CHECK_FALSE(buffer2.IsEmpty());
    CHECK_EQUAL(buffer2.GetSize(), 2U);
    CHECK_EQUAL(::strcmp(buffer2.Ptr(), "A"), 0);
}

TEST_MEMBER_FUNCTION(Buffer, operator_type_ptr, NA)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME("operator type*");

    buffer_type buffer;
    CHECK_NULL(static_cast<type*>(buffer));

    buffer.SetSize(1U);
    CHECK_NOT_NULL(static_cast<type*>(buffer));
}

TEST_MEMBER_FUNCTION(Buffer, operator_type_const_ptr, NA)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    TEST_OVERRIDE_FUNCTION_NAME("operator type const*");

    buffer_type buffer;
    CHECK_NULL(static_cast<type const*>(buffer));

    buffer.SetSize(1U);
    CHECK_NOT_NULL(static_cast<type const*>(buffer));
}

TEST_MEMBER_FUNCTION(Buffer, operator_subscript, size_t)
{
    typedef char type;

    TEST_OVERRIDE_FUNCTION_NAME("operator[]");

    Buffer<type> buffer(3U, "AB");
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], '\0');

    buffer[0] = 'C';
    CHECK_EQUAL(buffer[0], 'C');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], '\0');

    Buffer<type> const& buffer_const = buffer;
    CHECK_EQUAL(buffer_const[0], 'C');
    CHECK_EQUAL(buffer_const[1], 'B');
    CHECK_EQUAL(buffer_const[2], '\0');
}

TEST_MEMBER_FUNCTION(Buffer, GetMaxSize, NA)
{
    Buffer<char, std::size_t> buffer;

    std::size_t const max_size = std::numeric_limits<std::size_t>::max();
    CHECK_EQUAL(buffer.GetMaxSize(), max_size);
}

TEST_MEMBER_FUNCTION(Buffer, GetMemSize, NA)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type::memory_type mem;
    buffer_type buffer(1U);
    CHECK_EQUAL(buffer.GetMemSize(), 1U + mem.GetMemSize());
}

TEST_MEMBER_FUNCTION(Buffer, GetSize, NA)
{
    Buffer<char, std::size_t> buffer;

    CHECK_EQUAL(buffer.GetSize(), 0U);

    // Check setting size first time will return correct size.
    buffer.SetSize(1U);
    CHECK_EQUAL(buffer.GetSize(), 1U);

    // Check going larger will return correct size.
    buffer.SetSize(2U);
    CHECK_EQUAL(buffer.GetSize(), 2U);

    // Check going smaller will return correct size.
    buffer.SetSize(1U);
    CHECK_EQUAL(buffer.GetSize(), 1U);
}

TEST_MEMBER_FUNCTION(Buffer, IsEmpty, NA)
{
    Buffer<char, std::size_t> buffer;
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(1U);
    CHECK_FALSE(buffer.IsEmpty());

    buffer.SetSize(0U);
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(1U);
    CHECK_FALSE(buffer.IsEmpty());

    buffer.Clear();
    CHECK_TRUE(buffer.IsEmpty());
}

TEST_MEMBER_FUNCTION(Buffer, Ptr, NA)
{
    Buffer<char, std::size_t> buffer;
    Buffer<char, std::size_t> const& buffer_const = buffer;

    CHECK_NULL(buffer.Ptr());
    CHECK_NULL(buffer_const.Ptr());

    buffer.SetSize(1U);
    CHECK_NOT_NULL(buffer.Ptr());
    CHECK_NOT_NULL(buffer_const.Ptr());

    buffer.SetSize(0U);
    CHECK_NULL(buffer.Ptr());
    CHECK_NULL(buffer_const.Ptr());

    buffer.SetSize(1U);
    CHECK_NOT_NULL(buffer.Ptr());
    CHECK_NOT_NULL(buffer_const.Ptr());

    buffer.Clear();
    CHECK_NULL(buffer.Ptr());
    CHECK_NULL(buffer_const.Ptr());
}

TEST_MEMBER_FUNCTION(Buffer, Clear, NA)
{
    Buffer<char, std::size_t> buffer;
    CHECK_TRUE(buffer.IsEmpty());

    buffer.Clear();
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(1U);
    CHECK_FALSE(buffer.IsEmpty());

    buffer.Clear();
    CHECK_TRUE(buffer.IsEmpty());
}

TEST_MEMBER_FUNCTION(Buffer, Compact, NA)
{
    Buffer<char, std::size_t> buffer(3, "AB");

    char const* ptr = buffer.Ptr();
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], '\0');

    buffer.Resize(buffer.GetSize() - 2, false);
    CHECK_EQUAL(buffer.Ptr(), ptr);

    // When the space is freed the pointer will point to a new address.
    buffer.Compact();
    CHECK_NOT_EQUAL(buffer.Ptr(), ptr);
    CHECK_EQUAL(buffer[0], 'A');
}

TEST_MEMBER_FUNCTION(Buffer, SetSize, size_t)
{
    Buffer<char, std::size_t> buffer(3, "AB");

    // The re-size might or might not destroy the original data,
    // so there is no way to test for this and guarantee the test will pass.
    buffer.SetSize(4U);
    CHECK_EQUAL(buffer.GetSize(), 4U);

    buffer.Copy("ABC", 4U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], 'C');
    CHECK_EQUAL(buffer[3], '\0');
}

TEST_MEMBER_FUNCTION(Buffer, Resize, size_t_bool)
{
    TEST_OVERRIDE_ARGS("size_t, bool");

    Buffer<char, std::size_t> buffer(3, "AB");

    // Resize will always reallocate memory when growing in size.
    // Check the content is copied and the same as before the resize.
    char* ptr = buffer.Ptr();
    buffer.Resize(4U);
    CHECK_NOT_EQUAL(ptr, buffer.Ptr());
    CHECK_EQUAL(buffer.GetSize(), 4U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], '\0');

    // Reallocate will not reallocate the memory when becoming smaller, if reallocate is false.
    // Check the pointer has not changed since the resize.
    ptr = buffer.Ptr();
    buffer.Resize(3U, false);
    CHECK_EQUAL(ptr, buffer.Ptr());
    CHECK_EQUAL(buffer.GetSize(), 3U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], '\0');

    // Check the forced reallocate for the same size has changed the pointer but not the content.
    ptr = buffer.Ptr();
    buffer.Resize(3U, true);
    CHECK_NOT_EQUAL(ptr, buffer.Ptr());
    CHECK_EQUAL(buffer.GetSize(), 3U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], '\0');

    // Check that the resize to a smaller size does not effect the pointer or contents.
    ptr = buffer.Ptr();
    buffer.Resize(2U, false);
    CHECK_EQUAL(ptr, buffer.Ptr());
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(ptr[0], 'A');
    CHECK_EQUAL(ptr[1], 'B');
    CHECK_EQUAL(ptr[2], '\0');

    // Check the increase in size has changed the pointer and copies the contents.
    ptr = buffer.Ptr();
    buffer.Resize(3U, true);
    CHECK_NOT_EQUAL(ptr, buffer.Ptr());
    CHECK_EQUAL(buffer.GetSize(), 3U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
}

TEST_MEMBER_FUNCTION(Buffer, ShrinkBy, size_t)
{
    Buffer<char, std::size_t> buffer(3, "AB");
    char const* ptr1 = buffer.Ptr();
    CHECK_EQUAL(ptr1[0], 'A');
    CHECK_EQUAL(ptr1[1], 'B');
    CHECK_EQUAL(ptr1[2], '\0');

    buffer.ShrinkBy(1U);
    CHECK_EQUAL(buffer.GetSize(), 2U);
    char const* ptr2 = buffer.Ptr();
    CHECK_EQUAL(ptr1, ptr2);
    CHECK_EQUAL(ptr2[0], 'A');
    CHECK_EQUAL(ptr2[1], 'B');
    CHECK_EQUAL(ptr2[2], '\0');

    buffer.ShrinkBy(1U);
    CHECK_EQUAL(buffer.GetSize(), 1U);

    buffer.ShrinkBy(1U);
    CHECK_EQUAL(buffer.GetSize(), 0U);

    buffer.SetSize(1U);
    buffer.ShrinkBy(2U);
    CHECK_EQUAL(buffer.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, Set, type)
{
    Buffer<char, std::size_t> buffer;
    buffer.Set('A');
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(2U);
    buffer.Set('A');
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'A');

    buffer.Set('B');
    CHECK_EQUAL(buffer[0], 'B');
    CHECK_EQUAL(buffer[1], 'B');
}

TEST_MEMBER_FUNCTION(Buffer, Set, size_t_size_t_type)
{
    TEST_OVERRIDE_ARGS("size_t, size_t, type");

    Buffer<char, std::size_t> buffer;
    buffer.Set(0, 0, 'A');
    CHECK_TRUE(buffer.IsEmpty());
    buffer.Set(0, 1, 'A');
    CHECK_TRUE(buffer.IsEmpty());
    buffer.Set(1, 0, 'A');
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(2U);
    buffer.SetAt(0, '\0');
    buffer.SetAt(1, '\0');

    buffer.Set(1, 0, 'A');
    CHECK_TRUE(buffer[0] == '\0');
    CHECK_TRUE(buffer[1] == '\0');

    buffer.Set(1, 2, 'A');
    CHECK_TRUE(buffer[0] == '\0');
    CHECK_TRUE(buffer[1] == '\0');

    buffer.Set(0, 2, 'A');
    CHECK_TRUE(buffer[0] == '\0');
    CHECK_TRUE(buffer[1] == '\0');

    buffer.Set('X');
    buffer.Set(0, 0, 'A');
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'X');

    buffer.Set('X');
    buffer.Set(1, 1, 'A');
    CHECK_EQUAL(buffer[0], 'X');
    CHECK_EQUAL(buffer[1], 'A');

    buffer.Set('X');
    buffer.Set(0, 1, 'A');
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'A');
}

TEST_MEMBER_FUNCTION(Buffer, Copy, type_size_t)
{
    TEST_OVERRIDE_ARGS("type, size_type");

    Buffer<char, std::size_t> buffer;
    buffer.Copy('A', 0U);
    CHECK_TRUE(buffer.IsEmpty());

    buffer.Copy('A', 1U);
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');
}

TEST_MEMBER_FUNCTION(Buffer, Copy, type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, size_t");

    Buffer<char, std::size_t> buffer;
    buffer.Copy("A", 1U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer.GetSize(), 1U);

    buffer.SetSize(1U);
    buffer.Copy("A", 2U);
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], '\0');
}

TEST_MEMBER_FUNCTION(Buffer, Copy, Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("Buffer const&");

    Buffer<char, std::size_t> buffer1;
    Buffer<char, std::size_t> buffer2(1U, "A");
    Buffer<char, std::size_t> empty;

    buffer1.Copy(buffer2);
    CHECK_EQUAL(buffer1.GetSize(), 1U);
    CHECK_EQUAL(buffer1[0], 'A');

    buffer1.Copy(empty);
    CHECK_EQUAL(buffer1.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, Overwrite, in_type_size_t)
{
    TEST_OVERRIDE_ARGS("in_type, size_t");

    Buffer<char, std::size_t> buffer(5, "ABCD");

    buffer.Overwrite('Z', 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "ZBCD", 5));

    buffer.Overwrite('E', 6);
    CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEE", 6));
}

TEST_MEMBER_FUNCTION(Buffer, Overwrite, type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, size_t");

    Buffer<char, std::size_t> buffer(5, "ABCD");

    buffer.Overwrite("Z", 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "ZBCD", 5));

    buffer.Overwrite("EEEEEE", 6);
    CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEE", 6));
}

TEST_MEMBER_FUNCTION(Buffer, Overwrite, Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("Buffer<char> const&");

    Buffer<char, std::size_t> buffer(5, "ABCD");

    {
        Buffer<char, std::size_t> buffer2(1, "Z");
        buffer.Overwrite(buffer2);
        CHECK_TRUE(Compare(buffer.Ptr(), "ZBCD", 5));
    }

    {
        Buffer<char, std::size_t> buffer2(6, "EEEEEE");
        buffer.Overwrite(buffer2);
        CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEE", 6));
    }
}

TEST_MEMBER_FUNCTION(Buffer, Overwrite, size_t_in_type_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, in_type, size_t");

    Buffer<char, std::size_t> buffer(5, "ABCD");

    buffer.Overwrite(0, 'Z', 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "ZBCD", 5));

    buffer.Overwrite(0, 'E', 6);
    CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEE", 6));

    buffer.Overwrite(6, 'F', 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEEF", 7));

    buffer.Overwrite(8, 'G', 1);
    CHECK_EQUAL(buffer.GetSize(), 7U);
}

TEST_MEMBER_FUNCTION(Buffer, Overwrite, size_t_type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, type const*, size_t");

    Buffer<char, std::size_t> buffer(5, "ABCD");

    buffer.Overwrite(0, "Z", 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "ZBCD", 5));

    buffer.Overwrite(0, "EEEEEE", 6);
    CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEE", 6));

    buffer.Overwrite(6, "F", 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEEF", 7));
    
    buffer.Overwrite(8, "G", 1);
    CHECK_EQUAL(buffer.GetSize(), 7U);
}

TEST_MEMBER_FUNCTION(Buffer, Overwrite, size_t_Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("size_t, Buffer<char> const&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer(5, "ABCD");

    {
        buffer_type buffer2(1U, "Z");
        buffer.Overwrite(0, buffer2);
        CHECK_TRUE(Compare(buffer.Ptr(), "ZBCD", 5));
    }

    {
        buffer_type buffer2(6U, "EEEEEE");
        buffer.Overwrite(0, buffer2);
        CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEE", 6));
    }

    {
        buffer_type buffer2(1U, "F");
        buffer.Overwrite(6, buffer2);
        CHECK_TRUE(Compare(buffer.Ptr(), "EEEEEEF", 7));
    }

    {
        buffer.Overwrite(8, "G", 1);
        CHECK_EQUAL(buffer.GetSize(), 7U);
    }
}

TEST_MEMBER_FUNCTION(Buffer, SetAt, size_t_type)
{
    TEST_OVERRIDE_ARGS("size_t, type");

    Buffer<char, std::size_t> buffer;
    buffer.SetAt(0U, 'A');
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(1U);
    buffer.SetAt(0U, '\0');
    CHECK_EQUAL(buffer[0], '\0');

    buffer.SetAt(1U, 'A');
    CHECK_EQUAL(buffer.GetSize(), 1U);

    buffer.SetAt(0U, 'A');
    CHECK_EQUAL(buffer[0], 'A');
}

TEST_MEMBER_FUNCTION(Buffer, SetAt, size_t_type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, type const*, size_t");

    Buffer<char, std::size_t> buffer;
    buffer.SetAt(0U, "A", 1U);
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(1U);
    buffer.SetAt(0U, "AB", 2U);
    CHECK_EQUAL(buffer.GetSize(), 1U);

    buffer.SetAt(1U, "A", 1U);
    CHECK_EQUAL(buffer.GetSize(), 1U);

    buffer.SetAt(0U, "A", 1U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer.GetSize(), 1U);
}

TEST_MEMBER_FUNCTION(Buffer, SetAt, size_t_Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("size_t, buffer const&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;
    {
        buffer_type tmp(1U, "A");
        buffer.SetAt(0U, tmp);
        CHECK_TRUE(buffer.IsEmpty());
    }

    buffer.SetSize(1U);
    buffer.SetAt(0U, '\0');

    {
        buffer_type tmp(2U, "AB");
        buffer.SetAt(0U, tmp);
        CHECK_EQUAL(buffer.GetSize(), 1U);
        CHECK_EQUAL(buffer[0], '\0');
    }

    {
        buffer_type tmp(1U, "A");
        buffer.SetAt(1U, tmp);
        CHECK_EQUAL(buffer.GetSize(), 1U);
        CHECK_EQUAL(buffer[0], '\0');
    }

    {
        buffer_type tmp(1U, "A");
        buffer.SetAt(0U, tmp);
        CHECK_EQUAL(buffer.GetSize(), 1U);
        CHECK_EQUAL(buffer[0], 'A');
    }
}

TEST_MEMBER_FUNCTION(Buffer, SetAt, size_t_Buffer_move_ref)
{
    TEST_OVERRIDE_ARGS("size_t, buffer&&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;
    buffer.SetAt(0U, buffer_type(1U, "A"));
    CHECK_TRUE(buffer.IsEmpty());

    buffer.SetSize(1U);
    buffer.SetAt(0U, '\0');

    buffer.SetAt(0U, buffer_type(2U, "AB"));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], '\0');

    buffer.SetAt(1U, buffer_type(1U,"A"));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], '\0');

    buffer.SetAt(0U, buffer_type(1U, "A"));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');
}

TEST_MEMBER_FUNCTION(Buffer, Move, Buffer_ref)
{
    TEST_OVERRIDE_ARGS("Buffer&");

    {
        typedef Buffer<char, std::size_t> buffer_type;

        buffer_type buffer1(2U, "AB");
        buffer_type buffer2(1U, "C");
        buffer1.Move(buffer2);
        CHECK_EQUAL(buffer1.GetSize(), 1U);
        CHECK_EQUAL(buffer1[0], 'C');
        CHECK_TRUE(buffer2.IsEmpty());
        CHECK_EQUAL(buffer2.GetSize(), 0U);
    }

    {
        typedef Buffer<Data, std::size_t> buffer_type;

        Data init1[] = {{1, 'A'}, {2, 'B'}};
        Data init2[] = {{3, 'C'}};
        buffer_type buffer1(2U, init1);
        buffer_type buffer2(1U, init2);
        buffer1.Move(buffer2);
        CHECK_EQUAL(buffer1.GetSize(), 1U);
        CHECK_EQUAL(buffer1[0], init2[0]);
        CHECK_TRUE(buffer2.IsEmpty());
        CHECK_EQUAL(buffer2.GetSize(), 0U);
    }
}

TEST_MEMBER_FUNCTION(Buffer, Swap, Buffer_ref)
{
    TEST_OVERRIDE_ARGS("Buffer&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer1(2U, "AB");
    buffer_type buffer2(1U, "C");
    buffer1.Swap(buffer2);
    CHECK_EQUAL(buffer1.GetSize(), 1U);
    CHECK_EQUAL(buffer1[0], 'C');
    CHECK_EQUAL(buffer2.GetSize(), 2U);
    CHECK_EQUAL(buffer2[0], 'A');
    CHECK_EQUAL(buffer2[1], 'B');
}

TEST_MEMBER_FUNCTION(Buffer, Prepend, type)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;
    CHECK_TRUE(buffer.Prepend('B'));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'B');
    CHECK_TRUE(buffer.Prepend('A'));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
}

TEST_MEMBER_FUNCTION(Buffer, Prepend, type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;
    CHECK_TRUE(buffer.Prepend("DE", 2U));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(buffer[0], 'D');
    CHECK_EQUAL(buffer[1], 'E');
    CHECK_TRUE(buffer.Prepend("ABC", 3U));
    CHECK_EQUAL(buffer.GetSize(), 5U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], 'C');
    CHECK_EQUAL(buffer[3], 'D');
    CHECK_EQUAL(buffer[4], 'E');
}

TEST_MEMBER_FUNCTION(Buffer, Prepend, Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("Buffer const&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    {
        buffer_type tmp(2U, "DE");
        CHECK_TRUE(buffer.Prepend(tmp));
        CHECK_EQUAL(buffer.GetSize(), 2U);
        CHECK_EQUAL(buffer[0], 'D');
        CHECK_EQUAL(buffer[1], 'E');
    }

    {
        buffer_type tmp(3U, "ABC");
        CHECK_TRUE(buffer.Prepend(tmp));
        CHECK_EQUAL(buffer.GetSize(), 5U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'B');
        CHECK_EQUAL(buffer[2], 'C');
        CHECK_EQUAL(buffer[3], 'D');
        CHECK_EQUAL(buffer[4], 'E');
    }
}

TEST_MEMBER_FUNCTION(Buffer, Prepend, Buffer_move_ref)
{
    TEST_OVERRIDE_ARGS("Buffer&&");

    {
        typedef Buffer<char, std::size_t> buffer_type;

        buffer_type buffer;

        {
            buffer_type tmp(2U, "DE");
            CHECK_TRUE(buffer.Prepend(static_cast<buffer_type&&>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 2U);
            CHECK_EQUAL(buffer.GetSize(), 2U);
            CHECK_EQUAL(buffer[0], 'D');
            CHECK_EQUAL(buffer[1], 'E');
        }

        {
            buffer_type tmp(3U, "ABC");
            CHECK_TRUE(buffer.Prepend(static_cast<buffer_type&&>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 3U);
            CHECK_EQUAL(buffer.GetSize(), 5U);
            CHECK_EQUAL(buffer[0], 'A');
            CHECK_EQUAL(buffer[1], 'B');
            CHECK_EQUAL(buffer[2], 'C');
            CHECK_EQUAL(buffer[3], 'D');
            CHECK_EQUAL(buffer[4], 'E');
        }
    }

    {
        typedef Data type;
        typedef Buffer<type, std::size_t> buffer_type;

        type empty = {0, '\0'};
        buffer_type buffer;

        {
            type items[] = {{4, 'D'}, {5, 'E'}};
            type cmp[] = {{4, 'D'}, {5, 'E'}};
            buffer_type tmp(2U, items);
            CHECK_TRUE(buffer.Prepend(static_cast<buffer_type&&>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 2U);
            CHECK_EQUAL(tmp[0], empty);
            CHECK_EQUAL(tmp[1], empty);
            CHECK_EQUAL(buffer.GetSize(), 2U);
            CHECK_EQUAL(buffer[0], cmp[0]);
            CHECK_EQUAL(buffer[1], cmp[1]);
        }

        {
            type items[] = {{1, 'A'}, {2, 'B'}, {3, 'C'}};
            type cmp[] = {{1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}, {5, 'E'}};
            buffer_type tmp(3U, items);
            CHECK_TRUE(buffer.Prepend(static_cast<buffer_type&&>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 3U);
            CHECK_EQUAL(tmp[0], empty);
            CHECK_EQUAL(tmp[1], empty);
            CHECK_EQUAL(tmp[2], empty);
            CHECK_EQUAL(buffer.GetSize(), 5U);
            CHECK_EQUAL(buffer[0], cmp[0]);
            CHECK_EQUAL(buffer[1], cmp[1]);
            CHECK_EQUAL(buffer[2], cmp[2]);
            CHECK_EQUAL(buffer[3], cmp[3]);
            CHECK_EQUAL(buffer[4], cmp[4]);
        }
    }
}

TEST_MEMBER_FUNCTION(Buffer, Append, type)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;
    CHECK_TRUE(buffer.Append('A'));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_TRUE(buffer.Append('B'));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
}

TEST_MEMBER_FUNCTION(Buffer, Append, type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;
    CHECK_TRUE(buffer.Append("AB", 2U));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_TRUE(buffer.Append("CDE", 3U));
    CHECK_EQUAL(buffer.GetSize(), 5U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], 'C');
    CHECK_EQUAL(buffer[3], 'D');
    CHECK_EQUAL(buffer[4], 'E');
}

TEST_MEMBER_FUNCTION(Buffer, Append, Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("Buffer const&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    {
        buffer_type tmp(2, "AB");
        CHECK_TRUE(buffer.Append(tmp));
        CHECK_EQUAL(buffer.GetSize(), 2U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'B');
    }

    {
        buffer_type tmp(3, "CDE");
        CHECK_TRUE(buffer.Append(tmp));
        CHECK_EQUAL(buffer.GetSize(), 5U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'B');
        CHECK_EQUAL(buffer[2], 'C');
        CHECK_EQUAL(buffer[3], 'D');
        CHECK_EQUAL(buffer[4], 'E');
    }
}

TEST_MEMBER_FUNCTION(Buffer, Append, Buffer_move_ref)
{
    TEST_OVERRIDE_ARGS("Buffer&&");

    {
        typedef Buffer<char, std::size_t> buffer_type;

        buffer_type buffer;

        {
            buffer_type tmp(2, "AB");
            CHECK_TRUE(buffer.Append(static_cast<buffer_type>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 2U);
            CHECK_EQUAL(buffer.GetSize(), 2U);
            CHECK_EQUAL(buffer[0], 'A');
            CHECK_EQUAL(buffer[1], 'B');
        }

        {
            buffer_type tmp(3, "CDE");
            CHECK_TRUE(buffer.Append(static_cast<buffer_type>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 3U);
            CHECK_EQUAL(buffer.GetSize(), 5U);
            CHECK_EQUAL(buffer[0], 'A');
            CHECK_EQUAL(buffer[1], 'B');
            CHECK_EQUAL(buffer[2], 'C');
            CHECK_EQUAL(buffer[3], 'D');
            CHECK_EQUAL(buffer[4], 'E');
        }
    }

    {
        typedef Data type;
        typedef Buffer<type, std::size_t> buffer_type;

        buffer_type buffer;
        type empty = {0, '\0'};

        {
            type items[] = {{1, 'A'}, {2, 'B'}};
            type cmp[] = {{1, 'A'}, {2, 'B'}};
            buffer_type tmp(2, items);
            CHECK_TRUE(buffer.Append(static_cast<buffer_type&&>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 2U);
            CHECK_EQUAL(tmp[0], empty);
            CHECK_EQUAL(tmp[1], empty);
            CHECK_EQUAL(buffer.GetSize(), 2U);
            CHECK_EQUAL(buffer[0], cmp[0]);
            CHECK_EQUAL(buffer[1], cmp[1]);
        }

        {
            type items[] = {{3, 'C'}, {4, 'D'}, {5, 'E'}};
            type cmp[] = {{1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}, {5, 'E'}};
            buffer_type tmp(3U, items);
            CHECK_TRUE(buffer.Append(static_cast<buffer_type&&>(tmp)));
            CHECK_EQUAL(tmp.GetSize(), 3U);
            CHECK_EQUAL(tmp[0], empty);
            CHECK_EQUAL(tmp[1], empty);
            CHECK_EQUAL(tmp[2], empty);
            CHECK_EQUAL(buffer.GetSize(), 5U);
            CHECK_EQUAL(buffer[0], cmp[0]);
            CHECK_EQUAL(buffer[1], cmp[1]);
            CHECK_EQUAL(buffer[2], cmp[2]);
            CHECK_EQUAL(buffer[3], cmp[3]);
            CHECK_EQUAL(buffer[4], cmp[4]);
        }
    }
}

TEST_MEMBER_FUNCTION(Buffer, Insert, size_t_type)
{
    TEST_OVERRIDE_ARGS("size_t, type");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    CHECK_FALSE(buffer.Insert(1U, 'A'));

    CHECK_TRUE(buffer.Insert(0U, 'A'));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');

    CHECK_TRUE(buffer.Insert(1U, 'C'));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_TRUE(Compare(buffer.Ptr(), "AC", 2));

    CHECK_TRUE(buffer.Insert(1U, 'B'));
    CHECK_EQUAL(buffer.GetSize(), 3U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABC", 3));

    CHECK_TRUE(buffer.Insert(0U, '.'));
    CHECK_EQUAL(buffer.GetSize(), 4U);
    CHECK_TRUE(Compare(buffer.Ptr(), ".ABC", 4));

    CHECK_TRUE(buffer.Insert(4U, 'D'));
    CHECK_EQUAL(buffer.GetSize(), 5U);
    CHECK_TRUE(Compare(buffer.Ptr(), ".ABCD", 5));

    CHECK_FALSE(buffer.Insert(6U, 'E'));
}

TEST_MEMBER_FUNCTION(Buffer, Insert, size_t_type_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, type, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    CHECK_FALSE(buffer.Insert(1U, 'A', 1U));

    CHECK_TRUE(buffer.Insert(0U, 'A', 1U));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');

    CHECK_TRUE(buffer.Insert(1U, 'C', 1U));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_TRUE(Compare(buffer.Ptr(), "AC", 2));

    CHECK_TRUE(buffer.Insert(1U, 'B', 1U));
    CHECK_EQUAL(buffer.GetSize(), 3U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABC", 3));

    CHECK_TRUE(buffer.Insert(0U, '.', 1U));
    CHECK_EQUAL(buffer.GetSize(), 4U);
    CHECK_TRUE(Compare(buffer.Ptr(), ".ABC", 4));

    CHECK_TRUE(buffer.Insert(4U, 'D', 1U));
    CHECK_EQUAL(buffer.GetSize(), 5U);
    CHECK_TRUE(Compare(buffer.Ptr(), ".ABCD", 5));

    CHECK_TRUE(buffer.Insert(5U, 'E', 2U));
    CHECK_EQUAL(buffer.GetSize(), 7U);
    CHECK_TRUE(Compare(buffer.Ptr(), ".ABCDEE", 7));

    CHECK_TRUE(buffer.Insert(0U, '0', 3U));
    CHECK_EQUAL(buffer.GetSize(), 10U);
    CHECK_TRUE(Compare(buffer.Ptr(), "000.ABCDEE", 10));

    CHECK_FALSE(buffer.Insert(12U, 'Z', 1U));
}

TEST_MEMBER_FUNCTION(Buffer, Insert, size_t_type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, type const*, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    CHECK_FALSE(buffer.Insert(1U, "A", 1));
    CHECK_FALSE(buffer.Insert(1U, "AB", 2));

    CHECK_TRUE(buffer.Insert(0U, "A", 1));
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');

    CHECK_TRUE(buffer.Insert(1U, "C", 1));
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'C');

    CHECK_TRUE(buffer.Insert(1U, "B", 1));
    CHECK_EQUAL(buffer.GetSize(), 3U);
    CHECK_EQUAL(buffer[0], 'A');
    CHECK_EQUAL(buffer[1], 'B');
    CHECK_EQUAL(buffer[2], 'C');

    CHECK_TRUE(buffer.Insert(0U, ".", 1));
    CHECK_EQUAL(buffer.GetSize(), 4U);
    CHECK_EQUAL(buffer[0], '.');
    CHECK_EQUAL(buffer[1], 'A');
    CHECK_EQUAL(buffer[2], 'B');
    CHECK_EQUAL(buffer[3], 'C');

    CHECK_TRUE(buffer.Insert(4U, "D", 1));
    CHECK_EQUAL(buffer.GetSize(), 5U);
    CHECK_EQUAL(buffer[0], '.');
    CHECK_EQUAL(buffer[1], 'A');
    CHECK_EQUAL(buffer[2], 'B');
    CHECK_EQUAL(buffer[3], 'C');
    CHECK_EQUAL(buffer[4], 'D');

    CHECK_FALSE(buffer.Insert(6U, "E", 1));

    CHECK_TRUE(buffer.Insert(0U, "123", 3));
    CHECK_TRUE(Compare(buffer.Ptr(), "123.ABCD", 8));

    CHECK_TRUE(buffer.Insert(8U, "EFG", 3));
    CHECK_TRUE(Compare(buffer.Ptr(), "123.ABCDEFG", 11));

    CHECK_TRUE(buffer.Insert(3U, "456", 3));
    CHECK_TRUE(Compare(buffer.Ptr(), "123456.ABCDEFG", 14));
}

TEST_MEMBER_FUNCTION(Buffer, Insert, size_t_Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("size_t, Buffer const&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    {
        buffer_type tmp(1, "A");
        CHECK_FALSE(buffer.Insert(1U, tmp));
    }

    {
        buffer_type tmp(2, "AB");
        CHECK_FALSE(buffer.Insert(1U, tmp));
    }

    {
        buffer_type tmp(1, "A");
        CHECK_TRUE(buffer.Insert(0U, tmp));
        CHECK_EQUAL(buffer.GetSize(), 1U);
        CHECK_EQUAL(buffer[0], 'A');
    }

    {
        buffer_type tmp(1, "C");
        CHECK_TRUE(buffer.Insert(1U, tmp));
        CHECK_EQUAL(buffer.GetSize(), 2U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'C');
    }

    {
        buffer_type tmp(1, "B");
        CHECK_TRUE(buffer.Insert(1U, tmp));
        CHECK_EQUAL(buffer.GetSize(), 3U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'B');
        CHECK_EQUAL(buffer[2], 'C');
    }

    {
        buffer_type tmp(1, ".");
        CHECK_TRUE(buffer.Insert(0U, tmp));
        CHECK_EQUAL(buffer.GetSize(), 4U);
        CHECK_EQUAL(buffer[0], '.');
        CHECK_EQUAL(buffer[1], 'A');
        CHECK_EQUAL(buffer[2], 'B');
        CHECK_EQUAL(buffer[3], 'C');
    }

    {
        buffer_type tmp(1, "D");
        CHECK_TRUE(buffer.Insert(4U, tmp));
        CHECK_EQUAL(buffer.GetSize(), 5U);
        CHECK_EQUAL(buffer[0], '.');
        CHECK_EQUAL(buffer[1], 'A');
        CHECK_EQUAL(buffer[2], 'B');
        CHECK_EQUAL(buffer[3], 'C');
        CHECK_EQUAL(buffer[4], 'D');
    }

    {
        buffer_type tmp(1, "E");
        CHECK_FALSE(buffer.Insert(6U, tmp));
    }

    {
        buffer_type tmp(3, "123");
        CHECK_TRUE(buffer.Insert(0U, tmp));
        CHECK_TRUE(Compare(buffer.Ptr(), "123.ABCD", 8));
    }

    {
        buffer_type tmp(3, "EFG");
        CHECK_TRUE(buffer.Insert(8U, tmp));
        CHECK_TRUE(Compare(buffer.Ptr(), "123.ABCDEFG", 11));
    }

    {
        buffer_type tmp(3, "456");
        CHECK_TRUE(buffer.Insert(3U, tmp));
        CHECK_TRUE(Compare(buffer.Ptr(), "123456.ABCDEFG", 14));
    }
}

TEST_MEMBER_FUNCTION(Buffer, Insert, size_t_Buffer_move_ref)
{
    TEST_OVERRIDE_ARGS("size_t, Buffer&&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    {
        buffer_type tmp(1, "A");
        CHECK_FALSE(buffer.Insert(1U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(tmp.GetSize(), 1U);
        CHECK_EQUAL(tmp[0], 'A');
    }

    {
        buffer_type tmp(2, "AB");
        CHECK_FALSE(buffer.Insert(1U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(tmp.GetSize(), 2U);
        CHECK_EQUAL(tmp[0], 'A');
        CHECK_EQUAL(tmp[1], 'B');
    }

    {
        buffer_type tmp(1, "A");
        CHECK_TRUE(buffer.Insert(0U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(buffer.GetSize(), 1U);
        CHECK_EQUAL(buffer[0], 'A');
    }

    {
        buffer_type tmp(1, "C");
        CHECK_TRUE(buffer.Insert(1U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(buffer.GetSize(), 2U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'C');
    }

    {
        buffer_type tmp(1, "B");
        CHECK_TRUE(buffer.Insert(1U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(buffer.GetSize(), 3U);
        CHECK_EQUAL(buffer[0], 'A');
        CHECK_EQUAL(buffer[1], 'B');
        CHECK_EQUAL(buffer[2], 'C');
    }

    {
        buffer_type tmp(1, ".");
        CHECK_TRUE(buffer.Insert(0U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(buffer.GetSize(), 4U);
        CHECK_EQUAL(buffer[0], '.');
        CHECK_EQUAL(buffer[1], 'A');
        CHECK_EQUAL(buffer[2], 'B');
        CHECK_EQUAL(buffer[3], 'C');
    }

    {
        buffer_type tmp(1, "D");
        CHECK_TRUE(buffer.Insert(4U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(buffer.GetSize(), 5U);
        CHECK_EQUAL(buffer[0], '.');
        CHECK_EQUAL(buffer[1], 'A');
        CHECK_EQUAL(buffer[2], 'B');
        CHECK_EQUAL(buffer[3], 'C');
        CHECK_EQUAL(buffer[4], 'D');
    }

    {
        buffer_type tmp(1, "E");
        CHECK_FALSE(buffer.Insert(6U, static_cast<buffer_type&&>(tmp)));
        CHECK_EQUAL(tmp.GetSize(), 1U);
        CHECK_EQUAL(tmp[0], 'E');
    }

    {
        buffer_type tmp(3, "123");
        CHECK_TRUE(buffer.Insert(0U, static_cast<buffer_type&&>(tmp)));
        CHECK_TRUE(Compare(buffer.Ptr(), "123.ABCD", 8));
    }

    {
        buffer_type tmp(3, "EFG");
        CHECK_TRUE(buffer.Insert(8U, static_cast<buffer_type&&>(tmp)));
        CHECK_TRUE(Compare(buffer.Ptr(), "123.ABCDEFG", 11));
    }

    {
        buffer_type tmp(3, "456");
        CHECK_TRUE(buffer.Insert(3U, static_cast<buffer_type&&>(tmp)));
        CHECK_TRUE(Compare(buffer.Ptr(), "123456.ABCDEFG", 14));
    }
}

TEST_MEMBER_FUNCTION(Buffer, Embed, size_t_type)
{
    TEST_OVERRIDE_ARGS("size_t, type");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    CHECK_FALSE(buffer.Embed(0U, 'A'));
    CHECK_FALSE(buffer.Embed(1U, 'A'));

    buffer.Copy("BCDEF", 6U);

    CHECK_TRUE(buffer.Embed(0U, 'A'));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));

    CHECK_TRUE(buffer.Embed(5U, 'f'));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEf", 6));

    CHECK_FALSE(buffer.Embed(6U, 'g'));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEf", 6));
}

TEST_MEMBER_FUNCTION(Buffer, Embed, size_t_type_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, type, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    CHECK_FALSE(buffer.Embed(0U, 'A', 1U));
    CHECK_FALSE(buffer.Embed(1U, 'A', 1U));

    buffer.Copy("BCDEF", 6U);

    CHECK_TRUE(buffer.Embed(0U, 'A', 1U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));

    CHECK_TRUE(buffer.Embed(5U, 'f', 1U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEf", 6));

    buffer.Copy("BCDEF", 6U);

    CHECK_TRUE(buffer.Embed(0U, 'A', 2U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "AABCDE", 6));

    CHECK_TRUE(buffer.Embed(4U, 'd', 2U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "AABCdd", 6));

    CHECK_FALSE(buffer.Embed(4U, 'f', 3U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "AABCdd", 6));
}

TEST_MEMBER_FUNCTION(Buffer, Embed, size_t_type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, type const*, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    CHECK_FALSE(buffer.Embed(0U, "A", 1U));
    CHECK_FALSE(buffer.Embed(1U, "A", 1U));

    buffer.Copy("BCDEF", 6U);

    CHECK_TRUE(buffer.Embed(0U, "A", 1U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));

    CHECK_TRUE(buffer.Embed(5U, "f", 1U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEf", 6));

    buffer.Copy("CDEFG", 6U);

    CHECK_TRUE(buffer.Embed(0U, "AB", 2U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));

    CHECK_TRUE(buffer.Embed(4U, "ef", 2U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDef", 6));

    CHECK_FALSE(buffer.Embed(4U, "fgh", 3U));
    CHECK_EQUAL(buffer.GetSize(), 6U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDef", 6));
}

TEST_MEMBER_FUNCTION(Buffer, Embed, size_t_Buffer_const_ref)
{
    TEST_OVERRIDE_ARGS("size_t, Buffer const&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    {
        buffer_type buffer2(1U, "A");
        CHECK_FALSE(buffer.Embed(0U, buffer2));
        CHECK_FALSE(buffer.Embed(1U, buffer2));
    }

    buffer.Copy("BCDEF", 6U);

    {
        buffer_type buffer2(1U, "A");
        CHECK_TRUE(buffer.Embed(0U, buffer2));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));
    }

    {
        buffer_type buffer2(1U, "f");
        CHECK_TRUE(buffer.Embed(5U, buffer2));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEf", 6));
    }

    buffer.Copy("CDEFG", 6U);

    {
        buffer_type buffer2(2U, "AB");
        CHECK_TRUE(buffer.Embed(0U, buffer2));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));
    }

    {
        buffer_type buffer2(2U, "ef");
        CHECK_TRUE(buffer.Embed(4U, buffer2));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDef", 6));
    }

    {
        buffer_type buffer2(3U, "fgh");
        CHECK_FALSE(buffer.Embed(4U, buffer2));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDef", 6));
    }
}

TEST_MEMBER_FUNCTION(Buffer, Embed, size_t_Buffer_move_ref)
{
    TEST_OVERRIDE_ARGS("size_t, Buffer&&");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer;

    {
        buffer_type buffer2(1U, "A");
        CHECK_FALSE(buffer.Embed(0U, static_cast<buffer_type&&>(buffer2)));
        CHECK_FALSE(buffer.Embed(1U, buffer2));
    }

    buffer.Copy("BCDEF", 6U);

    {
        buffer_type buffer2(1U, "A");
        CHECK_TRUE(buffer.Embed(0U, static_cast<buffer_type&&>(buffer2)));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));
    }

    {
        buffer_type buffer2(1U, "f");
        CHECK_TRUE(buffer.Embed(5U, static_cast<buffer_type&&>(buffer2)));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEf", 6));
    }

    buffer.Copy("CDEFG", 6U);

    {
        buffer_type buffer2(2U, "AB");
        CHECK_TRUE(buffer.Embed(0U, static_cast<buffer_type&&>(buffer2)));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6));
    }

    {
        buffer_type buffer2(2U, "ef");
        CHECK_TRUE(buffer.Embed(4U, static_cast<buffer_type&&>(buffer2)));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDef", 6));
    }

    {
        buffer_type buffer2(3U, "fgh");
        CHECK_FALSE(buffer.Embed(4U, static_cast<buffer_type&&>(buffer2)));
        CHECK_EQUAL(buffer.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer.Ptr(), "ABCDef", 6));
    }
}

TEST_MEMBER_FUNCTION(Buffer, Erase, size_t_size_t)
{
    TEST_OVERRIDE_ARGS("size_t, size_t");

    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer(7U, "1ABCDEF");
    buffer.Erase(8, 1);
    CHECK_EQUAL(buffer.GetSize(), 7U);

    buffer.Erase(7, 1);
    CHECK_EQUAL(buffer.GetSize(), 7U);

    buffer.Erase(0, 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDEF", 6U));

    buffer.Erase(5, 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCDE", 5U));

    buffer.Erase(0, 1);
    CHECK_TRUE(Compare(buffer.Ptr(), "BCDE", 4U));

    buffer.Erase(1, 2);
    CHECK_TRUE(Compare(buffer.Ptr(), "BE", 2U));
}

TEST_MEMBER_FUNCTION(Buffer, EraseFront, NA)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer(2U, "AB");
    buffer.EraseFront();
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'B');

    buffer.EraseFront();
    CHECK_EQUAL(buffer.GetSize(), 0U);

    buffer.EraseFront();
    CHECK_EQUAL(buffer.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, EraseFront, size_t)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer(5U, "ABCDE");
    buffer.EraseFront(1U);
    CHECK_EQUAL(buffer.GetSize(), 4U);
    CHECK_TRUE(Compare(buffer.Ptr(), "BCDE", 4U));

    buffer.EraseFront(2U);
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_TRUE(Compare(buffer.Ptr(), "DE", 2U));

    buffer.EraseFront(3U);
    CHECK_EQUAL(buffer.GetSize(), 0U);

    buffer.EraseFront(1U);
    CHECK_EQUAL(buffer.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, EraseBack, NA)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer(2U, "AB");
    buffer.EraseBack();
    CHECK_EQUAL(buffer.GetSize(), 1U);
    CHECK_EQUAL(buffer[0], 'A');

    buffer.EraseBack();
    CHECK_EQUAL(buffer.GetSize(), 0U);

    buffer.EraseBack();
    CHECK_EQUAL(buffer.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, EraseBack, size_t)
{
    typedef Buffer<char, std::size_t> buffer_type;

    buffer_type buffer(5U, "ABCDE");
    buffer.EraseBack(1U);
    CHECK_EQUAL(buffer.GetSize(), 4U);
    CHECK_TRUE(Compare(buffer.Ptr(), "ABCD", 4U));

    buffer.EraseBack(2U);
    CHECK_EQUAL(buffer.GetSize(), 2U);
    CHECK_TRUE(Compare(buffer.Ptr(), "AB", 2U));

    buffer.EraseBack(3U);
    CHECK_EQUAL(buffer.GetSize(), 0U);

    buffer.EraseBack(1U);
    CHECK_EQUAL(buffer.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Buffer, Extract, Buffer_ref_size_t_size_t)
{
    TEST_OVERRIDE_ARGS("Buffer&, size_t, size_t");

    typedef char type;
    typedef Buffer<type, std::size_t> buffer_type;

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.Extract(buffer2, 6, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "12345", 6U));
    }

    {
        // Test extracting first element.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.Extract(buffer2, 0, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 5U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "2345", 5U));
        CHECK_EQUAL(buffer2.GetSize(), 1U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "1", 1U));
    }

    {
        // Test extracting last element.
        buffer_type buffer1(5U, "12345");
        buffer_type buffer2;
        buffer1.Extract(buffer2, 4, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 4U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "1234", 4U));
        CHECK_EQUAL(buffer2.GetSize(), 1U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "5", 1U));
    }

    {
        // Test extracting middle elements.
        buffer_type buffer1(5U, "12345");
        buffer_type buffer2;
        buffer1.Extract(buffer2, 1, 3U);
        CHECK_EQUAL(buffer1.GetSize(), 2U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "15", 2U));
        CHECK_EQUAL(buffer2.GetSize(), 3U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "234", 3U));
    }
}

TEST_MEMBER_FUNCTION(Buffer, ExtractFront, Buffer_ref_size_t)
{
    TEST_OVERRIDE_ARGS("Buffer&, size_t");

    typedef char type;
    typedef Buffer<type, std::size_t> buffer_type;

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractFront(buffer2, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 5U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "2345", 5U));
        CHECK_EQUAL(buffer2.GetSize(), 1U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "1", 1U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractFront(buffer2, 2U);
        CHECK_EQUAL(buffer1.GetSize(), 4U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "345", 4U));
        CHECK_EQUAL(buffer2.GetSize(), 2U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12", 2U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractFront(buffer2, 6U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
        CHECK_EQUAL(buffer2.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12345", 6U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractFront(buffer2, 7U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
        CHECK_EQUAL(buffer2.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12345", 6U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractFront(buffer2, 6U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
        CHECK_EQUAL(buffer2.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12345", 6U));

        buffer1.ExtractFront(buffer2, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
    }
}

TEST_MEMBER_FUNCTION(Buffer, ExtractBack, Buffer_ref_size_t)
{
    TEST_OVERRIDE_ARGS("Buffer&, size_t");

    typedef char type;
    typedef Buffer<type, std::size_t> buffer_type;

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractBack(buffer2, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 5U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "12345", 5U));
        CHECK_EQUAL(buffer2.GetSize(), 1U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "", 1U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractBack(buffer2, 2U);
        CHECK_EQUAL(buffer1.GetSize(), 4U);
        CHECK_TRUE(Compare(buffer1.Ptr(), "1234", 4U));
        CHECK_EQUAL(buffer2.GetSize(), 2U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "5", 2U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractBack(buffer2, 6U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
        CHECK_EQUAL(buffer2.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12345", 6U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractBack(buffer2, 7U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
        CHECK_EQUAL(buffer2.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12345", 6U));
    }

    {
        // Test for failure when position is past end.
        buffer_type buffer1(6U, "12345");
        buffer_type buffer2;
        buffer1.ExtractBack(buffer2, 6U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
        CHECK_EQUAL(buffer2.GetSize(), 6U);
        CHECK_TRUE(Compare(buffer2.Ptr(), "12345", 6U));

        buffer1.ExtractBack(buffer2, 1U);
        CHECK_EQUAL(buffer1.GetSize(), 0U);
    }
}

TEST_MEMBER_FUNCTION(Buffer, Exists, type)
{
    typedef char type;
    typedef Buffer<type, std::size_t> buffer_type;
    typedef ocl::SequentialSearchFunctor<type, std::size_t> seq_functor_type;
    typedef ocl::BinarySearchFunctor<type, std::size_t>     bin_functor_type;

    buffer_type buffer(6U, "ABDEF");

    // Test with sequential search functor.
    CHECK_FALSE(buffer.Exists<seq_functor_type>('0'));
    CHECK_TRUE(buffer.Exists<seq_functor_type>('A'));
    CHECK_TRUE(buffer.Exists<seq_functor_type>('B'));
    CHECK_FALSE(buffer.Exists<seq_functor_type>('C'));
    CHECK_TRUE(buffer.Exists<seq_functor_type>('D'));
    CHECK_TRUE(buffer.Exists<seq_functor_type>('E'));
    CHECK_TRUE(buffer.Exists<seq_functor_type>('F'));
    CHECK_FALSE(buffer.Exists<seq_functor_type>('G'));

    // Test with binary search functor.
    CHECK_FALSE(buffer.Exists<bin_functor_type>('0'));
    CHECK_TRUE(buffer.Exists<bin_functor_type>('A'));
    CHECK_TRUE(buffer.Exists<bin_functor_type>('B'));
    CHECK_FALSE(buffer.Exists<bin_functor_type>('C'));
    CHECK_TRUE(buffer.Exists<bin_functor_type>('D'));
    CHECK_TRUE(buffer.Exists<bin_functor_type>('E'));
    CHECK_TRUE(buffer.Exists<bin_functor_type>('F'));
    CHECK_FALSE(buffer.Exists<bin_functor_type>('G'));
}

TEST_MEMBER_FUNCTION(Buffer, FindPosition, type_size_t_ref)
{
    TEST_OVERRIDE_ARGS("type, size_t&");

    typedef char type;
    typedef Buffer<type, std::size_t> buffer_type;
    typedef ocl::SequentialSearchFunctor<type, std::size_t> seq_functor_type;
    typedef ocl::BinarySearchFunctor<type, std::size_t>     bin_functor_type;

    buffer_type buffer(5U, "ABDEF");
    std::size_t nearest_pos;

    // Test with sequential search functor.
    nearest_pos = buffer.GetSize() + 1U;
    CHECK_FALSE(buffer.FindPosition<seq_functor_type>('0', nearest_pos));
    CHECK_EQUAL(nearest_pos, 0U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<seq_functor_type>('A', nearest_pos));
    CHECK_EQUAL(nearest_pos, 0U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<seq_functor_type>('B', nearest_pos));
    CHECK_EQUAL(nearest_pos, 1U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_FALSE(buffer.FindPosition<seq_functor_type>('C', nearest_pos));
    CHECK_EQUAL(nearest_pos, 2U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<seq_functor_type>('D', nearest_pos));
    CHECK_EQUAL(nearest_pos, 2U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<seq_functor_type>('E', nearest_pos));
    CHECK_EQUAL(nearest_pos, 3U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<seq_functor_type>('F', nearest_pos));
    CHECK_EQUAL(nearest_pos, 4U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_FALSE(buffer.FindPosition<seq_functor_type>('G', nearest_pos));
    CHECK_EQUAL(nearest_pos, 5U);

    // Test with binary search functor.
    nearest_pos = buffer.GetSize() + 1U;
    CHECK_FALSE(buffer.FindPosition<bin_functor_type>('0', nearest_pos));
    CHECK_EQUAL(nearest_pos, 0U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<bin_functor_type>('A', nearest_pos));
    CHECK_EQUAL(nearest_pos, 0U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<bin_functor_type>('B', nearest_pos));
    CHECK_EQUAL(nearest_pos, 1U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_FALSE(buffer.FindPosition<bin_functor_type>('C', nearest_pos));
    CHECK_EQUAL(nearest_pos, 2U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<bin_functor_type>('D', nearest_pos));
    CHECK_EQUAL(nearest_pos, 2U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<bin_functor_type>('E', nearest_pos));
    CHECK_EQUAL(nearest_pos, 3U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_TRUE(buffer.FindPosition<bin_functor_type>('F', nearest_pos));
    CHECK_EQUAL(nearest_pos, 4U);

    nearest_pos = buffer.GetSize() + 1U;
    CHECK_FALSE(buffer.FindPosition<bin_functor_type>('G', nearest_pos));
    CHECK_EQUAL(nearest_pos, 5U);
}

namespace
{
    template<typename Type, typename SizeType>
    struct ApplyFunctor
    {
        bool operator()(Type* ptr, SizeType size)
        {
            for (Type* end = ptr + size; ptr < end; ++ptr)
                *ptr = static_cast<Type>(0);
            return true;
        }
    };
}

TEST_MEMBER_FUNCTION(Buffer, Apply, functor)
{
    typedef char type;
    typedef Buffer<type, std::size_t> buffer_type;

    {
        buffer_type buffer;
        ApplyFunctor<type, std::size_t> functor;
        CHECK_FALSE(buffer.Apply(functor));
    }

    {
        buffer_type buffer(6U, "ABDEF");
        ApplyFunctor<type, std::size_t> functor;
        CHECK_TRUE(buffer.Apply(functor));
        CHECK_TRUE(Compare(buffer.Ptr(), "\0\0\0\0\0\0", 6U));
    }
}
