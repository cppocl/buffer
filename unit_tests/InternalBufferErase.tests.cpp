/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferErase.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>

using ocl::InternalBufferErase;

namespace
{
    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}


TEST_MEMBER_FUNCTION(InternalBufferErase, Erase, type_ptr_size_type_size_type_size_type)
{
    // type* buffer, size_type size, size_type position, size_type count
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, size_type");

    {
        typedef char type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 0, 1);
        CHECK_TRUE(Compare(buffer, "ello", 5));
    }

    {
        typedef char type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 4, 1);
        CHECK_TRUE(Compare(buffer, "Hell", 5));
    }

    {
        typedef char type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 5, 1);
        CHECK_TRUE(Compare(buffer, "Hello", 5));
    }

    {
        typedef char type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 0, 2);
        CHECK_TRUE(Compare(buffer, "llo", 4));
    }

    {
        typedef char type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 3, 2);
        CHECK_TRUE(Compare(buffer, "Hel", 4));
    }

    {
        typedef std::uint16_t type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = {1234U, 2345U, 3456U, 4567U, 5678U, 0U};
        type expected_buffer[] = {2345U, 3456U, 4567U, 5678U, 0U};
        std::size_t size = 6U;
        buffer_erase_type::Erase(buffer, size, 0, 1);
        CHECK_TRUE(Compare(buffer, expected_buffer, 5));
    }

    {
        typedef std::uint16_t type;
        typedef InternalBufferErase<type> buffer_erase_type;

        type buffer[] = {1234U, 2345U, 3456U, 4567U, 5678U, 0U};
        type expected_buffer[] = {1234U, 2345U, 3456U, 4567U, 0U};
        std::size_t size = 6U;
        buffer_erase_type::Erase(buffer, size, 4, 1);
        CHECK_TRUE(Compare(buffer, expected_buffer, 5));
    }
}
