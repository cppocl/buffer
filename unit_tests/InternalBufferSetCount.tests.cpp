/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferSetCount.hpp"
#include <cstdint>

using ocl::InternalBufferSetCount;

typedef unsigned char type;
typedef std::uint64_t size_type;
typedef InternalBufferSetCount<type, size_type> buffer_set_count_type;

namespace
{
    template<typename Type, typename SizeType>
    bool Compare(Type const* ptr1, Type const* ptr2, SizeType count)
    {
        for (Type const* ptr1_end = ptr1 + count; ptr1 < ptr1_end; ++ptr1, ++ptr2)
            if (*ptr1 != *ptr2)
                return false;
        return true;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSetCount, GetCount, type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("unsigned char const*, size_type");

    {
        unsigned char buffer[] = { 5u };
        CHECK_EQUAL(buffer_set_count_type::GetCount(buffer, 1), 5);
    }

    {
        unsigned char buffer[] = { 0u, 0u };
        std::uint16_t* ptr = reinterpret_cast<std::uint16_t*>(buffer);
        *ptr = 300u;
        CHECK_EQUAL(buffer_set_count_type::GetCount(buffer, sizeof(*ptr)), 300u);
    }

    {
        unsigned char buffer[] = { 0u, 0u, 0u, 0u };
        std::uint32_t* ptr = reinterpret_cast<std::uint32_t*>(buffer);
        *ptr = 0xffffffu;
        CHECK_EQUAL(buffer_set_count_type::GetCount(buffer, sizeof(*ptr)), 0xffffffu);
    }

    {
        unsigned char buffer[] = { 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u };
        std::uint64_t* ptr = reinterpret_cast<std::uint64_t*>(buffer);
        *ptr = 0xffffffffffffull;
        CHECK_EQUAL(buffer_set_count_type::GetCount(buffer, sizeof(*ptr)), 0xffffffffffffull);
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSetCount, SetCount, type_ptr_size_type_size_type)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_type, size_type");

    {
        unsigned char compare[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        unsigned char buffer[10]  = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        buffer_set_count_type::SetCount(buffer, 1, 0U);
        CHECK_EQUAL(buffer[0], 0);
        CHECK_TRUE(Compare(buffer + 1, compare + 1, 9));
    }

    {
        unsigned char compare[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        unsigned char buffer[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        buffer_set_count_type::SetCount(buffer, 2, 0U);
        CHECK_EQUAL(buffer[0], 0);
        CHECK_EQUAL(buffer[1], 0);
        CHECK_TRUE(Compare(buffer + 2, compare + 2, 8));
    }

    {
        unsigned char compare[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        unsigned char buffer[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        buffer_set_count_type::SetCount(buffer, 4, 0U);
        CHECK_EQUAL(buffer[0], 0);
        CHECK_EQUAL(buffer[1], 0);
        CHECK_EQUAL(buffer[2], 0);
        CHECK_EQUAL(buffer[3], 0);
        CHECK_TRUE(Compare(buffer + 4, compare + 4, 6));
    }

    {
        unsigned char compare[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        unsigned char buffer[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        buffer_set_count_type::SetCount(buffer, 8, 0U);
        CHECK_EQUAL(buffer[0], 0);
        CHECK_EQUAL(buffer[1], 0);
        CHECK_EQUAL(buffer[2], 0);
        CHECK_EQUAL(buffer[3], 0);
        CHECK_EQUAL(buffer[4], 0);
        CHECK_EQUAL(buffer[5], 0);
        CHECK_EQUAL(buffer[6], 0);
        CHECK_EQUAL(buffer[7], 0);
        CHECK_TRUE(Compare(buffer + 8, compare + 8, 2));
    }
}
