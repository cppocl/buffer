/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferBinaryDifferenceArchive.hpp"
#include <cstdint>

using ocl::InternalBufferBinaryDifferenceArchive;
typedef unsigned char type;
typedef std::uint64_t size_type;
typedef InternalBufferBinaryDifferenceArchive<type, size_type> binary_difference_type;

TEST_MEMBER_FUNCTION(InternalBufferBinaryDifferenceArchive,
                     CreateDifference,
                     size_type_type_const_ptr_size_type_type_const_ptr_size_type_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("size_type, type const*, size_type, type const*, size_type, type*, size_type");

    //
    // Test GetDifferenceCount, GetBufferSizes and CreateBuffers at the same time as CreateDifference.
    //

    bool success;
    size_type const buffer1_output_size = 100;
    size_type const buffer2_output_size = 100;
    unsigned char buffer1_output[buffer1_output_size];
    unsigned char buffer2_output[buffer1_output_size];

    // Test simplest buffers matching.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("A");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("A");
        size_type const buffer1_size = 1;
        size_type const buffer2_size = 1;
        size_type const expected_size = 3;
        unsigned char const expected_buffer[] = "\x01""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simplest buffers differing where second buffer is empty.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("A");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("");
        size_type const buffer1_size = 1;
        size_type const buffer2_size = 0;
        size_type const expected_size = 5;
        unsigned char const expected_buffer[] = "\x01""\x00""\x01""A""\x00";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simplest buffers differing where first buffer is empty.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("A");
        size_type const buffer1_size = 0;
        size_type const buffer2_size = 1;
        size_type const expected_size = 5;
        unsigned char const expected_buffer[] = "\x01""\x00""\x00""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test two characters matching.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("AB");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("AB");
        size_type const buffer1_size = 2;
        size_type const buffer2_size = 2;
        size_type const expected_size = 4;
        unsigned char const expected_buffer[] = "\x01""\x02""AB";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simplest buffers differing.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("A");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("B");
        size_type const buffer1_size = 1;
        size_type const buffer2_size = 1;
        size_type const expected_size = 6;
        unsigned char const expected_buffer[] = "\x01""\x00""\x01""A""\x01""B";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simplest buffers same and differing.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("AB");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("AC");
        size_type const buffer1_size = 2;
        size_type const buffer2_size = 2;
        size_type const expected_size = 7;
        unsigned char const expected_buffer[] = "\x01""\x01""A""\x01""B""\x01""C";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simplest buffers differing and same.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("BA");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("CA");
        size_type const buffer1_size = 2;
        size_type const buffer2_size = 2;
        size_type const expected_size = 8;
        unsigned char const expected_buffer[] = "\x01""\x00""\x01""B""\x01""C""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simple buffers differing (with different lengths) and same.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("BDA");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("CA");
        size_type const buffer1_size = 3;
        size_type const buffer2_size = 2;
        size_type const expected_size = 9;
        unsigned char const expected_buffer[] = "\x01""\x00""\x02""BD""\x01""C""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simple buffers differing (with different lengths) and same.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("BA");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("CDA");
        size_type const buffer1_size = 2;
        size_type const buffer2_size = 3;
        size_type const expected_size = 9;
        unsigned char const expected_buffer[] = "\x01""\x00""\x01""B""\x02""CD""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simple buffers differing (with different lengths and one with zero difference) and same.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("BA");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("A");
        size_type const buffer1_size = 2;
        size_type const buffer2_size = 1;
        size_type const expected_size = 7;
        unsigned char const expected_buffer[] = "\x01""\x00""\x01""B""\x00""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }

    // Test simple buffers differing (with different lengths and one with zero difference) and same.
    {
        unsigned const char* buffer1 = reinterpret_cast<unsigned const char*>("A");
        unsigned const char* buffer2 = reinterpret_cast<unsigned const char*>("BA");
        size_type const buffer1_size = 1;
        size_type const buffer2_size = 2;
        size_type const expected_size = 7;
        unsigned char const expected_buffer[] = "\x01""\x00""\x00""\x01""B""\x01""A";
        unsigned char output_buffer[100];
        ::memset(output_buffer, 0, sizeof(output_buffer));

        size_type output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                          buffer2, buffer2_size,
                                                                          output_buffer,
                                                                          sizeof(output_buffer));
        CHECK_TRUE(output_bytes == expected_size);
        size_type const count = binary_difference_type::GetDifferenceCount(buffer1, buffer1_size, buffer2, buffer2_size);
        CHECK_EQUAL(count, expected_size);

        const size_t expected_buffer_size = sizeof(expected_buffer) - 1;
        CHECK_EQUAL(::memcmp(output_buffer, expected_buffer, expected_buffer_size), 0);

        // Verify that CreateDifference succeeds when the
        // output buffer is the exact size needed to store the archive.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size);
        CHECK_EQUAL(output_bytes, expected_size);

        // Verify that CreateDifference fails when the output buffer is not big enough.
        output_bytes = binary_difference_type::CreateDifference(buffer1, buffer1_size,
                                                                buffer2, buffer2_size,
                                                                output_buffer,
                                                                expected_size - 1);
        CHECK_EQUAL(output_bytes, 0);

        // Compare extracted sizes match the original sizes for the two buffers.
        size_type size1 = 0;
        size_type size2 = 0;
        binary_difference_type::GetBufferSizes(expected_buffer, expected_size, size1, size2);
        CHECK_EQUAL(size1, buffer1_size);
        CHECK_EQUAL(size2, buffer2_size);

        // Compare re-construction of two original buffers from the archive buffer containing differences.
        size_type buffer1_size_created = 0;
        size_type buffer2_size_created = 0;
        success = binary_difference_type::CreateBuffers(expected_buffer, expected_size,
                                                        buffer1_output, buffer1_output_size,
                                                        buffer2_output, buffer2_output_size,
                                                        buffer1_size_created, buffer2_size_created);
        CHECK_EQUAL(buffer1_size, buffer1_size_created);
        CHECK_EQUAL(buffer2_size, buffer2_size_created);
        CHECK_EQUAL(::memcmp(buffer1_output, buffer1, buffer1_size), 0);
        CHECK_EQUAL(::memcmp(buffer2_output, buffer2, buffer2_size), 0);
    }
}
