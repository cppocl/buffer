/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferCopySet.hpp"
#include <cstdint>

using ocl::InternalBufferCopySet;

namespace
{
    struct Data
    {
        int  i;
        char c;
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferCopy, Copy, char_ptr_size_t_char)
{
    TEST_OVERRIDE_ARGS("char*, size_t, char");

    typedef char type;
    typedef size_t size_type;

    size_t const count = 4;
    type buffer[count];

    InternalBufferCopySet<type, size_type>::CopySet(buffer, count, 'A');
    CHECK_TRUE(Compare(buffer, "AAAA", count));
}

TEST_MEMBER_FUNCTION(InternalBufferCopy, Copy, uint16_t_ptr_size_t_char)
{
    TEST_OVERRIDE_ARGS("uint16_t*, size_t, char");

    typedef std::uint16_t type;
    typedef size_t size_type;

    size_t const count = 4;
    type buffer[count];
    type cmp[count] = {1234, 1234, 1234, 1234};

    InternalBufferCopySet<type, size_type>::CopySet(buffer, count, 1234);
    CHECK_TRUE(Compare(buffer, cmp, count));
}

TEST_MEMBER_FUNCTION(InternalBufferCopy, Copy, Data_ptr_size_t_char)
{
    TEST_OVERRIDE_ARGS("Data*, size_t, char");

    typedef Data type;
    typedef size_t size_type;

    Data const value = {1234, 'A'};
    size_t const count = 4;
    type buffer[count];
    type cmp[count] = {value, value, value, value};

    InternalBufferCopySet<type, size_type>::CopySet(buffer, count, value);
    CHECK_TRUE(Compare(buffer, cmp, count));
}
