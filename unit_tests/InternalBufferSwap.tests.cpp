/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferSwap.hpp"

using ocl::InternalBufferSwap;

TEST_MEMBER_FUNCTION(InternalBufferSwap, Swap, type_ref_type_ref)
{
    TEST_OVERRIDE_ARGS("type&, type&");

    {
        typedef char type;
        type value1 = 'A';
        type value2 = 'B';
        InternalBufferSwap<type>::Swap(value1, value2);
        CHECK_EQUAL(value1, 'B');
        CHECK_EQUAL(value2, 'A');
    }

    {
        typedef char type;
        type value1 = 'A';
        type value2 = 'B';
        type* value1_ptr = &value1;
        type* value2_ptr = &value2;
        InternalBufferSwap<type*>::Swap(value1_ptr, value2_ptr);
        CHECK_EQUAL(value1, 'A');
        CHECK_EQUAL(value2, 'B');
        CHECK_EQUAL(value1_ptr, &value2);
        CHECK_EQUAL(value2_ptr, &value1);
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSwap, Swap, type_ref_size_t_type_ref_size_t)
{
    TEST_OVERRIDE_ARGS("type&, size_t&, type&, size_t&");

    {
        typedef char type;
        type value1 = 'A';
        type value2 = 'B';
        std::size_t value1_size = 1U;
        std::size_t value2_size = 1U;
        InternalBufferSwap<type>::Swap(value1, value1_size, value2, value2_size);
        CHECK_EQUAL(value1, 'B');
        CHECK_EQUAL(value2, 'A');
        CHECK_EQUAL(value1_size, 1U);
        CHECK_EQUAL(value2_size, 1U);
    }

    {
        typedef char type;
        type value1[] = "AB";
        type value2[] = "CDE";
        type* value1_ptr = value1;
        type* value2_ptr = value2;
        std::size_t value1_size = 2U;
        std::size_t value2_size = 3U;
        InternalBufferSwap<type*>::Swap(value1_ptr, value1_size, value2_ptr, value2_size);
        CHECK_EQUAL(value1[0], 'A');
        CHECK_EQUAL(value1[1], 'B');
        CHECK_EQUAL(value2[0], 'C');
        CHECK_EQUAL(value2[1], 'D');
        CHECK_EQUAL(value2[2], 'E');
        CHECK_EQUAL(value1_ptr, value2);
        CHECK_EQUAL(value2_ptr, value1);
        CHECK_EQUAL(value1_size, 3U);
        CHECK_EQUAL(value2_size, 2U);
    }
}
