/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferInsertCopy.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferInsertCopy;

namespace
{
    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferInsertCopy, InsertCopy, type_ptr_size_type_type_const_ptr_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type");

    {
        char src[5] = "ello";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 0, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "Hllo";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 1, src, "e", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "Hell";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 4, src, "o", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = {'H','e','l','l','o'};
        char str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 5, src, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferInsertCopy, InsertCopy, type_ptr_size_type_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type const*, size_type");

    {
        char src[5] = "ello";
        char str[6] = {0};
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 6, 0, src, 1);
        CHECK_EQUAL(ptr, &str[0]);
        CHECK_TRUE(Compare(str+1, "ello", 4U));
    }

    {
        char src[5] = "Hllo";
        char str[6] = {0};
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 1, src, 1);
        CHECK_EQUAL(ptr, &str[1]);
        CHECK_TRUE(Compare(str, "H", 1U));
        CHECK_TRUE(Compare(str+2, "llo", 3U));
    }

    {
        char src[5] = "Hell";
        char str[6] = {0};
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 4, src, 1);
        CHECK_EQUAL(ptr, &str[4]);
        CHECK_TRUE(Compare(str, "Hell", 1U));
    }

    {
        char src[5] = "Hell";
        char str[6] = {0};
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 6, 3, src, 2);
        CHECK_EQUAL(ptr, &str[3]);
        CHECK_TRUE(Compare(str, "Hel", 3U));
        CHECK_TRUE(Compare(str+5, "l", 1U));
    }

    {
        char src[5] = "Hell";
        char str[6] = {0};
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 4, src, 2);
        CHECK_EQUAL(ptr, &str[4]);
    }

    {
        char src[6] = "Hello";
        char str[6] = {0};
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 5, 5, src, 1);
        CHECK_EQUAL(ptr, &str[5]);
        CHECK_TRUE(Compare(str, "Hell", 4U));
    }

    {
        char src[6] = "Hello";
        char str[6] = {0};

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 6, 5, src, 1);
        CHECK_EQUAL(ptr, &str[5]);
        CHECK_TRUE(Compare(str, "Hello", 5U));
    }

    {
        char src[6] = "ABCDE";
        char str[6] = {0};

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 2, 0, src, 1);
        CHECK_EQUAL(ptr, &str[0]);
        CHECK_TRUE(Compare(str + 1, "A", 1U));
    }

    {
        char src[6] = "ABCDE";
        char str[6] = {0};

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 3, 1, src, 1);
        CHECK_EQUAL(ptr, &str[1]);
        CHECK_TRUE(Compare(str, "A", 1U));
        CHECK_TRUE(Compare(str + 2, "B", 1U));
    }

    {
        char src[6] = "ABCDE";
        char str[6] = {0};

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsertCopy<char, size_t>::InsertCopy(str, 4, 0, src, 1);
        CHECK_EQUAL(ptr, &str[0]);
        CHECK_TRUE(Compare(str + 1, "A", 1U));
    }
}
