/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferIsOverlapping.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferIsOverlapping;

namespace
{
    struct Data
    {
        int  i;
        char c;
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferIsOverlapping, IsOverlapping, type_const_ptr_type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, type const*, size_t");

    typedef char type;
    typedef InternalBufferIsOverlapping<type, std::size_t> is_overlapping_type;
    type buffer[6];

    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[0], &buffer[2], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[0], &buffer[1], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[2], &buffer[0], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[2], &buffer[1], 1U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[0], &buffer[1], 2U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[1], &buffer[0], 2U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[0], &buffer[0], 1U));
}

TEST_MEMBER_FUNCTION(InternalBufferIsOverlapping, IsOverlapping, type_const_ptr_size_t_type_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, size_t, type const*, size_t");

    typedef char type;
    typedef InternalBufferIsOverlapping<type, std::size_t> is_overlapping_type;
    type buffer[6];

    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[0], 1U, &buffer[2], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[0], 1U, &buffer[2], 2U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[0], 1U, &buffer[1], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[2], 1U, &buffer[0], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[2], 1U, &buffer[1], 1U));
    CHECK_FALSE(is_overlapping_type::IsOverlapping(&buffer[2], 2U, &buffer[1], 1U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[0], 2U, &buffer[1], 2U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[1], 2U, &buffer[0], 2U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[0], 1U, &buffer[0], 1U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[0], 4U, &buffer[1], 2U));
    CHECK_TRUE(is_overlapping_type::IsOverlapping(&buffer[1], 2U, &buffer[0], 4U));
}
