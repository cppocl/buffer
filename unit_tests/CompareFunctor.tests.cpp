/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../CompareFunctor.hpp"

using ocl::CompareFunctor;

TEST_MEMBER_FUNCTION(Buffer, operator_brackets, type_type)
{
    {
        typedef bool type;

        type a = false;
        type b = false;
        CompareFunctor<type> compare_functor;
        CHECK_EQUAL(compare_functor(a, b), 0);

        b = true;
        CHECK_EQUAL(compare_functor(a, b), -1);

        a = true;
        b = false;
        CHECK_EQUAL(compare_functor(a, b), 1);
    }

    {
        typedef char type;

        type a = 'a';
        type b = 'a';
        CompareFunctor<type> compare_functor;
        CHECK_EQUAL(compare_functor(a, b), 0);

        b = 'b';
        CHECK_EQUAL(compare_functor(a, b), -1);

        a = 'b';
        b = 'a';
        CHECK_EQUAL(compare_functor(a, b), 1);
    }

    {
        typedef int type;

        type a = 1;
        type b = 1;
        CompareFunctor<type> compare_functor;
        CHECK_EQUAL(compare_functor(a, b), 0);

        b = 2;
        CHECK_EQUAL(compare_functor(a, b), -1);

        a = 2;
        b = 1;
        CHECK_EQUAL(compare_functor(a, b), 1);
    }

    {
        typedef double type;

        type a = 1.1;
        type b = 1.1;
        CompareFunctor<type> compare_functor;
        CHECK_EQUAL(compare_functor(a, b), 0);

        b = 1.2;
        CHECK_EQUAL(compare_functor(a, b), -1);

        a = 1.2;
        b = 1.1;
        CHECK_EQUAL(compare_functor(a, b), 1);
    }

    {
        typedef int type;

        type values[2] = {0, 0};
        CompareFunctor<type*> compare_functor;
        CHECK_EQUAL(compare_functor(values, values), 0);
        CHECK_EQUAL(compare_functor(values, values + 1), -1);
        CHECK_EQUAL(compare_functor(values + 1, values), 1);
    }
}

TEST_MEMBER_FUNCTION(Buffer, operator_brackets, type_const_ptr_type_const_ptr_size_t)
{
    {
        typedef bool type;

        {
            type a[2] = {false, false};
            type b[2] = {false, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 0);
        }

        {
            type a[2] = {true, true};
            type b[2] = {true, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 0);
        }

        {
            type a[2] = {false, true};
            type b[2] = {false, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 0);
        }

        {
            type a[2] = {true, false};
            type b[2] = {true, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 0);
        }

        {
            type a[2] = {false, false};
            type b[2] = {true, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), -1);
        }

        {
            type a[2] = {false, false};
            type b[2] = {false, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), -1);
        }

        {
            type a[2] = {false, false};
            type b[2] = {true, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), -1);
        }

        {
            type a[2] = {false, true};
            type b[2] = {true, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), -1);
        }

        {
            type a[2] = {false, true};
            type b[2] = {true, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), -1);
        }

        {
            type a[2] = {true, false};
            type b[2] = {true, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), -1);
        }

        {
            type a[2] = {true, false};
            type b[2] = {false, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 1);
        }

        {
            type a[2] = {true, false};
            type b[2] = {false, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 1);
        }

        {
            type a[2] = {false, true};
            type b[2] = {false, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 1);
        }

        {
            type a[2] = {true, true};
            type b[2] = {true, false};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 1);
        }

        {
            type a[2] = {true, true};
            type b[2] = {false, true};
            CompareFunctor<type> compare_functor;
            CHECK_EQUAL(compare_functor(a, b, 2U), 1);
        }
    }

    {
        typedef char type;

        CompareFunctor<type> compare_functor;
        CHECK_EQUAL(compare_functor("", "", 1U), 0);
        CHECK_EQUAL(compare_functor("a", "a", 2U), 0);
        CHECK_EQUAL(compare_functor("ab", "ab", 3U), 0);
        CHECK_EQUAL(compare_functor("aa", "ab", 3U), -1);
        CHECK_EQUAL(compare_functor("aa", "ba", 3U), -1);
        CHECK_EQUAL(compare_functor("aa", "bb", 3U), -1);
        CHECK_EQUAL(compare_functor("ab", "aa", 3U), 1);
        CHECK_EQUAL(compare_functor("ba", "aa", 3U), 1);
        CHECK_EQUAL(compare_functor("bb", "aa", 3U), 1);
    }

    {
        // Just try something larger than a char.
        typedef wchar_t type;

        CompareFunctor<type> compare_functor;
        CHECK_EQUAL(compare_functor(L"", L"", 1U), 0);
        CHECK_EQUAL(compare_functor(L"a", L"a", 2U), 0);
        CHECK_EQUAL(compare_functor(L"ab", L"ab", 3U), 0);
        CHECK_EQUAL(compare_functor(L"aa", L"ab", 3U), -1);
        CHECK_EQUAL(compare_functor(L"aa", L"ba", 3U), -1);
        CHECK_EQUAL(compare_functor(L"aa", L"bb", 3U), -1);
        CHECK_EQUAL(compare_functor(L"ab", L"aa", 3U), 1);
        CHECK_EQUAL(compare_functor(L"ba", L"aa", 3U), 1);
        CHECK_EQUAL(compare_functor(L"bb", L"aa", 3U), 1);
    }
}
