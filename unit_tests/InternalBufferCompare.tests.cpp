/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferCompare.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferCompare;

namespace
{
    struct Data
    {
        int  i;
        char c;
    };

    struct CmpData
    {
        int  i;
        char c;

        bool operator <(CmpData const& data) const
        {
            return (i < data.i) && (c < data.c);
        }
    };
}

TEST_MEMBER_FUNCTION(InternalBufferCompare, Compare, type_ptr_type_const_ptr_size_t)
{
    size_t const size = 4;
    int cmp;

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        type first[size] = {false, false, false, false};
        type second[size] = {false, false, false, false};
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);
    }

    {
        typedef bool type;
        type first[size] = {false, false, false, false};
        type second[size] = {true, true, true, true};
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);
    }

    {
        typedef bool type;
        type first[size] = {true, true, true, true};
        type second[size] = {false, false, false, false};
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef char type;
        type first[size];
        type second[size];
        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";

        {
            type first[size] = {NULL, NULL, NULL, NULL};
            type second[size] = {NULL, NULL, NULL, NULL};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 0);
        }

        {
            type first[size] = {str1, str2, str3, str4};
            type second[size] = {str1, str2, str3, str4};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 0);
        }

        {
            type first[size] = {NULL, NULL, NULL, NULL};
            type second[size] = {str1, str2, str3, str4};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {str1, str2, str3, str4};
            type second[size] = {NULL, NULL, NULL, NULL};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }
    }

    {
        typedef signed char type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef unsigned char type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef signed short type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef unsigned short type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef signed int type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef unsigned int type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef signed long int type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef unsigned long int type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef signed long long int type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef unsigned long long int type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }

    {
        typedef float type;

        {
            type first[size] = {0.0f, 0.0f, 0.0f, 0.0f};
            type second[size] = {0.0f, 0.0f, 0.0f, 0.0f};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 0);
        }

        {
            type first[size] = {0.0f, 0.0f, 0.0f, 0.0f};
            type second[size] = {0.1f, 0.1f, 0.1f, 0.1f};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {-0.1f, -0.1f, -0.1f, -0.1f};
            type second[size] = {0.0f, 0.0f, 0.0f, 0.0f};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {0.1f, 0.1f, 0.1f, 0.1f};
            type second[size] = {0.0f, 0.0f, 0.0f, 0.0f};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }

        {
            type first[size] = {0.0f, 0.0f, 0.0f, 0.0f};
            type second[size] = {-0.1f, -0.1f, -0.1f, -0.1f};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }
    }

    {
        typedef double type;

        {
            type first[size] = {0.0, 0.0, 0.0, 0.0};
            type second[size] = {0.0, 0.0, 0.0, 0.0};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 0);
        }

        {
            type first[size] = {0.0, 0.0, 0.0, 0.0};
            type second[size] = {0.1, 0.1, 0.1, 0.1};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {-0.1, -0.1, -0.1, -0.1};
            type second[size] = {0.0, 0.0, 0.0, 0.0};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {0.1, 0.1, 0.1, 0.1};
            type second[size] = {0.0, 0.0, 0.0, 0.0};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }

        {
            type first[size] = {0.0, 0.0, 0.0, 0.0};
            type second[size] = {-0.1, -0.1, -0.1, -0.1};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }
    }

    {
        typedef long double type;

        {
            type first[size] = {0.0, 0.0, 0.0, 0.0};
            type second[size] = {0.0, 0.0, 0.0, 0.0};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 0);
        }

        {
            type first[size] = {0.0, 0.0, 0.0, 0.0};
            type second[size] = {0.1, 0.1, 0.1, 0.1};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {-0.1, -0.1, -0.1, -0.1};
            type second[size] = {0.0, 0.0, 0.0, 0.0};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, -1);
        }

        {
            type first[size] = {0.1, 0.1, 0.1, 0.1};
            type second[size] = {0.0, 0.0, 0.0, 0.0};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }

        {
            type first[size] = {0.0, 0.0, 0.0, 0.0};
            type second[size] = {-0.1, -0.1, -0.1, -0.1};
            cmp = InternalBufferCompare<type>::Compare(first, second, size);
            CHECK_EQUAL(cmp, 1);
        }
    }

    {
        typedef CmpData type;
        type first[size];
        type second[size];

        ::memset(first, 0, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 0);

        ::memset(first, 0, sizeof(first));
        ::memset(second, 1, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, -1);

        ::memset(first, 1, sizeof(first));
        ::memset(second, 0, sizeof(second));
        cmp = InternalBufferCompare<type>::Compare(first, second, size);
        CHECK_EQUAL(cmp, 1);
    }
}
