/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferFind.hpp"
#include <cstddef>
#include <cstring>

TEST_MEMBER_FUNCTION(InternalBufferFind, Find, char_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("char, char const*, size_t");

    using std::size_t;
    typedef ocl::InternalBufferFind<char, size_t> buffer_find_type;

    {
        char const* found = buffer_find_type::Find('A', "B", 1);
        CHECK_NULL(found);
    }

    {
        char const str[] = "A";
        char const* found = buffer_find_type::Find('A', str, sizeof(str)-1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str);
    }

    {
        char const str[] = "AA";
        char const* found = buffer_find_type::Find('A', str, sizeof(str) - 1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str);
    }

    {
        char const str[] = "AB";
        char const* found = buffer_find_type::Find('B', str, sizeof(str) - 1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str + 1);
    }
}

TEST_MEMBER_FUNCTION(InternalBufferFind, FindMatch, char_const_ptr_size_t_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("char const*, size_t, char const*, size_t");

    using std::size_t;
    typedef ocl::InternalBufferFind<char, size_t> buffer_find_type;

    {
        char const* found = buffer_find_type::FindMatch("A", 1, "B", 1);
        CHECK_NULL(found);
    }

    {
        char const str[] = "AB";
        char const* found = buffer_find_type::FindMatch("A", 1, str, sizeof(str) - 1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str);
    }

    {
        char const str[] = "AB";
        char const* found = buffer_find_type::FindMatch("B", 1, str, sizeof(str) - 1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str + 1);
    }

    {
        char const str[] = "ABC";
        char const* found = buffer_find_type::FindMatch("CB", 2, str, sizeof(str) - 1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str + 2);
    }

    {
        char const str[] = "ABC";
        char const* found = buffer_find_type::FindMatch("DC", 2, str, sizeof(str) - 1);
        CHECK_NOT_NULL(found);
        CHECK_EQUAL(found, str + 2);
    }

    {
        char const str[] = "ABC";
        char const* found = buffer_find_type::FindMatch("DC", 1, str, sizeof(str) - 1);
        CHECK_NULL(found);
    }
}
