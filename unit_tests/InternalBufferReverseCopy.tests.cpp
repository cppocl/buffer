/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferReverseCopy.hpp"

using ocl::InternalBufferReverseCopy;

namespace
{
    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferReverseCopy, ReverseCopy, type_ptr_type_const_ptr_size_t)
{
    size_t const copy_size = 4; // Size of buffer to copy.
    size_t const offset = 2;   // Offset into buffer for copying.
    size_t const size = copy_size + offset;
    // Note that the total buffer is size + offset.

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef char type;
        type buffer[size] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size]    = {'A', 'B', 'A', 'B', 'C', 'D'};
        InternalBufferReverseCopy<type>::ReverseCopy(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }

    {
        typedef signed int type;
        type buffer[size] = {1, 2, 3, 4, 5, 6};
        type cmp[size]    = {1, 2, 1, 2, 3, 4};
        InternalBufferReverseCopy<type>::ReverseCopy(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }
}
