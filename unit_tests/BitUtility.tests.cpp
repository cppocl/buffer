/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../BitUtility.hpp"
#include "../internal/InternalUnsignedType.hpp"

#include <cstring>
#include <cstddef>
#include <cstdint>
#include <limits>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

namespace
{
    // Use a simple bit counting algorithm to test bits for any type.
    template<typename T>
    static std::size_t CountSetBits(T value) noexcept
    {
        std::size_t count = 0;
        while (value > 0)
        {
            if ((value & 1) != 0)
                ++count;
            value >>= 1;
        }
        return count;
    }

    // Test the full range of bits being set against the simpler bit counting algorithm.
    template<typename T>
    static bool TestRangeForCountSetBits(T step = 1)
    {
        typedef typename ocl::InternalUnsignedType<T>::type unsigned_type;
        typedef ocl::BitUtility<T> bit_utility_type;

        T min_value = std::numeric_limits<T>::min();
        T max_value = std::numeric_limits<T>::max();
        for (T value = min_value; value < max_value; value += step)
        {
            if (bit_utility_type::CountSetBits(value) != CountSetBits(static_cast<unsigned_type>(value)))
                return false;
            if (max_value - value < step)
                break;
        }
        return bit_utility_type::CountSetBits(max_value) == CountSetBits(static_cast<unsigned_type>(max_value));
    }
}

using ocl::BitUtility;

TEST(BitUtility_BitsPerChar)
{
    CHECK_EQUAL(CHAR_BIT, 8);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, int8_t)
{
    bool success = TestRangeForCountSetBits<std::int8_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, uint8_t)
{
    bool success = TestRangeForCountSetBits<std::uint8_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, int16_t)
{
    bool success = TestRangeForCountSetBits<std::int16_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, uint16_t)
{
    bool success = TestRangeForCountSetBits<std::uint16_t>();
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, int32_t)
{
    bool success = TestRangeForCountSetBits<std::int32_t>(1000);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, uint32_t)
{
    bool success = TestRangeForCountSetBits<std::uint32_t>(1000U);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, int64_t)
{
    bool success = TestRangeForCountSetBits<std::int64_t>(10000000000000LL);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, uint64_t)
{
    bool success = TestRangeForCountSetBits<std::uint64_t>(10000000000000ULL);
    CHECK_TRUE(success);
}

TEST_MEMBER_FUNCTION(BitUtility, CountSetBits, unsigned_char_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned_char const*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char bits[2] {};
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 0U);

    bits[0] = 0x01u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 8U), 1U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 1U);

    bits[0] = 0x80u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 8U), 1U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 1U);

    bits[0] = 0x00u;
    bits[1] = 0x01u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 1U);

    bits[1] = 0x80u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 1U);

    bits[0] = 0x01u;
    bits[1] = 0x01u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 9U), 2U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 2U);

    bits[0] = 0x01u;
    bits[1] = 0x02u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 9U), 1U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 10U), 2U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 2U);

    bits[0] = 0x01u;
    bits[1] = 0x03u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 3U);

    bits[0] = 0x01u;
    bits[1] = 0x80u;
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 9U), 1U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 15U), 1U);
    CHECK_EQUAL(bit_utility_type::CountSetBits(bits, 16U), 2U);
}

TEST_MEMBER_FUNCTION(BitUtility, CountClearedBits, unsigned_char_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned_char const*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char bits[2]{};
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 16U);

    bits[0] = 0x01u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 8U), 7U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 15U);

    bits[0] = 0x80u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 8U), 7U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 15U);

    bits[0] = 0x00u;
    bits[1] = 0x01u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 15U);

    bits[1] = 0x80u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 15U);

    bits[0] = 0x01u;
    bits[1] = 0x01u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 9U), 7U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 14U);

    bits[0] = 0x01u;
    bits[1] = 0x02u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 9U), 8U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 10U), 8U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 14U);

    bits[0] = 0x01u;
    bits[1] = 0x03u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 13U);

    bits[0] = 0x01u;
    bits[1] = 0x80u;
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 9U), 8U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 15U), 14U);
    CHECK_EQUAL(bit_utility_type::CountClearedBits(bits, 16U), 14U);
}

TEST_MEMBER_FUNCTION(BitUtility, BytePosition, size_t)
{
    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    CHECK_EQUAL(bit_utility_type::BytePosition(0U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(1U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(2U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(3U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(4U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(5U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(6U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(7U), 0U);
    CHECK_EQUAL(bit_utility_type::BytePosition(8U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(9U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(10U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(11U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(12U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(13U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(14U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(15U), 1U);
    CHECK_EQUAL(bit_utility_type::BytePosition(16U), 2U);
}

TEST_MEMBER_FUNCTION(BitUtility, ByteCount, size_t)
{
    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    CHECK_EQUAL(bit_utility_type::ByteCount(0U), 0U);
    CHECK_EQUAL(bit_utility_type::ByteCount(1U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(2U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(3U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(4U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(5U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(6U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(7U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(8U), 1U);
    CHECK_EQUAL(bit_utility_type::ByteCount(9U), 2U);
}

TEST_MEMBER_FUNCTION(BitUtility, GetAt, unsigned_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned_char const*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char bit_test[]{ 0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U };
    unsigned char bits[2] {};

    for (std::size_t bit_position = 0; bit_position < static_cast<std::size_t>(CHAR_BIT * 2); ++bit_position)
    {
        unsigned char bit = bit_test[bit_position % 8];
        std::size_t byte_pos = bit_utility_type::BytePosition(bit_position);
        bits[byte_pos] = bit;
        CHECK_TRUE(bit_utility_type::GetAt(bits, bit_position));
    }
}

TEST_MEMBER_FUNCTION(BitUtility, SetAt, unsigned_char_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned_char*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char bit_test[]{ 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };
    unsigned char bits[2]{};

    // Test each single bit in the range of a byte is set for two bytes.
    for (std::size_t bit_position = 0; bit_position < static_cast<std::size_t>(CHAR_BIT * 2); ++bit_position)
    {
        std::memset(bits, 0, sizeof(bits));
        bit_utility_type::SetAt(bits, bit_position);
        std::size_t byte_pos = bit_utility_type::BytePosition(bit_position);
        CHECK_EQUAL(bits[byte_pos], bit_test[bit_position % 8]);
    }
}

TEST_MEMBER_FUNCTION(BitUtility, ClearAt, unsigned_char_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned_char*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char bit_test[]{ 0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U };
    unsigned char bits[2]{};

    // Test each single bit in the range of a byte is cleared for two bytes.
    for (std::size_t bit_position = 0; bit_position < static_cast<std::size_t>(CHAR_BIT * 2); ++bit_position)
    {
        std::memset(bits, bit_utility_type::ALL_BITS, sizeof(bits));
        bit_utility_type::ClearAt(bits, bit_position);
        std::size_t byte_pos = bit_utility_type::BytePosition(bit_position);
        unsigned char expected = bit_test[bit_position % 8] ^ bit_utility_type::ALL_BITS;
        CHECK_EQUAL(bits[byte_pos], expected);
    }
}

TEST_MEMBER_FUNCTION(BitUtility, Fill, unsigned_char_ptr_site_t_bool)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_t, bool");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    static const unsigned char bit_mask[CHAR_BIT] = { 0x01u, 0x03u, 0x07u, 0x0fu, 0x1fu, 0x3fu, 0x7fu, 0xffu };

    unsigned char bits[2]{};

    // Test fill from 1 to sizeof(bits) * CHAR_BIT bits.
    for (std::size_t bit_count = 0; bit_count < sizeof(bits) * CHAR_BIT; ++bit_count)
    {
        std::memset(bits, 0, sizeof(bits));
        bit_utility_type::Fill(bits, bit_count + 1);
        unsigned char bit_mask1 = bit_count < CHAR_BIT ? bit_mask[bit_count] : bit_mask[CHAR_BIT - 1];
        CHECK_EQUAL(bits[0], bit_mask1);
        if (bit_count >= CHAR_BIT)
        {
            unsigned char bit_mask2 = bit_mask[bit_count - CHAR_BIT];
            CHECK_EQUAL(bits[1], bit_mask2);
        }
    }
}

TEST_MEMBER_FUNCTION(BitUtility, Clear, unsigned_char_ptr_site_t_bool)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_t, bool");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    static const unsigned char bit_mask[CHAR_BIT] = { 0xfeu, 0xfcu, 0xf8u, 0xf0u, 0xe0u, 0xc0u, 0x80u, 0x00u };

    unsigned char bits[2]{};

    // Test fill from 1 to sizeof(bits) * CHAR_BIT bits.
    for (std::size_t bit_count = 0; bit_count < sizeof(bits) * CHAR_BIT; ++bit_count)
    {
        std::memset(bits, 0xff, sizeof(bits));
        bit_utility_type::Clear(bits, bit_count + 1);
        unsigned char bit_mask1 = bit_count < CHAR_BIT ? bit_mask[bit_count] : bit_mask[CHAR_BIT - 1];
        CHECK_EQUAL(bits[0], bit_mask1);
        if (bit_count >= CHAR_BIT)
        {
            unsigned char bit_mask2 = bit_mask[bit_count - CHAR_BIT];
            CHECK_EQUAL(bits[1], bit_mask2);
        }
    }
}

TEST_MEMBER_FUNCTION(BitUtility, Copy, unsigned_char_ptr_size_t_unsigned_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_t, unsigned char const*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char dst[2] {};
    unsigned char src[2] { 0x23u, 0x83u };

    bit_utility_type::Copy(dst, 1U, src, 1U);
    CHECK_EQUAL(dst[0], 1u);
    CHECK_EQUAL(dst[1], 0u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Copy(dst, 1U, src, 2U);
    CHECK_EQUAL(dst[0], 1u);
    CHECK_EQUAL(dst[1], 0u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Copy(dst, 2U, src, 2U);
    CHECK_EQUAL(dst[0], 3u);
    CHECK_EQUAL(dst[1], 0u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Copy(dst, 2U, src, 3U);
    CHECK_EQUAL(dst[0], 3u);
    CHECK_EQUAL(dst[1], 0u);
}

TEST_MEMBER_FUNCTION(BitUtility, Append, unsigned_char_ptr_size_t_unsigned_char_const_ptr_size_t)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_t, unsigned char const*, size_t");

    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    unsigned char dst[2]{};
    unsigned char src[2]{ 0x23u, 0x83u };

    bit_utility_type::Append(dst, 0U, src, 1U);
    CHECK_EQUAL(dst[0], 0x01u);
    CHECK_EQUAL(dst[1], 0x00u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 2U);
    CHECK_EQUAL(dst[0], 0x03u);
    CHECK_EQUAL(dst[1], 0x00u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 5U);
    CHECK_EQUAL(dst[0], 0x03u);
    CHECK_EQUAL(dst[1], 0x00u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 6U);
    CHECK_EQUAL(dst[0], 0x23u);
    CHECK_EQUAL(dst[1], 0x00u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 9U);
    CHECK_EQUAL(dst[0], 0x23u);
    CHECK_EQUAL(dst[1], 0x01u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 10U);
    CHECK_EQUAL(dst[0], 0x23u);
    CHECK_EQUAL(dst[1], 0x03u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 11U);
    CHECK_EQUAL(dst[0], 0x23u);
    CHECK_EQUAL(dst[1], 0x03u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 15U);
    CHECK_EQUAL(dst[0], 0x23u);
    CHECK_EQUAL(dst[1], 0x03u);

    std::memset(dst, 0, sizeof(dst));
    bit_utility_type::Append(dst, 0U, src, 16U);
    CHECK_EQUAL(dst[0], 0x23u);
    CHECK_EQUAL(dst[1], 0x83u);
}

TEST_MEMBER_FUNCTION(BitUtility, Expand, uint_8)
{
    typedef ocl::BitUtility<unsigned char> bit_utility_type;

    std::uint8_t value = 0x81U;
    std::uint16_t expected_value16 = 0x8181u;
    std::uint32_t expected_value32 = 0x81818181u;
    auto expanded_value16 = bit_utility_type::Expand<sizeof(expected_value16)>(value);
    CHECK_TRUE(expanded_value16 == expected_value16);

    auto expanded_value32 = bit_utility_type::Expand<sizeof(expected_value32)>(value);
    CHECK_TRUE(expanded_value32 == expected_value32);
}

namespace
{
    long fib(std::size_t n)
    {
        return (n <= 2) ? 1 : fib(n - 1) + fib(n - 2);
    }
}

TEST(DynamicProgramming)
{
    // 20 minutes into https://www.youtube.com/watch?v=oBt53YbR9Kk
    CHECK_TRUE(fib(6) == 8);
    CHECK_TRUE(fib(7) == 13);
    CHECK_TRUE(fib(8) == 21);
}
