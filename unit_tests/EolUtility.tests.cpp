/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../EolUtility.hpp"
#include <cstddef>

using ocl::EolUtility;
using ocl::EolPlatform;

TEST_MEMBER_FUNCTION(EolUtility, IsEndOfLineCharacter, NA)
{
    {
        typedef char char_type;
        typedef EolUtility<char_type, std::size_t> eol_utility;

        CHECK_TRUE(eol_utility::IsEndOfLineCharacter('\n'));
        CHECK_TRUE(eol_utility::IsEndOfLineCharacter('\r'));
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, std::size_t> eol_utility;

        CHECK_TRUE(eol_utility::IsEndOfLineCharacter(L'\n'));
        CHECK_TRUE(eol_utility::IsEndOfLineCharacter(L'\r'));
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, std::size_t> eol_utility;

        CHECK_TRUE(eol_utility::IsEndOfLineCharacter(u'\n'));
        CHECK_TRUE(eol_utility::IsEndOfLineCharacter(u'\r'));
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, std::size_t> eol_utility;

        CHECK_TRUE(eol_utility::IsEndOfLineCharacter(U'\n'));
        CHECK_TRUE(eol_utility::IsEndOfLineCharacter(U'\r'));
    }
}

TEST_MEMBER_FUNCTION(EolUtility, GetEolCharCount, EolPlatform)
{
    typedef char char_type;
    typedef EolUtility<char_type, std::size_t> eol_utility;

    CHECK_TRUE(eol_utility::GetEolCharCount(EolPlatform::Windows) == 2);
    CHECK_TRUE(eol_utility::GetEolCharCount(EolPlatform::Unix) == 1);
    CHECK_TRUE(eol_utility::GetEolCharCount(EolPlatform::Other) == 1);
    CHECK_TRUE(eol_utility::GetEolCharCount(EolPlatform::Mixed) == 0);
    CHECK_TRUE(eol_utility::GetEolCharCount(EolPlatform::Unknown) == 0);
}

TEST_MEMBER_FUNCTION(EolUtility, CountLines, char_type_ptr_size_t_bool)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type const*, size_t, bool");

    // Number of lines per string.
    size_type const text_lines_count[] =
    {
        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        2, 3, 4,
        2, 3, 4,
        2, 3, 4,
        4, 3, 3,
        4, 4,

        2, 3, 4,
        2, 3, 4,
        2, 3, 4,
        4, 3, 3,
        4, 4,

        3, 4,
        3, 4,
        3, 4,
        4, 3, 3,
        4, 4,

        0
    };

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            "A\nA\nA", "A\nA\nA\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\nA\n\rA", "A\r\nA\nA", "A\nA\r\nA",
            "A\nA\r\nA\nA", "A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines("", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(line, len);
            CHECK_TRUE(count == line_count);
            count = eol_utility::CountLines(line, len, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
                CHECK_TRUE(count == line_count - 1);
        }
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            L"\n", L"\n\n", L"\n\n\n",
            L"\r\n", L"\r\n\r\n", L"\r\n\r\n\r\n",
            L"\r", L"\r\r", L"\r\r\r",
            L"\n\n\r", L"\r\n\n", L"\n\r\n",
            L"\n\r\n\n", L"\n\n\r\n",

            L"A\n", L"A\n\n", L"A\n\n\n",
            L"A\r\n", L"A\r\n\r\n", L"A\r\n\r\n\r\n",
            L"A\r", L"A\r\r", L"A\r\r\r",
            L"A\n\n\r", L"A\r\n\n", L"A\n\r\n",
            L"A\n\r\n\n", L"A\n\n\r\n",

            L"\nA", L"\n\nA", L"\n\n\nA",
            L"\r\nA", L"\r\n\r\nA", L"\r\n\r\n\r\nA",
            L"\rA", L"\r\rA", L"\r\r\rA",
            L"\n\n\rA", L"\r\n\nA", L"\n\r\nA",
            L"\n\r\n\nA", L"\n\n\r\nA",

            L"A\nA", L"A\n\nA", L"A\n\n\nA",
            L"A\r\nA", L"A\r\n\r\nA", L"A\r\n\r\n\r\nA",
            L"A\rA", L"A\r\rA", L"A\r\r\rA",
            L"A\n\n\rA", L"A\r\n\nA", L"A\n\r\nA",
            L"A\n\r\n\nA", L"A\n\n\r\nA",

            L"A\nA\nA", L"A\nA\nA\nA",
            L"A\r\nA\r\nA", L"A\r\nA\r\nA\r\nA",
            L"A\rA\rA", L"A\rA\rA\rA",
            L"A\nA\n\rA", L"A\r\nA\nA", L"A\nA\r\nA",
            L"A\nA\r\nA\nA", L"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines(L"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(line, len);
            CHECK_TRUE(count == line_count);
            count = eol_utility::CountLines(line, len, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
                CHECK_TRUE(count == line_count - 1);
        }
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            u"\n", u"\n\n", u"\n\n\n",
            u"\r\n", u"\r\n\r\n", u"\r\n\r\n\r\n",
            u"\r", u"\r\r", u"\r\r\r",
            u"\n\n\r", u"\r\n\n", u"\n\r\n",
            u"\n\r\n\n", u"\n\n\r\n",

            u"A\n", u"A\n\n", u"A\n\n\n",
            u"A\r\n", u"A\r\n\r\n", u"A\r\n\r\n\r\n",
            u"A\r", u"A\r\r", u"A\r\r\r",
            u"A\n\n\r", u"A\r\n\n", u"A\n\r\n",
            u"A\n\r\n\n", u"A\n\n\r\n",

            u"\nA", u"\n\nA", u"\n\n\nA",
            u"\r\nA", u"\r\n\r\nA", u"\r\n\r\n\r\nA",
            u"\rA", u"\r\rA", u"\r\r\rA",
            u"\n\n\rA", u"\r\n\nA", u"\n\r\nA",
            u"\n\r\n\nA", u"\n\n\r\nA",

            u"A\nA", u"A\n\nA", u"A\n\n\nA",
            u"A\r\nA", u"A\r\n\r\nA", u"A\r\n\r\n\r\nA",
            u"A\rA", u"A\r\rA", u"A\r\r\rA",
            u"A\n\n\rA", u"A\r\n\nA", u"A\n\r\nA",
            u"A\n\r\n\nA", u"A\n\n\r\nA",

            u"A\nA\nA", u"A\nA\nA\nA",
            u"A\r\nA\r\nA", u"A\r\nA\r\nA\r\nA",
            u"A\rA\rA", u"A\rA\rA\rA",
            u"A\nA\n\rA", u"A\r\nA\nA", u"A\nA\r\nA",
            u"A\nA\r\nA\nA", u"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines(u"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(line, len);
            CHECK_TRUE(count == line_count);
            count = eol_utility::CountLines(line, len, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
                CHECK_TRUE(count == line_count - 1);
        }
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            U"\n", U"\n\n", U"\n\n\n",
            U"\r\n", U"\r\n\r\n", U"\r\n\r\n\r\n",
            U"\r", U"\r\r", U"\r\r\r",
            U"\n\n\r", U"\r\n\n", U"\n\r\n",
            U"\n\r\n\n", U"\n\n\r\n",

            U"A\n", U"A\n\n", U"A\n\n\n",
            U"A\r\n", U"A\r\n\r\n", U"A\r\n\r\n\r\n",
            U"A\r", U"A\r\r", U"A\r\r\r",
            U"A\n\n\r", U"A\r\n\n", U"A\n\r\n",
            U"A\n\r\n\n", U"A\n\n\r\n",

            U"\nA", U"\n\nA", U"\n\n\nA",
            U"\r\nA", U"\r\n\r\nA", U"\r\n\r\n\r\nA",
            U"\rA", U"\r\rA", U"\r\r\rA",
            U"\n\n\rA", U"\r\n\nA", U"\n\r\nA",
            U"\n\r\n\nA", U"\n\n\r\nA",

            U"A\nA", U"A\n\nA", U"A\n\n\nA",
            U"A\r\nA", U"A\r\n\r\nA", U"A\r\n\r\n\r\nA",
            U"A\rA", U"A\r\rA", U"A\r\r\rA",
            U"A\n\n\rA", U"A\r\n\nA", U"A\n\r\nA",
            U"A\n\r\n\nA", U"A\n\n\r\nA",

            U"A\nA\nA", U"A\nA\nA\nA",
            U"A\r\nA\r\nA", U"A\r\nA\r\nA\r\nA",
            U"A\rA\rA", U"A\rA\rA\rA",
            U"A\nA\n\rA", U"A\r\nA\nA", U"A\nA\r\nA",
            U"A\nA\r\nA\nA", U"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines(U"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(line, len);
            CHECK_TRUE(count == line_count);
            count = eol_utility::CountLines(line, len, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
                CHECK_TRUE(count == line_count - 1);
        }
    }
}

TEST_MEMBER_FUNCTION(EolUtility, CountLines, char_type_ptr_size_t_size_t_ref_bool)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type const*, size_t, size_t&, bool");

    // Number of lines per string.
    size_type const text_lines_count[] =
    {
        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        2, 3, 4,
        2, 3, 4,
        2, 3, 4,
        4, 3, 3,
        4, 4,

        2, 3, 4,
        2, 3, 4,
        2, 3, 4,
        4, 3, 3,
        4, 4,

        3, 4,
        3, 4,
        3, 4,
        4, 3, 3,
        4, 4,

        0
    };

    // Number of eol chars per string.
    size_type const text_lines_eol_count[] =
    {
        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        2, 3,
        4, 6,
        2, 3,
        3, 3, 3,
        4, 4,

        0
    };

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            "A\nA\nA", "A\nA\nA\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\nA\n\rA", "A\r\nA\nA", "A\nA\r\nA",
            "A\nA\r\nA\nA", "A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines("", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type line_eol_count = text_lines_eol_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(text_lines[row], len, eol_count);
            CHECK_TRUE(count == line_count);
            CHECK_TRUE(eol_count == line_eol_count);

            count = eol_utility::CountLines(text_lines[row], len, eol_count, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
            {
                CHECK_TRUE(count == line_count - 1);
                CHECK_TRUE(eol_count == line_eol_count);
            }
        }
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            L"\n", L"\n\n", L"\n\n\n",
            L"\r\n", L"\r\n\r\n", L"\r\n\r\n\r\n",
            L"\r", L"\r\r", L"\r\r\r",
            L"\n\n\r", L"\r\n\n", L"\n\r\n",
            L"\n\r\n\n", L"\n\n\r\n",

            L"A\n", L"A\n\n", L"A\n\n\n",
            L"A\r\n", L"A\r\n\r\n", L"A\r\n\r\n\r\n",
            L"A\r", L"A\r\r", L"A\r\r\r",
            L"A\n\n\r", L"A\r\n\n", L"A\n\r\n",
            L"A\n\r\n\n", L"A\n\n\r\n",

            L"\nA", L"\n\nA", L"\n\n\nA",
            L"\r\nA", L"\r\n\r\nA", L"\r\n\r\n\r\nA",
            L"\rA", L"\r\rA", L"\r\r\rA",
            L"\n\n\rA", L"\r\n\nA", L"\n\r\nA",
            L"\n\r\n\nA", L"\n\n\r\nA",

            L"A\nA", L"A\n\nA", L"A\n\n\nA",
            L"A\r\nA", L"A\r\n\r\nA", L"A\r\n\r\n\r\nA",
            L"A\rA", L"A\r\rA", L"A\r\r\rA",
            L"A\n\n\rA", L"A\r\n\nA", L"A\n\r\nA",
            L"A\n\r\n\nA", L"A\n\n\r\nA",

            L"A\nA\nA", L"A\nA\nA\nA",
            L"A\r\nA\r\nA", L"A\r\nA\r\nA\r\nA",
            L"A\rA\rA", L"A\rA\rA\rA",
            L"A\nA\n\rA", L"A\r\nA\nA", L"A\nA\r\nA",
            L"A\nA\r\nA\nA", L"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines(L"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type line_eol_count = text_lines_eol_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(text_lines[row], len, eol_count);
            CHECK_TRUE(count == line_count);
            CHECK_TRUE(eol_count == line_eol_count);

            count = eol_utility::CountLines(text_lines[row], len, eol_count, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
            {
                CHECK_TRUE(count == line_count - 1);
                CHECK_TRUE(eol_count == line_eol_count);
            }
        }
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            u"\n", u"\n\n", u"\n\n\n",
            u"\r\n", u"\r\n\r\n", u"\r\n\r\n\r\n",
            u"\r", u"\r\r", u"\r\r\r",
            u"\n\n\r", u"\r\n\n", u"\n\r\n",
            u"\n\r\n\n", u"\n\n\r\n",

            u"A\n", u"A\n\n", u"A\n\n\n",
            u"A\r\n", u"A\r\n\r\n", u"A\r\n\r\n\r\n",
            u"A\r", u"A\r\r", u"A\r\r\r",
            u"A\n\n\r", u"A\r\n\n", u"A\n\r\n",
            u"A\n\r\n\n", u"A\n\n\r\n",

            u"\nA", u"\n\nA", u"\n\n\nA",
            u"\r\nA", u"\r\n\r\nA", u"\r\n\r\n\r\nA",
            u"\rA", u"\r\rA", u"\r\r\rA",
            u"\n\n\rA", u"\r\n\nA", u"\n\r\nA",
            u"\n\r\n\nA", u"\n\n\r\nA",

            u"A\nA", u"A\n\nA", u"A\n\n\nA",
            u"A\r\nA", u"A\r\n\r\nA", u"A\r\n\r\n\r\nA",
            u"A\rA", u"A\r\rA", u"A\r\r\rA",
            u"A\n\n\rA", u"A\r\n\nA", u"A\n\r\nA",
            u"A\n\r\n\nA", u"A\n\n\r\nA",

            u"A\nA\nA", u"A\nA\nA\nA",
            u"A\r\nA\r\nA", u"A\r\nA\r\nA\r\nA",
            u"A\rA\rA", u"A\rA\rA\rA",
            u"A\nA\n\rA", u"A\r\nA\nA", u"A\nA\r\nA",
            u"A\nA\r\nA\nA", u"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines(u"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type line_eol_count = text_lines_eol_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(text_lines[row], len, eol_count);
            CHECK_TRUE(count == line_count);
            CHECK_TRUE(eol_count == line_eol_count);

            count = eol_utility::CountLines(text_lines[row], len, eol_count, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
            {
                CHECK_TRUE(count == line_count - 1);
                CHECK_TRUE(eol_count == line_eol_count);
            }
        }
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            U"\n", U"\n\n", U"\n\n\n",
            U"\r\n", U"\r\n\r\n", U"\r\n\r\n\r\n",
            U"\r", U"\r\r", U"\r\r\r",
            U"\n\n\r", U"\r\n\n", U"\n\r\n",
            U"\n\r\n\n", U"\n\n\r\n",

            U"A\n", U"A\n\n", U"A\n\n\n",
            U"A\r\n", U"A\r\n\r\n", U"A\r\n\r\n\r\n",
            U"A\r", U"A\r\r", U"A\r\r\r",
            U"A\n\n\r", U"A\r\n\n", U"A\n\r\n",
            U"A\n\r\n\n", U"A\n\n\r\n",

            U"\nA", U"\n\nA", U"\n\n\nA",
            U"\r\nA", U"\r\n\r\nA", U"\r\n\r\n\r\nA",
            U"\rA", U"\r\rA", U"\r\r\rA",
            U"\n\n\rA", U"\r\n\nA", U"\n\r\nA",
            U"\n\r\n\nA", U"\n\n\r\nA",

            U"A\nA", U"A\n\nA", U"A\n\n\nA",
            U"A\r\nA", U"A\r\n\r\nA", U"A\r\n\r\n\r\nA",
            U"A\rA", U"A\r\rA", U"A\r\r\rA",
            U"A\n\n\rA", U"A\r\n\nA", U"A\n\r\nA",
            U"A\n\r\n\nA", U"A\n\n\r\nA",

            U"A\nA\nA", U"A\nA\nA\nA",
            U"A\r\nA\r\nA", U"A\r\nA\r\nA\r\nA",
            U"A\rA\rA", U"A\rA\rA\rA",
            U"A\nA\n\rA", U"A\r\nA\nA", U"A\nA\r\nA",
            U"A\nA\r\nA\nA", U"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountLines(U"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_count[row];
            size_type line_eol_count = text_lines_eol_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::CountLines(text_lines[row], len, eol_count);
            CHECK_TRUE(count == line_count);
            CHECK_TRUE(eol_count == line_eol_count);

            count = eol_utility::CountLines(text_lines[row], len, eol_count, true);
            if (!eol_utility::IsEndOfLineCharacter(line[len - 1]))
            {
                CHECK_TRUE(count == line_count - 1);
                CHECK_TRUE(eol_count == line_eol_count);
            }
        }
    }
}

TEST_MEMBER_FUNCTION(EolUtility, CountContinuousLines, char_type_ptr_size_t)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type const*, size_t");

    // Number of lines per string.
    size_type const text_lines_count[] =
    {
        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 1,
        1, 1,
        1, 1,
        1, 1, 1,
        1, 1,

        0
    };

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            "A\nA\nA", "A\nA\nA\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\nA\n\rA", "A\r\nA\nA", "A\nA\r\nA",
            "A\nA\r\nA\nA", "A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines("", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len);
            CHECK_TRUE(count == text_lines_count[row]);
        }
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            L"\n", L"\n\n", L"\n\n\n",
            L"\r\n", L"\r\n\r\n", L"\r\n\r\n\r\n",
            L"\r", L"\r\r", L"\r\r\r",
            L"\n\n\r", L"\r\n\n", L"\n\r\n",
            L"\n\r\n\n", L"\n\n\r\n",

            L"A\n", L"A\n\n", L"A\n\n\n",
            L"A\r\n", L"A\r\n\r\n", L"A\r\n\r\n\r\n",
            L"A\r", L"A\r\r", L"A\r\r\r",
            L"A\n\n\r", L"A\r\n\n", L"A\n\r\n",
            L"A\n\r\n\n", L"A\n\n\r\n",

            L"\nA", L"\n\nA", L"\n\n\nA",
            L"\r\nA", L"\r\n\r\nA", L"\r\n\r\n\r\nA",
            L"\rA", L"\r\rA", L"\r\r\rA",
            L"\n\n\rA", L"\r\n\nA", L"\n\r\nA",
            L"\n\r\n\nA", L"\n\n\r\nA",

            L"A\nA", L"A\n\nA", L"A\n\n\nA",
            L"A\r\nA", L"A\r\n\r\nA", L"A\r\n\r\n\r\nA",
            L"A\rA", L"A\r\rA", L"A\r\r\rA",
            L"A\n\n\rA", L"A\r\n\nA", L"A\n\r\nA",
            L"A\n\r\n\nA", L"A\n\n\r\nA",

            L"A\nA\nA", L"A\nA\nA\nA",
            L"A\r\nA\r\nA", L"A\r\nA\r\nA\r\nA",
            L"A\rA\rA", L"A\rA\rA\rA",
            L"A\nA\n\rA", L"A\r\nA\nA", L"A\nA\r\nA",
            L"A\nA\r\nA\nA", L"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines(L"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len);
            if (count != text_lines_count[row])
                count = eol_utility::CountContinuousLines(line, len);
            CHECK_TRUE(count == text_lines_count[row]);
        }
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            u"\n", u"\n\n", u"\n\n\n",
            u"\r\n", u"\r\n\r\n", u"\r\n\r\n\r\n",
            u"\r", u"\r\r", u"\r\r\r",
            u"\n\n\r", u"\r\n\n", u"\n\r\n",
            u"\n\r\n\n", u"\n\n\r\n",

            u"A\n", u"A\n\n", u"A\n\n\n",
            u"A\r\n", u"A\r\n\r\n", u"A\r\n\r\n\r\n",
            u"A\r", u"A\r\r", u"A\r\r\r",
            u"A\n\n\r", u"A\r\n\n", u"A\n\r\n",
            u"A\n\r\n\n", u"A\n\n\r\n",

            u"\nA", u"\n\nA", u"\n\n\nA",
            u"\r\nA", u"\r\n\r\nA", u"\r\n\r\n\r\nA",
            u"\rA", u"\r\rA", u"\r\r\rA",
            u"\n\n\rA", u"\r\n\nA", u"\n\r\nA",
            u"\n\r\n\nA", u"\n\n\r\nA",

            u"A\nA", u"A\n\nA", u"A\n\n\nA",
            u"A\r\nA", u"A\r\n\r\nA", u"A\r\n\r\n\r\nA",
            u"A\rA", u"A\r\rA", u"A\r\r\rA",
            u"A\n\n\rA", u"A\r\n\nA", u"A\n\r\nA",
            u"A\n\r\n\nA", u"A\n\n\r\nA",

            u"A\nA\nA", u"A\nA\nA\nA",
            u"A\r\nA\r\nA", u"A\r\nA\r\nA\r\nA",
            u"A\rA\rA", u"A\rA\rA\rA",
            u"A\nA\n\rA", u"A\r\nA\nA", u"A\nA\r\nA",
            u"A\nA\r\nA\nA", u"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines(u"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len);
            if (count != text_lines_count[row])
                count = eol_utility::CountContinuousLines(line, len);
            CHECK_TRUE(count == text_lines_count[row]);
        }
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            U"\n", U"\n\n", U"\n\n\n",
            U"\r\n", U"\r\n\r\n", U"\r\n\r\n\r\n",
            U"\r", U"\r\r", U"\r\r\r",
            U"\n\n\r", U"\r\n\n", U"\n\r\n",
            U"\n\r\n\n", U"\n\n\r\n",

            U"A\n", U"A\n\n", U"A\n\n\n",
            U"A\r\n", U"A\r\n\r\n", U"A\r\n\r\n\r\n",
            U"A\r", U"A\r\r", U"A\r\r\r",
            U"A\n\n\r", U"A\r\n\n", U"A\n\r\n",
            U"A\n\r\n\n", U"A\n\n\r\n",

            U"\nA", U"\n\nA", U"\n\n\nA",
            U"\r\nA", U"\r\n\r\nA", U"\r\n\r\n\r\nA",
            U"\rA", U"\r\rA", U"\r\r\rA",
            U"\n\n\rA", U"\r\n\nA", U"\n\r\nA",
            U"\n\r\n\nA", U"\n\n\r\nA",

            U"A\nA", U"A\n\nA", U"A\n\n\nA",
            U"A\r\nA", U"A\r\n\r\nA", U"A\r\n\r\n\r\nA",
            U"A\rA", U"A\r\rA", U"A\r\r\rA",
            U"A\n\n\rA", U"A\r\n\nA", U"A\n\r\nA",
            U"A\n\r\n\nA", U"A\n\n\r\nA",

            U"A\nA\nA", U"A\nA\nA\nA",
            U"A\r\nA\r\nA", U"A\r\nA\r\nA\r\nA",
            U"A\rA\rA", U"A\rA\rA\rA",
            U"A\nA\n\rA", U"A\r\nA\nA", U"A\nA\r\nA",
            U"A\nA\r\nA\nA", U"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines(U"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len);
            if (count != text_lines_count[row])
                count = eol_utility::CountContinuousLines(line, len);
            CHECK_TRUE(count == text_lines_count[row]);
        }
    }
}

TEST_MEMBER_FUNCTION(EolUtility, CountContinuousLines, char_type_ptr_size_t_size_t_ref)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type const*, size_t, size_t&");

    // Number of lines per string.
    size_type const text_lines_count[] =
    {
        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        1, 1,
        1, 1,
        1, 1,
        1, 1, 1,
        1, 1,

        0
    };

    // Number of eol chars per string.
    size_type const text_lines_eol_count[] =
    {
        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 2, 3,
        2, 4, 6,
        1, 2, 3,
        3, 3, 3,
        4, 4,

        1, 1,
        2, 2,
        1, 1,
        1, 2, 1,
        1, 1,

        0
    };

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            "A\nA\nA", "A\nA\nA\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\nA\n\rA", "A\r\nA\nA", "A\nA\r\nA",
            "A\nA\r\nA\nA", "A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines("", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len, eol_count);
            CHECK_TRUE(count == text_lines_count[row]);
            CHECK_TRUE(eol_count == text_lines_eol_count[row]);
        }
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            L"\n", L"\n\n", L"\n\n\n",
            L"\r\n", L"\r\n\r\n", L"\r\n\r\n\r\n",
            L"\r", L"\r\r", L"\r\r\r",
            L"\n\n\r", L"\r\n\n", L"\n\r\n",
            L"\n\r\n\n", L"\n\n\r\n",

            L"A\n", L"A\n\n", L"A\n\n\n",
            L"A\r\n", L"A\r\n\r\n", L"A\r\n\r\n\r\n",
            L"A\r", L"A\r\r", L"A\r\r\r",
            L"A\n\n\r", L"A\r\n\n", L"A\n\r\n",
            L"A\n\r\n\n", L"A\n\n\r\n",

            L"\nA", L"\n\nA", L"\n\n\nA",
            L"\r\nA", L"\r\n\r\nA", L"\r\n\r\n\r\nA",
            L"\rA", L"\r\rA", L"\r\r\rA",
            L"\n\n\rA", L"\r\n\nA", L"\n\r\nA",
            L"\n\r\n\nA", L"\n\n\r\nA",

            L"A\nA", L"A\n\nA", L"A\n\n\nA",
            L"A\r\nA", L"A\r\n\r\nA", L"A\r\n\r\n\r\nA",
            L"A\rA", L"A\r\rA", L"A\r\r\rA",
            L"A\n\n\rA", L"A\r\n\nA", L"A\n\r\nA",
            L"A\n\r\n\nA", L"A\n\n\r\nA",

            L"A\nA\nA", L"A\nA\nA\nA",
            L"A\r\nA\r\nA", L"A\r\nA\r\nA\r\nA",
            L"A\rA\rA", L"A\rA\rA\rA",
            L"A\nA\n\rA", L"A\r\nA\nA", L"A\nA\r\nA",
            L"A\nA\r\nA\nA", L"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines(L"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len, eol_count);
            CHECK_TRUE(count == text_lines_count[row]);
            CHECK_TRUE(eol_count == text_lines_eol_count[row]);
        }
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            u"\n", u"\n\n", u"\n\n\n",
            u"\r\n", u"\r\n\r\n", u"\r\n\r\n\r\n",
            u"\r", u"\r\r", u"\r\r\r",
            u"\n\n\r", u"\r\n\n", u"\n\r\n",
            u"\n\r\n\n", u"\n\n\r\n",

            u"A\n", u"A\n\n", u"A\n\n\n",
            u"A\r\n", u"A\r\n\r\n", u"A\r\n\r\n\r\n",
            u"A\r", u"A\r\r", u"A\r\r\r",
            u"A\n\n\r", u"A\r\n\n", u"A\n\r\n",
            u"A\n\r\n\n", u"A\n\n\r\n",

            u"\nA", u"\n\nA", u"\n\n\nA",
            u"\r\nA", u"\r\n\r\nA", u"\r\n\r\n\r\nA",
            u"\rA", u"\r\rA", u"\r\r\rA",
            u"\n\n\rA", u"\r\n\nA", u"\n\r\nA",
            u"\n\r\n\nA", u"\n\n\r\nA",

            u"A\nA", u"A\n\nA", u"A\n\n\nA",
            u"A\r\nA", u"A\r\n\r\nA", u"A\r\n\r\n\r\nA",
            u"A\rA", u"A\r\rA", u"A\r\r\rA",
            u"A\n\n\rA", u"A\r\n\nA", u"A\n\r\nA",
            u"A\n\r\n\nA", u"A\n\n\r\nA",

            u"A\nA\nA", u"A\nA\nA\nA",
            u"A\r\nA\r\nA", u"A\r\nA\r\nA\r\nA",
            u"A\rA\rA", u"A\rA\rA\rA",
            u"A\nA\n\rA", u"A\r\nA\nA", u"A\nA\r\nA",
            u"A\nA\r\nA\nA", u"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines(u"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len, eol_count);
            CHECK_TRUE(count == text_lines_count[row]);
            CHECK_TRUE(eol_count == text_lines_eol_count[row]);
        }
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            U"\n", U"\n\n", U"\n\n\n",
            U"\r\n", U"\r\n\r\n", U"\r\n\r\n\r\n",
            U"\r", U"\r\r", U"\r\r\r",
            U"\n\n\r", U"\r\n\n", U"\n\r\n",
            U"\n\r\n\n", U"\n\n\r\n",

            U"A\n", U"A\n\n", U"A\n\n\n",
            U"A\r\n", U"A\r\n\r\n", U"A\r\n\r\n\r\n",
            U"A\r", U"A\r\r", U"A\r\r\r",
            U"A\n\n\r", U"A\r\n\n", U"A\n\r\n",
            U"A\n\r\n\n", U"A\n\n\r\n",

            U"\nA", U"\n\nA", U"\n\n\nA",
            U"\r\nA", U"\r\n\r\nA", U"\r\n\r\n\r\nA",
            U"\rA", U"\r\rA", U"\r\r\rA",
            U"\n\n\rA", U"\r\n\nA", U"\n\r\nA",
            U"\n\r\n\nA", U"\n\n\r\nA",

            U"A\nA", U"A\n\nA", U"A\n\n\nA",
            U"A\r\nA", U"A\r\n\r\nA", U"A\r\n\r\n\r\nA",
            U"A\rA", U"A\r\rA", U"A\r\r\rA",
            U"A\n\n\rA", U"A\r\n\nA", U"A\n\r\nA",
            U"A\n\r\n\nA", U"A\n\n\r\nA",

            U"A\nA\nA", U"A\nA\nA\nA",
            U"A\r\nA\r\nA", U"A\r\nA\r\nA\r\nA",
            U"A\rA\rA", U"A\rA\rA\rA",
            U"A\nA\n\rA", U"A\r\nA\nA", U"A\nA\r\nA",
            U"A\nA\r\nA\nA", U"A\nA\nA\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::CountContinuousLines(U"", 0) == 0);

        for (size_type row = 0; (text_lines[row] != nullptr) && (text_lines_count[row] != 0); ++row)
        {
            size_type eol_count = 0;
            char_type const* line = text_lines[row];
            while (!eol_utility::IsEndOfLineCharacter(*line))
                ++line;
            size_type len = StrLen(line);
            size_type count = eol_utility::CountContinuousLines(line, len, eol_count);
            CHECK_TRUE(count == text_lines_count[row]);
            CHECK_TRUE(eol_count == text_lines_eol_count[row]);
        }
    }
}

TEST_MEMBER_FUNCTION(EolUtility, GetPlatform, char_type_char_type_size_t_ref)
{
    typedef std::size_t size_type;

     TEST_OVERRIDE_ARGS("char_type, char_type, size_t&");

    size_type count = 0;

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatform('\0', '\0', count) == EolPlatform::Unknown);
        CHECK_TRUE(count == 0);

        CHECK_TRUE(eol_utility::GetPlatform('\n', '\0', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform('\r', '\n', count) == EolPlatform::Windows);
        CHECK_TRUE(count == 2);

        CHECK_TRUE(eol_utility::GetPlatform('\r', '\0', count) == EolPlatform::Other);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform('\n', '\r', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatform(L'\0', L'\0', count) == EolPlatform::Unknown);
        CHECK_TRUE(count == 0);

        CHECK_TRUE(eol_utility::GetPlatform(L'\n', L'\0', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform(L'\r', L'\n', count) == EolPlatform::Windows);
        CHECK_TRUE(count == 2);

        CHECK_TRUE(eol_utility::GetPlatform(L'\r', L'\0', count) == EolPlatform::Other);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform(L'\n', L'\r', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatform(u'\0', u'\0', count) == EolPlatform::Unknown);
        CHECK_TRUE(count == 0);

        CHECK_TRUE(eol_utility::GetPlatform(u'\n', u'\0', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform(u'\r', u'\n', count) == EolPlatform::Windows);
        CHECK_TRUE(count == 2);

        CHECK_TRUE(eol_utility::GetPlatform(u'\r', u'\0', count) == EolPlatform::Other);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform(u'\n', u'\r', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatform(U'\0', U'\0', count) == EolPlatform::Unknown);
        CHECK_TRUE(count == 0);

        CHECK_TRUE(eol_utility::GetPlatform(U'\n', U'\0', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform(U'\r', U'\n', count) == EolPlatform::Windows);
        CHECK_TRUE(count == 2);

        CHECK_TRUE(eol_utility::GetPlatform(U'\r', U'\0', count) == EolPlatform::Other);
        CHECK_TRUE(count == 1);

        CHECK_TRUE(eol_utility::GetPlatform(U'\n', U'\r', count) == EolPlatform::Unix);
        CHECK_TRUE(count == 1);
    }
}

TEST_MEMBER_FUNCTION(EolUtility, GetPlatformCount, char_type_char_type)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type, char_type");

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatformCount('\0', '\0') == 0);
        CHECK_TRUE(eol_utility::GetPlatformCount('\n', '\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount('\r', '\n') == 2);
        CHECK_TRUE(eol_utility::GetPlatformCount('\r', '\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount('\n', '\r') == 1);
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatformCount(L'\0', L'\0') == 0);
        CHECK_TRUE(eol_utility::GetPlatformCount(L'\n', L'\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount(L'\r', L'\n') == 2);
        CHECK_TRUE(eol_utility::GetPlatformCount(L'\r', L'\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount(L'\n', L'\r') == 1);
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatformCount(u'\0', u'\0') == 0);
        CHECK_TRUE(eol_utility::GetPlatformCount(u'\n', u'\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount(u'\r', u'\n') == 2);
        CHECK_TRUE(eol_utility::GetPlatformCount(u'\r', u'\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount(u'\n', u'\r') == 1);
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        CHECK_TRUE(eol_utility::GetPlatformCount(U'\0', U'\0') == 0);
        CHECK_TRUE(eol_utility::GetPlatformCount(U'\n', U'\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount(U'\r', U'\n') == 2);
        CHECK_TRUE(eol_utility::GetPlatformCount(U'\r', U'\0') == 1);
        CHECK_TRUE(eol_utility::GetPlatformCount(U'\n', U'\r') == 1);
    }
}

TEST_MEMBER_FUNCTION(EolUtility, SafeSetEol, char_type_ptr_size_t_EolPlatform)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type*, size_t, EolPlatform");

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, 'A', 'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == 'A');

        buffer[0] = 'A';
        buffer[1] = 'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed));
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown));
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, L'A', L'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == L'A');

        buffer[0] = L'A';
        buffer[1] = L'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed));
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown));
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, u'A', u'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == u'A');

        buffer[0] = u'A';
        buffer[1] = u'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed));
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown));
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, U'A', U'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows));
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == U'A');

        buffer[0] = U'A';
        buffer[1] = U'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed));
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown));
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix));
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');
    }
}

TEST_MEMBER_FUNCTION(EolUtility, SafeSetEol, char_type_ptr_size_t_EolPlatform_size_t_ref)
{
    typedef std::size_t size_type;
    size_type count = 0;

    TEST_OVERRIDE_ARGS("char_type*, size_t, EolPlatform, size_t&");

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, 'A', 'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows, count));
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == 'A');

        buffer[0] = 'A';
        buffer[1] = 'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, L'A', L'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows, count));
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == L'A');

        buffer[0] = L'A';
        buffer[1] = L'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, u'A', u'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows, count));
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == u'A');

        buffer[0] = u'A';
        buffer[1] = u'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, U'A', U'A' };

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Other, count));
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_TRUE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Windows, count));
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == U'A');

        buffer[0] = U'A';
        buffer[1] = U'A';
        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Mixed, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, BUFFER_SIZE, EolPlatform::Unknown, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_FALSE(eol_utility::SafeSetEol(nullptr, BUFFER_SIZE, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        CHECK_FALSE(eol_utility::SafeSetEol(buffer, 0, EolPlatform::Unix, count));
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');
    }
}

TEST_MEMBER_FUNCTION(EolUtility, UnsafeSetEol, char_type_ptr_EolPlatform)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type*, EolPlatform");

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, 'A', 'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == 'A');

        buffer[0] = 'A';
        buffer[1] = 'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, L'A', L'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == L'A');

        buffer[0] = L'A';
        buffer[1] = L'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, u'A', u'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == u'A');

        buffer[0] = u'A';
        buffer[1] = u'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, U'A', U'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == U'A');

        buffer[0] = U'A';
        buffer[1] = U'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');
    }
}

TEST_MEMBER_FUNCTION(EolUtility, UnsafeSetEol, char_type_ptr_EolPlatform_size_t_ref)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type*, EolPlatform, size_t&");

    size_type count = 0;

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, 'A', 'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows, count);
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == 'A');

        buffer[0] = 'A';
        buffer[1] = 'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == 'A');
        CHECK_TRUE(buffer[1] == 'A');
        CHECK_TRUE(buffer[2] == 'A');
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, L'A', L'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows, count);
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == L'A');

        buffer[0] = L'A';
        buffer[1] = L'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == L'A');
        CHECK_TRUE(buffer[1] == L'A');
        CHECK_TRUE(buffer[2] == L'A');
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, u'A', u'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows, count);
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == u'A');

        buffer[0] = u'A';
        buffer[1] = u'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == u'A');
        CHECK_TRUE(buffer[1] == u'A');
        CHECK_TRUE(buffer[2] == u'A');
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;
        size_type const BUFFER_SIZE = 3;
        char_type buffer[BUFFER_SIZE] = { eol_utility::CHAR_NULL, U'A', U'A' };

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unix, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Other, count);
        CHECK_EQUAL(count, 1);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Windows, count);
        CHECK_EQUAL(count, 2);
        CHECK_TRUE(buffer[0] == eol_utility::CHAR_CR);
        CHECK_TRUE(buffer[1] == eol_utility::CHAR_LF);
        CHECK_TRUE(buffer[2] == U'A');

        buffer[0] = U'A';
        buffer[1] = U'A';
        eol_utility::UnsafeSetEol(buffer, EolPlatform::Mixed, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');

        eol_utility::UnsafeSetEol(buffer, EolPlatform::Unknown, count);
        CHECK_EQUAL(count, 0);
        CHECK_TRUE(buffer[0] == U'A');
        CHECK_TRUE(buffer[1] == U'A');
        CHECK_TRUE(buffer[2] == U'A');
    }
}

TEST_MEMBER_FUNCTION(EolUtility, GetPlatform, char_type_ptr_size_t)
{
    typedef std::size_t size_type;

    EolPlatform const text_lines_platform[] =
    {
        EolPlatform::Unix, EolPlatform::Unix, EolPlatform::Unix,
        EolPlatform::Windows, EolPlatform::Windows, EolPlatform::Windows,
        EolPlatform::Other, EolPlatform::Other, EolPlatform::Other,
        EolPlatform::Mixed, EolPlatform::Mixed, EolPlatform::Mixed,
        EolPlatform::Mixed, EolPlatform::Mixed,

        EolPlatform::Unix, EolPlatform::Unix, EolPlatform::Unix,
        EolPlatform::Windows, EolPlatform::Windows, EolPlatform::Windows,
        EolPlatform::Other, EolPlatform::Other, EolPlatform::Other,
        EolPlatform::Mixed, EolPlatform::Mixed, EolPlatform::Mixed,
        EolPlatform::Mixed, EolPlatform::Mixed,

        EolPlatform::Unix, EolPlatform::Unix, EolPlatform::Unix,
        EolPlatform::Windows, EolPlatform::Windows, EolPlatform::Windows,
        EolPlatform::Other, EolPlatform::Other, EolPlatform::Other,
        EolPlatform::Mixed, EolPlatform::Mixed, EolPlatform::Mixed,
        EolPlatform::Mixed, EolPlatform::Mixed,

        EolPlatform::Unix, EolPlatform::Unix, EolPlatform::Unix,
        EolPlatform::Windows, EolPlatform::Windows, EolPlatform::Windows,
        EolPlatform::Other, EolPlatform::Other, EolPlatform::Other,
        EolPlatform::Mixed, EolPlatform::Mixed, EolPlatform::Mixed,
        EolPlatform::Mixed, EolPlatform::Mixed,

        EolPlatform::Unknown
    };

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetPlatform("", 0) == EolPlatform::Unknown);

        for (size_type row = 0;
             text_lines[row] != nullptr && text_lines_platform[row] != EolPlatform::Unknown;
             ++row)
        {
            char_type const* line = text_lines[row];
            size_type len = StrLen(line);
            EolPlatform platform = eol_utility::GetPlatform(line, len);
            CHECK_TRUE(platform == text_lines_platform[row]);
        }
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            L"\n", L"\n\n", L"\n\n\n",
            L"\r\n", L"\r\n\r\n", L"\r\n\r\n\r\n",
            L"\r", L"\r\r", L"\r\r\r",
            L"\n\n\r", L"\r\n\n", L"\n\r\n",
            L"\n\r\n\n", L"\n\n\r\n",

            L"A\n", L"A\n\n", L"A\n\n\n",
            L"A\r\n", L"A\r\n\r\n", L"A\r\n\r\n\r\n",
            L"A\r", L"A\r\r", L"A\r\r\r",
            L"A\n\n\r", L"A\r\n\n", L"A\n\r\n",
            L"A\n\r\n\n", L"A\n\n\r\n",

            L"\nA", L"\n\nA", L"\n\n\nA",
            L"\r\nA", L"\r\n\r\nA", L"\r\n\r\n\r\nA",
            L"\rA", L"\r\rA", L"\r\r\rA",
            L"\n\n\rA", L"\r\n\nA", L"\n\r\nA",
            L"\n\r\n\nA", L"\n\n\r\nA",

            L"A\nA", L"A\n\nA", L"A\n\n\nA",
            L"A\r\nA", L"A\r\n\r\nA", L"A\r\n\r\n\r\nA",
            L"A\rA", L"A\r\rA", L"A\r\r\rA",
            L"A\n\n\rA", L"A\r\n\nA", L"A\n\r\nA",
            L"A\n\r\n\nA", L"A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetPlatform(L"", 0) == EolPlatform::Unknown);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_platform[row] != EolPlatform::Unknown;
            ++row)
        {
            EolPlatform platform = eol_utility::GetPlatform(text_lines[row], StrLen(text_lines[row]));
            CHECK_TRUE(platform == text_lines_platform[row]);
        }
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            u"\n", u"\n\n", u"\n\n\n",
            u"\r\n", u"\r\n\r\n", u"\r\n\r\n\r\n",
            u"\r", u"\r\r", u"\r\r\r",
            u"\n\n\r", u"\r\n\n", u"\n\r\n",
            u"\n\r\n\n", u"\n\n\r\n",

            u"A\n", u"A\n\n", u"A\n\n\n",
            u"A\r\n", u"A\r\n\r\n", u"A\r\n\r\n\r\n",
            u"A\r", u"A\r\r", u"A\r\r\r",
            u"A\n\n\r", u"A\r\n\n", u"A\n\r\n",
            u"A\n\r\n\n", u"A\n\n\r\n",

            u"\nA", u"\n\nA", u"\n\n\nA",
            u"\r\nA", u"\r\n\r\nA", u"\r\n\r\n\r\nA",
            u"\rA", u"\r\rA", u"\r\r\rA",
            u"\n\n\rA", u"\r\n\nA", u"\n\r\nA",
            u"\n\r\n\nA", u"\n\n\r\nA",

            u"A\nA", u"A\n\nA", u"A\n\n\nA",
            u"A\r\nA", u"A\r\n\r\nA", u"A\r\n\r\n\r\nA",
            u"A\rA", u"A\r\rA", u"A\r\r\rA",
            u"A\n\n\rA", u"A\r\n\nA", u"A\n\r\nA",
            u"A\n\r\n\nA", u"A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetPlatform(u"", 0) == EolPlatform::Unknown);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_platform[row] != EolPlatform::Unknown;
            ++row)
        {
            EolPlatform platform = eol_utility::GetPlatform(text_lines[row], StrLen(text_lines[row]));
            CHECK_TRUE(platform == text_lines_platform[row]);
        }
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            U"\n", U"\n\n", U"\n\n\n",
            U"\r\n", U"\r\n\r\n", U"\r\n\r\n\r\n",
            U"\r", U"\r\r", U"\r\r\r",
            U"\n\n\r", U"\r\n\n", U"\n\r\n",
            U"\n\r\n\n", U"\n\n\r\n",

            U"A\n", U"A\n\n", U"A\n\n\n",
            U"A\r\n", U"A\r\n\r\n", U"A\r\n\r\n\r\n",
            U"A\r", U"A\r\r", U"A\r\r\r",
            U"A\n\n\r", U"A\r\n\n", U"A\n\r\n",
            U"A\n\r\n\n", U"A\n\n\r\n",

            U"\nA", U"\n\nA", U"\n\n\nA",
            U"\r\nA", U"\r\n\r\nA", U"\r\n\r\n\r\nA",
            U"\rA", U"\r\rA", U"\r\r\rA",
            U"\n\n\rA", U"\r\n\nA", U"\n\r\nA",
            U"\n\r\n\nA", U"\n\n\r\nA",

            U"A\nA", U"A\n\nA", U"A\n\n\nA",
            U"A\r\nA", U"A\r\n\r\nA", U"A\r\n\r\n\r\nA",
            U"A\rA", U"A\r\rA", U"A\r\r\rA",
            U"A\n\n\rA", U"A\r\n\nA", U"A\n\r\nA",
            U"A\n\r\n\nA", U"A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetPlatform(U"", 0) == EolPlatform::Unknown);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_platform[row] != EolPlatform::Unknown;
            ++row)
        {
            EolPlatform platform = eol_utility::GetPlatform(text_lines[row], StrLen(text_lines[row]));
            CHECK_TRUE(platform == text_lines_platform[row]);
        }
    }
}

TEST_MEMBER_FUNCTION(EolUtility, GetBufferCount, char_type_ptr_size_t_size_t_ref)
{
    typedef std::size_t size_type;

    // Expected string length for lines containing unix '\n' characters.
    // NOTE: '\r' for Other character will have the same string length.
    size_type const text_lines_unix_count[] =
    {
        1, 2, 3,
        1, 2, 3,
        1, 2, 3,
        3, 2, 2,
        3, 3,

        2, 3, 4,
        2, 3, 4,
        2, 3, 4,
        4, 3, 3,
        4, 4,

        2, 3, 4,
        2, 3, 4,
        2, 3, 4,
        4, 3, 3,
        4, 4,

        3, 4, 5,
        3, 4, 5,
        3, 4, 5,
        5, 4, 4,
        5, 5,

        0
    };

    // Expected string length for lines containing unix "\r\n" characters.
    size_type const text_lines_win_count[] =
    {
        2, 4, 6,
        2, 4, 6,
        2, 4, 6,
        6, 4, 4,
        6, 6,

        3, 5, 7,
        3, 5, 7,
        3, 5, 7,
        7, 5, 5,
        7, 7,

        3, 5, 7,
        3, 5, 7,
        3, 5, 7,
        7, 5, 5,
        7, 7,

        4, 6, 8,
        4, 6, 8,
        4, 6, 8,
        8, 6, 6,
        8, 8,

        0
    };

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetBufferCount("", 0, EolPlatform::Windows) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount("", 0, EolPlatform::Unix) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount("", 0, EolPlatform::Other) == 0);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_unix_count[row] != 0;
            ++row)
        {
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_unix_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::GetBufferCount(line, len, EolPlatform::Unix);
            CHECK_TRUE(count == line_count);
            count = eol_utility::GetBufferCount(line, len, EolPlatform::Other);
            CHECK_TRUE(count == line_count);
        }

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_win_count[row] != 0;
            ++row)
        {
            char_type const* line = text_lines[row];
            size_type line_count = text_lines_win_count[row];
            size_type len = StrLen(line);
            size_type count = eol_utility::GetBufferCount(line, len, EolPlatform::Windows);
            CHECK_TRUE(count == line_count);
        }
    }

    {
        typedef wchar_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            L"\n", L"\n\n", L"\n\n\n",
            L"\r\n", L"\r\n\r\n", L"\r\n\r\n\r\n",
            L"\r", L"\r\r", L"\r\r\r",
            L"\n\n\r", L"\r\n\n", L"\n\r\n",
            L"\n\r\n\n", L"\n\n\r\n",

            L"A\n", L"A\n\n", L"A\n\n\n",
            L"A\r\n", L"A\r\n\r\n", L"A\r\n\r\n\r\n",
            L"A\r", L"A\r\r", L"A\r\r\r",
            L"A\n\n\r", L"A\r\n\n", L"A\n\r\n",
            L"A\n\r\n\n", L"A\n\n\r\n",

            L"\nA", L"\n\nA", L"\n\n\nA",
            L"\r\nA", L"\r\n\r\nA", L"\r\n\r\n\r\nA",
            L"\rA", L"\r\rA", L"\r\r\rA",
            L"\n\n\rA", L"\r\n\nA", L"\n\r\nA",
            L"\n\r\n\nA", L"\n\n\r\nA",

            L"A\nA", L"A\n\nA", L"A\n\n\nA",
            L"A\r\nA", L"A\r\n\r\nA", L"A\r\n\r\n\r\nA",
            L"A\rA", L"A\r\rA", L"A\r\r\rA",
            L"A\n\n\rA", L"A\r\n\nA", L"A\n\r\nA",
            L"A\n\r\n\nA", L"A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetBufferCount(L"", 0, EolPlatform::Windows) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount(L"", 0, EolPlatform::Unix) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount(L"", 0, EolPlatform::Other) == 0);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_unix_count[row] != 0;
            ++row)
        {
            size_type len = StrLen(text_lines[row]);
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Unix);
            CHECK_TRUE(count == text_lines_unix_count[row]);
            count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Other);
            CHECK_TRUE(count == text_lines_unix_count[row]);
        }

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_win_count[row] != 0;
            ++row)
        {
            size_type len = StrLen(text_lines[row]);
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Windows);
            CHECK_TRUE(count == text_lines_win_count[row]);
        }
    }

    {
        typedef char16_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            u"\n", u"\n\n", u"\n\n\n",
            u"\r\n", u"\r\n\r\n", u"\r\n\r\n\r\n",
            u"\r", u"\r\r", u"\r\r\r",
            u"\n\n\r", u"\r\n\n", u"\n\r\n",
            u"\n\r\n\n", u"\n\n\r\n",

            u"A\n", u"A\n\n", u"A\n\n\n",
            u"A\r\n", u"A\r\n\r\n", u"A\r\n\r\n\r\n",
            u"A\r", u"A\r\r", u"A\r\r\r",
            u"A\n\n\r", u"A\r\n\n", u"A\n\r\n",
            u"A\n\r\n\n", u"A\n\n\r\n",

            u"\nA", u"\n\nA", u"\n\n\nA",
            u"\r\nA", u"\r\n\r\nA", u"\r\n\r\n\r\nA",
            u"\rA", u"\r\rA", u"\r\r\rA",
            u"\n\n\rA", u"\r\n\nA", u"\n\r\nA",
            u"\n\r\n\nA", u"\n\n\r\nA",

            u"A\nA", u"A\n\nA", u"A\n\n\nA",
            u"A\r\nA", u"A\r\n\r\nA", u"A\r\n\r\n\r\nA",
            u"A\rA", u"A\r\rA", u"A\r\r\rA",
            u"A\n\n\rA", u"A\r\n\nA", u"A\n\r\nA",
            u"A\n\r\n\nA", u"A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetBufferCount(u"", 0, EolPlatform::Windows) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount(u"", 0, EolPlatform::Unix) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount(u"", 0, EolPlatform::Other) == 0);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_unix_count[row] != 0;
            ++row)
        {
            size_type len = StrLen(text_lines[row]);
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Unix);
            CHECK_TRUE(count == text_lines_unix_count[row]);
            count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Other);
            CHECK_TRUE(count == text_lines_unix_count[row]);
        }

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_win_count[row] != 0;
            ++row)
        {
            size_type len = StrLen(text_lines[row]);
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Windows);
            CHECK_TRUE(count == text_lines_win_count[row]);
        }
    }

    {
        typedef char32_t char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            U"\n", U"\n\n", U"\n\n\n",
            U"\r\n", U"\r\n\r\n", U"\r\n\r\n\r\n",
            U"\r", U"\r\r", U"\r\r\r",
            U"\n\n\r", U"\r\n\n", U"\n\r\n",
            U"\n\r\n\n", U"\n\n\r\n",

            U"A\n", U"A\n\n", U"A\n\n\n",
            U"A\r\n", U"A\r\n\r\n", U"A\r\n\r\n\r\n",
            U"A\r", U"A\r\r", U"A\r\r\r",
            U"A\n\n\r", U"A\r\n\n", U"A\n\r\n",
            U"A\n\r\n\n", U"A\n\n\r\n",

            U"\nA", U"\n\nA", U"\n\n\nA",
            U"\r\nA", U"\r\n\r\nA", U"\r\n\r\n\r\nA",
            U"\rA", U"\r\rA", U"\r\r\rA",
            U"\n\n\rA", U"\r\n\nA", U"\n\r\nA",
            U"\n\r\n\nA", U"\n\n\r\nA",

            U"A\nA", U"A\n\nA", U"A\n\n\nA",
            U"A\r\nA", U"A\r\n\r\nA", U"A\r\n\r\n\r\nA",
            U"A\rA", U"A\r\rA", U"A\r\r\rA",
            U"A\n\n\rA", U"A\r\n\nA", U"A\n\r\nA",
            U"A\n\r\n\nA", U"A\n\n\r\nA",

            nullptr
        };

        CHECK_TRUE(eol_utility::GetBufferCount(U"", 0, EolPlatform::Windows) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount(U"", 0, EolPlatform::Unix) == 0);
        CHECK_TRUE(eol_utility::GetBufferCount(U"", 0, EolPlatform::Other) == 0);

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_unix_count[row] != 0;
            ++row)
        {
            size_type len = StrLen(text_lines[row]);
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Unix);
            CHECK_TRUE(count == text_lines_unix_count[row]);
            count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Other);
            CHECK_TRUE(count == text_lines_unix_count[row]);
        }

        for (size_type row = 0;
            text_lines[row] != nullptr && text_lines_win_count[row] != 0;
            ++row)
        {
            size_type len = StrLen(text_lines[row]);
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Windows);
            CHECK_TRUE(count == text_lines_win_count[row]);
        }
    }
}

TEST_MEMBER_FUNCTION(EolUtility, ConvertEol, char_type_ptr_size_t_char_type_const_ptr_size_t_EolPlatform)
{
    typedef std::size_t size_type;

    TEST_OVERRIDE_ARGS("char_type*, size_type, char_type const*, size_type, EolPlatform");

    {
        typedef char char_type;
        typedef EolUtility<char_type, size_type> eol_utility;

        char_type const* text_lines[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r", "\r\r", "\r\r\r",
            "\n\n\r", "\r\n\n", "\n\r\n",
            "\n\r\n\n", "\n\n\r\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r", "A\r\r", "A\r\r\r",
            "A\n\n\r", "A\r\n\n", "A\n\r\n",
            "A\n\r\n\n", "A\n\n\r\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\rA", "\r\rA", "\r\r\rA",
            "\n\n\rA", "\r\n\nA", "\n\r\nA",
            "\n\r\n\nA", "\n\n\r\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\n\n\rA", "A\r\n\nA", "A\n\r\nA",
            "A\n\r\n\nA", "A\n\n\r\nA",

            "A\nA\nA", "A\nA\nA\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\nA\nA\rA", "A\r\nA\nA", "A\nA\r\nA",
            "A\nA\r\nA\nA", "A\nA\nA\r\nA",

            nullptr
        };

        /// Expected results when converted to Unix (\n) line endings.
        char_type const* text_lines_unix[] =
        {
            "\n", "\n\n", "\n\n\n",
            "\n", "\n\n", "\n\n\n",
            "\n", "\n\n", "\n\n\n",
            "\n\n\n", "\n\n", "\n\n",
            "\n\n\n", "\n\n\n",

            "A\n", "A\n\n", "A\n\n\n",
            "A\n", "A\n\n", "A\n\n\n",
            "A\n", "A\n\n", "A\n\n\n",
            "A\n\n\n", "A\n\n", "A\n\n",
            "A\n\n\n", "A\n\n\n",

            "\nA", "\n\nA", "\n\n\nA",
            "\nA", "\n\nA", "\n\n\nA",
            "\nA", "\n\nA", "\n\n\nA",
            "\n\n\nA", "\n\nA", "\n\nA",
            "\n\n\nA", "\n\n\nA",

            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\nA", "A\n\nA", "A\n\n\nA",
            "A\n\n\nA", "A\n\nA", "A\n\nA",
            "A\n\n\nA", "A\n\n\nA",

            "A\nA\nA", "A\nA\nA\nA",
            "A\nA\nA", "A\nA\nA\nA",
            "A\nA\nA", "A\nA\nA\nA",
            "A\nA\nA\nA", "A\nA\nA", "A\nA\nA",
            "A\nA\nA\nA", "A\nA\nA\nA",

            nullptr
        };

        char_type const* text_lines_other[] =
        {
            "\r", "\r\r", "\r\r\r",
            "\r", "\r\r", "\r\r\r",
            "\r", "\r\r", "\r\r\r",
            "\r\r\r", "\r\r", "\r\r",
            "\r\r\r", "\r\r\r",

            "A\r", "A\r\r", "A\r\r\r",
            "A\r", "A\r\r", "A\r\r\r",
            "A\r", "A\r\r", "A\r\r\r",
            "A\r\r\r", "A\r\r", "A\r\r",
            "A\r\r\r", "A\r\r\r",

            "\rA", "\r\rA", "\r\r\rA",
            "\rA", "\r\rA", "\r\r\rA",
            "\rA", "\r\rA", "\r\r\rA",
            "\r\r\rA", "\r\rA", "\r\rA",
            "\r\r\rA", "\r\r\rA",

            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\rA", "A\r\rA", "A\r\r\rA",
            "A\r\r\rA", "A\r\rA", "A\r\rA",
            "A\r\r\rA", "A\r\r\rA",

            "A\rA\rA", "A\rA\rA\rA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\rA\rA", "A\rA\rA\rA",
            "A\rA\rA\rA", "A\rA\rA", "A\rA\rA",
            "A\rA\rA\rA", "A\rA\rA\rA",

            nullptr
        };

        char_type const* text_lines_win[] =
        {
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r\n", "\r\n\r\n", "\r\n\r\n\r\n",
            "\r\n\r\n\r\n", "\r\n\r\n", "\r\n\r\n",
            "\r\n\r\n\r\n", "\r\n\r\n\r\n",

            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r\n", "A\r\n\r\n", "A\r\n\r\n\r\n",
            "A\r\n\r\n\r\n", "A\r\n\r\n", "A\r\n\r\n",
            "A\r\n\r\n\r\n", "A\r\n\r\n\r\n",

            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\r\nA", "\r\n\r\nA", "\r\n\r\n\r\nA",
            "\r\n\r\n\r\nA", "\r\n\r\nA", "\r\n\r\nA",
            "\r\n\r\n\r\nA", "\r\n\r\n\r\nA",

            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\r\nA", "A\r\n\r\nA", "A\r\n\r\n\r\nA",
            "A\r\n\r\n\r\nA", "A\r\n\r\nA", "A\r\n\r\nA",
            "A\r\n\r\n\r\nA", "A\r\n\r\n\r\nA",

            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\r\nA\r\nA", "A\r\nA\r\nA\r\nA",
            "A\r\nA\r\nA\r\nA", "A\r\nA\r\nA", "A\r\nA\r\nA",
            "A\r\nA\r\nA\r\nA", "A\r\nA\r\nA\r\nA",

            nullptr
        };

        size_type const MAX_BUFFER = 100;
        char_type buffer[MAX_BUFFER];

        CHECK_TRUE(eol_utility::ConvertEol(buffer, MAX_BUFFER, "", 0, EolPlatform::Windows) == 0);
        CHECK_TRUE(eol_utility::ConvertEol(buffer, MAX_BUFFER, "", 0, EolPlatform::Unix) == 0);
        CHECK_TRUE(eol_utility::ConvertEol(buffer, MAX_BUFFER, "", 0, EolPlatform::Other) == 0);

        for (size_type row = 0; text_lines[row] != nullptr; ++row)
        {
            char_type const* line = text_lines[row];
            size_type len = StrLen(line);


            // Test Unix (\n) converted line endings.
            char_type const* expected_line = text_lines_unix[row];
            size_type expected_count = StrLen(expected_line);

            // Firstly check the calculated buffer count matches the length of the line from text_lines_unix.
            size_type count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Unix);
            CHECK_EQUAL(count, expected_count);

            // Check that the converted line matches the line within text_lines_unix.
            size_type converted = eol_utility::ConvertEol(buffer, count, line, len, EolPlatform::Unix);
            buffer[converted] = eol_utility::CHAR_NULL;
            CHECK_EQUAL(converted, expected_count);
            CHECK_EQUAL(StrCmp(buffer, expected_line), 0);


            // Test Other (\r) converted line endings.
            expected_line = text_lines_other[row];
            expected_count = StrLen(expected_line);

            // Firstly check the calculated buffer count matches the length of the line from text_lines_unix.
            count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Other);
            CHECK_EQUAL(count, expected_count);

            // Check that the converted line matches the line within text_lines_unix.
            converted = eol_utility::ConvertEol(buffer, count, line, len, EolPlatform::Other);
            buffer[converted] = eol_utility::CHAR_NULL;
            CHECK_EQUAL(converted, expected_count);
            CHECK_EQUAL(StrCmp(buffer, expected_line), 0);


            // Test Windows (\r\n) converted line endings.
            expected_line = text_lines_win[row];
            expected_count = StrLen(expected_line);

            // Firstly check the calculated buffer count matches the length of the line from text_lines_unix.
            count = eol_utility::GetBufferCount(text_lines[row], len, EolPlatform::Windows);
            CHECK_EQUAL(count, expected_count);

            // Check that the converted line matches the line within text_lines_unix.
            converted = eol_utility::ConvertEol(buffer, count, line, len, EolPlatform::Windows);
            buffer[converted] = eol_utility::CHAR_NULL;
            CHECK_EQUAL(converted, expected_count);
            CHECK_EQUAL(StrCmp(buffer, expected_line), 0);
        }
    }
}
