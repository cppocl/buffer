/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferAdvance.hpp"
#include <cstdint>

using ocl::InternalBufferAdvance;

typedef unsigned char type;
typedef std::uint64_t size_type;
typedef InternalBufferAdvance<type, size_type> buffer_advance_type;

TEST_MEMBER_FUNCTION(InternalBufferAdvance, Advance, type_ptr_size_type_size_type)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_type, size_type");

    type buffer[2] = { 1U, 2U };
    type* ptr = buffer;
    size_type ptr_size = 2;
    buffer_advance_type::Advance(ptr, ptr_size, 1);
    CHECK_EQUAL(*ptr, 2U);
    CHECK_EQUAL(ptr_size, 1U);
}

TEST_MEMBER_FUNCTION(InternalBufferAdvance, Advance, type_ptr_size_type_type_ptr_size_type_size_type)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_type, unsigned char*, size_type, size_type");

    type buffer1[2] = { 1U, 2U };
    type buffer2[2] = { 3U, 4U };
    type* ptr1 = buffer1;
    size_type ptr1_size = 2;
    type* ptr2 = buffer2;
    size_type ptr2_size = 2;
    buffer_advance_type::Advance(ptr1, ptr1_size, ptr2, ptr2_size, 1);
    CHECK_EQUAL(*ptr1, 2U);
    CHECK_EQUAL(ptr1_size, 1U);
    CHECK_EQUAL(*ptr2, 4U);
    CHECK_EQUAL(ptr2_size, 1U);
}

TEST_MEMBER_FUNCTION(InternalBufferAdvance, Advance, type_ptr_size_typetype_ptr_size_typetype_ptr_size_type_size_type)
{
    TEST_OVERRIDE_ARGS("unsigned char*, size_type, unsigned char*, size_type, unsigned char*, size_type, size_type");

    type buffer1[2] = { 1U, 2U };
    type buffer2[2] = { 3U, 4U };
    type buffer3[2] = { 5U, 6U };
    type* ptr1 = buffer1;
    size_type ptr1_size = 2;
    type* ptr2 = buffer2;
    size_type ptr2_size = 2;
    type* ptr3 = buffer3;
    size_type ptr3_size = 2;
    buffer_advance_type::Advance(ptr1, ptr1_size, ptr2, ptr2_size, ptr3, ptr3_size, 1);
    CHECK_EQUAL(*ptr1, 2U);
    CHECK_EQUAL(ptr1_size, 1U);
    CHECK_EQUAL(*ptr2, 4U);
    CHECK_EQUAL(ptr2_size, 1U);
    CHECK_EQUAL(*ptr3, 6U);
    CHECK_EQUAL(ptr3_size, 1U);
}
