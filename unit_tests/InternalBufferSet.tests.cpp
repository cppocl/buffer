/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferSet.hpp"
#include <cstdint>

using ocl::InternalBufferSet;

namespace
{
    template<typename Type, typename SizeType>
    inline void Set(Type* buffer, SizeType count, Type value)
    {
        Type* end = buffer + count;
        for (Type* curr = buffer; curr < end; ++curr)
            *curr = value;
    }

    template<typename Type>
    inline bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }

    template<typename Type, size_t const size>
    inline size_t CountOf(Type(&)[size])
    {
        return size;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_bool)
{
    TEST_OVERRIDE_ARGS("void*, size_type, bool");

    typedef bool type;

    type const min_value    = false;
    type const max_value    = true;
    std::size_t const total = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, true);
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, false);
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_char)
{
    TEST_OVERRIDE_ARGS("void*, size_type, char");

    typedef char type;

    bool const is_signed    = static_cast<char>(-1) < static_cast<char>(0);
    type const min_value    = static_cast<type>(is_signed ? -128 : 0);
    type const max_value    = static_cast<type>(is_signed ? 127 : 255);
    type const mixed_value  = static_cast<type>('A');
    std::size_t const total = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_wchar_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, wchar_t");

    typedef wchar_t type;

    type const min_value    = static_cast<type>(L' ');
    type const max_value    = static_cast<type>(L'~');
    type const mixed_value  = static_cast<type>(L'A');
    std::size_t const total = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_int8_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, int8_t");

    typedef std::int8_t type;

    type const min_value    = static_cast<type>(-128);
    type const max_value    = static_cast<type>(127);
    type const mixed_value  = static_cast<type>(0x7d);
    std::size_t const total = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_uint8_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, uint8_t");

    typedef std::uint8_t type;

    type const min_value    = static_cast<type>(0);
    type const max_value    = static_cast<type>(255);
    type const mixed_value  = static_cast<type>(0x7d);
    std::size_t const total = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_int16_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, int16_t");

    typedef std::int16_t type;

    type const min_value   = static_cast<type>(-0x7fff-1);
    type const max_value   = static_cast<type>(0x7fff);
    type const mixed_value = static_cast<type>(0x71a3);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_uint16_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, uint16_t");

    typedef std::uint16_t type;

    type const min_value   = static_cast<type>(0);
    type const max_value   = static_cast<type>(0xffff);
    type const mixed_value = static_cast<type>(0x71a3);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_int32_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, int32_t");

    typedef std::int32_t type;

    type const min_value   = static_cast<type>(-0x7fffffff-1);
    type const max_value   = static_cast<type>(0x7fffffff);
    type const mixed_value = static_cast<type>(0x71a3c52d);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_uint32_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, uint32_t");

    typedef std::uint32_t type;

    type const min_value   = static_cast<type>(0);
    type const max_value   = static_cast<type>(0xffffffff);
    type const mixed_value = static_cast<type>(0x71a3c52d);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_int64_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, int64_t");

    typedef std::int64_t type;

    type const min_value   = static_cast<type>(-0x7fffffffffffffff-1);
    type const max_value   = static_cast<type>(0x7fffffffffffffff);
    type const mixed_value = static_cast<type>(0x71a3c52d6b4e89f0);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_uint64_t)
{
    TEST_OVERRIDE_ARGS("void*, size_type, uint64_t");

    typedef std::uint64_t type;

    type const min_value   = static_cast<type>(0);
    type const max_value   = static_cast<type>(0xffffffffffffffff);
    type const mixed_value = static_cast<type>(0x71a3c52d6b4e89f0);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferSet, Set, void_ptr_size_type_float)
{
    TEST_OVERRIDE_ARGS("void*, size_type, float");

    typedef float type;

    type const min_value   = static_cast<type>(FLT_MIN);
    type const max_value   = static_cast<type>(FLT_MAX);
    type const mixed_value = static_cast<type>(FLT_MAX / 2);
    std::size_t const total    = sizeof(void*) * 3U;
    type buffer[total];
    type cmp[total];

    for (std::size_t count = 1; count < total; ++count)
    {
        // Test minimum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, min_value);
        InternalBufferSet<type>::Set(buffer, count, min_value);
        bool is_same = Compare(buffer, cmp, count);
        CHECK_TRUE(is_same);
        if (!is_same)
        {
            Set(buffer, total, static_cast<type>(1));
            Set(cmp, total, min_value);
            InternalBufferSet<type>::Set(buffer, count, min_value);
            is_same = Compare(buffer, cmp, count);
            CHECK_TRUE(is_same);
        }

        // Test maximum boundary.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, max_value);
        InternalBufferSet<type>::Set(buffer, count, max_value);
        CHECK_TRUE(Compare(buffer, cmp, count));

        // Test mixed bits.
        Set(buffer, total, static_cast<type>(1));
        Set(cmp, total, mixed_value);
        InternalBufferSet<type>::Set(buffer, count, mixed_value);
        CHECK_TRUE(Compare(buffer, cmp, count));
    }
}
