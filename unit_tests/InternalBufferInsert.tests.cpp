/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferInsert.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferInsert;

namespace
{
    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferInsert, Insert, type_ptr_size_type_size_type_type_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type, size_type");

    {
        char str[6] = "ello";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 0, 'H', 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hllo";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 1, 'e', 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hell";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 4, 'o', 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hell";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 3, 'o', 2);
        CHECK_TRUE(Compare(str, "Helool", 6));
    }

    {
        char str[6] = "Hell";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 4, 'o', 2);
        CHECK_TRUE(Compare(str, "Helloo", 6));
    }

    {
        char str[6] = "Hello";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 5, '!', 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 6, 5, '!', 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 6, 2, 'A', 3);
        CHECK_TRUE(Compare(str, "HeAAAl", 6));
    }

    {
        char str[6] = "ABCDE";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 2, 0, '!', 1);
        CHECK_TRUE(Compare(str, "!A", 2));

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 3, 1, '.', 1);
        CHECK_TRUE(Compare(str, "!.A", 3));

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 4, 0, ':', 1);
        CHECK_TRUE(Compare(str, ":!.A", 4));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferInsert, Insert, type_ptr_size_type_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type const*, size_type");

    {
        char str[6] = "ello";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 0, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hllo";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 1, "e", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hell";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 4, "o", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hell";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 3, "o.", 2);
        CHECK_TRUE(Compare(str, "Helo.l", 6));
    }

    {
        char str[6] = "Hell";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 4, "o.", 2);
        CHECK_TRUE(Compare(str, "Hello.", 6));
    }

    {
        char str[6] = "Hello";
        InternalBufferInsert<char, size_t>::Insert(str, 6, 5, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 6, 5, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 6, 2, "ABC", 3);
        CHECK_TRUE(Compare(str, "HeABCl", 6));
    }

    {
        char str[6] = "ABCDE";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 2, 0, "!", 1);
        CHECK_TRUE(Compare(str, "!A", 2));

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 3, 1, ".", 1);
        CHECK_TRUE(Compare(str, "!.A", 3));

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InternalBufferInsert<char, size_t>::Insert(str, 4, 0, ":", 1);
        CHECK_TRUE(Compare(str, ":!.A", 4));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferInsert, Insert, type_ptr_size_type_size_type_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, size_type");

    {
        char str[6] = "ello";
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 5, 0, 1);
        CHECK_EQUAL(ptr, &str[0]);
        CHECK_TRUE(Compare(str+1, "ello", 4U));
    }

    {
        char str[6] = "Hllo";
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 5, 1, 1);
        CHECK_EQUAL(ptr, &str[1]);
        CHECK_TRUE(Compare(str, "H", 1U));
        CHECK_TRUE(Compare(str+2, "llo", 3U));
    }

    {
        char str[6] = "Hell";
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 5, 4, 1);
        CHECK_EQUAL(ptr, &str[4]);
        CHECK_TRUE(Compare(str, "Hell", 1U));
    }

    {
        char str[6] = "Hell";
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 6, 3, 2);
        CHECK_EQUAL(ptr, &str[3]);
        CHECK_TRUE(Compare(str, "Hel", 3U));
        CHECK_TRUE(Compare(str+5, "l", 1U));
    }

    {
        char str[6] = "Hell";
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 5, 4, 2);
        CHECK_EQUAL(ptr, &str[4]);
    }

    {
        char str[6] = "Hello";
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 5, 5, 1);
        CHECK_EQUAL(ptr, &str[5]);
        CHECK_TRUE(Compare(str, "Hell", 4U));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 6, 5, 1);
        CHECK_EQUAL(ptr, &str[5]);
        CHECK_TRUE(Compare(str, "Hello", 5U));
    }

    {
        char str[6] = "ABCDE";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 2, 0, 1);
        CHECK_EQUAL(ptr, &str[0]);
        CHECK_TRUE(Compare(str + 1, "A", 1U));
    }

    {
        char str[6] = "ABCDE";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 3, 1, 1);
        CHECK_EQUAL(ptr, &str[1]);
        CHECK_TRUE(Compare(str, "A", 1U));
        CHECK_TRUE(Compare(str + 2, "B", 1U));
    }

    {
        char str[6] = "ABCDE";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        char* ptr = InternalBufferInsert<char, size_t>::Insert(str, 4, 0, 1);
        CHECK_EQUAL(ptr, &str[0]);
        CHECK_TRUE(Compare(str + 1, "A", 1U));
    }
}
