/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferPrependMove.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferPrependMove;

namespace
{
    struct Data
    {
        int  i;
        char c;

        Data& operator=(Data const& src) noexcept
        {
            i = src.i;
            c = src.c;
            return *this;
        }

        Data& operator=(Data&& src) noexcept
        {
            i = src.i;
            c = src.c;
            src.i = 0;
            src.c = '\0';
            return *this;
        }

        bool operator ==(Data const& other) const noexcept
        {
            return (i == other.i) && (c == other.c);
        }

        bool operator !=(Data const& other) const noexcept
        {
            return (i != other.i) || (c != other.c);
        }
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        for (Type const* end = a + count; a < end; ++a, ++b)
            if (*a != *b)
                return false;
        return true;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferPrependMove, PrependMove, type_ptr_size_type_type_ptr_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type*, type*, size_type");

    {
        typedef char type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependMove<type, size_t>::PrependMove(str, 6, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;

        type src[5] = "llo";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependMove<type, size_t>::PrependMove(str, 6, src, "He", 2);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;

        type src[6] = "ABCDE";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependMove<type, size_t>::PrependMove(str, 6, src, "Hello", 6);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef Data type;

        type src[5]   = {{2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value = {1, 'A'};
        InternalBufferPrependMove<type, size_t>::PrependMove(dest, 6, src, &value, 1);
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }

    {
        typedef Data type;

        type src[6]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[6]   = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value[6] = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        InternalBufferPrependMove<type, size_t>::PrependMove(dest, 6, src, value, 6);
        CHECK_FALSE(Compare(src, empty, 6)); // src won't change as there is nothing to move.
        CHECK_TRUE(Compare(dest, cmp, 6));
        CHECK_TRUE(Compare(value, empty, 6));
    }
}

TEST_MEMBER_FUNCTION(InternalBufferPrependMove, PrependMove, type_ptr_size_type_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type*, size_type");

    {
        typedef char type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependMove<type, size_t>::PrependMove(str, 6, src, 1);
        CHECK_TRUE(Compare(str + 1, "ello", 5));
    }

    {
        typedef char type;

        type src[5] = "llo";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InternalBufferPrependMove<type, size_t>::PrependMove(str, 6, src, 2);
        CHECK_TRUE(Compare(str + 2, "llo", 4));
    }

    {
        typedef Data type;

        type src[5]   = {{2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        InternalBufferPrependMove<type, size_t>::PrependMove(dest, 6, src, 1);
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(dest + 1, cmp + 1, 4));
    }

    {
        typedef Data type;

        type src[6]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[6]   = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        InternalBufferPrependMove<type, size_t>::PrependMove(dest, 6, src, 6);
        CHECK_FALSE(Compare(src, empty, 6)); // src won't change as there is nothing to move.
    }
}
