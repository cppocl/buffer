/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../Memory.hpp"
#include "../InsecureBufferUtility.hpp"
#include <type_traits>
#include <cstddef>
#include <limits>
#include <string.h>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

using ocl::Memory;
using ocl::InsecureBufferUtility;

namespace
{
    struct Data
    {
        std::uint64_t i;
        double d;

        Data(std::uint64_t i_, double d_)
            : i(i_)
            , d(d_)
        {
        }

        Data()
        {
        }

        Data(Data const& src)
            : i(src.i)
            , d(src.d)
        {
        }

        Data(Data&& src)
            : i(src.i)
            , d(src.d)
        {
            src.i = 0;
            src.d = 0.0;
        }

        Data& operator =(Data const& src) noexcept
        {
            i = src.i;
            d = src.d;
            return *this;
        }
    };

    template<typename Type, typename SizeType>
    void Set(Type* ptr, SizeType size, Type value)
    {
        for (Type* end = ptr + size; ptr < end; ++ptr)
            *ptr = value;
    }
}

TEST_MEMBER_FUNCTION(Memory, constructor, NA)
{
    TEST_OVERRIDE_FUNCTION_NAME("Memory");

    {
        typedef char type;
        Memory<type, std::size_t> alloc;
        CHECK_NULL(static_cast<type*>(alloc));
    }
}

TEST_MEMBER_FUNCTION(Memory, constructor, size_t)
{
    TEST_OVERRIDE_FUNCTION_NAME("Memory");

    {
        typedef char type;
        Memory<type, std::size_t> alloc(1U);
        CHECK_NOT_NULL(static_cast<type*>(alloc));
    }
}

TEST_MEMBER_FUNCTION(Memory, Free, NA)
{
    Memory<char, std::size_t> alloc(1U);
    CHECK_NOT_NULL(static_cast<char*>(alloc));
    alloc.Free();
    CHECK_NULL(static_cast<char*>(alloc));
    CHECK_EQUAL(alloc.GetSize(), 0U);
}

TEST_MEMBER_FUNCTION(Memory, GetSize, NA)
{
    Memory<char, std::size_t> alloc(1U);
    CHECK_EQUAL(alloc.GetSize(), 1U);
}

TEST_MEMBER_FUNCTION(Memory, GetMaxSize, NA)
{
    Memory<char, std::size_t> alloc(1U);
    constexpr std::size_t max_size = std::numeric_limits<std::size_t>::max();
    CHECK_EQUAL(alloc.GetMaxSize(), max_size);
}

TEST_MEMBER_FUNCTION(Memory, GetMemSize, NA)
{
    Memory<char, std::size_t> alloc(1U);
    CHECK_EQUAL(alloc.GetMemSize(), 1U + sizeof(std::size_t) + sizeof(char*));
}

TEST_MEMBER_FUNCTION(Memory, SetSize, size_t)
{
    TEST_OVERRIDE_ARGS("size_t");

    typedef char type;
    typedef std::size_t size_type;

    Memory<type, size_type> alloc(1U);
    CHECK_EQUAL(alloc.GetSize(), 1U);

    type* ptr = alloc.Ptr();
    alloc.SetSize(2U);
    type* new_ptr = alloc.Ptr();
    bool is_same = ptr == new_ptr;
    CHECK_FALSE(is_same);
    CHECK_NOT_EQUAL(ptr, new_ptr);
    CHECK_EQUAL(alloc.GetSize(), 2U);
}

TEST_MEMBER_FUNCTION(Memory, Swap, type)
{
    typedef char type;
    typedef std::size_t size_type;

    Memory<type, size_type> alloc1(1U);
    Set(static_cast<type*>(alloc1), alloc1.GetSize(), 'A');
    CHECK_EQUAL(alloc1[0], 'A');

    Memory<type, size_type> alloc2(2U);
    Set(static_cast<type*>(alloc2), alloc2.GetSize(), 'B');
    CHECK_EQUAL(alloc2[0], 'B');
    CHECK_EQUAL(alloc2[1], 'B');

    alloc1.Swap(alloc2);
    CHECK_EQUAL(alloc1.GetSize(), 2U);
    CHECK_EQUAL(alloc2.GetSize(), 1U);
    CHECK_EQUAL(alloc1[0], 'B');
    CHECK_EQUAL(alloc1[1], 'B');
    CHECK_EQUAL(alloc2[0], 'A');
}

TEST_MEMBER_FUNCTION(Memory, Move, type)
{
    typedef char type;
    typedef std::size_t size_type;

    Memory<type, size_type> alloc1(1U);
    Set(static_cast<type*>(alloc1), alloc1.GetSize(), 'A');
    CHECK_EQUAL(alloc1[0], 'A');

    Memory<type, size_type> alloc2(2U);
    alloc2.Move(alloc1);
    CHECK_EQUAL(alloc1.GetSize(), 0U);
    CHECK_EQUAL(alloc2.GetSize(), 1U);
    CHECK_EQUAL(alloc2[0], 'A');
}

TEST_MEMBER_FUNCTION(Memory, Detach, NA)
{
    // Also tests Attach function.

    typedef char type;
    typedef std::size_t size_type;

    Memory<type, size_type> alloc(1U);
    type* ptr = alloc.Detach();
    CHECK_NOT_NULL(ptr);
    CHECK_NULL(static_cast<type*>(alloc));

    // Do the attach to free the memory and test test attach works.
    alloc.Attach(ptr, 1U);
    CHECK_NOT_NULL(static_cast<type*>(alloc));
}

TEST_MEMBER_FUNCTION(Memory, Detach, size_type_ref)
{
    TEST_OVERRIDE_ARGS("size_t&");

    typedef char type;
    typedef std::size_t size_type;

    Memory<type, size_type> alloc(1U);
    size_type size = 0;
    type* ptr = alloc.Detach(size);
    CHECK_NOT_NULL(ptr);
    CHECK_EQUAL(size, 1U);
    CHECK_NULL(static_cast<type*>(alloc));

    // Do the attach to free the memory.
    alloc.Attach(ptr, 1U);
    CHECK_NOT_NULL(static_cast<type*>(alloc));
}
