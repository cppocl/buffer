/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferOverlapCopy.hpp"
#include <cstring>
#include <cstddef>

using ocl::InternalBufferOverlapCopy;

namespace
{
    struct Data
    {
        int  i;
        char c;
    };

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        return ::memcmp(a, b, count * sizeof(Type)) == 0;
    }
}

TEST_MEMBER_FUNCTION(InternalBufferOverlapCopy, OverlapCopy, type_ptr_type_const_ptr_size_t)
{
    size_t const size   = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type source[size] = {str1, str2, str3, str4};
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef wchar_t type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef float type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef long double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef Data type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InternalBufferOverlapCopy<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    // Test overlapping of destination buffer > source buffer.
    {
        typedef char type;
        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'A', 'B', 'A', 'B', 'C', 'D'};
        InternalBufferOverlapCopy<type>::OverlapCopy(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {1, 2, 1, 2, 3, 4};
        InternalBufferOverlapCopy<type>::OverlapCopy(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    // Test overlapping of destination buffer < source buffer.
    {
        typedef char type;
        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'C', 'D', 'E', 'F', 'E', 'F'};
        InternalBufferOverlapCopy<type>::OverlapCopy(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {3, 4, 5, 6, 5, 6};
        InternalBufferOverlapCopy<type>::OverlapCopy(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }
}
