/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../InsecureBufferUtility.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>

using ocl::InsecureBufferUtility;

namespace
{
    struct Data
    {
        int  i;
        char c;

        Data& operator=(Data const& src) noexcept
        {
            i = src.i;
            c = src.c;
            return *this;
        }

        Data& operator=(Data&& src) noexcept
        {
            i = src.i;
            c = src.c;
            src.i = 0;
            src.c = '\0';
            return *this;
        }

        bool operator ==(Data const& other) const noexcept
        {
            return (i == other.i) && (c == other.c);
        }

        bool operator !=(Data const& other) const noexcept
        {
            return (i != other.i) || (c != other.c);
        }
    };

    template<typename Type>
    void Copy(Type* dest, Type const* src, size_t count)
    {
        ::memcpy(dest, src, count * sizeof(Type));
    }

    template<typename Type>
    bool Compare(Type const* a, Type const* b, size_t count)
    {
        for (Type const* end = a + count; a < end; ++a, ++b)
            if (*a != *b)
                return false;
        return true;
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Copy, type_ptr_type_const_ptr_size_t)
{
    size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type source[size] = {str1, str2, str3, str4};
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef wchar_t type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef float type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef long double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef Data type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Copy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Move, type_ptr_type_const_ptr_size_t)
{
    size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type source[size] = {str1, str2, str3, str4};
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_NULL(source[0]);
        CHECK_NULL(source[1]);
        CHECK_NULL(source[2]);
        CHECK_NULL(source[3]);
        CHECK_EQUAL(*dest[0], '1');
        CHECK_EQUAL(*dest[1], '2');
        CHECK_EQUAL(*dest[2], '3');
        CHECK_EQUAL(*dest[3], '4');
    }

    {
        typedef wchar_t type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef float type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef long double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        typedef char* type_ptr;
        type_ptr dest_ptr[size];
        type_ptr source_ptr[size];
        type source[size];
        source[0] = 'A';
        source[1] = 'B';
        source[2] = 'C';
        source[3] = 'D';
        source_ptr[0] = &source[0];
        source_ptr[1] = &source[1];
        source_ptr[2] = &source[2];
        source_ptr[3] = &source[3];
        ::memset(dest_ptr, 0, sizeof(dest_ptr));
        InsecureBufferUtility<type_ptr>::Move(dest_ptr, source_ptr, size);
        CHECK_NULL(source_ptr[0]);
        CHECK_NULL(source_ptr[1]);
        CHECK_NULL(source_ptr[2]);
        CHECK_NULL(source_ptr[3]);
        CHECK_EQUAL(*dest_ptr[0], 'A');
        CHECK_EQUAL(*dest_ptr[1], 'B');
        CHECK_EQUAL(*dest_ptr[2], 'C');
        CHECK_EQUAL(*dest_ptr[3], 'D');
    }

    {
        typedef Data type;
        type dest[size];
        type source[size];
        source[0].i = 1;
        source[0].c = 'A';
        source[1].i = 2;
        source[1].c = 'B';
        source[2].i = 3;
        source[2].c = 'C';
        source[3].i = 4;
        source[3].c = 'D';
        InsecureBufferUtility<type>::Move(dest, source, size);
        CHECK_FALSE(Compare(dest, source, size)); // Move should clear source.
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Erase, type_ptr_size_type_size_type_size_type)
{
    // type* buffer, size_type size, size_type position, size_type count
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, size_type");

    {
        typedef char type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 0, 1);
        CHECK_TRUE(Compare(buffer, "ello", 5));
    }

    {
        typedef char type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 4, 1);
        CHECK_TRUE(Compare(buffer, "Hell", 5));
    }

    {
        typedef char type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 5, 1);
        CHECK_TRUE(Compare(buffer, "Hello", 5));
    }

    {
        typedef char type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 0, 2);
        CHECK_TRUE(Compare(buffer, "llo", 4));
    }

    {
        typedef char type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = "Hello";
        std::size_t size = ::strlen(buffer) + 1;
        buffer_erase_type::Erase(buffer, size, 3, 2);
        CHECK_TRUE(Compare(buffer, "Hel", 4));
    }

    {
        typedef std::uint16_t type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = {1234U, 2345U, 3456U, 4567U, 5678U, 0U};
        type expected_buffer[] = {2345U, 3456U, 4567U, 5678U, 0U};
        std::size_t size = 6U;
        buffer_erase_type::Erase(buffer, size, 0, 1);
        CHECK_TRUE(Compare(buffer, expected_buffer, 5));
    }

    {
        typedef std::uint16_t type;
        typedef InsecureBufferUtility<type> buffer_erase_type;

        type buffer[] = {1234U, 2345U, 3456U, 4567U, 5678U, 0U};
        type expected_buffer[] = {1234U, 2345U, 3456U, 4567U, 0U};
        std::size_t size = 6U;
        buffer_erase_type::Erase(buffer, size, 4, 1);
        CHECK_TRUE(Compare(buffer, expected_buffer, 5));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Insert, type_ptr_size_type_size_type_type_const_ptr_size_type)
{
    // type* dest, size_type dest_size, size_type position, type const* to_insert, size_type to_insert_size
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type const*, size_type");

    {
        char str[6] = "ello";
        InsecureBufferUtility<char, size_t>::Insert(str, 5, 0, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hllo";
        InsecureBufferUtility<char, size_t>::Insert(str, 5, 1, "e", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hell";
        InsecureBufferUtility<char, size_t>::Insert(str, 5, 4, "o", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "Hell";
        InsecureBufferUtility<char, size_t>::Insert(str, 6, 3, "o.", 2);
        CHECK_TRUE(Compare(str, "Helo.l", 6));
    }

    {
        char str[6] = "Hell";
        InsecureBufferUtility<char, size_t>::Insert(str, 5, 4, "o.", 2);
        CHECK_TRUE(Compare(str, "Hello.", 6));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InsecureBufferUtility<char, size_t>::Insert(str, 5, 5, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        char str[6] = "Hello";

        // Check that even though str is specified as 5 in size, to copy is still performed.
        InsecureBufferUtility<char, size_t>::Insert(str, 6, 5, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, InsertCopy, type_ptr_size_type_type_const_ptr_type_const_ptr_size_type)
{
    // type* dest, size_type position, type const* src, size_type src_size, type const* to_insert, size_type to_insert_size
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type");

    {
        char src[5] = "ello";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::InsertCopy(str, 5, 0, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "Hllo";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::InsertCopy(str, 5, 1, src, "e", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "Hell";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::InsertCopy(str, 5, 4, src, "o", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = {'H','e','l','l','o'};
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::InsertCopy(str, 5, 5, src, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, InsertMove, type_ptr_size_type_type_ptr_size_type_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type*, size_type, type*, size_type");

    {
        typedef char type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<type, size_t>::InsertMove(str, 6, 0, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<type, size_t>::InsertMove(str, 6, 0, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef Data type;

        type src[5]   = {{2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value = {1, 'A'};
        InsecureBufferUtility<type, size_t>::InsertMove(dest, 6, 0, src, &value, 1);
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }

    {
        typedef char type;

        type src[5] = "Hell";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<type, size_t>::InsertMove(str, 6, 4, src, "o", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = {'H','e','l','l','o'};
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::InsertMove(str, 6, 5, src, "!", 1);
        CHECK_TRUE(Compare(str, "Hello!", 6));
    }

    {
        typedef Data type;

        type src[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type empty[5] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type dest[6];
        type value = {6, 'F'};
        InsecureBufferUtility<type, size_t>::InsertMove(dest, 6, 5, src, &value, 1);
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Prepend, type_ptr_size_type_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, size_type, type const*, size_type");

    {
        char str[6] = "ello";
        InsecureBufferUtility<char, size_t>::Prepend(str, 6, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "llo";
        InsecureBufferUtility<char, size_t>::Prepend(str, 6, "He", 2);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char str[6] = "ABCDE";
        InsecureBufferUtility<char, size_t>::Prepend(str, 6, "Hello", 6);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, PrependCopy, type_ptr_size_type_type_const_ptr_size_type_type_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type const*, size_type, type const*, size_type");

    {
        char src[5] = "ello";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::PrependCopy(str, 6, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[5] = "llo";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::PrependCopy(str, 6, src, "He", 2);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        char src[6] = "ABCDE";
        char str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<char, size_t>::PrependCopy(str, 6, src, "Hello", 6);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, PrependMove, type_ptr_size_type_type_ptr_size_type_type_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("type*, size_type, type*, size_type, type*, size_type");

    {
        typedef char type;

        type src[5] = "ello";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<type, size_t>::PrependMove(str, 6, src, "H", 1);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;

        type src[5] = "llo";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<type, size_t>::PrependMove(str, 6, src, "He", 2);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef char type;

        type src[6] = "ABCDE";
        type str[6];
        ::memset(str, 0, sizeof(str));
        InsecureBufferUtility<type, size_t>::PrependMove(str, 6, src, "Hello", 6);
        CHECK_TRUE(Compare(str, "Hello", 6));
    }

    {
        typedef Data type;

        type src[5]   = {{2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[5]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value = {1, 'A'};
        InsecureBufferUtility<type, size_t>::PrependMove(dest, 6, src, &value, 1);
        CHECK_TRUE(Compare(src, empty, 5));
        CHECK_TRUE(Compare(dest, cmp, 5));
        CHECK_TRUE(Compare(&value, &empty[0], 1));
    }

    {
        typedef Data type;

        type src[6]   = {{1, 'A'} , {2, 'B'} , {3, 'C'} , {4, 'D'} , {5, 'E'} , {0, '\0'}};
        type cmp[6]   = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        type dest[6]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type empty[6] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type value[6] = {{11, 'F'}, {12, 'G'}, {13, 'H'}, {14, 'I'}, {15, 'J'}, {0, '\0'}};
        InsecureBufferUtility<type, size_t>::PrependMove(dest, 6, src, value, 6);
        CHECK_FALSE(Compare(src, empty, 6)); // src won't change as there is nothing to move.
        CHECK_TRUE(Compare(dest, cmp, 6));
        CHECK_TRUE(Compare(value, empty, 6));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, ReverseCopy, type_ptr_type_const_ptr_size_t)
{
    size_t const copy_size = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.
    size_t const size = copy_size + offset;
    // Note that the total buffer is size + offset.

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef char type;
        type buffer[size] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size] = {'A', 'B', 'A', 'B', 'C', 'D'};
        InsecureBufferUtility<type>::ReverseCopy(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }

    {
        typedef signed int type;
        type buffer[size] = {1, 2, 3, 4, 5, 6};
        type cmp[size] = {1, 2, 1, 2, 3, 4};
        InsecureBufferUtility<type>::ReverseCopy(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, OverlapCopy, type_ptr_type_const_ptr_size_t)
{
    size_t const size = 4;

    TEST_OVERRIDE_ARGS("type*, type const*, size_t");

    {
        typedef bool type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type source[size] = {str1, str2, str3, str4};
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef wchar_t type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef float type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef long double type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }

    {
        typedef Data type;
        type dest[size];
        type source[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(source, 1, sizeof(source));
        InsecureBufferUtility<type>::OverlapCopy(dest, source, size);
        CHECK_TRUE(Compare(dest, source, size));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, ReverseMove, type_ptr_type_ptr_size_t)
{
    size_t const copy_size = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.
    size_t const size = copy_size + offset;
    // Note that the total buffer is size + offset.

    TEST_OVERRIDE_ARGS("type*, type*, size_t");

    {
        typedef char type;
        type buffer[size] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size]    = {'A', 'B', 'A', 'B', 'C', 'D'};
        InsecureBufferUtility<type>::ReverseMove(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }

    {
        typedef signed int type;
        type buffer[size] = {1, 2, 3, 4, 5, 6};
        type cmp[size]    = {1, 2, 1, 2, 3, 4};
        InsecureBufferUtility<type>::ReverseMove(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }

    {
        typedef Data type;
        type buffer[size] = {{1, 'A'},  {2, 'B'},  {3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}};
        type cmp[size]    = {{0, '\0'}, {0, '\0'}, {1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}};
        InsecureBufferUtility<type>::ReverseMove(buffer + offset, buffer, copy_size);
        CHECK_TRUE(Compare(buffer, cmp, size));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, OverlapMove, type_ptr_type_ptr_size_t)
{
    size_t const size   = 4; // Size of buffer to copy.
    size_t const offset = 2; // Offset into buffer for copying.

    TEST_OVERRIDE_ARGS("type*, type*, size_t");

    {
        typedef bool type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef char type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {

        typedef char const* type;
        char const* str1 = "1";
        char const* str2 = "2";
        char const* str3 = "3";
        char const* str4 = "4";
        type dest[size] = {NULL, NULL, NULL, NULL};
        type src[size] = {str1, str2, str3, str4};
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef wchar_t type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed char type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned char type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed short type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned short type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef signed long long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef unsigned long long int type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef float type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef double type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef long double type;
        type dest[size];
        type src[size];
        ::memset(dest, 0, sizeof(dest));
        ::memset(src, 1, sizeof(src));
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, src, size));
    }

    {
        typedef Data type;
        type dest[size]  = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        type src[size]   = {{1, 'A'},  {2, 'B'},  {3, 'C'},  {4, 'D'}};
        type cmp[size]   = {{1, 'A'},  {2, 'B'},  {3, 'C'},  {4, 'D'}};
        type empty[size] = {{0, '\0'}, {0, '\0'}, {0, '\0'}, {0, '\0'}};
        InsecureBufferUtility<type>::OverlapMove(dest, src, size);
        CHECK_TRUE(Compare(dest, cmp, size));
        CHECK_TRUE(Compare(src, empty, size));
    }

    // Test overlapping of destination buffer > source buffer.
    {
        typedef char type;
        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'A', 'B', 'A', 'B', 'C', 'D'};
        InsecureBufferUtility<type>::OverlapMove(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {1, 2, 1, 2, 3, 4};
        InsecureBufferUtility<type>::OverlapMove(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef Data type;
        type buffer[size + offset] = {{1, 'A'},  {2, 'B'},  {3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}};
        type cmp[size + offset]    = {{0, '\0'}, {0, '\0'}, {1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}};
        InsecureBufferUtility<type>::OverlapMove(buffer + offset, buffer, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    // Test overlapping of destination buffer < source buffer.
    {
        typedef char type;
        type buffer[size + offset] = {'A', 'B', 'C', 'D', 'E', 'F'};
        type cmp[size + offset]    = {'C', 'D', 'E', 'F', 'E', 'F'};
        InsecureBufferUtility<type>::OverlapMove(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef signed int type;
        type buffer[size + offset] = {1, 2, 3, 4, 5, 6};
        type cmp[size + offset]    = {3, 4, 5, 6, 5, 6};
        InsecureBufferUtility<type>::OverlapMove(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }

    {
        typedef Data type;
        type buffer[size + offset] = {{1, 'A'}, {2, 'B'}, {3, 'C'}, {4, 'D'}, {5, 'E'},  {6, 'F'}};
        type cmp[size + offset]    = {{3, 'C'}, {4, 'D'}, {5, 'E'}, {6, 'F'}, {0, '\0'}, {0, '\0'}};
        InsecureBufferUtility<type>::OverlapMove(buffer, buffer + offset, size);
        CHECK_TRUE(Compare(buffer, cmp, size + offset));
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Swap, type_ref_type_ref)
{
    TEST_OVERRIDE_ARGS("type&, type&");

    {
        typedef char type;
        type value1 = 'A';
        type value2 = 'B';
        InsecureBufferUtility<type>::Swap(value1, value2);
        CHECK_EQUAL(value1, 'B');
        CHECK_EQUAL(value2, 'A');
    }

    {
        typedef char type;
        type value1 = 'A';
        type value2 = 'B';
        type* value1_ptr = &value1;
        type* value2_ptr = &value2;
        InsecureBufferUtility<type*>::Swap(value1_ptr, value2_ptr);
        CHECK_EQUAL(value1, 'A');
        CHECK_EQUAL(value2, 'B');
        CHECK_EQUAL(value1_ptr, &value2);
        CHECK_EQUAL(value2_ptr, &value1);
    }
}

TEST_MEMBER_FUNCTION(InsecureBufferUtility, Swap, type_ref_size_t_type_ref_size_t)
{
    TEST_OVERRIDE_ARGS("type&, size_t&, type&, size_t&");

    {
        typedef char type;
        type value1 = 'A';
        type value2 = 'B';
        std::size_t value1_size = 1U;
        std::size_t value2_size = 1U;
        InsecureBufferUtility<type, std::size_t>::Swap(value1, value1_size, value2, value2_size);
        CHECK_EQUAL(value1, 'B');
        CHECK_EQUAL(value2, 'A');
        CHECK_EQUAL(value1_size, 1U);
        CHECK_EQUAL(value2_size, 1U);
    }

    {
        typedef char type;
        type value1[] = "AB";
        type value2[] = "CDE";
        type* value1_ptr = value1;
        type* value2_ptr = value2;
        std::size_t value1_size = 2U;
        std::size_t value2_size = 3U;
        InsecureBufferUtility<type*, std::size_t>::Swap(value1_ptr, value1_size, value2_ptr, value2_size);
        CHECK_EQUAL(value1[0], 'A');
        CHECK_EQUAL(value1[1], 'B');
        CHECK_EQUAL(value2[0], 'C');
        CHECK_EQUAL(value2[1], 'D');
        CHECK_EQUAL(value2[2], 'E');
        CHECK_EQUAL(value1_ptr, value2);
        CHECK_EQUAL(value2_ptr, value1);
        CHECK_EQUAL(value1_size, 3U);
        CHECK_EQUAL(value2_size, 2U);
    }
}

TEST_FUNCTION(InsecureCompare, type_ref_size_t_type_ref_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, type const*, size_t");

    typedef std::size_t size_type;

    {
        typedef char type;

        size_type const size = 6;
        type buffer1[size];
        type buffer2[size];
        Copy(buffer1, "Hello", size);
        Copy(buffer2, "Hello", size);
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), 0);

        buffer1[0] = 'h';
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), 1);

        buffer1[0] = 'H';
        buffer2[0] = 'h';
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), -1);
    }

    {
        typedef uint16_t type;

        // Check all elements are the same.
        size_type const size = 5;
        type buffer1[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), 0);
    }

    {
        typedef uint16_t type;

        // Check first element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xfe00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), -1);
    }

    {
        typedef uint16_t type;

        // Check last element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0e};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), -1);
    }

    {
        typedef uint16_t type;

        // Check first element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xff10, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), 1);
    }

    {
        typedef uint16_t type;

        // Check last element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xff10, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0e};
        CHECK_EQUAL(ocl::InsecureCompare(buffer1, buffer2, size), 1);
    }
}

TEST_FUNCTION(InsecureCompareEqual, type_ref_size_t_type_ref_size_t)
{
    TEST_OVERRIDE_ARGS("type const*, type const*, size_t");

    typedef std::size_t size_type;

    {
        typedef char type;

        size_type const size = 6;
        type buffer1[size];
        type buffer2[size];
        Copy(buffer1, "Hello", size);
        Copy(buffer2, "Hello", size);
        CHECK_TRUE(ocl::InsecureCompareEqual(buffer1, buffer2, size));

        buffer1[0] = 'h';
        CHECK_FALSE(ocl::InsecureCompareEqual(buffer1, buffer2, size));

        buffer1[0] = 'H';
        buffer2[0] = 'h';
        CHECK_FALSE(ocl::InsecureCompareEqual(buffer1, buffer2, size));
    }

    {
        typedef uint16_t type;

        // Check all elements are the same.
        size_type const size = 5;
        type buffer1[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_TRUE(ocl::InsecureCompareEqual(buffer1, buffer2, size));
    }

    {
        typedef uint16_t type;

        // Check first element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xfe00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_FALSE(ocl::InsecureCompareEqual(buffer1, buffer2, size));
    }

    {
        typedef uint16_t type;

        // Check last element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0e};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_FALSE(ocl::InsecureCompareEqual(buffer1, buffer2, size));
    }

    {
        typedef uint16_t type;

        // Check first element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xff10, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        CHECK_FALSE(ocl::InsecureCompareEqual(buffer1, buffer2, size));
    }

    {
        typedef uint16_t type;

        // Check last element difference is identified.
        size_type const size = 5;
        type buffer1[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f0f};
        type buffer2[size] = {0xff00, 0x00ff, 0xfefe, 0xf0f0, 0x0f1f};
        CHECK_FALSE(ocl::InsecureCompareEqual(buffer1, buffer2, size));
    }
}
