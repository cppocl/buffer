/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_COPYFUNCTOR_HPP
#define OCL_GUARD_BUFFER_COPYFUNCTOR_HPP

#include "internal/InternalBufferCopy.hpp"

namespace ocl
{

template<typename Type, typename SizeType>
struct CopyFunctor
{
    typedef InternalBufferCopy<Type, SizeType> buffer_copy_utility_type;

    inline void operator()(Type* dest, Type const* src, SizeType count) const
    {
        buffer_copy_utility_type::Copy(dest, src, count);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_COPYFUNCTOR_HPP
