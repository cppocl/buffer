/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSET_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSET_HPP

#include "internalBufferTypes.hpp"
#include "internalBufferCopySet.hpp"
#include <cstring>
#include <cstddef>
#include <cstdint>

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = std::size_t>
class InternalBufferSet
{
// Types.
public:
    typedef          Type                               type;
    typedef typename InternalBufferTypes<type>::in_type in_type;
    typedef          SizeType                           size_type;

// Static member functions.
public:
    /// Set count number of elements from source to dest.
    /// source and dest must not be null.
    static void Set(type* buffer, size_type count, in_type value)
    {
        InternalBufferCopySet<type, size_type>::CopySet(buffer, buffer + count, value);
    }
};

template<typename SizeType>
class InternalBufferSet<char, SizeType>
{
// Types.
public:
    typedef          char                               type;
    typedef typename InternalBufferTypes<type>::in_type in_type;
    typedef          SizeType                           size_type;

// Static member functions.
public:
    static void Set(type* buffer, size_type count, in_type value)
    {
        ::memset(buffer, static_cast<int>(value), count);
    }
};

template<typename SizeType>
class InternalBufferSet<signed char, SizeType>
{
// Types.
public:
    typedef          signed char                        type;
    typedef typename InternalBufferTypes<type>::in_type in_type;
    typedef          SizeType                           size_type;

// Static member functions.
public:
    static void Set(type* buffer, size_type count, in_type value)
    {
        ::memset(buffer, static_cast<int>(value), count);
    }
};

template<typename SizeType>
class InternalBufferSet<unsigned char, SizeType>
{
// Types.
public:
    typedef          unsigned char                      type;
    typedef typename InternalBufferTypes<type>::in_type in_type;
    typedef          SizeType                           size_type;

// Static member functions.
public:
    static void Set(type* buffer, size_type count, in_type value)
    {
        ::memset(buffer, static_cast<int>(value), count);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSET_HPP
