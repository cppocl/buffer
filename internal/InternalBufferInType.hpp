/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINTYPE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINTYPE_HPP

#include "internalBufferConstTypes.hpp"
#include "internalBufferPointerTypes.hpp"
#include "internalBufferRefTypes.hpp"
#include "internalBufferPrimitiveTypes.hpp"

namespace ocl
{

template<typename Type>
struct InternalBufferInType
{
private:
    // Default is not primitive and not pointer.
    template<typename T, bool is_primitive, bool is_pointer>
    struct InType
    {
        typedef T const& in_type;
    };

    template<typename T>
    struct InType<T, true, true> // primitive + pointer.
    {
        typedef T const in_type;
    };

    template<typename T>
    struct InType<T, true, false>  // primitive.
    {
        typedef T in_type;
    };

    template<typename T>
    struct InType<T, false, true>  // pointer.
    {
        typedef T const in_type;
    };

    typedef typename InternalBufferRefTypes<Type>::no_ref_type no_ref_type;
    typedef typename InternalBufferConstTypes<no_ref_type>::no_const_type no_const_no_ref_type;

    static bool const is_primitive = InternalBufferPrimitiveTypes<no_const_no_ref_type>::is_primitive;
    static bool const is_pointer   = InternalBufferPointerTypes<no_const_no_ref_type>::is_pointer;

public:
    typedef Type type;

    typedef typename InType<no_const_no_ref_type, is_primitive, is_pointer>::in_type in_type;
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINTYPE_HPP
