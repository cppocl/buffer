/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOUNT_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOUNT_HPP

#include "internalBufferFind.hpp"

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = std::size_t>
class InternalBufferCount
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Count values that are sequentially the same from the start of first and second buffers.
    static SizeType CountSame(Type const* first,
                              Type const* second,
                              SizeType size)
    {
        Type const* first_start = first;
        Type const* first_end = first + size;

        while ((first < first_end) && (*first == *second))
        {
            ++first;
            ++second;
        }

        return static_cast<SizeType>(first - first_start);
    }

    /// Count values that are sequentially the same from the start of first and second buffers.
    static SizeType CountSame(Type const* first,
                              SizeType first_size,
                              Type const* second,
                              SizeType second_size)
    {
        SizeType size = first_size < second_size ? first_size : second_size;
        Type const* first_start = first;
        Type const* first_end = first + size;

        while ((first < first_end) && (*first == *second))
        {
            ++first;
            ++second;
        }

        return static_cast<SizeType>(first - first_start);
    }

    /// Count the differences between two buffers and return the count for each buffer.
    static void CountDifferent(Type const* first,
                               SizeType first_size,
                               Type const* second,
                               SizeType second_size,
                               SizeType& first_difference_count,
                               SizeType& second_difference_count)
    {
        typedef InternalBufferFind<Type, SizeType> find_type;

        Type const* found_second = find_type::FindMatch(first, first_size, second, second_size);
        Type const* found_first = find_type::FindMatch(second, second_size, first, first_size);

        first_difference_count = found_first != nullptr ? static_cast<SizeType>(found_first - first) : first_size;
        second_difference_count = found_second != nullptr ? static_cast<SizeType>(found_second - second) : second_size;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOUNT_HPP
