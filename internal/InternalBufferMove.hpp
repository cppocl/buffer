/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERMOVE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERMOVE_HPP

#include "internalBufferRefTypes.hpp"
#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fast move of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = size_t>
class InternalBufferMove
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

/// Types (internal use only.
private:
    typedef typename InternalBufferRefTypes<type>::move_ref_type move_ref_type;

// Static member functions.
public:
    /// Move count number of elements from src to dest.
    /// src and dest must not be null.
    static void Move(type* dest, type* src, size_type count)
    {
        type const* src_end = src + count;
        while (src < src_end)
        {
            *dest = static_cast<move_ref_type>(*src);
            ++dest;
            ++src;
        }
    }
};

template<typename Type, typename SizeType>
class InternalBufferMove<Type*, SizeType>
{
// Types.
public:
    typedef Type* type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
        ::memset(src, 0, count * sizeof(type)); // null all the moved pointers.
    }
};

template<typename SizeType>
class InternalBufferMove<bool, SizeType>
{
// Types.
public:
    typedef bool type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<char, SizeType>
{
// Types.
public:
    typedef char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferMove<wchar_t, SizeType>
{
// Types.
public:
    typedef wchar_t type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<signed char, SizeType>
{
// Types.
public:
    typedef signed char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferMove<unsigned char, SizeType>
{
// Types.
public:
    typedef unsigned char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferMove<signed short, SizeType>
{
// Types.
public:
    typedef signed short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<unsigned short, SizeType>
{
// Types.
public:
    typedef unsigned short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<signed int, SizeType>
{
// Types.
public:
    typedef signed int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<unsigned int, SizeType>
{
// Types.
public:
    typedef unsigned int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<signed long int, SizeType>
{
// Types.
public:
    typedef signed long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<unsigned long int, SizeType>
{
// Types.
public:
    typedef unsigned long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<signed long long int, SizeType>
{
// Types.
public:
    typedef signed long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<unsigned long long int, SizeType>
{
// Types.
public:
    typedef unsigned long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<float, SizeType>
{
// Types.
public:
    typedef float type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<double, SizeType>
{
// Types.
public:
    typedef double type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferMove<long double, SizeType>
{
// Types.
public:
    typedef long double type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Move(type* dest, type* src, size_type count)
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERMOVE_HPP
