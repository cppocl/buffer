/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERREVERSECOPY_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERREVERSECOPY_HPP

#include <cstring>
#include <cstddef>

namespace ocl
{

/// Copy buffers from the end to the start.
template<typename Type, typename SizeType = size_t>
class InternalBufferReverseCopy
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Copy count number of elements from source to dest.
    /// source and dest must not be null.
    static void ReverseCopy(type* dest, type const* source, size_type count)
    {
        type const* source_last = source + count - 1;
        type*       dest_last   = dest   + count - 1;

        while (source_last >= source)
            *dest_last-- = *source_last--;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERREVERSECOPY_HPP
