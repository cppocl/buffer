/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERERASE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERERASE_HPP

#include "internalBufferMove.hpp"

namespace ocl
{

/// Support erasing from a buffer, either by updating an existing buffer
/// or by moving existing content to a destination buffer without moving the erased part.
template<typename Type, typename SizeType = size_t>
class InternalBufferErase
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Erase a number of elements at a given position.
    /// position + count must less than size,
    /// and size - position - count must be greater than 0.
    static void Erase(type* buffer,
                      size_type size,
                      size_type position,
                      size_type count)
    {
        size_type move_count = size - position - count;
        type* buffer_start = buffer + position;
        InternalBufferMove<type, size_type>::Move(buffer_start, buffer_start + count, move_count);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERERASE_HPP
