/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPREPEND_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPREPEND_HPP

#include "internalBufferCopy.hpp"
#include "internalBufferOverlapMove.hpp"

namespace ocl
{

/// Support prepending into a buffer, either by updating an existing buffer
/// or by moving existing content to a destination buffer as part of the prepend.
template<typename Type, typename SizeType = size_t>
class InternalBufferPrepend
{
/// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

/// Static member functions.
public:
    /// Prepend to_prepend buffer into start of destination buffer.
    /// to_prepend and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to take the elements to be prepended.
    static void Prepend(type* dest,                // destination buffer which takes the prepend.
                        size_type dest_size,       // current size of dest buffer in elements.
                        type const* to_prepend,    // elements to be prepended into dest.
                        size_type to_prepend_size) // number of elements to prepend from to_prepend.
    {
        typedef InternalBufferOverlapMove<type, size_type> overlap_move_type;
        typedef InternalBufferCopy<type, size_type> copy_type;

        overlap_move_type::OverlapMove(dest + to_prepend_size,
                                       dest,
                                       dest_size - to_prepend_size);

        copy_type::Copy(dest, to_prepend, to_prepend_size);
    }

    /// Prepend space into start of buffer.
    /// dest must not be null.
    /// dest must be big enough to take the space.
    static void Prepend(type* dest,                 // destination buffer which takes the prepended space.
                        size_type dest_size,        // current size of dest buffer in elements.
                        size_type space_to_prepend) // number of elements to prepend.
    {
        typedef InternalBufferOverlapMove<type, size_type> overlap_move_type;
        typedef InternalBufferCopy<type, size_type> copy_type;

        overlap_move_type::OverlapMove(dest + space_to_prepend,
                                       dest,
                                       dest_size - space_to_prepend);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPREPEND_HPP
