/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOPYSAME_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOPYSAME_HPP

#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = std::size_t>
class InternalBufferCopySame
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Copy count number of elements from src to dest.
    /// src and dest must not be null.
    static void CopySame(type* dest,
                         size_type dest_size,
                         type const* src1,
                         size_type src1_size,
                         type const* src2,
                         size_type src2_size,
                         size_type& bytes_copied)
    {
        size_type size = (dest_size < src1_size) ?
                         (dest_size < src2_size ? dest_size : src2_size) :
                         (src1_size < src2_size ? src1_size : src2_size);

        type const* src1_start = src1;
        type const* src1_end = src1 + size;
        while ((src1 < src1_end) && (*src1 == *src2))
        {
            *dest = *src1;
            ++dest;
            ++src1;
            ++src2;
        }
        bytes_copied = static_cast<size_type>(src1 - src1_start);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOPYSAME_HPP
