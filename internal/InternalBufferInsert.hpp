/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINSERT_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINSERT_HPP

#include "internalBufferSet.hpp"
#include "internalBufferCopy.hpp"
#include "internalBufferOverlapMove.hpp"
#include "internalBufferInType.hpp"

namespace ocl
{

/// Support inserting into a buffer, either by updating an existing buffer
/// or by moving existing content to a destination buffer as part of the insert.
template<typename Type, typename SizeType = size_t>
class InternalBufferInsert
{
/// Types.
public:
    typedef Type type;
    typedef SizeType size_type;
    typedef typename InternalBufferInType<type>::in_type in_type;

/// Static member functions.
public:
    /// Insert src buffer into position at destination buffer.
    /// to_insert and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to take the elements to be inserted,
    /// and position must be less than dest_size.
    static void Insert(type* dest,               // destination buffer which takes the insertion.
                       size_type dest_size,      // current size of dest buffer in elements.
                       size_type position,       // position to insert src into dest.
                       in_type to_insert,        // element to be inserted into dest.
                       size_type to_insert_size) // number of elements to insert from src.
    {
        typedef InternalBufferOverlapMove<type, size_type> overlap_move_type;
        typedef InternalBufferSet<type, size_type> set_type;

        type* insert_dest = dest + position; // Position of pointer for insert.
        if (position + to_insert_size < dest_size)
            overlap_move_type::OverlapMove(insert_dest + to_insert_size,
                                           insert_dest,
                                           dest_size - position - to_insert_size);

        set_type::Set(insert_dest, to_insert_size, to_insert);
    }

    /// Insert src buffer into position at destination buffer.
    /// to_insert and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to take the elements to be inserted,
    /// and position must be less than dest_size.
    static void Insert(type* dest,               // destination buffer which takes the insertion.
                       size_type dest_size,      // current size of dest buffer in elements.
                       size_type position,       // position to insert src into dest.
                       type const* to_insert,    // elements to be inserted into dest.
                       size_type to_insert_size) // number of elements to insert from src.
    {
        typedef InternalBufferOverlapMove<type, size_type> overlap_move_type;
        typedef InternalBufferCopy<type, size_type> copy_type;

        type* insert_dest = dest + position; // Position of pointer for insert.
        if (position + to_insert_size < dest_size)
            overlap_move_type::OverlapMove(insert_dest + to_insert_size,
                                           insert_dest,
                                           dest_size - position - to_insert_size);

        copy_type::Copy(insert_dest, to_insert, to_insert_size);
    }

    /// Insert space for elements into the buffer and
    // return the pointer at the start of the inserted space.
    static type* Insert(type* buffer,              // destination buffer which takes the insertion.
                        size_type buffer_size,     // current size of dest buffer in elements.
                        size_type position,        // position to insert src into dest.
                        size_type space_to_insert) // number of elements to insert into dest.
    {
        typedef InternalBufferOverlapMove<type, size_type> overlap_move_type;

        type* insert_buffer = buffer + position; // Position of pointer for insert.
        if (position + space_to_insert < buffer_size)
            overlap_move_type::OverlapMove(insert_buffer + space_to_insert,
                                           insert_buffer,
                                           buffer_size - position - space_to_insert);

        return insert_buffer;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINSERT_HPP
