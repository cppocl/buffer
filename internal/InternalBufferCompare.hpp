/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOMPARE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOMPARE_HPP

#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = std::size_t>
class InternalBufferCompare
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Compare first against second and return -1 if first is less than second,
    /// 1 if first is greater than second, or 0 if they are the same.
    /// first and second must not be null.
    static int Compare(type const* first, type const* second, size_type count)
    {
        int cmp = 0;
        type const* first_end = first + count;
        while (first < first_end)
        {
            if (*first < *second)
            {
                cmp = -1;
                break;
            }
            if (*second < *first)
            {
                cmp = 1;
                break;
            }
            ++first;
            ++second;
        }

        return cmp;
    }
};

template<typename Type, typename SizeType>
class InternalBufferCompare<Type*, SizeType>
{
// Types.
public:
    typedef Type* type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<bool, SizeType>
{
// Types.
public:
    typedef bool type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<char, SizeType>
{
// Types.
public:
    typedef char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferCompare<wchar_t, SizeType>
{
// Types.
public:
    typedef wchar_t type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<signed char, SizeType>
{
// Types.
public:
    typedef signed char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferCompare<unsigned char, SizeType>
{
// Types.
public:
    typedef unsigned char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferCompare<signed short, SizeType>
{
// Types.
public:
    typedef signed short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<unsigned short, SizeType>
{
// Types.
public:
    typedef unsigned short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<signed int, SizeType>
{
// Types.
public:
    typedef signed int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<unsigned int, SizeType>
{
// Types.
public:
    typedef unsigned int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<signed long int, SizeType>
{
// Types.
public:
    typedef signed long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<unsigned long int, SizeType>
{
// Types.
public:
    typedef unsigned long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<signed long long int, SizeType>
{
// Types.
public:
    typedef signed long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCompare<unsigned long long int, SizeType>
{
// Types.
public:
    typedef unsigned long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static int Compare(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type));
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOMPARE_HPP
