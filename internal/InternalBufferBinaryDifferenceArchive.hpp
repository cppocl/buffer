/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERBINARYDIFFERENCEARCHIVE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERBINARYDIFFERENCEARCHIVE_HPP

#include "internalBufferAdvance.hpp"
#include "internalBufferCopySame.hpp"
#include "internalBufferCount.hpp"
#include "internalBufferSetCount.hpp"
#include "../../typeinfo/TypeLimits.hpp"
#include "../../typeinfo/TypeSigned.hpp"
#include "../../typeinfo/TypeIncrement.hpp"

namespace ocl
{

/// Create or extract a buffer storing the archive of two buffers as a difference.
/// The format of the archive is:
///
///    1 byte - number of bytes required to store a count (will be value 1, 2, 4 or 8).
///    1 byte - count for matching values (0..n).
///    0..n   - the matching value.
///    1 byte - count for differences in first buffer  (0..n).
///    0..n   - the different values for the first buffer.
///    1 byte - count for differences in second buffer  (0..n).
///    0..n   - the different values for the second buffer.
///
/// Type must be signed char or unsigned char, as the difference algorithm only works with
/// types matching the number of bits specified by CHAR_BIT.
template<typename Type, typename SizeType = std::size_t>
class InternalBufferBinaryDifferenceArchive
{
public:
    typedef Type type;
    typedef SizeType size_type;

private:
    typedef TypeLimits<size_type> type_limits_type;
    typedef TypeIncrement<size_type, bool> type_increment_type;

public:
    static constexpr size_type GetByteCount(size_type value) noexcept
    {
        return type_limits_type::GetByteCount(value);
    }

    /// Count number of bytes required to store the archive buffer for differences of two buffers.
    /// If the buffers are too big to fit into size_type then 0 is returned.
    static size_type GetDifferenceCount(type const* buffer1,
                                        size_type buffer1_size,
                                        type const* buffer2,
                                        size_type buffer2_size,
                                        size_type byte_count = 0);

    /// Create a buffer that stores a difference as an archive of the two buffers.
    /// Return the number of bytes stored in the output buffer.
    /// If the created buffer is bigger than the value that can be stored in a size_type
    /// then 0 is returned.
    static size_type CreateDifference(type const* buffer1, size_type buffer1_size,
                                      type const* buffer2, size_type buffer2_size,
                                      type* output_buffer, size_type output_buffer_size,
                                      size_type byte_count = 0);

    /// Calculate the two buffer sizes required to extract the archive buffer containing
    /// the differences, If there any parsing problems buffer1_size and buffer2_size will be 0.
    static void GetBufferSizes(type const* archive_buffer,
                               size_type archive_buffer_size,
                               size_type& buffer1_size,
                               size_type& buffer2_size);

    /// Extract the differences from the archive buffer into two buffers, buffer1 and buffer2.
    static bool CreateBuffers(type const* archive_buffer,
                              size_type archive_buffer_size,
                              type* buffer1,
                              size_type buffer1_size,
                              type* buffer2,
                              size_type buffer2_size,
                              size_type& buffer1_output_size,
                              size_type& buffer2_output_size);

private:
    typedef InternalBufferAdvance<type, size_type> buffer_advance_type;
    typedef InternalBufferCount<type, size_type> buffer_count_type;
    typedef InternalBufferSetCount<type, size_type> buffer_set_count;

    /// Copy same bytes and set count for output buffer.
    /// If the output buffer is not big enough to store all the same bytes, the return is false.
    static bool SetSameBytes(size_type byte_count,
                             type*& output_buffer,
                             size_type& output_buffer_size,
                             type const*& buffer1,
                             size_type& buffer1_size,
                             type const*& buffer2,
                             size_type& buffer2_size,
                             size_type& bytes_set,
                             bool first_compare);

    /// Update output_buffer with a copy of bytes from both buffers where the bytes
    /// don't match.
    static bool SetDifferentBytes(size_type byte_count,
                                  unsigned char*& output_buffer,
                                  size_type& output_buffer_size,
                                  unsigned char const*& buffer1,
                                  size_type& buffer1_size,
                                  unsigned char const*& buffer2,
                                  size_type& buffer2_size,
                                  size_type& bytes_set);
};

#include "internalBufferBinaryDifferenceArchive.inl"

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERBINARYDIFFERENCEARCHIVE_HPP
