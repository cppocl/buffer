/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALCOUNTBYTEBITS_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALCOUNTBYTEBITS_HPP

#include <cstddef>

namespace ocl
{

template<typename SizeType = std::size_t>
class InternalCountByteBits
{
public:
    typedef SizeType size_type;

public:
    static inline size_type CountSetBits(unsigned char value) noexcept
    {
        static size_type constexpr const bit_count[16] =
        {
            0, //  0 = 0000
            1, //  1 = 0001
            1, //  2 - 0010
            2, //  3 = 0011
            1, //  4 = 0100
            2, //  5 = 0101
            2, //  6 = 0110
            3, //  7 = 0111
            1, //  8 = 1000
            2, //  9 = 1001
            2, // 10 = 1010
            3, // 11 = 1011
            2, // 12 = 1100
            3, // 13 = 1101
            3, // 14 = 1110
            4  // 15 = 1111
        };

        return bit_count[value & 0x0fU] + bit_count[value >> 4U];
    }

    static inline size_type CountClearedBits(unsigned char value) noexcept
    {
        static size_type const constexpr bit_count[16] =
        {
            4, //  0 = 0000
            3, //  1 = 0001
            3, //  2 - 0010
            2, //  3 = 0011
            3, //  4 = 0100
            2, //  5 = 0101
            2, //  6 = 0110
            1, //  7 = 0111
            3, //  8 = 1000
            2, //  9 = 1001
            2, // 10 = 1010
            1, // 11 = 1011
            2, // 12 = 1100
            1, // 13 = 1101
            1, // 14 = 1110
            0  // 15 = 1111
        };

        return bit_count[value & 0x0fU] + bit_count[value >> 4U];
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALCOUNTBYTEBITS_HPP
