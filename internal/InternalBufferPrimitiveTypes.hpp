/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPRIMITIVETYPES_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPRIMITIVETYPES_HPP

namespace ocl
{

template<typename Type>
struct InternalBufferPrimitiveTypes
{
    typedef Type type;

    static bool const is_primitive = false;
};

template<>
struct InternalBufferPrimitiveTypes<bool>
{
    typedef bool type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<char>
{
    typedef char type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<wchar_t>
{
    typedef wchar_t type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<signed char>
{
    typedef signed char type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<unsigned char>
{
    typedef unsigned char type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<signed short>
{
    typedef signed short type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<unsigned short>
{
    typedef unsigned short type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<signed int>
{
    typedef signed int type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<unsigned int>
{
    typedef unsigned int type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<signed long int>
{
    typedef signed long int type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<unsigned long int>
{
    typedef unsigned long int type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<signed long long int>
{
    typedef signed long long int type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<unsigned long long int>
{
    typedef unsigned long long int type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<float>
{
    typedef float type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<double>
{
    typedef double type;

    static bool const is_primitive = true;
};

template<>
struct InternalBufferPrimitiveTypes<long double>
{
    typedef long double type;

    static bool const is_primitive = true;
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPRIMITIVETYPES_HPP
