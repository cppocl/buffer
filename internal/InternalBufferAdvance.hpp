/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERADVANCE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERADVANCE_HPP

namespace ocl
{

/// Simple buffer helper class to reduce lines of repetitive buffer increments and size decrements.
template<typename Type, typename SizeType>
class InternalBufferAdvance
{
public:
    typedef Type type;
    typedef SizeType size_type;

public:
    /// Helper for increasing buffer pointer and decrease buffer size.
    static void Advance(type*& buffer, size_type& buffer_size, size_type by)
    {
        buffer += by;
        buffer_size -= by;
    }

    /// Helper for increasing buffer pointer and decrease buffer size.
    static void Advance(type const*& buffer, size_type& buffer_size, size_type by)
    {
        buffer += by;
        buffer_size -= by;
    }

    /// Helper for increasing buffer pointer and decrease buffer size.
    static void Advance(type*& buffer1, size_type& buffer1_size,
                        type*& buffer2, size_type& buffer2_size,
                        size_type by)
    {
        buffer1 += by;
        buffer1_size -= by;
        buffer2 += by;
        buffer2_size -= by;
    }

    /// Helper for increasing buffer pointer and decrease buffer size.
    static void Advance(type const*& buffer1, size_type& buffer1_size,
                        type const*& buffer2, size_type& buffer2_size,
                        size_type by)
    {
        buffer1 += by;
        buffer1_size -= by;
        buffer2 += by;
        buffer2_size -= by;
    }

    /// Helper for increasing buffer pointer and decrease buffer size.
    static void Advance(type*& buffer1, size_type& buffer1_size,
                        type*& buffer2, size_type& buffer2_size,
                        type*& buffer3, size_type& buffer3_size,
                        size_type by)
    {
        buffer1 += by;
        buffer1_size -= by;
        buffer2 += by;
        buffer2_size -= by;
        buffer3 += by;
        buffer3_size -= by;
    }

    /// Helper for increasing buffer pointer and decrease buffer size.
    static void Advance(type const*& buffer1, size_type& buffer1_size,
                        type const*& buffer2, size_type& buffer2_size,
                        type const*& buffer3, size_type& buffer3_size,
                        size_type by)
    {
        buffer1 += by;
        buffer1_size -= by;
        buffer2 += by;
        buffer2_size -= by;
        buffer3 += by;
        buffer3_size -= by;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERADVANCE_HPP
