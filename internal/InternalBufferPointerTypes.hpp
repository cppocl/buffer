/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPOINTERTYPES_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPOINTERTYPES_HPP

namespace ocl
{

template<typename Type>
struct InternalBufferPointerTypes
{
    // Non-const pointer type.
    typedef Type  type;

    // remove one level of pointer.
    typedef Type  remove_pointer_type;

    // Non-const pointer type.
    typedef Type* pointer_type;

    // All const and * removed down to raw type.
    typedef Type  no_pointer_type;

    static bool const is_pointer = false;
};

template<typename Type>
struct InternalBufferPointerTypes<Type*>
{
    // Non-const pointer type.
    typedef Type* type;

    // remove one level of pointer.
    typedef Type remove_pointer_type;

    // Non-const pointer type. (this could be Type**, Type*const*, Type***, etc.)
    typedef Type* pointer_type;

    // All const and * removed down to raw type.
    typedef typename InternalBufferPointerTypes<Type>::no_pointer_type no_pointer_type;

    static bool const is_pointer = true;
};

template<typename Type>
struct InternalBufferPointerTypes<Type const>
{
    // Non-const pointer type.
    typedef Type const type;

    // remove one level of pointer.
    typedef typename InternalBufferPointerTypes<Type>::remove_pointer_type remove_pointer_type;

    // Non-const pointer type.
    typedef remove_pointer_type* pointer_type;

    // All const and * removed down to raw type.
    typedef typename InternalBufferPointerTypes<Type>::no_pointer_type no_pointer_type;

    static bool const is_pointer = InternalBufferPointerTypes<Type>::is_pointer;
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPOINTERTYPES_HPP
