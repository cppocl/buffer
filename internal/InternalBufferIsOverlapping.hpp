/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERISOVERLAPPING_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERISOVERLAPPING_HPP

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = size_t>
class InternalBufferIsOverlapping
{
public:
    static bool IsOverlapping(Type const* ptr1,
                              Type const* ptr2,
                              SizeType size) noexcept
    {
        return (ptr1 < ptr2 + size) && (ptr1 + size > ptr2);
    }

    static bool IsOverlapping(Type const* ptr1,
                              SizeType ptr1_size,
                              Type const* ptr2,
                              SizeType ptr2_size) noexcept
    {
        return (ptr1 < ptr2 + ptr2_size) && (ptr1 + ptr1_size > ptr2);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERISOVERLAPPING_HPP
