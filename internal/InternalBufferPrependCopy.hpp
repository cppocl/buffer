/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPREPENDCOPY_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPREPENDCOPY_HPP

#include "internalBufferCopy.hpp"

namespace ocl
{

/// Support inserting into a buffer, either by updating an existing buffer
/// or by moving existing content to a destination buffer as part of the insert.
template<typename Type, typename SizeType = size_t>
class InternalBufferPrependCopy
{
/// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

/// Static member functions.
public:
    /// Copy src buffer to dest buffer then prepend to_prepend elements before src.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to contain src_size and to_prepend_size must be <= src_size.
    /// Only elements that fit from src will be copied.
    static void PrependCopy(type* dest,                // destination buffer which takes the copy and prepend.
                            size_type dest_size,       // size of dest buffer in elements.
                            type const* src,           // src to be copied into destination.
                            type const* to_prepend,    // elements to prepend into destination.
                            size_type to_prepend_size) // number of elements to prepend.
    {
        typedef InternalBufferCopy<type, size_type> copy_type;

        copy_type::Copy(dest, to_prepend, to_prepend_size);

        copy_type::Copy(dest + to_prepend_size, src, dest_size - to_prepend_size);
    }

    /// Copy src buffer to dest buffer then prepend to_prepend elements before src.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to contain src_size and to_prepend_size must be <= src_size.
    /// Only elements that fit from src will be copied.
    static void PrependCopy(type* dest,                 // destination buffer which takes the copy and prepend.
                            size_type dest_size,        // size of dest buffer in elements.
                            type const* src,            // src to be copied into destination.
                            size_type space_to_prepend) // number of elements to prepend.
    {
        typedef InternalBufferCopy<type, size_type> copy_type;

        copy_type::Copy(dest + space_to_prepend, src, dest_size - space_to_prepend);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERPREPENDCOPY_HPP
