/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSIZETYPES_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSIZETYPES_HPP

#include <cstdint>
#include <cstddef>

namespace ocl
{

template<std::size_t SizeOfType, bool const is_signed>
struct InternalBufferSizeTypes;

template<>
struct InternalBufferSizeTypes<1U, true>
{
    typedef std::int8_t  type;
    typedef std::int16_t next_type;
    static bool const is_signed = true;
};

template<>
struct InternalBufferSizeTypes<1U, false>
{
    typedef std::uint8_t  type;
    typedef std::uint16_t next_type;
    static bool const is_signed = false;
};

template<>
struct InternalBufferSizeTypes<2U, true>
{
    typedef std::int16_t type;
    typedef std::int8_t  prev_type;
    typedef std::int32_t next_type;
    static bool const is_signed = true;
};

template<>
struct InternalBufferSizeTypes<2U, false>
{
    typedef std::uint16_t type;
    typedef std::uint8_t  prev_type;
    typedef std::uint32_t next_type;
    static bool const is_signed = false;
};

template<>
struct InternalBufferSizeTypes<4U, true>
{
    typedef std::int32_t type;
    typedef std::int16_t prev_type;
    typedef std::int64_t next_type;
    static bool const is_signed = true;
};

template<>
struct InternalBufferSizeTypes<4U, false>
{
    typedef std::uint32_t type;
    typedef std::uint16_t prev_type;
    typedef std::uint64_t next_type;
    static bool const is_signed = false;
};

template<>
struct InternalBufferSizeTypes<8U, true>
{
    typedef std::int64_t type;
    typedef std::int32_t prev_type;
    static bool const is_signed = true;
};

template<>
struct InternalBufferSizeTypes<8U, false>
{
    typedef std::uint64_t type;
    typedef std::uint32_t prev_type;
    static bool const is_signed = false;
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSIZETYPES_HPP
