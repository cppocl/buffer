/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOMPAREEQUAL_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOMPAREEQUAL_HPP

#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = size_t>
class InternalBufferCompareEqual
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        bool is_equal = true;
        type const* first_end = first + count;
        while (first < first_end)
        {
            if (*first == *second)
            {
                ++first;
                ++second;
            }
            else
            {
                is_equal = false;
                break;
            }
        }

        return is_equal;
    }
};

template<typename Type, typename SizeType>
class InternalBufferCompareEqual<Type*, SizeType>
{
// Types.
public:
    typedef Type* type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<bool, SizeType>
{
// Types.
public:
    typedef bool type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<char, SizeType>
{
// Types.
public:
    typedef char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<wchar_t, SizeType>
{
// Types.
public:
    typedef wchar_t type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<signed char, SizeType>
{
// Types.
public:
    typedef signed char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<unsigned char, SizeType>
{
// Types.
public:
    typedef unsigned char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<signed short, SizeType>
{
// Types.
public:
    typedef signed short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<unsigned short, SizeType>
{
// Types.
public:
    typedef unsigned short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<signed int, SizeType>
{
// Types.
public:
    typedef signed int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<unsigned int, SizeType>
{
// Types.
public:
    typedef unsigned int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<signed long int, SizeType>
{
// Types.
public:
    typedef signed long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<unsigned long int, SizeType>
{
// Types.
public:
    typedef unsigned long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<signed long long int, SizeType>
{
// Types.
public:
    typedef signed long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

template<typename SizeType>
class InternalBufferCompareEqual<unsigned long long int, SizeType>
{
// Types.
public:
    typedef unsigned long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static bool CompareEqual(type const* first, type const* second, size_type count)
    {
        return ::memcmp(first, second, static_cast<size_t>(count) * sizeof(type)) == 0;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOMPAREEQUAL_HPP
