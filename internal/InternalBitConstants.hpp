/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBITCONSTANTS_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBITCONSTANTS_HPP

#include <climits>
#include <cstddef>
#include <cstdint>

namespace ocl
{

template<typename Type>
struct InternalBitConstants;

template<>
struct InternalBitConstants<unsigned char>
{
    typedef unsigned char type;

    static const constexpr std::size_t BIT_COUNT = CHAR_BIT;

    static const constexpr unsigned char NO_BITS  = static_cast<type>(0x00U);
    static const constexpr unsigned char ALL_BITS = static_cast<type>(UCHAR_MAX);

    static const constexpr unsigned char REMAINDER_MASK[BIT_COUNT] = { 0x01U, 0x03U, 0x07U, 0x0fU, 0x1fU, 0x3fU, 0x7fU, 0xffU };
};

template<>
struct InternalBitConstants<std::uint16_t>
{
    typedef std::uint16_t type;

    static const constexpr std::size_t BIT_COUNT = 16;

    static const constexpr unsigned char NO_BITS  = static_cast<type>(0x0000U);
    static const constexpr unsigned char ALL_BITS = static_cast<type>(0xffffU);

    static const constexpr unsigned char REMAINDER_MASK[BIT_COUNT] =
    {
        0x0001U, 0x0003U, 0x0007U, 0x000fU, 0x001fU, 0x003fU, 0x007fU, 0x00ffU,
        0x01ffU, 0x03ffU, 0x07ffU, 0x0fffU, 0x1fffU, 0x3fffU, 0x7fffU, 0xffffU
    };
};

template<>
struct InternalBitConstants<std::uint32_t>
{
    typedef std::uint32_t type;

    static const constexpr std::size_t BIT_COUNT = 32;

    static const constexpr unsigned char NO_BITS  = static_cast<type>(0x00000000UL);
    static const constexpr unsigned char ALL_BITS = static_cast<type>(0xffffffffUL);

    static const constexpr unsigned char REMAINDER_MASK[BIT_COUNT] =
    {
        0x00000001UL, 0x00000003UL, 0x00000007UL, 0x0000000fUL, 0x0000001fUL, 0x0000003fUL, 0x0000007fUL, 0x000000ffUL,
        0x000001ffUL, 0x000003ffUL, 0x000007ffUL, 0x00000fffUL, 0x00001fffUL, 0x00003fffUL, 0x00007fffUL, 0x0000ffffUL,
        0x0001ffffUL, 0x0003ffffUL, 0x0007ffffUL, 0x000fffffUL, 0x001fffffUL, 0x003fffffUL, 0x007fffffUL, 0x00ffffffUL,
        0x01ffffffUL, 0x03ffffffUL, 0x07ffffffUL, 0x0fffffffUL, 0x1fffffffUL, 0x3fffffffUL, 0x7fffffffUL, 0xffffffffUL
    };
};

template<>
struct InternalBitConstants<std::uint64_t>
{
    typedef std::uint64_t type;

    static const constexpr std::size_t BIT_COUNT = 64;

    static const constexpr unsigned char NO_BITS  = static_cast<type>(0x0000000000000000ULL);
    static const constexpr unsigned char ALL_BITS = static_cast<type>(0xffffffffffffffffULL);

    static const constexpr unsigned char REMAINDER_MASK[BIT_COUNT] =
    {
        0x0000000000000001ULL, 0x0000000000000003ULL, 0x0000000000000007ULL, 0x000000000000000fULL, 0x000000000000001fULL, 0x000000000000003fULL, 0x000000000000007fULL, 0x00000000000000ffULL,
        0x00000000000001ffULL, 0x00000000000003ffULL, 0x00000000000007ffULL, 0x0000000000000fffULL, 0x0000000000001fffULL, 0x0000000000003fffULL, 0x0000000000007fffULL, 0x000000000000ffffULL,
        0x000000000001ffffULL, 0x000000000003ffffULL, 0x000000000007ffffULL, 0x00000000000fffffULL, 0x00000000001fffffULL, 0x00000000003fffffULL, 0x00000000007fffffULL, 0x0000000000ffffffULL,
        0x0000000001ffffffULL, 0x0000000003ffffffULL, 0x0000000007ffffffULL, 0x000000000fffffffULL, 0x000000001fffffffULL, 0x000000003fffffffULL, 0x000000007fffffffULL, 0x00000000ffffffffULL,
        0x00000001ffffffffULL, 0x00000003ffffffffULL, 0x00000007ffffffffULL, 0x0000000fffffffffULL, 0x0000001fffffffffULL, 0x0000003fffffffffULL, 0x0000007fffffffffULL, 0x000000ffffffffffULL,
        0x000001ffffffffffULL, 0x000003ffffffffffULL, 0x000007ffffffffffULL, 0x00000fffffffffffULL, 0x00001fffffffffffULL, 0x00003fffffffffffULL, 0x00007fffffffffffULL, 0x0000ffffffffffffULL,
        0x0001ffffffffffffULL, 0x0003ffffffffffffULL, 0x0007ffffffffffffULL, 0x000fffffffffffffULL, 0x001fffffffffffffULL, 0x003fffffffffffffULL, 0x007fffffffffffffULL, 0x00ffffffffffffffULL,
        0x01ffffffffffffffULL, 0x03ffffffffffffffULL, 0x07ffffffffffffffULL, 0x0fffffffffffffffULL, 0x1fffffffffffffffULL, 0x3fffffffffffffffULL, 0x7fffffffffffffffULL, 0xffffffffffffffffULL
    };
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBITCONSTANTS_HPP
