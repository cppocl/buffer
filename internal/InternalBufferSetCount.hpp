/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSETCOUNT_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSETCOUNT_HPP

#include <cstdint>

namespace ocl
{

template<typename Type, typename SizeType>
struct InternalBufferSetCount
{
    typedef Type type;
    typedef SizeType size_type;

    static SizeType GetCount(type const* buffer, size_type byte_count)
    {
        switch (byte_count)
        {
        case 1: return static_cast<size_type>(*reinterpret_cast<std::uint8_t const*>(buffer));
        case 2: return static_cast<size_type>(*reinterpret_cast<std::uint16_t const*>(buffer));
        case 4: return static_cast<size_type>(*reinterpret_cast<std::uint32_t const*>(buffer));

        default: // default to 8 bytes.
            return static_cast<size_type>(*reinterpret_cast<std::uint64_t const*>(buffer));
        }
    }

    /// Set the count within a buffer with the number of bytes specified by byte_count,
    /// irrespective of the buffer type.
    /// If byte_count is not 1, 2 or 4, then 8 bytes are used as the default byte count.
    static void SetCount(type* buffer, size_type byte_count, size_type count)
    {
        switch (byte_count)
        {
        case 1:
            *reinterpret_cast<std::uint8_t*>(buffer) = static_cast<uint8_t>(count);
            break;
        case 2:
            *reinterpret_cast<std::uint16_t*>(buffer) = static_cast<uint16_t>(count);
            break;
        case 4:
            *reinterpret_cast<std::uint32_t*>(buffer) = static_cast<uint32_t>(count);
            break;
        default: // default to 8 bytes.
            *reinterpret_cast<std::uint64_t*>(buffer) = static_cast<uint64_t>(count);
        }
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSETCOUNT_HPP
