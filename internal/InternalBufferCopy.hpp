/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOPY_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOPY_HPP

#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = size_t>
class InternalBufferCopy
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Copy count number of elements from src to dest.
    /// src and dest must not be null.
    static void Copy(type* dest, type const* src, size_type count)
    {
        type const* source_end = src + count;
        while (src < source_end)
            *dest++ = *src++;
    }
};

template<typename Type, typename SizeType>
class InternalBufferCopy<Type*, SizeType>
{
// Types.
public:
    typedef Type* type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<bool, SizeType>
{
// Types.
public:
    typedef bool type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<char, SizeType>
{
// Types.
public:
    typedef char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferCopy<wchar_t, SizeType>
{
// Types.
public:
    typedef wchar_t type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<signed char, SizeType>
{
// Types.
public:
    typedef signed char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferCopy<unsigned char, SizeType>
{
// Types.
public:
    typedef unsigned char type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count));
    }
};

template<typename SizeType>
class InternalBufferCopy<signed short, SizeType>
{
// Types.
public:
    typedef signed short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<unsigned short, SizeType>
{
// Types.
public:
    typedef unsigned short type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<signed int, SizeType>
{
// Types.
public:
    typedef signed int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<unsigned int, SizeType>
{
// Types.
public:
    typedef unsigned int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<signed long int, SizeType>
{
// Types.
public:
    typedef signed long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<unsigned long int, SizeType>
{
// Types.
public:
    typedef unsigned long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<signed long long int, SizeType>
{
// Types.
public:
    typedef signed long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<unsigned long long int, SizeType>
{
// Types.
public:
    typedef unsigned long long int type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<float, SizeType>
{
// Types.
public:
    typedef float type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<double, SizeType>
{
// Types.
public:
    typedef double type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

template<typename SizeType>
class InternalBufferCopy<long double, SizeType>
{
// Types.
public:
    typedef long double type;
    typedef SizeType size_type;

// Static member functions.
public:
    static void Copy(type* dest, type const* src, size_type count) noexcept
    {
        ::memcpy(dest, src, static_cast<size_t>(count) * sizeof(type));
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERCOPY_HPP
