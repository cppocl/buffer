/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINSERTMOVE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINSERTMOVE_HPP

#include "internalBufferMove.hpp"

namespace ocl
{

/// Support inserting into a buffer, either by updating an existing buffer
/// or by moving existing content to a destination buffer as part of the insert.
template<typename Type, typename SizeType = size_t>
class InternalBufferInsertMove
{
/// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

/// Static member functions.
public:
    /// Move src buffer to dest buffer while inserting elements.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// position must be less or equal to source_count or there will be a buffer overrun.
    /// src must be at least dest size minus the insert size.
    static void InsertMove(type* dest,               // destination buffer which takes the source and insertion.
                           size_type dest_size,      // number of elements in destination buffer.
                           size_type position,       // position to insert src into destination.
                           type* src,                // src to be moved into destination.
                           type* to_insert,          // elements to move into destination.
                           size_type to_insert_size) // number of elements to insert.
    {
        typedef InternalBufferMove<type, size_type> move_type;

        type* insert_dest = dest + position; // Position of pointer for insert.
        move_type::Move(dest, src, position);
        move_type::Move(insert_dest, to_insert, to_insert_size);

        if (position + to_insert_size < dest_size)
            move_type::Move(insert_dest + to_insert_size,
                            src + position,
                            dest_size - position - to_insert_size);
    }

    /// Move src buffer to dest buffer while inserting elements.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// position must be less or equal to source_count or there will be a buffer overrun.
    /// src must be at least dest size minus the insert size.
    static type* InsertMove(type* dest,                // destination buffer which takes the source and insertion.
                            size_type dest_size,       // number of elements in destination buffer.
                            size_type position,        // position to insert src into destination.
                            type* src,                 // src to be moved into destination.
                            size_type space_to_insert) // number of elements to insert.
    {
        typedef InternalBufferMove<type, size_type> move_type;

        type* insert_dest = dest + position; // Position of pointer for insert.
        move_type::Move(dest, src, position);

        if (position + space_to_insert < dest_size)
            move_type::Move(insert_dest + space_to_insert,
                            src + position,
                            dest_size - position - space_to_insert);

        return insert_dest;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERINSERTMOVE_HPP
