/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALUNSIGNEDTYPE_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALUNSIGNEDTYPE_HPP

namespace ocl
{

/*
* Get the unsigned type for the integer type specified in the template parameter.
*/
template<typename Type> struct InternalUnsignedType { typedef Type type; };
template<> struct InternalUnsignedType<signed char> { typedef unsigned char type; };
template<> struct InternalUnsignedType<signed short> { typedef unsigned short type; };
template<> struct InternalUnsignedType<signed int> { typedef unsigned int type; };
template<> struct InternalUnsignedType<signed long int> { typedef unsigned long int type; };
template<> struct InternalUnsignedType<signed long long int> { typedef unsigned long long int type; };

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALUNSIGNEDTYPE_HPP
