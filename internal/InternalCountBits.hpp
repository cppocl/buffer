/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALCOUNTBITS_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALCOUNTBITS_HPP

#include <cstdint>
#include <cstddef>
#include "InternalCountByteBits.hpp"

namespace ocl
{

template<typename Type, typename SizeType = std::size_t>
class InternalCountBits;

template<typename SizeType>
class InternalCountBits<std::int8_t, SizeType>
{
public:
    typedef std::int8_t type;
    typedef SizeType size_type;

private:
    typedef InternalCountByteBits<size_type> count_byte_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return count_byte_type::CountSetBits(static_cast<std::uint8_t>(value));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value));
    }
};

template<typename SizeType>
class InternalCountBits<std::uint8_t, SizeType>
{
public:
    typedef std::uint8_t type;
    typedef SizeType size_type;

private:
    typedef InternalCountByteBits<size_type> count_byte_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return count_byte_type::CountSetBits(value);
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return count_byte_type::CountClearedBits(value);
    }
};

template<typename SizeType>
class InternalCountBits<std::int16_t, SizeType>
{
public:
    typedef std::int16_t type;
    typedef SizeType size_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return InternalCountBits<std::uint16_t>::CountSetBits(static_cast<std::uint16_t>(value));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return InternalCountBits<std::uint16_t>::CountClearedBits(static_cast<std::uint16_t>(value));
    }
};

template<typename SizeType>
class InternalCountBits<std::uint16_t, SizeType>
{
public:
    typedef std::uint16_t type;
    typedef SizeType size_type;

private:
    typedef InternalCountByteBits<size_type> count_byte_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 8U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 8U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value));
    }
};

template<typename SizeType>
class InternalCountBits<std::int32_t, SizeType>
{
public:
    typedef std::int32_t type;
    typedef SizeType size_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return InternalCountBits<std::uint32_t>::CountSetBits(static_cast<std::uint32_t>(value));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return InternalCountBits<std::uint32_t>::CountClearedBits(static_cast<std::uint32_t>(value));
    }
};

template<typename SizeType>
class InternalCountBits<std::uint32_t, SizeType>
{
public:
    typedef std::uint32_t type;
    typedef SizeType size_type;

private:
    typedef InternalCountByteBits<size_type> count_byte_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return count_byte_type::CountSetBits(static_cast<std::uint8_t>(value)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 8U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 16U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 24U));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 8U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 16U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 24U));
    }
};

template<typename SizeType>
class InternalCountBits<std::int64_t, SizeType>
{
public:
    typedef std::int64_t type;
    typedef SizeType size_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return InternalCountBits<std::uint64_t>::CountSetBits(static_cast<std::uint64_t>(value));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return InternalCountBits<std::uint64_t>::CountClearedBits(static_cast<std::uint64_t>(value));
    }
};

template<typename SizeType>
class InternalCountBits<std::uint64_t, SizeType>
{
public:
    typedef std::uint64_t type;
    typedef SizeType size_type;

private:
    typedef InternalCountByteBits<size_type> count_byte_type;

public:
    static inline size_type CountSetBits(type value) noexcept
    {
        return count_byte_type::CountSetBits(static_cast<std::uint8_t>(value)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 8U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 16U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 24U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 32U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 40U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 48U)) +
               count_byte_type::CountSetBits(static_cast<std::uint8_t>(value >> 56U));
    }

    static inline size_type CountClearedBits(type value) noexcept
    {
        return count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 8U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 16U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 24U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 32U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 40U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 48U)) +
               count_byte_type::CountClearedBits(static_cast<std::uint8_t>(value >> 56U));
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALCOUNTBITS_HPP
