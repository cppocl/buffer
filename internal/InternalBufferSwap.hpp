/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSWAP_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSWAP_HPP

#include <cstring>
#include <cstddef>

namespace ocl
{

/// Fast copy of buffers, but does not handle overlapping regions of buffers.
template<typename Type, typename SizeType = std::size_t>
class InternalBufferSwap
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Swap value1 and value2.
    static void Swap(type& value1, type& value2)
    {
        type tmp = value1;
        value1 = value2;
        value2 = tmp;
    }

    static void Swap(type& value1, size_type& value1_size, type& value2, size_type& value2_size)
    {
        type tmp_value = value1;
        size_type tmp_size = value1_size;
        value1 = value2;
        value2 = tmp_value;
        value1_size = value2_size;
        value2_size = tmp_size;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERSWAP_HPP
