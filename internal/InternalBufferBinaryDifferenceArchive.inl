/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename Type, typename SizeType>
SizeType InternalBufferBinaryDifferenceArchive<Type, SizeType>::GetDifferenceCount(
    type const* buffer1,
    size_type buffer1_size,
    type const* buffer2,
    size_type buffer2_size,
    size_type byte_count)
{
    bool first_compare = true;
    size_type count1 = 0;
    size_type count2 = 0;

    if (byte_count == 0)
        byte_count = GetByteCount(buffer1_size > buffer2_size ? buffer1_size : buffer2_size);
    size_type total_bytes = byte_count;

    // Keep looping until no more same or different bytes can be copied.
    do
    {
        // Count the same bytes in buffer1 and buffer2.
        // Always count the byte count for same bytes when the first loop.
        count2 = 0;
        count1 = buffer_count_type::CountSame(buffer1, buffer1_size, buffer2, buffer2_size);
        if (first_compare || (count1 > 0))
        {
            if (!type_increment_type::Increment(total_bytes, byte_count) ||
                !type_increment_type::Increment(total_bytes, count1))
            {
                total_bytes = 0; // Overflow, so return 0.
                break;
            }
            buffer_advance_type::Advance(buffer1, buffer1_size, buffer2, buffer2_size, count1);

            // Count the different bytes and different counts in buffer1 and buffer2.
            buffer_count_type::CountDifferent(buffer1, buffer1_size,
                                              buffer2, buffer2_size,
                                              count1, count2);

            // Count total bytes for differences stored in archive,
            // and move buffer1 and buffer2 past differences.
            if ((count1 > 0) || (count2 > 0))
            {
                if (!type_increment_type::Increment(total_bytes, byte_count) ||
                    !type_increment_type::Increment(total_bytes, count1)     ||
                    !type_increment_type::Increment(total_bytes, byte_count) ||
                    !type_increment_type::Increment(total_bytes, count2))
                {
                    total_bytes = 0; // Overflow, so return 0.
                    break;
                }
                buffer_advance_type::Advance(buffer1, buffer1_size, count1);
                buffer_advance_type::Advance(buffer2, buffer2_size, count2);
            }
        }

        if (first_compare)
            first_compare = false;
    }
    while (count1 + count2 > 0);

    return total_bytes;
}

template<typename Type, typename SizeType>
SizeType InternalBufferBinaryDifferenceArchive<Type, SizeType>::CreateDifference(
    type const* buffer1,
    size_type buffer1_size,
    type const* buffer2,
    size_type buffer2_size,
    type* output_buffer,
    size_type output_buffer_size,
    size_type byte_count)
{
    bool success = true;
    size_type bytes_set = 0;
    size_type total_bytes = 0;
    bool first_compare = true;
    if (byte_count == 0)
        byte_count = GetByteCount(buffer1_size > buffer2_size ? buffer1_size : buffer2_size);

    if (output_buffer_size > byte_count)
    {
        // First byte is always the number of bytes to be used for storing a count
        // for matching or different values.
        buffer_set_count::SetCount(output_buffer, 1, byte_count);
        buffer_advance_type::Advance(output_buffer, output_buffer_size, byte_count);
        total_bytes += byte_count;

        // Keep looping until no more same or different bytes can be copied.
        do
        {
            // Set the same bytes and count in buffer1 and buffer2.
            success = SetSameBytes(byte_count, output_buffer, output_buffer_size,
                                   buffer1, buffer1_size, buffer2, buffer2_size,
                                   bytes_set, first_compare);

            // First pass must always attempt copying different values, as it's possible
            // the buffer won't start with matching values.
            if (success && (first_compare || bytes_set > 0))
            {
                if (!type_increment_type::Increment(total_bytes, bytes_set))
                {
                    success = false; // Output buffer would be too big.
                    break;
                }

                // Set the different bytes and different counts in buffer1 and buffer2.
                success = SetDifferentBytes(byte_count, output_buffer, output_buffer_size,
                                            buffer1, buffer1_size, buffer2, buffer2_size,
                                            bytes_set);

                if (success && (bytes_set > 0) &&
                    !type_increment_type::Increment(total_bytes, bytes_set))
                {
                    success = false; // Increment failed, so output buffer would be too big.
                    break;
                }
            }

            // Only need to keep track of first loop to always record count for same bytes.
            if (first_compare)
                first_compare = false;
        }
        while (success && bytes_set > 0);
    }

    return success ? total_bytes : 0;
}

template<typename Type, typename SizeType>
void InternalBufferBinaryDifferenceArchive<Type, SizeType>::GetBufferSizes(
    type const* archive_buffer,
    size_type archive_buffer_size,
    size_type& buffer1_size,
    size_type& buffer2_size)
{
    // Extract the value describing how many bytes are required to store a count.
    // This value is always stored in a single byte.
    size_type count;
    size_type byte_count = static_cast<size_type>(*archive_buffer);
    buffer_advance_type::Advance(archive_buffer, archive_buffer_size, 1);

    buffer1_size = buffer2_size = 0;

    while (archive_buffer_size > 0)
    {
        // Read matching byte count and increment buffer sizes.
        count = buffer_set_count::GetCount(archive_buffer, byte_count);
        if (!type_increment_type::Increment(buffer1_size, count) ||
            !type_increment_type::Increment(buffer2_size, count))
        {
            buffer1_size = buffer2_size = 0;
            break;
        }
        buffer_advance_type::Advance(archive_buffer, archive_buffer_size, byte_count + count);

        if (archive_buffer_size > 0)
        {
            // Read different byte count for first buffer.
            count = buffer_set_count::GetCount(archive_buffer, byte_count);
            if (!type_increment_type::Increment(buffer1_size, count))
            {
                buffer1_size = buffer2_size = 0;
                break;
            }
            buffer_advance_type::Advance(archive_buffer, archive_buffer_size, byte_count + count);

            // Read different byte count for second buffer.
            count = buffer_set_count::GetCount(archive_buffer, byte_count);
            if (!type_increment_type::Increment(buffer2_size, count))
            {
                buffer1_size = buffer2_size = 0;
                break;
            }
            buffer_advance_type::Advance(archive_buffer, archive_buffer_size, byte_count + count);
        }
    }
}

template<typename Type, typename SizeType>
bool InternalBufferBinaryDifferenceArchive<Type, SizeType>::CreateBuffers(
    type const* archive_buffer,
    size_type archive_buffer_size,
    type* buffer1,
    size_type buffer1_size,
    type* buffer2,
    size_type buffer2_size,
    size_type& buffer1_output_size,
    size_type& buffer2_output_size)
{
    bool success = true;

    // Extract the value describing how many bytes are required to store a count.
    // This value is always stored in a single byte.
    size_type count;
    size_type byte_count = static_cast<size_type>(*archive_buffer);
    buffer_advance_type::Advance(archive_buffer, archive_buffer_size, 1);

    buffer1_output_size = buffer2_output_size = 0;

    while (archive_buffer_size > 0)
    {
        // Read matching byte count and increment buffer sizes.
        count = buffer_set_count::GetCount(archive_buffer, byte_count);
        if ((buffer1_size < count) || (buffer2_size < count) ||
            !type_increment_type::Increment(buffer1_output_size, count) ||
            !type_increment_type::Increment(buffer2_output_size, count))
        {
            success = false; // Cannot fit bytes into buffer1 and/or buffer2.
            break;
        }

        // Skip the count and copy the same bytes to buffer1 and buffer2.
        buffer_advance_type::Advance(archive_buffer, archive_buffer_size, byte_count);
        ::memcpy(buffer1, archive_buffer, count);
        ::memcpy(buffer2, archive_buffer, count);
        buffer_advance_type::Advance(buffer1, buffer1_size, count);
        buffer_advance_type::Advance(buffer2, buffer2_size, count);
        buffer_advance_type::Advance(archive_buffer, archive_buffer_size, count);

        if (archive_buffer_size > 0)
        {
            // Read different byte count for first buffer.
            count = buffer_set_count::GetCount(archive_buffer, byte_count);
            if ((buffer1_size < count) || 
                !type_increment_type::Increment(buffer1_output_size, count))
            {
                success = false; // Cannot fit bytes into buffer1.
                break;
            }

            // Skip the count and copy the different bytes to buffer1.
            buffer_advance_type::Advance(archive_buffer, archive_buffer_size, byte_count);
            ::memcpy(buffer1, archive_buffer, count);
            buffer_advance_type::Advance(buffer1, buffer1_size, count);
            buffer_advance_type::Advance(archive_buffer, archive_buffer_size, count);

            // Read different byte count for second buffer.
            count = buffer_set_count::GetCount(archive_buffer, byte_count);
            if ((buffer2_size < count) ||
                !type_increment_type::Increment(buffer2_output_size, count))
            {
                success = false; // Cannot fit bytes into buffer2.
                break;
            }

            // Skip the count and copy the different bytes to buffer2.
            buffer_advance_type::Advance(archive_buffer, archive_buffer_size, byte_count);
            ::memcpy(buffer2, archive_buffer, count);
            buffer_advance_type::Advance(buffer2, buffer2_size, count);
            buffer_advance_type::Advance(archive_buffer, archive_buffer_size, count);
        }
    }

    return success;
}

template<typename Type, typename SizeType>
bool InternalBufferBinaryDifferenceArchive<Type, SizeType>::SetSameBytes(
    size_type byte_count,
    type*& output_buffer,
    size_type& output_buffer_size,
    type const*& buffer1,
    size_type& buffer1_size,
    type const*& buffer2,
    size_type& buffer2_size,
    size_type& bytes_set,
    bool first_compare)
{
    typedef InternalBufferCopySame<type, size_type> buffer_copy_same_type;

    bool success = true;

    // Check there is enough space to store the byte count and the same bytes.
    size_type min_buffer_size = buffer1_size < buffer2_size ? buffer1_size : buffer2_size;

    // If either buffer is empty, don't continue unless it's the first pass,
    // as we want to store the count of 0.
    if (first_compare || (min_buffer_size > 0))
    {
        if ((output_buffer_size > byte_count) &&
            (output_buffer_size - byte_count >= min_buffer_size))
        {
            buffer_copy_same_type::CopySame(output_buffer + byte_count,
                                            output_buffer_size - byte_count,
                                            buffer1,
                                            buffer1_size,
                                            buffer2,
                                            buffer2_size,
                                            bytes_set);

            // Set the number of bytes copied at start of output_buffer.
            // The number of bytes used for the count is specified by byte_count.
            buffer_set_count::SetCount(output_buffer, byte_count, bytes_set);

            // Move the pointers ready for next copy.
            buffer_advance_type::Advance(buffer1, buffer1_size, buffer2, buffer2_size, bytes_set);

            // Increment the output buffer by buffer1_size + buffer2_size + byte_count.
            if (!type_increment_type::Increment(bytes_set, byte_count))
                success = false;
            buffer_advance_type::Advance(output_buffer, output_buffer_size, bytes_set);
        }
        else
        {
            success = false;
            bytes_set = 0;
        }
    }
    else
        bytes_set = 0;

    return success;
}

template<typename Type, typename SizeType>
bool InternalBufferBinaryDifferenceArchive<Type, SizeType>::SetDifferentBytes(
    size_type byte_count,
    unsigned char*& output_buffer,
    size_type& output_buffer_size,
    unsigned char const*& buffer1,
    size_type& buffer1_size,
    unsigned char const*& buffer2,
    size_type& buffer2_size,
    size_type& bytes_set)
{
    bool success = true;

    if ((buffer1_size > 0) || (buffer2_size > 0))
    {
        size_type bytes1_count = 0;
        size_type bytes2_count = 0;
        buffer_count_type::CountDifferent(buffer1,
                                          buffer1_size,
                                          buffer2,
                                          buffer2_size,
                                          bytes1_count,
                                          bytes2_count);

        // Is there is enough space for the two different buffers and their counts?
        // Ensure the increments don't overflow the range of what size_type can store.
        size_type required_size = byte_count * 2;

        if (type_increment_type::Increment(required_size, bytes1_count) &&
            type_increment_type::Increment(required_size, bytes2_count) &&
            output_buffer_size >= required_size)
        {
            bytes_set = required_size;
            output_buffer_size -= required_size;

            buffer_set_count::SetCount(output_buffer, byte_count, bytes1_count);
            output_buffer += byte_count;
            ::memcpy(output_buffer, buffer1, bytes1_count);
            output_buffer += bytes1_count;

            buffer_set_count::SetCount(output_buffer, byte_count, bytes2_count);
            output_buffer += byte_count;
            ::memcpy(output_buffer, buffer2, bytes2_count);
            output_buffer += bytes2_count;

            // Move the pointers ready for next copy.
            buffer_advance_type::Advance(buffer1, buffer1_size, bytes1_count);
            buffer_advance_type::Advance(buffer2, buffer2_size, bytes2_count);
        }
        else
        {
            success = false;
            bytes_set = 0;
        }
    }
    else
        bytes_set = 0;

    return success;
}
