/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERFIND_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERFIND_HPP

namespace ocl
{

template<typename Type, typename SizeType = std::size_t>
class InternalBufferFind
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

// Static member functions.
public:
    /// Find value within buffer and return pointer to matching value within buffer,
    /// or nullptr when not found.
    static Type const* Find(Type value, Type const* buffer, SizeType size)
    {
        Type const* found = nullptr;

        for (Type const* buffer_end = buffer + size; buffer < buffer_end; ++buffer)
            if (*buffer == value)
            {
                found = buffer;
                break;
            }

        return found;
    }

    /// Find the first value within first that is within second and
    /// returns the second pointer that matches.
    /// Otherwise return nullptr if no value matches within second.
    static Type const* FindMatch(Type const* first,
                                 SizeType first_size,
                                 Type const* second,
                                 SizeType second_size)
    {
        Type const* found = nullptr;
        if (second_size > 0)
        {
            Type const* first_end = first + first_size;
            for (; first < first_end; ++first)
            {
                found = Find(*first, second, second_size);
                if (found != nullptr)
                    break;
            }
        }
        return found;
    }

};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALBUFFERFIND_HPP
