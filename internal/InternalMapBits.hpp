/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_INTERNAL_INTERNALMAPBITS_HPP
#define OCL_GUARD_BUFFER_INTERNAL_INTERNALMAPBITS_HPP

#include <cstdint>
#include <cstddef>

namespace ocl
{

template<typename FromType, typename ToType>
class InternalMapBits
{
public:
    typedef FromType from_type;
    typedef ToType   to_type;

public:
    // Map the bits in memory without using a cast (which could change the bits in memory).
    static inline to_type ToBits(from_type value) noexcept
    {
        typedef union { from_type from_value; to_type to_value; } u;
        u convert{}; // Guarantee that when the two types don't match size that any spare bits are cleared.
        convert.from_value = value;
        return convert.to_value;
    }

    // Map the bits in memory without using a cast (which could change the bits in memory).
    // Return the possibility of loss of data.
    static inline to_type ToBits(from_type value, bool& lossy) noexcept
    {
        typedef union { from_type from_value; to_type to_value; } u;
        u convert{}; // Guarantee that when the two types don't match size that any spare bits are cleared.
        convert.from_value = value;
        lossy = sizeof(to_type) < sizeof(from_type);
        return convert.to_value;
    }
};

template<typename ToType>
class InternalMapBits<bool, ToType>
{
public:
    typedef bool from_type;
    typedef ToType to_type;

public:
    // Map the bits in memory without using a cast (which could change the bits in memory).
    static inline to_type ToBits(from_type value) noexcept
    {
        return static_cast<to_type>(value ? 1 : 0);
    }

    // Map the bits in memory without using a cast (which could change the bits in memory).
    // Return the possibility of loss of data.
    static inline to_type ToBits(from_type value, bool& lossy) noexcept
    {
        lossy = sizeof(to_type) < sizeof(from_type);
        return static_cast<to_type>(value ? 1 : 0);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_INTERNAL_INTERNALMAPBITS_HPP
