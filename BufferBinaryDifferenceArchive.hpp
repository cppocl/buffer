/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_BUFFERBINARYDIFFERENCEARCHIVE_HPP
#define OCL_GUARD_BUFFER_BUFFERBINARYDIFFERENCEARCHIVE_HPP

#include "internal/InternalBufferBinaryDifferenceArchive.hpp"

namespace ocl
{

/// Create or extract a buffer storing the archive of two buffers as a difference.
/// The format of the archive is:
///
///    1 byte - number of bytes required to store a count (will be value 1, 2, 4 or 8).
///    1 byte - count for matching values (0..n).
///    0..n   - the matching value.
///    1 byte - count for differences in first buffer  (0..n).
///    0..n   - the different values for the first buffer.
///    1 byte - count for differences in second buffer  (0..n).
///    0..n   - the different values for the second buffer.
///
/// Type must be signed char or unsigned char, as the difference algorithm only works with
/// types matching the number of bits specified by CHAR_BIT.
template<typename Type, typename SizeType = std::size_t>
class BufferBinaryDifferenceArchive
{
/// Types.
public:
    typedef Type type;
    typedef SizeType size_type;

/// Static member functions.
public:
    /// Count number of bytes required to store the archive buffer for differences of two buffers.
    /// If the buffers are too big to fit into size_type then 0 is returned.
    static size_type GetDifferenceCount(type const* buffer1,
                                        size_type buffer1_size,
                                        type const* buffer2,
                                        size_type buffer2_size,
                                        size_type byte_count = 0)
    {
        return buffer_binary_difference_archive_type::GetDifferenceCount(buffer1,
                                                                         buffer1_size,
                                                                         buffer2,
                                                                         buffer2_size,
                                                                         byte_count);
    }

    /// Create a buffer that stores a difference as an archive of the two buffers.
    /// Return the number of bytes stored in the output buffer.
    /// If the created buffer is bigger than the value that can be stored in a size_type
    /// then 0 is returned.
    static size_type CreateDifference(type const* buffer1, size_type buffer1_size,
                                      type const* buffer2, size_type buffer2_size,
                                      type* output_buffer, size_type output_buffer_size,
                                      size_type byte_count = 0)
    {
        return buffer_binary_difference_archive_type::CreateDifference(buffer1,
                                                                       buffer1_size,
                                                                       buffer2,
                                                                       buffer2_size,
                                                                       output_buffer,
                                                                       output_buffer_size,
                                                                       byte_count);
    }

    /// Calculate the two buffer sizes required to extract the archive buffer containing
    /// the differences, If there any parsing problems buffer1_size and buffer2_size will be 0.
    static void GetBufferSizes(type const* archive_buffer,
                               size_type archive_buffer_size,
                               size_type& buffer1_size,
                               size_type& buffer2_size)
    {
        buffer_binary_difference_archive_type::GetBufferSizes(archive_buffer,
                                                              archive_buffer_size,
                                                              buffer1_size,
                                                              buffer2_size);
    }

    /// Extract the differences from the archive buffer into two buffers, buffer1 and buffer2.
    static bool CreateBuffers(type const* archive_buffer,
                              size_type archive_buffer_size,
                              type* buffer1,
                              size_type buffer1_size,
                              type* buffer2,
                              size_type buffer2_size,
                              size_type& buffer1_output_size,
                              size_type& buffer2_output_size)
    {
        return buffer_binary_difference_archive_type::CreateBuffers(archive_buffer,
                                                                    archive_buffer_size,
                                                                    buffer1,
                                                                    buffer1_size,
                                                                    buffer2,
                                                                    buffer2_size,
                                                                    buffer1_output_size,
                                                                    buffer2_output_size);
    }

/// Types (internal use only).
private:
    typedef InternalBufferBinaryDifferenceArchive<Type, SizeType> buffer_binary_difference_archive_type;
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_BUFFERBINARYDIFFERENCEARCHIVE_HPP
