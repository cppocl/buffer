/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_BINARYSEARCHFUNCTOR_HPP
#define OCL_GUARD_BUFFER_BINARYSEARCHFUNCTOR_HPP

#include "internal/InternalBufferInType.hpp"
#include "internal/InternalBufferSignedTypes.hpp"

namespace ocl
{

template<typename Type, typename SizeType>
struct BinarySearchFunctor
{
    typedef typename InternalBufferInType<Type>::in_type in_type;

    bool operator()(Type const* values,
                    SizeType size,
                    in_type value_to_find) const noexcept
    {
        bool found;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            SizeType start = 0;
            SizeType end = size - static_cast<SizeType>(1);
            SizeType mid;
            int cmp;

            do
            {
                mid = (start + end) >> static_cast<SizeType>(1);
                Type to_compare = values[mid];
                cmp = (value_to_find < to_compare) ? -1 : ((to_compare < value_to_find) ? 1 : 0);
                if (cmp < 0)
                {
                    if (mid > 0)
                        end = mid - static_cast<SizeType>(1);
                    else
                        break;
                }
                else if (cmp > 0)
                    start = mid + static_cast<SizeType>(1);
                else
                    break;
            }
            while (start <= end);

            found = cmp == 0;
        }
        else
            found = false;

        return found;
    }

    bool operator()(Type const* values,
        SizeType size,
        in_type value_to_find,
        SizeType& nearest_pos) const noexcept
    {
        bool found;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            SizeType start = 0;
            SizeType end = size - static_cast<SizeType>(1);
            SizeType mid;
            int cmp;

            do
            {
                mid = (start + end) >> static_cast<SizeType>(1);
                Type to_compare = values[mid];
                cmp = (value_to_find < to_compare) ? -1 : ((to_compare < value_to_find) ? 1 : 0);
                if (cmp < 0)
                {
                    if (mid > 0)
                        end = mid - static_cast<SizeType>(1);
                    else
                        break;
                }
                else if (cmp > 0)
                    start = mid + static_cast<SizeType>(1);
                else
                    break;
            }
            while (start <= end);

            /// If value_to_find is greater than value at nearest position, add 1 to nearest position.
            SizeType const offset = cmp > 0 ? 1 : 0;
            mid = ((start + end) >> static_cast<SizeType>(1)) + offset;
            nearest_pos = mid;
            found = cmp == 0;
        }
        else
        {
            nearest_pos = static_cast<SizeType>(0);
            found = false;
        }

        return found;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_BINARYSEARCHFUNCTOR_HPP
