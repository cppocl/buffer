/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_SETFUNCTOR_HPP
#define OCL_GUARD_BUFFER_SETFUNCTOR_HPP

#include "internal/InternalBufferCopySet.hpp"
#include "internal/InternalBufferInType.hpp"
#include <cstddef>

namespace ocl
{

template<typename Type, typename SizeType = std::size_t>
struct SetFunctor
{
    typedef typename InternalBufferInType<Type>::in_type in_type;
    typedef InternalBufferCopySet<Type, SizeType> buffer_set_utility_type;

    inline void operator()(Type* dest, SizeType count, in_type value) const
    {
        buffer_set_utility_type::CopySet(dest, count, value);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_SETFUNCTOR_HPP
