/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_SECUREBUFFERUTILITY_HPP
#define OCL_GUARD_BUFFER_SECUREBUFFERUTILITY_HPP

#include "internal/InternalBufferCompare.hpp"
#include "internal/InternalBufferCompareEqual.hpp"
#include "internal/InternalBufferCopy.hpp"
#include "internal/InternalBufferMove.hpp"
#include "internal/InternalBufferErase.hpp"
#include "internal/InternalBufferInsert.hpp"
#include "internal/InternalBufferInsertCopy.hpp"
#include "internal/InternalBufferInsertMove.hpp"
#include "internal/InternalBufferPrepend.hpp"
#include "internal/InternalBufferPrependCopy.hpp"
#include "internal/InternalBufferPrependMove.hpp"
#include "internal/InternalBufferOverlapCopy.hpp"
#include "internal/InternalBufferOverlapMove.hpp"
#include "internal/InternalBufferReverseCopy.hpp"
#include "internal/InternalBufferReverseMove.hpp"
#include "internal/InternalBufferSet.hpp"
#include "internal/InternalBufferSwap.hpp"
#include "internal/InternalBufferTypes.hpp"
#include <cstddef>

namespace ocl
{

/// Static functions in a utility class for manipulating buffers.
/// Type must be copyable for this utility class to be usable.
/// These functions do not check for null pointers or buffer overruns,
/// and are designed for performance, with the user handling pointers and ranges.
template<typename Type, typename SizeType = std::size_t>
class SecureBufferUtility
{
// Types.
public:
    typedef Type type;
    typedef SizeType size_type;
    typedef typename InternalBufferTypes<type>::in_type in_type;

// Static member functions.
public:
    /// Copy n elements to destination from source.
    static bool Copy(type* dest, type const* src, size_type count);

    /// Copy n elements to destination from source in reverse order.
    static bool ReverseCopy(type* dest, type const* src, size_type count);

    /// Copy n elements to destination from source safely, even when regions overlap.
    static bool OverlapCopy(type* dest, type const* src, size_type count);

    /// Copy elements that have the same value within src1 and src2 to dest.
    static bool CopySame(type* dest,
                         size_type dest_size,
                         type const* src1,
                         size_type src1_size,
                         type const* src2,
                         size_type src2_size,
                         size_type& bytes_copied);

    /// Move n elements to destination from source.
    static bool Move(type* dest, type* src, size_type count);

    /// Move n elements to destination from source in reverse order.
    static bool ReverseMove(type* dest, type* src, size_type count);

    /// Move n elements to destination from source safely, even when regions overlap.
    static bool OverlapMove(type* dest, type* src, size_type count);

    /// Fill n elements into buffer with the specified value.
    static bool Set(type* buffer, size_type count, in_type value);

    /// Erase a number of elements at a given position.
    /// position + count must less than size.
    static bool Erase(type* buffer,
                      size_type size,
                      size_type position,
                      size_type count);

    /// Insert src buffer into position at destination buffer.
    /// to_insert and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to take the insert elements,
    /// and position must be less or equal to size.
    static bool Insert(type* dest,                // destination buffer which takes the insertion.
                       size_type dest_size,       // current size of dest buffer in elements.
                       size_type position,        // position to insert src into dest.
                       type const* to_insert,     // elements to be inserted into dest.
                       size_type to_insert_size); // number of elements to insert from src.

    /// Copy src buffer to dest buffer while inserting elements.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// position must be less or equal to src_size or there will be a buffer overrun.
    /// dest must be big enough to contain src_size + to_insert_size.
    static bool InsertCopy(type* dest,                // destination buffer which takes the copy and insertion.
                           size_type dest_size,       // size of dest buffer in elements.
                           size_type position,        // position to insert src into destination.
                           type const* src,           // src to be copied into destination.
                           size_type src_size,        // size of src buffer in elements.
                           type const* to_insert,     // elements to insert into destination.
                           size_type to_insert_size); // number of elements to insert.

    /// Move src buffer to dest buffer while inserting elements.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// position must be less or equal to src_size or there will be a buffer overrun.
    /// dest must be big enough to contain src_size + to_insert_size.
    static bool InsertMove(type* dest,                // destination buffer which takes the source and insertion.
                           size_type dest_size,       // size of dest buffer in elements.
                           size_type position,        // position to insert src into destination.
                           type* src,                 // src to be moved into destination.
                           size_type src_size,        // size of src buffer in elements.
                           type* to_insert,           // elements to move into destination.
                           size_type to_insert_size); // number of elements to insert.

    /// Prepend to_prepend buffer into start of destination buffer.
    /// to_prepend and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to take the elements to be prepended.
    static bool Prepend(type* dest,                 // destination buffer which takes the prepend.
                        size_type dest_size,        // current size of dest buffer in elements.
                        type const* to_prepend,     // elements to be prepended into dest.
                        size_type to_prepend_size); // number of elements to prepend from to_prepend.

    /// Copy src buffer to dest buffer then prepend to_prepend elements before src.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to contain src_size + to_prepend_size.
    static bool PrependCopy(type* dest,                 // destination buffer which takes the copy and prepend.
                            size_type dest_size,        // current size of dest buffer in elements.
                            type const* src,            // src to be copied into destination.
                            size_type src_size,         // size of src buffer in elements.
                            type const* to_prepend,     // elements to prepend into destination.
                            size_type to_prepend_size); // number of elements to prepend.

    /// Move src buffer to dest buffer then prepend to_prepend elements before src.
    /// src and dest must not be null, and must not be overlapping buffers.
    /// dest must be big enough to contain src_size + to_prepend_size.
    static bool PrependMove(type* dest,                 // destination buffer which takes the source and prepending elements.
                            size_type dest_size,        // current size of dest buffer in elements.
                            type* src,                  // src to be moved into destination.
                            size_type src_size,         // size of src buffer in elements.
                            type* to_prepend,           // elements to move into start of destination.
                            size_type to_prepend_size); // number of elements to prepend.

    /// Swap value1 and value2.
    static void Swap(type& value1, type& value2);

    /// Swap value1 and value2.
    static void Swap(type& value1,
                     size_type& value1_size,
                     type& value2,
                     size_type& value2_size);

    /// Count values that are sequentially the same from the start of first and second buffers.
    static SizeType CountSame(Type const* first,
                              Type const* second,
                              SizeType size);

    /// Count values that are sequentially the same from the start of first and second buffers.
    static SizeType CountSame(Type const* first,
                              SizeType first_size,
                              Type const* second,
                              SizeType second_size);

    /// Count the differences between two buffers and return the count for each buffer.
    static void CountDifferent(Type const* buffer1,
                               SizeType buffer1_size,
                               Type const* buffer2,
                               SizeType buffer2_size,
                               SizeType& buffer1_difference_count,
                               SizeType& buffer2_difference_count);

    /// Find value within buffer and return pointer to matching value within buffer,
    /// or nullptr when not found.
    static Type const* Find(Type value, Type const* buffer, SizeType size);

    /// Find the first value within buffer1 that is within buffer2 and
    /// returns the buffer2 pointer that matches.
    /// Otherwise return nullptr if no value matches within buffer2.
    static Type const* FindMatch(Type const* buffer1,
                                 SizeType buffer1_size,
                                 Type const* buffer2,
                                 SizeType buffer2_size);
};

#include "SecureBufferUtility.inl"

/// Provide compare functions outside of InsecureBufferUtility,
/// ensuring that InsecureBufferUtility can be used with types that don't implement < or == operators.
template<typename Type, typename SizeType>
inline int SecureCompare(Type const* first, Type const* second, SizeType count)
{
    typedef InternalBufferCompare<Type, SizeType> buffer_compare_type;

    int cmp;
    if (first != nullptr)
    {
        if (second != nullptr)
            cmp = (count > static_cast<SizeType>(0)) ? buffer_compare_type::Compare(first, second, count) : 0;
        else
            cmp = 1;
    }
    else
        cmp = (second != nullptr) ? -1 : 0;

    return cmp;
}

template<typename Type, typename SizeType>
inline bool SecureCompareEqual(Type const* first, Type const* second, SizeType count)
{
    typedef InternalBufferCompare<Type, SizeType> buffer_compare_type;

    bool is_equal;
    if (first != nullptr)
    {
        if (second != nullptr)
            is_equal = (count > static_cast<SizeType>(0)) ? buffer_compare_type::CompareEqual(first, second, count) : true;
        else
            is_equal = false;
    }
    else
        is_equal = (second != nullptr) ? false : true;

    return is_equal;
}

} // namespace ocl

#endif // OCL_GUARD_BUFFER_SECUREBUFFERUTILITY_HPP
