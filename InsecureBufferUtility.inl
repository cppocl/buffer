/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Copy(type* dest,
                                                 type const* src,
                                                 size_type count)
{
    InternalBufferCopy<type, size_type>::Copy(dest, src, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::ReverseCopy(type* dest,
                                                        type const* src,
                                                        size_type count)
{
    InternalBufferReverseCopy<type, size_type>::ReverseCopy(dest, src, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::OverlapCopy(type* dest,
                                                        type const* src,
                                                        size_type count)
{
    InternalBufferOverlapCopy<type, size_type>::OverlapCopy(dest, src, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::CopySame(type* dest,
                                                     size_type dest_size,
                                                     type const* src1,
                                                     size_type src1_size,
                                                     type const* src2,
                                                     size_type src2_size,
                                                     size_type& bytes_copied)
{
    InternalBufferCopySame<type, size_type>::CopySame(dest, dest_size, src1, src1_size, src2, src2_size, bytes_copied);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Move(type* dest,
                                                 type* src,
                                                 size_type count)
{
    InternalBufferMove<type, size_type>::Move(dest, src, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::ReverseMove(type* dest,
                                                        type* src,
                                                        size_type count)
{
    InternalBufferReverseMove<type, size_type>::ReverseMove(dest, src, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::OverlapMove(type* dest,
                                                        type* src,
                                                        size_type count)
{
    InternalBufferOverlapMove<type, size_type>::OverlapMove(dest, src, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Set(type* buffer,
                                                size_type count,
                                                in_type value)
{
    InternalBufferSet<type, size_type>::Set(buffer, count, value);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Erase(type* buffer,
                                                  size_type size,
                                                  size_type position,
                                                  size_type count)
{
    InternalBufferErase<type, size_type>::Erase(buffer, size, position, count);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Insert(type* dest,
                                                   size_type dest_size,
                                                   size_type position,
                                                   in_type to_insert,
                                                   size_type to_insert_size)
{
    InternalBufferInsert<type, size_type>::Insert(dest,
                                                  dest_size,
                                                  position,
                                                  to_insert,
                                                  to_insert_size);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Insert(type* dest,
                                                   size_type dest_size,
                                                   size_type position,
                                                   type const* to_insert,
                                                   size_type to_insert_size)
{
    InternalBufferInsert<type, size_type>::Insert(dest,
                                                  dest_size,
                                                  position,
                                                  to_insert,
                                                  to_insert_size);
}

template<typename Type, typename SizeType>
Type* InsecureBufferUtility<Type, SizeType>::Insert(type* dest,
                                                    size_type dest_size,
                                                    size_type position,
                                                    size_type space_to_insert)
{
    return InternalBufferInsert<type, size_type>::Insert(dest,
                                                         dest_size,
                                                         position,
                                                         space_to_insert);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::InsertCopy(type* dest,
                                                       size_type dest_size,
                                                       size_type position,
                                                       type const* src,
                                                       type const* to_insert,
                                                       size_type to_insert_size)
{
    InternalBufferInsertCopy<type, size_type>::InsertCopy(dest,
                                                          dest_size,
                                                          position,
                                                          src,
                                                          to_insert,
                                                          to_insert_size);
}

template<typename Type, typename SizeType>
Type* InsecureBufferUtility<Type, SizeType>::InsertCopy(type* dest,
                                                        size_type dest_size,
                                                        size_type position,
                                                        type const* src,
                                                        size_type space_to_insert)
{
    return InternalBufferInsertCopy<type, size_type>::InsertCopy(dest,
                                                                 dest_size,
                                                                 position,
                                                                 src,
                                                                 space_to_insert);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::InsertMove(type* dest,
                                                       size_type dest_size,
                                                       size_type position,
                                                       type* src,
                                                       type* to_insert,
                                                       size_type to_insert_size)
{
    InternalBufferInsertMove<type, size_type>::InsertMove(dest,
                                                          dest_size,
                                                          position,
                                                          src,
                                                          to_insert,
                                                          to_insert_size);
}

template<typename Type, typename SizeType>
Type* InsecureBufferUtility<Type, SizeType>::InsertMove(type* dest,
                                                       size_type dest_size,
                                                       size_type position,
                                                       type* src,
                                                       size_type to_insert_size)
{
    return InternalBufferInsertMove<type, size_type>::InsertMove(dest,
                                                                 dest_size,
                                                                 position,
                                                                 src,
                                                                 to_insert_size);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Prepend(type* dest,
                                                    size_type dest_size,
                                                    type const* to_prepend,
                                                    size_type to_prepend_size)
{
    InternalBufferPrepend<type, size_type>::Prepend(dest,
                                                    dest_size,
                                                    to_prepend,
                                                    to_prepend_size);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::PrependCopy(type* dest,
                                                        size_type dest_size,
                                                        type const* src,
                                                        type const* to_prepend,
                                                        size_type to_prepend_size)
{
    InternalBufferPrependCopy<type, size_type>::PrependCopy(dest,
                                                            dest_size,
                                                            src,
                                                            to_prepend,
                                                            to_prepend_size);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::PrependMove(type* dest,
                                                        size_type dest_size,
                                                        type* src,
                                                        type* to_prepend,
                                                        size_type to_prepend_size)
{
    InternalBufferPrependMove<type, size_type>::PrependMove(dest,
                                                            dest_size,
                                                            src,
                                                            to_prepend,
                                                            to_prepend_size);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Swap(type& value1, type& value2)
{
    InternalBufferSwap<type>::Swap(value1, value2);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::Swap(type& value1,
                                                 size_type& value1_size,
                                                 type& value2,
                                                 size_type& value2_size)
{
    InternalBufferSwap<type>::Swap(value1, value1_size, value2, value2_size);
}

template<typename Type, typename SizeType>
SizeType InsecureBufferUtility<Type, SizeType>::CountSame(Type const* first,
                                                          Type const* second,
                                                          SizeType size)
{
    return InternalBufferCount<Type, SizeType>::CountSame(first, second, size);
}

template<typename Type, typename SizeType>
SizeType InsecureBufferUtility<Type, SizeType>::CountSame(Type const* first,
                                                          SizeType first_size,
                                                          Type const* second,
                                                          SizeType second_size)
{
    return InternalBufferCount<Type, SizeType>::CountSame(first, first_size, second, second_size);
}

template<typename Type, typename SizeType>
void InsecureBufferUtility<Type, SizeType>::CountDifferent(Type const* first,
                                                           SizeType first_size,
                                                           Type const* second,
                                                           SizeType second_size,
                                                           SizeType& first_difference_count,
                                                           SizeType& second_difference_count)
{
    InternalBufferCount<Type, SizeType>::CountDifferent(first,
                                                        first_size,
                                                        second,
                                                        second_size,
                                                        first_difference_count,
                                                        second_difference_count);
}

template<typename Type, typename SizeType>
Type const* InsecureBufferUtility<Type, SizeType>::Find(Type value,
                                                        Type const* buffer,
                                                        SizeType size)
{
    return InternalBufferFind<Type, SizeType>::Find(value, buffer, size);
}

template<typename Type, typename SizeType>
Type const* InsecureBufferUtility<Type, SizeType>::FindMatch(Type const* first,
                                                             SizeType first_size,
                                                             Type const* second,
                                                             SizeType second_size)
{
    return InternalBufferFind<Type, SizeType>::Find(first, first_size, second, second_size);
}
