/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::Copy(type* dest,
                                               type const* src,
                                               size_type count)
{
    bool success = (dest != nullptr) && (src != nullptr) && (count > 0);
    if (success)
        InternalBufferCopy<type, size_type>::Copy(dest, src, count);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::ReverseCopy(type* dest,
                                                      type const* src,
                                                      size_type count)
{
    bool success = (dest != nullptr) && (src != nullptr) && (count > 0);
    if (success)
        InternalBufferReverseCopy<type, size_type>::ReverseCopy(dest, src, count);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::OverlapCopy(type* dest,
                                                      type const* src,
                                                      size_type count)
{
    bool success = (dest != nullptr) && (src != nullptr) && (count > 0);
    if (success)
        InternalBufferOverlapCopy<type, size_type>::OverlapCopy(dest, src, count);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::CopySame(type* dest,
                                                   size_type dest_size,
                                                   type const* src1,
                                                   size_type src1_size,
                                                   type const* src2,
                                                   size_type src2_size,
                                                   size_type& bytes_copied)
{
    bool success = (dest != nullptr) && (src1 != nullptr) && (src2 != nullptr);
    if (success)
        InternalBufferCopySame<type, size_type>::CopySame(dest, dest_size, src1, src1_size, src2, src2_size, bytes_copied);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::Move(type* dest, type* src, size_type count)
{
    bool success = (dest != nullptr) && (src != nullptr) && (count > 0);
    if (success)
        InternalBufferMove<type, size_type>::Move(dest, src, count);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::ReverseMove(type* dest, type* src, size_type count)
{
    bool success = (dest != nullptr) && (src != nullptr) && (count > 0);
    if (success)
        InternalBufferReverseMove<type, size_type>::ReverseMove(dest, src, count);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::OverlapMove(type* dest, type* src, size_type count)
{
    bool success = (dest != nullptr) && (src != nullptr) && (count > 0);
    if (success)
        InternalBufferOverlapMove<type, size_type>::OverlapMove(dest, src, count);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::Set(type* buffer, size_type count, in_type value)
{
    bool success = (buffer != nullptr) && (count > 0);
    if (success)
        InternalBufferSet<type, size_type>::Set(buffer, count, value);
    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::Erase(type* buffer,
                                                size_type size,
                                                size_type position,
                                                size_type count)
{
    bool success = (buffer != nullptr) &&
                   (position < size) &&
                   (count > static_cast<size_type>(0));

    if (success)
    {
        size_type erase_count = position + count < size ?
                                count : size - position - static_cast<size_type>(1);

        InternalBufferErase<type, size_type>::Erase(buffer,
                                                    size,
                                                    position,
                                                    erase_count);
    }

    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::Insert(type* dest,
                                                 size_type dest_size,
                                                 size_type position,
                                                 type const* to_insert,
                                                 size_type to_insert_size)
{
    bool success = (dest != nullptr) &&
                   (to_insert != nullptr) &&
                   (position + to_insert_size <= dest_size);

    if (success)
    {
        InternalBufferInsert<type, size_type>::Insert(dest,
                                                      dest_size,
                                                      position,
                                                      to_insert,
                                                      to_insert_size);
    }

    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::InsertCopy(type* dest,
                                                     size_type dest_size,
                                                     size_type position,
                                                     type const* src,
                                                     size_type src_size,
                                                     type const* to_insert,
                                                     size_type to_insert_size)
{
    bool success = (dest != nullptr) &&
                   (src != nullptr) &&
                   (to_insert != nullptr) &&
                   (src_size + to_insert_size <= dest_size) &&
                   (position + to_insert_size <= dest_size);

    if (success)
    {
        InternalBufferInsertCopy<type, size_type>::InsertCopy(dest,
                                                              dest_size,
                                                              position,
                                                              src,
                                                              to_insert,
                                                              to_insert_size);
    }

    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::InsertMove(type* dest,
                                                     size_type dest_size,
                                                     size_type position,
                                                     type* src,
                                                     size_type src_size,
                                                     type* to_insert,
                                                     size_type to_insert_size)
{
    bool success = (dest != nullptr) &&
                   (src != nullptr) &&
                   (to_insert != nullptr) &&
                   (src_size + to_insert_size <= dest_size) &&
                   (position + to_insert_size <= dest_size);

    if (success)
    {
        InternalBufferInsertMove<type, size_type>::InsertMove(dest,
                                                              dest_size,
                                                              position,
                                                              src,
                                                              to_insert,
                                                              to_insert_size);
    }

    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::Prepend(type* dest,
                                                  size_type dest_size,
                                                  type const* to_prepend,
                                                  size_type to_prepend_size)
{
    bool success = (dest != nullptr) &&
                   (to_prepend != nullptr) &&
                   (to_prepend_size <= dest_size);

    if (success)
    {
        InternalBufferPrepend<type, size_type>::Prepend(dest,
                                                        dest_size,
                                                        to_prepend,
                                                        to_prepend_size);
    }

    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::PrependCopy(type* dest,
                                                      size_type dest_size,
                                                      type const* src,
                                                      size_type src_size,
                                                      type const* to_prepend,
                                                      size_type to_prepend_size)
{
    bool success = (dest != nullptr) &&
                   (src != nullptr) &&
                   (to_prepend != nullptr) &&
                   (src_size + to_prepend_size <= dest_size);

    if (success)
    {
        InternalBufferPrependCopy<type, size_type>::PrependCopy(dest,
                                                                dest_size,
                                                                src,
                                                                to_prepend,
                                                                to_prepend_size);
    }

    return success;
}

template<typename Type, typename SizeType>
bool SecureBufferUtility<Type, SizeType>::PrependMove(type* dest,
                                                      size_type dest_size,
                                                      type* src,
                                                      size_type src_size,
                                                      type* to_prepend,
                                                      size_type to_prepend_size)
{
    bool success = (dest != nullptr) &&
                   (src != nullptr) &&
                   (to_prepend != nullptr) &&
                   (to_prepend_size <= dest_size) &&
                   (src_size >= dest_size - to_prepend_size);

    if (success)
    {
        InternalBufferPrependMove<type, size_type>::PrependMove(dest,
                                                                dest_size,
                                                                src,
                                                                to_prepend,
                                                                to_prepend_size);
    }

    return success;
}

template<typename Type, typename SizeType>
void SecureBufferUtility<Type, SizeType>::Swap(type& value1, type& value2)
{
    InternalBufferSwap<type>::Swap(value1, value2);
}

template<typename Type, typename SizeType>
void SecureBufferUtility<Type, SizeType>::Swap(type& value1,
                                               size_type& value1_size,
                                               type& value2,
                                               size_type& value2_size)
{
    InternalBufferSwap<type>::Swap(value1, value1_size, value2, value2_size);
}

template<typename Type, typename SizeType>
SizeType SecureBufferUtility<Type, SizeType>::CountSame(Type const* first,
                                                        Type const* second,
                                                        SizeType size)
{
    return (first != nullptr) && (second != nullptr)
        ? InternalBufferCount<Type, SizeType>::CountSame(first, second, size)
        : 0;
}

template<typename Type, typename SizeType>
SizeType SecureBufferUtility<Type, SizeType>::CountSame(Type const* first,
                                                        SizeType first_size,
                                                        Type const* second,
                                                        SizeType second_size)
{
    return (first != nullptr) && (second != nullptr)
        ? InternalBufferCount<Type, SizeType>::CountSame(first, first_size, second, second_size)
        : 0;
}

template<typename Type, typename SizeType>
void SecureBufferUtility<Type, SizeType>::CountDifferent(Type const* first,
                                                         SizeType first_size,
                                                         Type const* second,
                                                         SizeType second_size,
                                                         SizeType& first_difference_count,
                                                         SizeType& second_difference_count)
{
    if ((first != nullptr) && (second != nullptr))
        InternalBufferCount<Type, SizeType>::CountDifferent(first,
                                                            first_size,
                                                            second,
                                                            second_size,
                                                            first_difference_count,
                                                            second_difference_count);
}

template<typename Type, typename SizeType>
Type const* SecureBufferUtility<Type, SizeType>::Find(Type value,
                                                      Type const* buffer,
                                                      SizeType size)
{
    return (first != nullptr) && (second != nullptr)
        ? InternalBufferFind<Type, SizeType>::Find(value, buffer, size)
        : nullptr;
}

template<typename Type, typename SizeType>
Type const* SecureBufferUtility<Type, SizeType>::FindMatch(Type const* first,
                                                           SizeType first_size,
                                                           Type const* second,
                                                           SizeType second_size)
{
    return (first != nullptr) && (second != nullptr)
        ? InternalBufferFind<Type, SizeType>::Find(first, first_size, second, second_size)
        : nullptr;
}
