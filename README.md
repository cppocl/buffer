# buffer

![](header_image.jpg)

## Overview

C++ Buffer manipulation classes.
Currently supports a buffer class and buffer utility classes that are either secure (but slower) or insecure (but higher performing) code.
These buffer classes support allocation, reallocation, copy, move, prepend, append, swap, etc.
The `Buffer` class is secure, so won't fail when being provided with null pointers.

## Follow

Telegram messenger: https://t.me/cppocl

## Code Examples

```cpp
#include "buffer/Buffer.hpp"

int main()
{
    ocl::Buffer<char> buff(41);

    // Set all characters to 'A'.
    buff.Set('A');

    buff[buff.GetSize() - 1] = '\0';

    printf("Many A characters: %s\n", buff.Ptr());
}
```
