/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_SEQUENTIALSEARCHFUNCTOR_HPP
#define OCL_GUARD_BUFFER_SEQUENTIALSEARCHFUNCTOR_HPP

#include "internal/InternalBufferInType.hpp"

namespace ocl
{

template<typename Type, typename SizeType>
struct SequentialSearchFunctor
{
    typedef typename InternalBufferInType<Type>::in_type in_type;

    bool operator()(Type const* values,
                    SizeType size,
                    in_type value_to_find) const noexcept
    {
        bool found = false;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            Type const* end_value = values + size;

            for (Type const* curr_value = values; curr_value < end_value; ++curr_value)
                if (value_to_find == *curr_value)
                {
                    found = true;
                    break;
                }
        }

        return found;
    }

    bool operator()(Type const* values,
                    SizeType size,
                    in_type value_to_find,
                    SizeType& nearest_pos) const noexcept
    {
        bool found = false;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            SizeType best_pos = size;
            Type const* values_end = values + size;

            for (Type const* curr_value = values; curr_value < values_end; ++curr_value)
            {
                in_type compare_value = *curr_value;
                SizeType curr_pos = static_cast<SizeType>(curr_value - values);
                if (value_to_find < compare_value)
                {
                    // if best value so far is less than value_to_find
                    // then this compare is nearer than the previous best value.
                    if ((values[best_pos] < value_to_find) || (best_pos == size))
                        best_pos = curr_pos;
                }
                else if (!(compare_value < value_to_find))
                {
                    best_pos = curr_pos;
                    found = true;
                    break;
                }
            }
            // If best_pos was not set then the value to find
            // is larger than all values in the array, so set last position.
            nearest_pos = best_pos < size ? best_pos : size;
        }
        else
            nearest_pos = static_cast<SizeType>(0);

        return found;
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_SEQUENTIALSEARCHFUNCTOR_HPP
