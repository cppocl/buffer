/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_MEMORY_HPP
#define OCL_GUARD_BUFFER_MEMORY_HPP

#include <cstdlib>
#include <cstddef>
#include "MoveFunctor.hpp"

namespace ocl
{

template<typename Type, typename SizeType = std::size_t, typename MoveFunctorType = MoveFunctor<Type, SizeType>, bool const can_throw = false>
class Memory;

// Memory for POD (Plain old data) types with malloc and free.
template<typename Type, typename SizeType, typename MoveFunctorType>
class Memory<Type, SizeType, MoveFunctorType, false>
{
public:
    typedef Type type;
    typedef SizeType size_type;
    typedef MoveFunctorType move_functor_type;
    typedef Memory<Type, SizeType, MoveFunctorType, false> allocation_type;

    static bool const CAN_THROW_EXCEPTION = false;

public:
    Memory() = default;

    Memory(SizeType size) noexcept;

    Memory(Memory const&) = delete;

    Memory(Memory&& other) noexcept;

    ~Memory() noexcept;

    Memory& operator =(Memory const&) = delete;

    // Move the other object to this object.
    Memory& operator =(Memory&& other) noexcept;

    // Get the raw constant pointer.
    operator Type const*() const noexcept;

    // Get the raw pointer.
    operator Type*() noexcept;

    // Get the raw constant pointer.
    Type const* Ptr() const noexcept;

    // Get the raw pointer.
    Type* Ptr() noexcept;

    // Detach ownership of the pointer.
    Type* Detach() noexcept;

    // Detach ownership of the pointer and get current size.
    Type* Detach(SizeType& size) noexcept;

    // Attach ownership of the poiner to this allocation object.
    // It is up to the caller to ensure malloc/free or new/delete match the Memory object behaviour.
    void Attach(Type*& memory, SizeType size) noexcept;

    // Reserve memory for one or more elements, destroying any existing values.
    void SetSize(SizeType size) noexcept;

    // Re-size the buffer, only re-allocating memory when the new size is bigger than the existing size or reallocate is true.
    void Resize(SizeType size, bool reallocate = false);

    // Free the allocated memory.
    void Free() noexcept;

    // Move the other object to this object.
    void Move(Memory& other) noexcept;

    /// Get the allocation size, or zero if not allocated.
    SizeType GetSize() const noexcept;

    /// Get the allocation size, or zero if not allocated.
    SizeType GetSizeInBytes() const noexcept;

    /// Get the number of bytes the buffer uses, including any other internal memory usage.
    SizeType GetMemSize() const noexcept;

    /// Get the maximum size (number of elements) that can be allocated.
    SizeType GetMaxSize() const noexcept;

    /// Get the maximum size in bytes that can be allocated.
    SizeType GetMaxSizeInBytes() const noexcept;

    // Swap the memory with the other memory object.
    void Swap(Memory& other) noexcept;

private:
    static Type* InternalAllocate(SizeType size) noexcept;

    static void InternalFree(Type* ptr) noexcept;

    void InternalReserve(SizeType size) noexcept;

private:
    Type* m_memory = nullptr;
    SizeType m_size = 0;
};

// Memory for all types with new, new[], delete, and delete[].
template<typename Type, typename SizeType, typename MoveFunctorType>
class Memory<Type, SizeType, MoveFunctorType, true>
{
public:
    typedef Type type;
    typedef SizeType size_type;
    typedef MoveFunctorType move_functor_type;
    typedef Memory<Type, SizeType, MoveFunctorType, true> allocation_type;

    static bool const CAN_THROW_EXCEPTION = true;

public:
    Memory() = default;

    Memory(SizeType size);

    Memory(Memory const&) = delete;

    Memory(Memory&& other) noexcept;

    ~Memory();

    Memory& operator =(Memory const&) = delete;

    // Move the other object to this object.
    Memory& operator =(Memory&& other);

    // Get the raw constant pointer.
    operator Type const*() const noexcept;

    // Get the raw pointer.
    operator Type*() noexcept;

    // Get the raw constant pointer.
    Type const* Ptr() const noexcept;

    // Get the raw pointer.
    Type* Ptr() noexcept;

    // Detach ownership of the pointer.
    Type* Detach() noexcept;

    // Detach ownership of the pointer and get current size.
    Type* Detach(SizeType& size) noexcept;

    // Attach ownership of the poiner to this allocation object.
    // It is up to the caller to ensure malloc/free or new/delete match the Memory object behaviour.
    void Attach(Type*& memory, SizeType size) noexcept;

    // Reserve memory for one or more elements, destroying any existing values.
    void SetSize(SizeType size);

    // Re-size the buffer, only re-allocating memory when the new size is bigger than the existing size or reallocate is true.
    void Resize(SizeType size, bool reallocate = false);

    // Free the allocated memory.
    void Free();

    // Move the other object to this object.
    void Move(Memory& other);

    /// Get the allocation size, or zero if not allocated.
    SizeType GetSize() const noexcept;

    /// Get the allocation size, or zero if not allocated.
    SizeType GetSizeInBytes() const noexcept;

    /// Get the number of bytes the buffer uses, including any other internal memory usage.
    SizeType GetMemSize() const noexcept;

    /// Get the maximum size (number of elements) that can be allocated.
    SizeType GetMaxSize() const noexcept;

    /// Get the maximum size in bytes that can be allocated.
    SizeType GetMaxSizeInBytes() const noexcept;

    // Swap two memory objects.
    void Swap(Memory& other) noexcept;

private:
    // Handle the allocation of memory for a single element or an array of elements.
    static Type* InternalAllocate(SizeType size);

    // Handle the freeing of memory for a single element or an array of elements.
    static void InternalFree(Type* memory, SizeType size);

private:
    Type* m_memory = nullptr;
    SizeType m_size = 0;
};

} // namespace ocl

#include "Memory.inl"

#endif // OCL_GUARD_BUFFER_MEMORY_HPP
