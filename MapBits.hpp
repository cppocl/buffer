/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_MAPBITS_HPP
#define OCL_GUARD_BUFFER_MAPBITS_HPP

#include "internal/InternalMapBits.hpp"

namespace ocl
{

/// Map the bits between two types without any loss of bits from conversion using a cast.
template<typename FromType, typename ToType>
class MapBits
{
public:
    MapBits() = default;

    ToType operator()(FromType value) noexcept
    {
        return ToBits(value);
    }

    ToType operator()(FromType value, bool& lossy) noexcept
    {
        return ToBits(value, lossy);
    }

    /// Convert numeric type to an integer type, converting the value in memory to bits.
    /// E.g. converting from a float to a uint32_t will map the bits to an integer type.
    static ToType ToBits(FromType value) noexcept
    {
        return InternalMapBits<FromType, ToType>::ToBits(value);
    }

    /// Convert numeric type to an integer type, converting the value in memory to bits.
    // Return the possibility of loss of data, e.g. converting a double to a uint16_t.
    /// E.g. converting from a float to a uint32_t will map the bits to an integer type.
    static ToType ToBits(FromType value, bool& lossy) noexcept
    {
        return InternalMapBits<FromType, ToType>::ToBits(value, lossy);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_MAPBITS_HPP
