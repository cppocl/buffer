/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#if 0

#include "../../unit_test_framework/test/Test.hpp"
#include "../Buffer.hpp"
#include <cstdint>
#include <cstdint>
#include <vector>
#include <limits>
#include <cstdlib>
#include <cassert>
#include "../../../../github_3rd_party/EASTL/include/EASTL/vector.h"

using ocl::Buffer;

void* operator new[](size_t size, size_t, size_t,
    const char*, int, unsigned int,
    const char*, int)
{
    return malloc(size);
}

void* operator new[](size_t size, const char*, int,
    unsigned int, const char*, int)
{
    return malloc(size);
}

TEST_MEMBER_FUNCTION_TIME(ea_vector, assign, size_type_char, 0, 100)
{
    TEST_OVERRIDE_ARGS("size_type, char");

    typedef char type;
    typedef eastl::vector<type> vector_type;

    std::size_t const num_elements = 250000U;
    vector_type buffer(num_elements);
    type const value = 'A';

    RESET_TIME();
    CHECK_TIME(buffer.assign(num_elements, value));
}

TEST_MEMBER_FUNCTION_TIME(ea_vector, assign, size_type_uint32_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("size_type, uint32_t");

    typedef uint32_t type;
    typedef eastl::vector<type> vector_type;

    std::size_t const num_elements = 250000U;
    vector_type buffer(num_elements);
    type const value = 123456U;

    RESET_TIME();
    CHECK_TIME(buffer.assign(num_elements, value));
}

TEST_MEMBER_FUNCTION_TIME(ea_vector, assign, size_type_uint64_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("size_type, uint64_t");

    typedef uint64_t type;
    typedef eastl::vector<type> vector_type;

    std::size_t const num_elements = 250000U;
    vector_type buffer(num_elements);
    type const value = 1234567890UL;

    RESET_TIME();
    CHECK_TIME(buffer.assign(num_elements, value));
}

TEST_MEMBER_FUNCTION_TIME(ea_vector, operator_eq, vector_const_ref, 0, 100)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "ea_vector<char> const&");

    typedef char type;
    typedef eastl::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    ::memcpy(value.data(), "0123456789", num_elements);

    RESET_TIME();
    CHECK_TIME(buffer = value);
}

TEST_MEMBER_FUNCTION_TIME(ea_vector, operator_eq, vector_const_ref_int32_t, 0, 100)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "vector<int32_t> const&");

    typedef uint32_t type;
    typedef eastl::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    type const init[num_elements] = { 100001U, 100002U, 100003U, 100004U, 100005U, 100006U, 100007U, 100008U, 100009U, 1000010U, 1000011U };
    ::memcpy(value.data(), init, num_elements * sizeof(type));

    RESET_TIME();
    CHECK_TIME(buffer = value);
}

TEST_FUNCTION_TIME(ea_copy, ea_vector_const_refint32_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("eastl::vector<int32_t>::iterator, eastl::vector<int32_t>::iterator, eastl::vector<int32_t>::iterator");

    typedef uint32_t type;
    typedef eastl::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    type const init[num_elements] = { 100001U, 100002U, 100003U, 100004U, 100005U, 100006U, 100007U, 100008U, 100009U, 1000010U, 1000011U };
    ::memcpy(value.data(), init, num_elements * sizeof(type));

    RESET_TIME();
    CHECK_TIME(eastl::copy(value.begin(), value.end(), buffer.begin()));
}

#endif
