/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/InternalBufferSet.hpp"
#include "../internal/InternalBufferCopy.hpp"
#include <cstdint>
#include <cstdint>
#include <vector>
#include <limits>
#include <cstdlib>

using ocl::InternalBufferSet;
using ocl::InternalBufferCopy;

TEST_MEMBER_FUNCTION_TIME(InternalBufferSet, Set, void_ptr_size_type_char, 0, 100)
{
    TEST_OVERRIDE_ARGS("void*, size_type, char");

    typedef char type;
    typedef InternalBufferSet<type> buffer_set_type;

    std::size_t const num_elements = 250000U;
    type* buffer = new type[num_elements];
    type const value = 'A';

    RESET_TIME();
    CHECK_TIME(buffer_set_type::Set(buffer, num_elements, value));

    delete[] buffer;
}

TEST_MEMBER_FUNCTION_TIME(InternalBufferSet, Set, void_ptr_size_type_uint32_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("void*, size_type, uint32_t");

    std::size_t const num_elements = 250000U;
    typedef uint32_t type;
    typedef InternalBufferSet<type> buffer_set_type;

    type* buffer = new type[num_elements];
    type const value = 123456U;

    RESET_TIME();
    CHECK_TIME(buffer_set_type::Set(buffer, num_elements, value));

    delete[] buffer;
}

TEST_MEMBER_FUNCTION_TIME(InternalBufferSet, Set, void_ptr_size_type_uint64_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("void*, size_type, uint64_t");

    std::size_t const num_elements = 250000U;
    typedef uint64_t type;
    typedef InternalBufferSet<type> buffer_set_type;

    type* buffer = new type[num_elements];
    type const value = 1234567890UL;

    RESET_TIME();
    CHECK_TIME(buffer_set_type::Set(buffer, num_elements, value));

    delete[] buffer;
}
