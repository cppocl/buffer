/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../Buffer.hpp"
#include <cstdint>
#include <cstdint>
#include <vector>
#include <limits>
#include <cstdlib>
#include <cassert>

using ocl::Buffer;

// Buffer::Set, Buffer::Copy and vector::assign all produce the same end result,
// and are therefor equivalent.
TEST_MEMBER_FUNCTION_TIME(Buffer, Set, char, 0, 100)
{
    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 250000U;
    buffer_type buffer(num_elements);
    type const value = 'A';

    RESET_TIME();
    CHECK_TIME(buffer.Set(value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Copy, char_size_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("char, size_t");

    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 250000U;
    buffer_type buffer(num_elements);
    type const value = 'A';

    RESET_TIME();
    CHECK_TIME(buffer.Copy(value, num_elements));
}

TEST_MEMBER_FUNCTION_TIME(vector, assign, size_type_char, 0, 100)
{
    TEST_OVERRIDE_ARGS("size_type, char");

    typedef char type;

    std::size_t const num_elements = 250000U;
    std::vector<type> buffer(num_elements);
    type const value = 'A';

    RESET_TIME();
    CHECK_TIME(buffer.assign(num_elements, value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Set, uint32_t, 0, 100)
{
    typedef uint32_t type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 250000U;
    buffer_type buffer(num_elements);
    type const value = 123456U;

    RESET_TIME();
    CHECK_TIME(buffer.Set(value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Copy, uint32_t_size_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("uint32_t, size_t");

    typedef uint32_t type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 250000U;
    buffer_type buffer(num_elements);
    type const value = 123456U;

    RESET_TIME();
    CHECK_TIME(buffer.Copy(value, num_elements));
}

TEST_MEMBER_FUNCTION_TIME(vector, assign, size_type_uint32_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("size_type, uint32_t");

    typedef uint32_t type;

    std::size_t const num_elements = 250000U;
    std::vector<type> buffer(num_elements);
    type const value = 123456U;

    RESET_TIME();
    CHECK_TIME(buffer.assign(num_elements, value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Set, uint64_t, 0, 100)
{
    typedef uint64_t type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 250000U;
    buffer_type buffer(num_elements);
    type const value = 1234567890UL;

    RESET_TIME();
    CHECK_TIME(buffer.Set(value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Copy, uint64_t_size_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("uint64_t, size_t");

    typedef uint32_t type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 250000U;
    buffer_type buffer(num_elements);
    type const value = 1234567890UL;

    RESET_TIME();
    CHECK_TIME(buffer.Copy(value, num_elements));
}

TEST_MEMBER_FUNCTION_TIME(vector, assign, size_type_uint64_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("size_type, uint64_t");

    typedef uint64_t type;

    std::size_t const num_elements = 250000U;
    std::vector<type> buffer(num_elements);
    type const value = 1234567890UL;

    RESET_TIME();
    CHECK_TIME(buffer.assign(num_elements, value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Copy, char_const_ptr_size_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("char const*, size_t");

    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 11U;
    buffer_type buffer(num_elements);
    buffer_type value(num_elements, "0123456789");
    char const* ptr = value.Ptr();
    size_t const size = value.GetSize();

    RESET_TIME();
    CHECK_TIME(buffer.Copy(ptr, size));
}

// This overload of Buffer::Copy is equivalent to vector::operator= and std::copy.
TEST_MEMBER_FUNCTION_TIME(Buffer, Copy, Buffer_const_ref, 0, 100)
{
    TEST_OVERRIDE_ARGS("Buffer<char> const&");

    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 11U;
    buffer_type buffer(num_elements);
    buffer_type value(num_elements, "0123456789");

    RESET_TIME();
    CHECK_TIME(buffer.Copy(value));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, UnsafeCopy, char_const_ptr_size_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("char const*, size_t");

    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 11U;
    buffer_type buffer(num_elements);
    buffer_type value(num_elements, "0123456789");
    char const* ptr = value.Ptr();
    size_t const size = value.GetSize();

    RESET_TIME();
    CHECK_TIME(buffer.UnsafeCopy(ptr, size));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, UnsafeCopy, Buffer_const_ref, 0, 100)
{
    TEST_OVERRIDE_ARGS("Buffer<char> const&");

    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 11U;
    buffer_type buffer(num_elements);
    buffer_type value(num_elements, "0123456789");

    RESET_TIME();
    CHECK_TIME(buffer.UnsafeCopy(value));
}

// This overload of Buffer::Copy is equivalent to vector::operator= and std::copy.
TEST_MEMBER_FUNCTION_TIME(Buffer, operator_eq, Buffer_const_ref, 0, 100)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "Buffer<char> const&");

    typedef char type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 11U;
    buffer_type buffer(num_elements);
    buffer_type value(num_elements, "0123456789");

    RESET_TIME();
    CHECK_TIME(buffer = value);
}

TEST_MEMBER_FUNCTION_TIME(vector, operator_eq, vector_const_ref, 0, 100)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "vector<char> const&");

    typedef char type;
    typedef std::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    ::memcpy(value.data(), "0123456789", num_elements);

    RESET_TIME();
    CHECK_TIME(buffer = value);
}

TEST_FUNCTION_TIME(copy, vector_const_ref, 0, 100)
{
    TEST_OVERRIDE_ARGS("vector<char> ::iterator, vector<char> ::iterator, vector<char> ::iterator");

    typedef char type;
    typedef std::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    ::memcpy(value.data(), "0123456789", num_elements);

    RESET_TIME();
    CHECK_TIME(std::copy(value.begin(), value.end(), buffer.begin()));
}

TEST_MEMBER_FUNCTION_TIME(Buffer, Copy, Buffer_const_ref_int32_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("Buffer<int32_t> const&");

    typedef uint32_t type;
    typedef Buffer<type> buffer_type;

    std::size_t const num_elements = 11U;
    buffer_type buffer(num_elements);
    type const init[num_elements] = {100001U, 100002U, 100003U, 100004U, 100005U, 100006U, 100007U, 100008U, 100009U, 1000010U, 1000011U};
    buffer_type value(num_elements, init);

    RESET_TIME();
    CHECK_TIME(buffer.Copy(value));
}

TEST_MEMBER_FUNCTION_TIME(vector, operator_eq, vector_const_ref_int32_t, 0, 100)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator=", "vector<int32_t> const&");

    typedef uint32_t type;
    typedef std::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    type const init[num_elements] = {100001U, 100002U, 100003U, 100004U, 100005U, 100006U, 100007U, 100008U, 100009U, 1000010U, 1000011U};
    ::memcpy(value.data(), init, num_elements * sizeof(type));

    RESET_TIME();
    CHECK_TIME(buffer = value);
}

TEST_FUNCTION_TIME(copy, vector_const_refint32_t, 0, 100)
{
    TEST_OVERRIDE_ARGS("vector<int32_t>::iterator, vector<int32_t>::iterator, vector<int32_t>::iterator");

    typedef uint32_t type;
    typedef std::vector<type> vector_type;

    std::size_t const num_elements = 11U;
    vector_type buffer(num_elements);
    vector_type value(11U);
    type const init[num_elements] = {100001U, 100002U, 100003U, 100004U, 100005U, 100006U, 100007U, 100008U, 100009U, 1000010U, 1000011U};
    ::memcpy(value.data(), init, num_elements * sizeof(type));

    RESET_TIME();
    CHECK_TIME(std::copy(value.begin(), value.end(), buffer.begin()));
}
