/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_BITUTILITY_HPP
#define OCL_GUARD_BUFFER_BITUTILITY_HPP

#include "internal/InternalMapBits.hpp"
#include "internal/InternalCountBits.hpp"
#include "internal/InternalExpandBits.hpp"
#include <climits>
#include <cstddef>
#include <cstdint>

namespace ocl
{

/// Manipulate bits for a numeric type.
template<typename Type, typename SizeType = std::size_t>
class BitUtility
{
public:
    typedef Type type;
    typedef SizeType size_type;

    static const constexpr size_type NUM_BYTES = static_cast<size_type>(sizeof(type));
    static const constexpr unsigned char NO_BITS = static_cast<unsigned char>(0x00u);
    static const constexpr unsigned char ALL_BITS = static_cast<unsigned char>(0xffu);

public:
    /// Count number of bits set in an integer type.
    static size_type CountSetBits(type value) noexcept
    {
        return InternalCountBits<type, size_type>::CountSetBits(value);
    }

    /// Count number of bits set in an integer type.
    static size_type CountClearedBits(type value)
    {
        return InternalCountBits<type, size_type>::CountClearedBits(value);
    }

    static size_type CountSetBits(type const* bits, size_type bit_count) noexcept
    {
        static const unsigned char remainder_mask[] = { 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };

        union u{
            type const* bits;
            unsigned char const* bytes;
        };
        u ubits{ {bits} };

        size_type set_bit_count = 0;
        if (bit_count > 0)
        {
            // Count all bits except for the last byte.
            size_type byte_count = ByteCount(bit_count);
            unsigned char const* bits_last = ubits.bytes + byte_count - 1;
            for (; ubits.bytes < bits_last; ++ubits.bytes)
                set_bit_count += InternalCountBits<unsigned char, size_type>::CountSetBits(*ubits.bytes);

            // Count the last 1 to CHAR_BIT set bits.
            size_type remainder = bit_count % CHAR_BIT;
            if (remainder != 0)
            {
                unsigned char last_byte = *ubits.bytes & remainder_mask[remainder - 1U];
                set_bit_count += InternalCountBits<unsigned char, size_type>::CountSetBits(last_byte);
            }
            else
                set_bit_count += InternalCountBits<unsigned char, size_type>::CountSetBits(*ubits.bytes);
        }
        return set_bit_count;
    }

    static size_type CountClearedBits(type const* bits, size_type bit_count) noexcept
    {
        static const unsigned char remainder_mask[] = { 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };

        union u {
            type const* bits;
            unsigned char const* bytes;
        };
        u ubits{ {bits} };

        size_type cleared_bit_count = 0;
        if (bit_count > 0)
        {
            // Count all bits except for the last byte.
            size_type byte_count = ByteCount(bit_count);
            unsigned char const* bits_last = ubits.bytes + byte_count - 1;
            for (; ubits.bytes < bits_last; ++ubits.bytes)
                cleared_bit_count += InternalCountBits<unsigned char, size_type>::CountClearedBits(*ubits.bytes);

            // Count the last 1 to CHAR_BIT cleared bits.
            size_type remainder = bit_count % CHAR_BIT;
            if (remainder != 0)
            {
                unsigned char mask = remainder_mask[remainder - 1U];
                unsigned char last_byte = *ubits.bytes & mask;
                last_byte |= mask ^ ALL_BITS; // Set all spare bits to avoid them being counted.
                cleared_bit_count += InternalCountBits<unsigned char, size_type>::CountClearedBits(last_byte);
            }
            else
                cleared_bit_count += InternalCountBits<unsigned char, size_type>::CountClearedBits(*ubits.bytes);
        }
        return cleared_bit_count;
    }

    // Return byte position for a bit position.
    static size_type BytePosition(size_type bit_position) noexcept
    {
        return bit_position / 8U;
    }

    // Return number of bytes required to store the bit count.
    static size_type ByteCount(size_type bit_count) noexcept
    {
        return (bit_count + 7U) / 8U;
    }

    // Get bit value of 0 or 1 for the bit position.
    static bool GetAt(unsigned char const* bits, size_type bit_position) noexcept
    {
        unsigned char byte = bits ? bits[BytePosition(bit_position)] : NO_BITS;
        return (byte & (1U << (bit_position % 8U))) != 0U;
    }

    // Set bit value of 0 or 1 for the bit position.
    static void SetAt(unsigned char* bits, size_type bit_position) noexcept
    {
        unsigned char& byte = bits[BytePosition(bit_position)];
        byte |= static_cast<unsigned char>(1U << (bit_position % 8U));
    }

    // Clear a bit at a bit position (setting to 0).
    static void ClearAt(unsigned char* bits, size_type bit_position) noexcept
    {
        unsigned char& byte = bits[BitUtility::BytePosition(bit_position)];
        byte &= static_cast<unsigned char>(ALL_BITS - (1U << (bit_position % 8U)));
    }

    // Set or clear a bit at a bit position.
    static void SetAt(unsigned char* bits, size_type bit_position, bool value) noexcept
    {
        if (value)
            SetAt(bits, bit_position);
        else
            ClearAt(bits, bit_position);
    }

    // Fill bits with 1.
    static void Fill(unsigned char* bits, size_type bit_size)
    {
        static const unsigned char remainder_mask[] = { 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };

        size_type byte_count = ByteCount(bit_size);
        if (byte_count > 0)
        {
            std::size_t remainder = bit_size % 8;
            if (remainder != 0)
            {
                if (byte_count > 1)
                    std::memset(bits, ALL_BITS, byte_count - 1);
                bits[byte_count - 1] |= remainder_mask[remainder - 1];
            }
            else
                std::memset(bits, ALL_BITS, byte_count);
        }
    }

    // Clear bits with 0.
    static void Clear(unsigned char* bits, size_type bit_size)
    {
        static const unsigned char remainder_mask[] = { 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };

        size_type byte_count = ByteCount(bit_size);
        if (byte_count > 0)
        {
            std::size_t remainder = bit_size % 8;
            if (remainder != 0)
            {
                if (byte_count > 1)
                    std::memset(bits, NO_BITS, byte_count - 1);
                bits[byte_count - 1] &= static_cast<unsigned char>(ALL_BITS ^ remainder_mask[remainder - 1]);
            }
            else
                std::memset(bits, NO_BITS, byte_count);
        }
    }

    // Copy bits from src to dst, ensuring the target buffer does not copy beyond the end.
    static void Copy(unsigned char* dst_bits, size_type dst_bit_size, unsigned char const* src_bits, size_type src_bit_size)
    {
        static const unsigned char remainder_mask[] = { 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff };

        size_type bits_to_copy = dst_bit_size < src_bit_size ? dst_bit_size : src_bit_size;
        size_type bytes_to_copy = ByteCount(bits_to_copy);
        if (bytes_to_copy > 0)
        {
            if (bytes_to_copy > 1)
                std::memcpy(dst_bits, src_bits, bytes_to_copy - 1);
            size_type remainder = bits_to_copy % CHAR_BIT;
            unsigned char mask = remainder_mask[remainder - 1U];
            dst_bits[bytes_to_copy - 1] = src_bits[bytes_to_copy - 1] & mask;
        }
    }

    // Append src bits to dst bits, assuming that the dst buffer is large enough to contain all the bits.
    static void Append(unsigned char* dst_bits, size_type dst_bit_count, unsigned char const* src_bits, size_type src_bit_size)
    {
        if (((dst_bit_count % CHAR_BIT) == 0) && ((src_bit_size % CHAR_BIT) == 0))
        {
            // Fast append when src and dst are on byte boundaries.
            size_type dst_byte_position = BytePosition(dst_bit_count);
            size_type src_byte_count = ByteCount(src_bit_size);
            std::memcpy(dst_bits + dst_byte_position, src_bits, src_byte_count);
        }
        else
        {
            // TODO: optimise for faster bit setting in bulk.
            for (size_type src_bit_position = 0; src_bit_position < src_bit_size; ++src_bit_position)
                SetAt(dst_bits, dst_bit_count + src_bit_position, GetAt(src_bits, src_bit_position));
        }
    }

    /// Expand the bits into a larger unsigned integer type.
    /// E.g. bits from a 1 byte to 4 bytes would be 0x84 in a uint8_t becoming 0x84848484 in a uint32_t.
    template<std::size_t const ExpandSizeOfType>
    static typename InternalExpandBits<type, sizeof(type), ExpandSizeOfType>::expand_type
    Expand(type value)  noexcept
    {
        return InternalExpandBits<type, sizeof(type), ExpandSizeOfType>::ExpandBits(value);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_BITUTILITY_HPP
