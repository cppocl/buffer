/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_COMPAREFUNCTOR_HPP
#define OCL_GUARD_BUFFER_COMPAREFUNCTOR_HPP

#include "internal/InternalBufferCompare.hpp"
#include "internal/InternalBufferTypes.hpp"
#include <cstddef>

namespace ocl
{

/// Default compare functor for comparing a single value or a sequence of values.
/// Works with primitive types, pointer addresses or objects that overload less than operator.
/// The functor that uses two pointers and a size expects both pointers not to be null.
template<typename Type, typename SizeType = std::size_t>
struct CompareFunctor
{
    typedef Type type;
    typedef SizeType size_type;
    typedef typename InternalBufferTypes<type>::in_type in_type;

    inline int operator()(in_type first, in_type second) const
    {
        return first < second ? -1 : (second < first ? 1 : 0);
    }

    inline int operator()(type const* first, type const* second, size_type count) const
    {
        return InternalBufferCompare<Type, SizeType>::Compare(first, second, count);
    }
};

} // namespace ocl

#endif // OCL_GUARD_BUFFER_COMPAREFUNCTOR_HPP
