/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_BUFFER_EOLUTILITY_HPP
#define OCL_GUARD_BUFFER_EOLUTILITY_HPP

#include "../const/ASCIIChars.hpp"
#include "EolPlatform.hpp"
#include <cstring>

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
struct EolUtility
{
    typedef CharType char_type;
    typedef SizeType size_type;

    static char_type const CHAR_NULL = ASCIIChars<char_type>::CHAR_NULL;
    static char_type const CHAR_CR = ASCIIChars<char_type>::CHAR_CR;
    static char_type const CHAR_LF = ASCIIChars<char_type>::CHAR_LF;

    /// Check if character is '\n' or '\r'.
    static bool IsEndOfLineCharacter(char_type ch) noexcept;

    /// Get platform as a string, e.g. "Unix" or "Windows".
    static const char* GetPlatformAsString(EolPlatform platform) noexcept;

    /// Get number of end of line characters for a platform.
    static size_type GetEolCharCount(EolPlatform platform) noexcept;

    /// Count number of lines within a buffer.
    static size_type CountLines(char_type const* buffer,
                                size_type size,
                                bool whole_lines = false) noexcept;

    /// Count number of lines within a buffer, and return the number of eol characters.
    static size_type CountLines(char_type const* buffer,
                                size_type size,
                                size_type& eol_count,
                                bool whole_lines = false) noexcept;

    /// Count number of continuous lines within a buffer.
    static size_type CountContinuousLines(char_type const* buffer,
                                          size_type size) noexcept;

    /// Count number of lines continuous within a buffer, and return the number of eol characters.
    static size_type CountContinuousLines(char_type const* buffer,
                                          size_type size,
                                          size_type& eol_count) noexcept;

    /// If end of line is \r\n then return Windows platform and set count to 2.
    /// Otherwise platform will be Unix, Other or Unknown and count will be 1.
    static EolPlatform GetPlatform(char_type ch1, char_type ch2, size_type& count) noexcept;

    /// Parse buffer to identify platform end of line style.
    static EolPlatform GetPlatform(char_type const* buffer, size_type size) noexcept;

    /// If end of line is \r\n then return Windows platform and set count to 2.
    /// Otherwise platform will be Unix, Other or Unknown and count will be 1.
    static size_type GetPlatformCount(char_type ch1, char_type ch2) noexcept;

    /// Set character(s) at start of buffer with the end of line characters for the platform.
    static bool SafeSetEol(char_type* buffer, size_type size, EolPlatform platform);

    /// Set character(s) at start of buffer with the end of line characters for the platform,
    /// and return the number of characters set in count.
    static bool SafeSetEol(char_type* buffer, size_type size, EolPlatform platform, size_type& count);

    /// Set character(s) at start of buffer with the end of line characters for the platform.
    static void UnsafeSetEol(char_type* buffer, EolPlatform platform);

    /// Set character(s) at start of buffer with the end of line characters for the platform.
    static void UnsafeSetEol(char_type* buffer, EolPlatform platform, size_type& count);

    /// Examine existing buffer and return required buffer size for converting
    /// existing end of line characters to new platform line endings.
    static size_type GetBufferCount(char_type const* buffer, size_type size, EolPlatform platform) noexcept;

    /// Convert end of line characters from the source buffer into a destination buffer,
    /// matching the end of line style for the platform.
    /// Return number of characters stored in destination buffer.
    /// Return 0 if the buffer is not big enough, or there is an error with the input data.
    static size_type ConvertEol(char_type* dest_buffer,
                                size_type dest_size,
                                char_type const* source_buffer,
                                size_type source_size,
                                EolPlatform platform);
};

#include "EolUtility.inl"

} // namespace ocl

#endif // OCL_GUARD_BUFFER_EOLUTILITY_HPP
