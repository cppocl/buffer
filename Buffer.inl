/*
Copyright 2023 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::Buffer() noexcept
{
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::Buffer(size_type size)
    : m_memory(size)
{
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::Buffer(size_type size, type const* buffer)
    : m_memory(size)
{
    type* ptr = static_cast<type*>(m_memory);
    if ((buffer != nullptr) && (ptr != nullptr))
        insecure_buffer_utility::Copy(ptr, buffer, size);
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::Buffer(buffer_type const& buffer)
    : m_memory(buffer.GetSize())
{
    size_type size = buffer.GetSize();
    if (size > 0)
        insecure_buffer_utility::Copy(Ptr(), buffer.Ptr(), size);
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::Buffer(buffer_type const& buffer,
                                           size_type position,
                                           size_type count)
    : m_memory(GetCopySize(position, count, buffer.GetSize()))
{
    if (!IsEmpty())
        insecure_buffer_utility::Copy(Ptr() + position, buffer.Ptr(), GetSize());
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::Buffer(buffer_type&& buffer)
    : m_memory(static_cast<memory_type&&>(buffer.m_memory))
{
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::~Buffer()
{
}

template<typename Type, typename SizeType, typename MemoryType>
typename Buffer<Type, SizeType, MemoryType>::buffer_type&
Buffer<Type, SizeType, MemoryType>::operator=(buffer_type const& buffer)
{
    static_cast<void>(Copy(buffer));
    return *this;
}

template<typename Type, typename SizeType, typename MemoryType>
typename Buffer<Type, SizeType, MemoryType>::buffer_type&
Buffer<Type, SizeType, MemoryType>::operator=(buffer_type&& buffer) noexcept
{
    m_memory.Move(buffer.m_memory);
    return *this;
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::operator type*() noexcept
{
    return static_cast<type*>(m_memory);
}

template<typename Type, typename SizeType, typename MemoryType>
Buffer<Type, SizeType, MemoryType>::operator type const*() const noexcept
{
    return static_cast<type const*>(m_memory);
}

template<typename Type, typename SizeType, typename MemoryType>
Type& Buffer<Type, SizeType, MemoryType>::operator[](size_type index)
{
    return m_memory[index];
}

template<typename Type, typename SizeType, typename MemoryType>
typename ocl::Buffer<Type, SizeType, MemoryType>::const_return_type
ocl::Buffer<Type, SizeType, MemoryType>::operator[](size_type index) const
{
    return m_memory[index];
}

template<typename Type, typename SizeType, typename MemoryType>
bool ocl::Buffer<Type, SizeType, MemoryType>::operator<(Buffer const& rhs) const
{
    SizeType lhs_size = GetSize();
    SizeType rhs_size = rhs.GetSize();
    SizeType size = lhs_size < rhs_size ? lhs_size : rhs_size;
    int cmp = InternalBufferCompare<Type, SizeType>::Compare(Ptr(), rhs.Ptr(), size);
    return cmp < 0 || (cmp == 0 && lhs_size < rhs_size);
}

template<typename Type, typename SizeType, typename MemoryType>
SizeType Buffer<Type, SizeType, MemoryType>::GetSize() const noexcept
{
    return m_memory.GetSize();
}

template<typename Type, typename SizeType, typename MemoryType>
constexpr SizeType Buffer<Type, SizeType, MemoryType>::GetMaxSize() const noexcept
{
    return m_memory.GetMaxSize();
}

template<typename Type, typename SizeType, typename MemoryType>
SizeType Buffer<Type, SizeType, MemoryType>::GetMemSize() const noexcept
{
    return m_memory.GetMemSize();
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::IsEmpty() const noexcept
{
    return GetSize() == static_cast<SizeType>(0);
}

template<typename Type, typename SizeType, typename MemoryType>
Type* Buffer<Type, SizeType, MemoryType>::Ptr() noexcept
{
    return static_cast<type*>(m_memory);
}

template<typename Type, typename SizeType, typename MemoryType>
Type const* Buffer<Type, SizeType, MemoryType>::Ptr() const noexcept
{
    return static_cast<type const*>(m_memory);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Clear()
{
    return m_memory.Free();
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Compact()
{
    m_memory.Resize(m_memory.GetSize(), true);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::SetSize(size_type size)
{
    if (size != m_memory.GetSize())
        m_memory.SetSize(size);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Resize(size_type size, bool reallocate)
{
    if (reallocate || size != m_memory.GetSize())
        m_memory.Resize(size, reallocate);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::ShrinkBy(size_type size)
{
    size_type curr_size = GetSize();
    if (size < curr_size)
        m_memory.Resize(curr_size - size, false);
    else
        m_memory.Free();
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Set(in_type value)
{
    size_type const size = GetSize();
    if (size > static_cast<size_type>(0))
        insecure_buffer_utility::Set(Ptr(), size, value);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Set(size_type start, size_type end, in_type value)
{
    size_type const size = GetSize();
    if ((size > static_cast<size_type>(0)) && ((start <= end) && (end < size)))
        insecure_buffer_utility::Set(Ptr() + start, static_cast<SizeType>(1) + end - start, value);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Copy(in_type value, size_type count)
{
    if (count > GetSize())
        SetSize(count);
    if (!IsEmpty())
        insecure_buffer_utility::Set(Ptr(), count, value);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Copy(type const* buffer, size_type size)
{
    if ((size > static_cast<SizeType>(0)) && (buffer != nullptr))
    {
        SetSize(size);
        if (!IsEmpty())
            insecure_buffer_utility::Copy(Ptr(), buffer, size);
    }
    else
        Clear();
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Copy(buffer_type const& buffer)
{
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        SetSize(size);
        if (m_memory.Ptr() && buffer.Ptr())
            insecure_buffer_utility::Copy(Ptr(), buffer.Ptr(), size);
    }
    else
        Clear();
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::UnsafeCopy(type const* buffer, size_type size)
{
    SetSize(size);
    insecure_buffer_utility::Copy(Ptr(), buffer, size);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::UnsafeCopy(buffer_type const& buffer)
{
    SizeType size = buffer.GetSize();
    SetSize(size);
    insecure_buffer_utility::Copy(Ptr(), buffer.Ptr(), size);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Overwrite(in_type value, size_type count)
{
    if (count > static_cast<SizeType>(0))
    {
        if (count > GetSize())
            SetSize(count);
        insecure_buffer_utility::Set(Ptr(), count, value);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Overwrite(type const* buffer, size_type size)
{
    if ((size > static_cast<SizeType>(0)) && (buffer != nullptr))
    {
        if (size > GetSize())
            SetSize(size);
        insecure_buffer_utility::Copy(Ptr(), buffer, size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Overwrite(buffer_type const& buffer)
{
    size_type size = buffer.GetSize();
    if (size > static_cast<SizeType>(0))
    {
        if (size > GetSize())
            SetSize(size);
        insecure_buffer_utility::Copy(Ptr(), buffer.Ptr(), size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Overwrite(size_type position,
                                                   in_type value,
                                                   size_type count)
{
    size_type curr_size = GetSize();
    if ((count > static_cast<size_type>(0)) && (position <= curr_size))
    {
        size_type insert_end_pos = position + count;
        if (insert_end_pos > curr_size)
        {
            type* ptr = AppendSpace(insert_end_pos - curr_size, position);
            if (ptr != nullptr)
                insecure_buffer_utility::Set(ptr, insert_end_pos - position, value);
        }
        else
            insecure_buffer_utility::Set(Ptr() + position, count, value);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Overwrite(size_type position,
                                                   type const* buffer,
                                                   size_type size)
{
    size_type curr_size = GetSize();
    if ((size > static_cast<size_type>(0)) && (position <= curr_size) && (buffer != nullptr))
    {
        size_type insert_end_pos = position + size;
        if (insert_end_pos > curr_size)
        {
            type* ptr = AppendSpace(insert_end_pos - curr_size, position);
            if (ptr != nullptr)
                insecure_buffer_utility::Copy(ptr, buffer, size);
        }
        else
            insecure_buffer_utility::Copy(Ptr() + position, buffer, size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Overwrite(size_type position,
                                                   buffer_type const& buffer)
{
    size_type curr_size = GetSize();
    size_type buffer_size = buffer.GetSize();
    if ((buffer_size > static_cast<size_type>(0)) && (position <= curr_size))
    {
        size_type insert_end_pos = position + buffer_size;
        if (insert_end_pos > curr_size)
        {
            type* ptr = AppendSpace(insert_end_pos - curr_size, position);
            if (ptr != nullptr)
                insecure_buffer_utility::Copy(ptr, buffer.Ptr(), buffer_size);
        }
        else
            insecure_buffer_utility::Copy(Ptr() + position, buffer.Ptr(), buffer_size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::SetAt(size_type position, in_type value)
{
    if (position < GetSize())
        m_memory[position] = value;
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::SetAt(size_type position, type const* buffer, size_type size)
{
    if ((buffer != nullptr) &&
        (size > static_cast<SizeType>(0)) &&
        (position + size - static_cast<SizeType>(1) < GetSize()))
    {
        type* ptr = Ptr() + position;
        insecure_buffer_utility::Copy(ptr, buffer, size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::SetAt(size_type position, buffer_type const& buffer)
{
    size_type size = buffer.GetSize();
    if ((size > static_cast<size_type>(0)) &&
        (position + size - static_cast<SizeType>(1) < GetSize()))
    {
        type* ptr = Ptr();
        insecure_buffer_utility::Copy(ptr + position, buffer.Ptr(), size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::SetAt(size_type position, buffer_type&& buffer)
{
    size_type size = buffer.GetSize();
    if ((size > static_cast<size_type>(0)) &&
        (position + size - static_cast<SizeType>(1) < GetSize()))
    {
        type* ptr = Ptr();
        insecure_buffer_utility::Move(ptr + position, buffer.Ptr(), size);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Move(buffer_type& buffer)
{
    m_memory.Move(buffer.m_memory);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Swap(buffer_type& buffer) noexcept
{
    m_memory.Swap(buffer.m_memory);
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Prepend(in_type value)
{
    type* ptr = PrependSpace(static_cast<SizeType>(1));
    bool success = ptr != nullptr;
    if (success)
        *ptr = value;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Prepend(type const* buffer, size_type size)
{
    bool success;
    if ((buffer != nullptr) && (size > static_cast<SizeType>(0)))
    {
        type* ptr = PrependSpace(size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer, size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Prepend(buffer_type const& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = PrependSpace(size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Prepend(buffer_type&& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = PrependSpace(size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Move(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Append(in_type value)
{
    type* ptr = AppendSpace(static_cast<SizeType>(1));
    bool success = ptr != nullptr;
    if (success)
        *ptr = value;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Append(type const* buffer, size_type size)
{
    bool success;
    if ((size > static_cast<size_type>(0)) && (buffer != nullptr))
    {
        type* ptr = AppendSpace(size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer, size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Append(buffer_type const& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = AppendSpace(size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Append(buffer_type&& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = AppendSpace(size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Move(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Insert(size_type position, in_type value)
{
    type* ptr = InsertSpace(position, static_cast<SizeType>(1));
    bool success = ptr != nullptr;
    if (success)
        *ptr = value;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Insert(size_type position, in_type value, size_type count)
{
    type* ptr = InsertSpace(position, count);
    bool success = ptr != nullptr;
    if (success)
        insecure_buffer_utility::Set(ptr, count, value);
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Insert(size_type position, type const* buffer, size_type size)
{
    bool success;
    if ((buffer != nullptr) && (size > static_cast<SizeType>(0)))
    {
        type* ptr = InsertSpace(position, size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer, size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Insert(size_type position, buffer_type const& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = InsertSpace(position, size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Insert(size_type position, buffer_type&& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = InsertSpace(position, size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Move(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Embed(size_type position, in_type value)
{
    type* ptr = InsertSpaceNoResize(position, static_cast<size_type>(1));
    bool success = ptr != nullptr;
    if (success)
        *ptr = value;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Embed(size_type position, in_type value, size_type count)
{
    type* ptr = InsertSpaceNoResize(position, count);
    bool success = ptr != nullptr;
    if (success)
        insecure_buffer_utility::Set(ptr, count, value);
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Embed(size_type position, type const* buffer, size_type size)
{
    bool success;
    if ((buffer != nullptr) && (size > static_cast<SizeType>(0)))
    {
        type* ptr = InsertSpaceNoResize(position, size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer, size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Embed(size_type position, buffer_type const& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = InsertSpaceNoResize(position, size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Copy(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
bool Buffer<Type, SizeType, MemoryType>::Embed(size_type position, buffer_type&& buffer)
{
    bool success;
    SizeType size = buffer.GetSize();
    if (size > static_cast<size_type>(0))
    {
        type* ptr = InsertSpaceNoResize(position, size);
        success = ptr != nullptr;
        if (success)
            insecure_buffer_utility::Move(ptr, buffer.Ptr(), size);
    }
    else
        success = false;
    return success;
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Erase(size_type position, size_type count)
{
    // Calculate max elements that can be erased.
    size_type size = GetSize();
    if (position < size)
    {
        SizeType max_erase_count = size - position;

        if (count < max_erase_count) // Will the erase leave some data after position?
        {
            type* ptr = static_cast<type*>(m_memory);
            insecure_buffer_utility::Erase(ptr, size, position, count);
            ShrinkBy(count);
        }
        else if (max_erase_count > static_cast<size_type>(0))
            ShrinkBy(max_erase_count);
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::EraseFront()
{
    if (!IsEmpty())
        Erase(static_cast<SizeType>(0), static_cast<SizeType>(1));
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::EraseFront(size_type count)
{
    if (!IsEmpty())
        Erase(static_cast<SizeType>(0), count);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::EraseBack()
{
    if (!IsEmpty())
        ShrinkBy(static_cast<SizeType>(1));
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::EraseBack(size_type count)
{
    if (!IsEmpty())
        ShrinkBy(count);
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::Extract(buffer_type& buffer, size_type position, size_type count)
{
    size_type move_size = GetCopySize(position, count, GetSize());
    if (move_size > static_cast<size_type>(0))
    {
        buffer.SetSize(move_size);
        if (buffer.Ptr())
        {
            insecure_buffer_utility::Move(buffer.Ptr(), Ptr() + position, move_size);
            Erase(position, move_size);
        }
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::ExtractFront(buffer_type& buffer, size_type count)
{
    size_type size = GetSize();
    size_type move_size = (count < size) ? count : size;
    if (move_size > static_cast<size_type>(0))
    {
        buffer.SetSize(move_size);
        if (buffer.Ptr())
        {
            insecure_buffer_utility::Move(buffer.Ptr(), Ptr(), move_size);
            EraseFront(move_size);
        }
    }
}

template<typename Type, typename SizeType, typename MemoryType>
void Buffer<Type, SizeType, MemoryType>::ExtractBack(buffer_type& buffer, size_type count)
{
    size_type size = GetSize();
    size_type move_size = (count < size) ? count : size;
    if (move_size > static_cast<size_type>(0))
    {
        buffer.SetSize(move_size);
        if (buffer.Ptr())
        {
            size_type position = size - move_size;
            insecure_buffer_utility::Move(buffer.Ptr(), Ptr() + position, move_size);
            EraseBack(move_size);
        }
    }
}

template<typename Type, typename SizeType, typename MemoryType>
template<typename SearchFunctorType>
bool Buffer<Type, SizeType, MemoryType>::Exists(in_type value) const
{
    SearchFunctorType functor;
    return functor(Ptr(), GetSize(), value);
}

template<typename Type, typename SizeType, typename MemoryType>
template<typename SearchFunctorType>
bool Buffer<Type, SizeType, MemoryType>::FindPosition(in_type value, size_type& nearest_position) const
{
    SearchFunctorType functor;
    return functor(Ptr(), GetSize(), value, nearest_position);
}

template<typename Type, typename SizeType, typename MemoryType>
template<typename FunctorType>
bool Buffer<Type, SizeType, MemoryType>::Apply(FunctorType& functor)
{
    size_type size = GetSize();
    return (size > 0) ? functor(Ptr(), size) : false;
}

template<typename Type, typename SizeType, typename MemoryType>
Type* Buffer<Type, SizeType, MemoryType>::PrependSpace(size_type space_to_prepend)
{
    type* new_ptr;

    if (space_to_prepend > static_cast<size_type>(0))
    {
        // Move m_memory into old_memory, ready to hold new buffer with extra space.
        memory_type old_memory(static_cast<memory_type&&>(m_memory));
        size_type old_size = old_memory.GetSize();

        m_memory.SetSize(old_size + space_to_prepend);
        new_ptr = static_cast<type*>(m_memory);
        if (new_ptr)
        {
            // Move any existing elements ready for new elements at the start.
            if (old_size > static_cast<size_type>(0))
                insecure_buffer_utility::Move(new_ptr + space_to_prepend,
                                              static_cast<type*>(old_memory),
                                              old_size);
        }
    }
    else
        new_ptr = nullptr;

    return new_ptr;
}

template<typename Type, typename SizeType, typename MemoryType>
Type* Buffer<Type, SizeType, MemoryType>::AppendSpace(size_type space_to_append)
{
    type* new_ptr;

    if (space_to_append > static_cast<size_type>(0))
    {
        // Move m_memory into old_memory, ready to hold new buffer with extra space.
        memory_type old_memory(static_cast<memory_type&&>(m_memory));
        size_type old_size = old_memory.GetSize();

        m_memory.SetSize(old_size + space_to_append);
        new_ptr = static_cast<type*>(m_memory);
        if (new_ptr)
        {
            // Move any existing elements ready for new elements at the start.
            if (old_size > static_cast<size_type>(0))
            {
                insecure_buffer_utility::Move(new_ptr, static_cast<type*>(old_memory), old_size);
                new_ptr += old_size;
            }
        }
    }
    else
        new_ptr = nullptr;

    return new_ptr;
}

template<typename Type, typename SizeType, typename MemoryType>
Type* Buffer<Type, SizeType, MemoryType>::AppendSpace(size_type space_to_append, size_type copy_limit)
{
    type* new_ptr;

    if (space_to_append > static_cast<size_type>(0))
    {
        // Move m_memory into old_memory, ready to hold new buffer with extra space.
        memory_type old_memory(static_cast<memory_type&&>(m_memory));
        size_type old_size = old_memory.GetSize();

        m_memory.SetSize(old_size + space_to_append);
        new_ptr = static_cast<type*>(m_memory);
        if (new_ptr)
        {

            // Move any existing elements ready for new elements at the start.
            if (old_size > static_cast<size_type>(0))
            {
                size_type copy_size = copy_limit < old_size ? copy_limit : old_size;
                insecure_buffer_utility::Move(new_ptr, static_cast<type*>(old_memory), copy_size);
                new_ptr += copy_size;
            }
        }
    }
    else
        new_ptr = nullptr;

    return new_ptr;
}

template<typename Type, typename SizeType, typename MemoryType>
Type* Buffer<Type, SizeType, MemoryType>::InsertSpace(size_type position, size_type space_to_insert)
{
    type* new_ptr;

    size_type old_size = m_memory.GetSize();
    if ((space_to_insert > static_cast<size_type>(0)) && (position <= old_size))
    {
        // Move m_memory into old_memory, ready to hold new buffer with extra space.
        memory_type old_memory(static_cast<memory_type&&>(m_memory));
        size_type new_size = old_size + space_to_insert;
        m_memory.SetSize(new_size);
        new_ptr = static_cast<type*>(m_memory);
        if (new_ptr && (old_size > static_cast<size_type>(0)))
        {
            // Move any existing elements ready for new elements at the start.
            new_ptr = insecure_buffer_utility::InsertMove(new_ptr,
                                                          new_size,
                                                          position,
                                                          static_cast<type*>(old_memory),
                                                          space_to_insert);
        }
    }
    else
        new_ptr = nullptr;

    return new_ptr;
}

template<typename Type, typename SizeType, typename MemoryType>
Type* Buffer<Type, SizeType, MemoryType>::InsertSpaceNoResize(size_type position, size_type space_to_insert)
{
    type* new_ptr;

    size_type size = m_memory.GetSize();
    if ((space_to_insert > static_cast<size_type>(0)) && (position + space_to_insert <= size))
    {
        new_ptr = static_cast<type*>(m_memory);
        insecure_buffer_utility::Insert(new_ptr, size, position, space_to_insert);
        new_ptr += position;
    }
    else
        new_ptr = nullptr;

    return new_ptr;
}

template<typename Type, typename SizeType, typename MemoryType>
SizeType Buffer<Type, SizeType, MemoryType>::GetCopySize(size_type position,
                                                         size_type count,
                                                         size_type buffer_size) noexcept
{
    size_type copy_size;
    if (position < buffer_size)
    {
        size_type max_copy_size = buffer_size - position;
        copy_size = count < max_copy_size ? count : max_copy_size;
    }
    else
        copy_size = static_cast<size_type>(0);
    return copy_size;
}
